/**
 * Copyright (C) 2016 Hyphenate Inc. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyphenate.easeui;

public class EaseConstant {
    public static final String MESSAGE_ATTR_IS_VOICE_CALL = "is_voice_call";
    public static final String MESSAGE_ATTR_IS_VIDEO_CALL = "is_video_call";

    public static final String MESSAGE_ATTR_IS_BIG_EXPRESSION = "em_is_big_expression";
    public static final String MESSAGE_ATTR_EXPRESSION_ID = "em_expression_id";

    public static final String MESSAGE_ATTR_AT_MSG = "em_at_list";
    public static final String MESSAGE_ATTR_VALUE_AT_MSG_ALL = "ALL";


    public static final int CHATTYPE_SINGLE = 1;

    public static final String EXTRA_CHAT_TYPE = "chatType";
    public static final String EXTRA_USER_ID = "userId";
    public static final String EXTRA_USER_NICKNAME = "nickname";
    public static final String EXTRA_USER_POS = "pos";
    public static final String EXTRA_USER_MSGTYPE = "msgType";
    public static final String EXTRA_USER_AVATOR = "headimg";
    public static final String EXTRA_KEFU_ID = "kefuID";
    public static final String EXTRA_MY_USER_ID = "my_userId";
    public static final String EXTRA_CHAT_USER = "chat_user";
    public static final String IS_SHOW_GIFT_BUTTON = "isShowGiftButton";
    public static final String MESSAGE_ATTRIBUTE_USERID = "userId";

    public static final String MESSAGE_SEND_LIMIT_BOY = "message_limit_boy";
    public static final String MESSAGE_SEND_LIMIT_GRIL = "message_limit_gril";
    public static final String USER_SEX = "user_sex";
    public static final String MESSAGE_SEND_USERS = "message_users";
    public static final String MESSAGE_SEND_TIME = "message_time";
    public static final String LIMIT_COM_STATUS = "LIMIT_COM_STATUS";
}
