package com.hyphenate.easeui.interfaces;

import android.content.Context;
import android.view.ViewGroup;

/**
 * Created by leaves on 2017/4/24.
 */

public interface IContact {
    public void goToUserDetail(Context context, int userID);

    public void gotoCharge(Context context, ViewGroup group);
}
