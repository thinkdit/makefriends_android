package com.hyphenate.easeui.manager;

import android.content.Context;

import com.hyphenate.easeui.EaseConstant;
import com.thinkdit.lib.util.SharePreferenceUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import static com.hyphenate.easeui.EaseConstant.USER_SEX;

/**
 */
public class MessageLimitManager {
    private Context mContext;
    private static MessageLimitManager instance;
    HashMap<String, Integer> list = new HashMap<>();
    private int boymax = 1;
    private int grilmax = 1;
    public static final String MESSAGE_SEND_NOUSERS = "message_no_users";

    public static MessageLimitManager instance(Context context) {
        if (instance == null) {
            instance = new MessageLimitManager(context);

        }
        return instance;
    }

    public void setGrilmax(int grilmax) {
        this.grilmax = grilmax;
    }

    public void setBoymax(int boymax) {
        this.boymax = boymax;
    }

    boolean isInit = false;

    private MessageLimitManager(Context context) {
        mContext = context;
        isInit = true;
        initData();
    }

    private void initData() {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");// 设置日期格式
        String nowdate = df.format(new Date());
        String old = SharePreferenceUtils.getString(mContext, String.valueOf(SharePreferenceUtils
                .getInt(mContext, "key_userId")), EaseConstant.MESSAGE_SEND_TIME, "");
        SharePreferenceUtils.putString(mContext, String.valueOf(SharePreferenceUtils.getInt
                (mContext, "key_userId")), EaseConstant.MESSAGE_SEND_TIME, nowdate);
        if (nowdate != null && old != null && nowdate.equalsIgnoreCase(old)) {
            String result = SharePreferenceUtils.getString(mContext, String.valueOf
                    (SharePreferenceUtils.getInt(mContext, "key_userId")), EaseConstant
                    .MESSAGE_SEND_USERS, "");

            if (result != null && !result.equalsIgnoreCase("")) {
                String deal = result.substring(1, result.length() - 1);
                String[] users = deal.split(",");
                if (users != null) {
                    for (int i = 0; i < users.length; i++) {
                        String[] data = users[i].split("=");
                        if (data != null && data.length == 2) {
                            int value = 0;
                            try {
                                value = Integer.parseInt(data[1].trim());
                            } catch (Exception e) {

                            }
                            list.put(data[0].trim(), value);
                        }
                    }
                }
            }
        } else {
            SharePreferenceUtils.putString(mContext, String.valueOf(SharePreferenceUtils.getInt
                    (mContext, "key_userId")), EaseConstant.MESSAGE_SEND_USERS, "");
            SharePreferenceUtils.putInt(mContext, MESSAGE_SEND_NOUSERS, 0);
            list.clear();
        }
        boymax = SharePreferenceUtils.getInt(mContext, EaseConstant.MESSAGE_SEND_LIMIT_BOY, Integer
                .MAX_VALUE);
        grilmax= SharePreferenceUtils.getInt(mContext, EaseConstant.MESSAGE_SEND_LIMIT_GRIL, Integer
                .MAX_VALUE);
    }

    public void reset() {
        initData();
    }

    public boolean canSendMessage(int userid) {
//        if (list.get(String.valueOf(userid)) != null && list.get(String.valueOf(userid)).intValue
//                () >= max) {
//            return false;
//        }
//        int current = list.get(String.valueOf(userid)) == null ? 0 : list.get(String.valueOf
//                (userid)).intValue();
//        list.put(String.valueOf(userid), current + 1);
//        SharePreferenceUtils.putString(mContext, String.valueOf(SharePreferenceUtils.getInt
//                (mContext, "key_userId")), EaseConstant.MESSAGE_SEND_USERS, list.toString());
        int current = SharePreferenceUtils.getInt(mContext, MESSAGE_SEND_NOUSERS, 0);
        String user_sex = SharePreferenceUtils.getString(mContext, USER_SEX, "");
        int value=1;
        if(user_sex.equals("boy")){
            value=boymax;
        }else if(user_sex.equals("gril"))
        {
            value=grilmax;
        }
        if (current >= value) {
            return false;
        }
        SharePreferenceUtils.putInt(mContext, MESSAGE_SEND_NOUSERS, current + 1);
        return true;
    }

    private String address = "";

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

}
