package com.hyphenate.easeui.manager;


import android.content.Context;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hyphenate.easeui.common.UrlConstant;
import com.hyphenate.easeui.model.SensitiveBean;
import com.hyphenate.easeui.utils.DirTraversal;
import com.hyphenate.easeui.utils.ZipUtils;
import com.thinkdit.lib.util.SharePreferenceUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by hx_Alex on 2017/7/18.
 * 违禁词库管理类
 */

public class ForbiddenWordsManger implements DownloadManager.IDownloadListener{
    private static final String  SP_SENSITIVEBEAN= "SensitiveBean";//违禁字符库文件名
    private static final String FILE_SENSITIVE= "SensitiveWords.txt";//违禁字符库文件名
    private static final String REPLACE_STRING="*";//违禁词汇替代字符
    private volatile static ForbiddenWordsManger manger;
    private Context context;
    private RequestUpdate mPresenter;//下载词库P类
    private ArrayList<String> fwList;//违禁字符缓存列表
    private String localCachePath;//本地下载缓存根目录
    private SensitiveBean bean;
    private DownloadManager downloadManager;
    private ForbiddenWordsManger(Context context)
    {
        if(context==null) return;
        this.context=context;
        fwList=new ArrayList<>();
        intiBaseLocalPath(context);
        mPresenter=new RequestUpdate(new SensitiveWordsListenerImp());
        downloadManager=new DownloadManager(this);
    }

    public void setBean(SensitiveBean bean) {
        this.bean = bean;
    }

    public ArrayList<String> getFwList() {
        return fwList;
    }
    
    public static ForbiddenWordsManger getInstance(@NonNull Context context)
    {
        if(manger==null)
        {
            synchronized (ForbiddenWordsManger.class)
            {
                if(manger==null)
                {
                    manger=new ForbiddenWordsManger(context);
                    return manger;
                }
                return manger;
            }
        }
        return manger;
    }

    public void startCheckForbiddenWords()
    {
        mPresenter.getSensitivePkg();
    }
    
    private void intiBaseLocalPath(Context context)
    {
        boolean sdCardExist = Environment.getExternalStorageState().equals(Environment
                .MEDIA_MOUNTED);
        if (sdCardExist) {
            localCachePath = Environment.getExternalStorageDirectory().getPath() + "/" +
                    context.getPackageName() + "/forbidden/";//获取跟目录
        } else {
            localCachePath = context.getCacheDir() + "/forbidden/";
        }
        File f=new File(localCachePath);
        if (!f.exists()) { //下载文件路径不存在则新建
            f.mkdirs();
        }
    }
    
    /***
     * 检查是否需要下载
     */
    private void checkIsDownLoadForbiddenWords() {
        Log.i("checkForbiddenWords","checkIsDownLoadForbiddenWords");
        if (bean == null) return;
        SensitiveBean localBean = (SensitiveBean) SharePreferenceUtils.readObject(context, SP_SENSITIVEBEAN);//检查本地是否有词库记录
        //首先判断是否需要更新
        if (localBean != null) {
            boolean isNeedUpdate = localBean.getId() != bean.getId() && localBean.getCreateTime() != bean.getCreateTime();
            //需要更新
            if (isNeedUpdate) {
                Log.i("checkForbiddenWords","词库文件有更新,开始下载最新包");
                //下载最新包
                downLoadForbiddenWordsPkg();
            } else //不需要更新，再次检查目录下是否存在当前文件，不存在的话，还是需要下载并解压
            {
                //存在词库-->加载词库至list缓存
                Log.i("checkForbiddenWords","存在词库-->加载词库至list缓存");
                if (checkFileIsExists(localCachePath + FILE_SENSITIVE)) {
                    readData();
                } else {
                    Log.i("checkForbiddenWords","本地没有检查到有词库文件,开始下载最新包");
                    //下载最新包
                    downLoadForbiddenWordsPkg();
                }
            }
        } else {//本地没有检查到有词库记录
            Log.i("checkForbiddenWords","本地没有检查到有词库记录,开始下载最新包");
            //下载最新包
            downLoadForbiddenWordsPkg();
        }

    }
    
    
    
    
    private void downLoadForbiddenWordsPkg()
    {
        String zipName = getZipName(bean.getUrl());
        downloadManager.startDownload(bean.getUrl(),localCachePath+zipName);
     
    }
      

    /***
     * 务必首先下载远端文件完成后进行读取操作
     * 读取下载至本地的文件，并缓存至list中
     */
    private void readData()
    {
        if(fwList==null) return;
        //读取前先清空list缓存
        if(fwList.size()>0)  { fwList.clear();}
        
        File sensitiveFile = DirTraversal.getFilePath(localCachePath, FILE_SENSITIVE);
        if(sensitiveFile!=null)
        {
            BufferedReader reader=null;
            try {
                 reader=new BufferedReader(new FileReader(sensitiveFile));
                String tempLineString=null;
                while ((tempLineString=reader.readLine())!=null)
                {
                    fwList.add(tempLineString);
                }
                reader.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
    
    /***
     * 检查语句是否违禁,返回过滤后的消息 ->对外公开
     * @param checkSentence 检查语句
     * @return 返回检查后的msg信息
     */
    public String filterForbidden(String checkSentence)
    {
        String tempCheckSentence=checkSentence;//临时缓存字符串
        //当缓存list为空或者无缓存数据返回false
        if( fwList==null||fwList.size()==0)
        {
            return  tempCheckSentence;
        }
        
        for(String forbidden:fwList)
        {
            if(tempCheckSentence.contains(forbidden))
            {
               tempCheckSentence= filterMessageWithForBidden(tempCheckSentence,forbidden);
            }
        }
        return tempCheckSentence;
    }

    /***
     * 过滤违禁词汇，返回过滤后的消息
     * @param msg  消息
     * @param forbiddenString 需过滤词汇
     * @return
     */
    private String filterMessageWithForBidden(String msg, String forbiddenString)
    {
        
        StringBuffer replaceString=new StringBuffer();
        for(int i=0;i<forbiddenString.length();i++)
        {
         replaceString.append(REPLACE_STRING);   
        }
        String newMsg = msg.replaceAll(forbiddenString, replaceString.toString());
        return newMsg;   
    }

    /**
     * 检查文件是否存在
     * @return
     */
    private  boolean checkFileIsExists(String filePath)
    {
        File f=new File(filePath);
        if(!f.exists())
        {
            return false;
        }
        return true;
    }

    /**
     * 获取zip名
     * @return
     */
    private  String getZipName(String url)
    {
        if(url==null) return "";
        int startIndex = url.lastIndexOf("/")+1;
        return  url.substring(startIndex,url.length());
    }

    

    @Override
    public void onDownloadStart() {
        Log.i("downLoadZip","开始下载违禁词库");
    }

    @Override
    public void onDownloadProgress(long count, long current) {
      
    }

    @Override
    public void onDownloadSuccess(String url, String localPath) {
        String zipName = getZipName(url);
        Log.e("onDownloadSuccess","url:"+url);
        Log.e("onDownloadSuccess","localPath:"+localPath);
        Log.e("onDownloadSuccess","zipName:"+zipName);
        File file = DirTraversal.getFilePath(localCachePath, zipName);
        try {
            ZipUtils.upZipFile(file,localCachePath);
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("file","解压失败");
            return;
        }
        Log.e("file","解压成功");
        
        readData();
        SharePreferenceUtils.saveObject(context,SP_SENSITIVEBEAN,bean);
    }

    @Override
    public void onDownloadFailure() {

    }


    /**
     * 请求词库
     */
    class RequestUpdate
    {
        private final String URL_GET_SENSITIVE_PKG="/sensitive-word/get-pkg";//获取敏感词库zip包
        private  SensitiveWordsListener sensitiveWordsListener;

        public RequestUpdate(SensitiveWordsListener sensitiveWordsListener) {
            this.sensitiveWordsListener = sensitiveWordsListener;
        }

        public String getUrl(String path) {
            return UrlConstant.URL_BASE + path;
        }

      public void   getSensitivePkg()
      {
          OkHttpClient client=new OkHttpClient();
          Request request=new Request.Builder().url(getUrl(URL_GET_SENSITIVE_PKG)).method("GET",null).build();
          Call call=client.newCall(request);
          call.enqueue(new Callback() {
              @Override
              public void onFailure(Call call, IOException e) {
                  sensitiveWordsListener.onGetSensitiveWordsError("网络连接错误");
                  Log.e("getSensitivePkg","网络连接错误");
              }

              @Override
              public void onResponse(Call call, Response response) throws IOException {
                  if(response.code()==200){
                        String responseBody = response.body().string();
                        JSONObject dataJson = JSON.parseObject(responseBody);
                      SensitiveBean bean = JSON.parseObject(dataJson.getString("data"), SensitiveBean.class);
                      sensitiveWordsListener.onGetSensitiveWordsSucceed(bean);
                      Log.e("getSensitivePkg","onGetSensitiveWordsSucceed");
                  }else 
                  {
                      sensitiveWordsListener.onGetSensitiveWordsError("网络连接错误");
                      Log.e("getSensitivePkg","网络连接错误");
                  }
              }
          });
      }

        
    }
    public  interface SensitiveWordsListener
    {
        void  onGetSensitiveWordsSucceed(SensitiveBean bean);
        void  onGetSensitiveWordsError(String msg);
    }
    class SensitiveWordsListenerImp implements  SensitiveWordsListener
    {

        @Override
        public void onGetSensitiveWordsSucceed(SensitiveBean bean) {
            Log.e("getSensitivePkg","onGetSensitiveWordsSucceed");
            setBean(bean);
            checkIsDownLoadForbiddenWords();
        }

        @Override
        public void onGetSensitiveWordsError(String msg) {

        }
    }
}
