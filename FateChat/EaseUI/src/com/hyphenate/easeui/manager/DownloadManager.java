package com.hyphenate.easeui.manager;

import com.thinkdit.lib.util.L;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSource;
import okio.ForwardingSource;
import okio.Okio;
import okio.Source;

/**
 * 文件下载类
 */
public class DownloadManager {
    private static final String TAG = "DownloadManager";

    private IDownloadListener mListener;
    private String mApkUrl, mFilePath;

    public DownloadManager() {
    }

    public DownloadManager(IDownloadListener listener) {
        mListener = listener;
    }

    public void startDownload(String url, String filePath) {
        startDownload(url, filePath, mListener);
    }

    public void startDownload(String url, String filePath, IDownloadListener listener) {
        try {
            mApkUrl = url;
            mFilePath = filePath;
            L.d(TAG, "start download");
            if (listener != null) {
                listener.onDownloadStart();
            }
            //监听下载进度
            final ProgressListener progressListener = (long bytesRead, long contentLength, boolean done) -> {
                if (done) {
                    L.d(TAG, "download compete");
                    if (listener != null) {
                        listener.onDownloadSuccess(mApkUrl, mFilePath);
                    }
                } else {
                    L.d(TAG, "done " + (100 * bytesRead) / contentLength);
                    if (listener != null) {
                        listener.onDownloadProgress(contentLength, bytesRead);
                    }
                }
            };

            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder().body(
                            new ProgressResponseBody(originalResponse.body(), progressListener))
                            .build();

                }
            }).build();

            //封装请求
            Request request = null;
            try {
                request = new Request.Builder().url(url).build();
            } catch (Exception e) {
                L.e(TAG, e);
                if (listener != null) {
                    listener.onDownloadFailure();
                }
                return;
            }
            //发送异步请求
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    L.e(TAG, "download fail", e);
                    if (listener != null) {
                        listener.onDownloadFailure();
                    }
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    //将返回结果转化为流，并写入文件
                    int len;
                    byte[] buf = new byte[2048];
                    InputStream inputStream = response.body().byteStream();
                    //可以在这里自定义路径
                    File file = new File(mFilePath);
                    if (!file.exists()) {
                        file.createNewFile();
                    }
                    FileOutputStream fileOutputStream = new FileOutputStream(file);

                    while ((len = inputStream.read(buf)) != -1) {
                        fileOutputStream.write(buf, 0, len);
                    }
                    fileOutputStream.flush();
                    fileOutputStream.close();
                    inputStream.close();
                }
            });
        } catch (Exception e) {
            L.e(TAG, "download fail", e);
            if (listener != null) {
                listener.onDownloadFailure();
            }
        }
    }

    /**
     * 添加进度监听的ResponseBody
     */
    private static class ProgressResponseBody extends ResponseBody {
        private final ResponseBody responseBody;
        private final ProgressListener progressListener;
        private BufferedSource bufferedSource;

        public ProgressResponseBody(ResponseBody responseBody, ProgressListener progressListener) {
            this.responseBody = responseBody;
            this.progressListener = progressListener;
        }

        @Override
        public MediaType contentType() {
            return responseBody.contentType();
        }


        @Override
        public long contentLength() {
            return responseBody.contentLength();
        }

        @Override
        public BufferedSource source() {
            if (bufferedSource == null) {
                bufferedSource = Okio.buffer(source(responseBody.source()));
            }
            return bufferedSource;
        }

        private Source source(Source source) {
            return new ForwardingSource(source) {
                long totalBytesRead = 0L;

                @Override
                public long read(Buffer sink, long byteCount) throws IOException {
                    long bytesRead = super.read(sink, byteCount);
                    totalBytesRead += bytesRead != -1 ? bytesRead : 0;
                    progressListener.update(totalBytesRead, responseBody.contentLength(), bytesRead == -1);
                    return bytesRead;
                }
            };
        }
    }


    interface ProgressListener {
        /**
         * @param bytesRead     已下载字节数
         * @param contentLength 总字节数
         * @param done          是否下载完成
         */
        void update(long bytesRead, long contentLength, boolean done);
    }

    public interface IDownloadListener {
        /**
         * 开始下载
         */
        public void onDownloadStart();

        /**
         * 下载进度
         *
         * @param count   总大小
         * @param current 下载大小
         */
        public void onDownloadProgress(long count, long current);

        /**
         * 下载成功
         *
         * @param url       apk地址
         * @param localPath 下载的文件路径
         */
        public void onDownloadSuccess(String url, String localPath);

        /**
         * 下载失败
         * TODO 后续添加异常信息返回
         */
        public void onDownloadFailure();
    }
}
