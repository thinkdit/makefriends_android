package com.hyphenate.easeui.manager;


import com.hyphenate.easeui.interfaces.IContact;

/**
 */
public class ContactManager {
    private static ContactManager instance;
    private IContact iContact;

    public static ContactManager instance() {
        if (instance == null) {
            instance = new ContactManager();

        }
        return instance;
    }

    public IContact getiContact() {
        return iContact;
    }

    public void setiContact(IContact iContact) {
        this.iContact = iContact;
    }

    private ContactManager() {
    }


}
