package com.hyphenate.easeui.common;

import android.util.Log;

/**
 * url的基类
 */
public class UrlConstant {

    public static final String SAVE_KEY_URL_API = "SAVE_KEY_URL_API";
    public static final String SAVE_KEY_URL_CHAT = "SAVE_KEY_URL_CHAT";
    public static final String SAVE_KEY_URL_PAY = "SAVE_KEY_URL_PAY";
    public static final String SAVE_KEY_URL_H5 = "SAVE_KEY_URL_H5";
    public static final String SAVE_KEY_URL_FUN = "SAVE_KEY_URL_FUN";


    //    public static boolean IS_DEBUG = BuildConfig.DEBUG;
    public static boolean IS_DEBUG = false;

    private static final String URL_BASE_RELEASE = "http://121.41.38.204:19090";
    private static final String URL_BASE_CHAT_RELEASE = "http://121.41.38.204:19090";
    private static final String URL_BASE_PAY_RELEASE = "http://121.41.38.204:19090";
    private static final String URL_BASE_H5_RELEASE = "http://121.41.38.204:19090";
    private static final String URL_BASE_FUN_RELEASE = "http://121.41.38.204:19090";

    private static final String URL_BASE_GAME_RELEASE = "http://116.62.96.236";

//    private static final String URL_BASE_RELEASE = "http://api.dingsns.com";
//    private static final String URL_BASE_CHAT_RELEASE = "https://chat.dingsns.com";
//    private static final String URL_BASE_PAY_RELEASE = "https://pay.dingsns.com";
//    private static final String URL_BASE_H5_RELEASE = "http://h5.dingsns.com";
//    private static final String URL_BASE_FUN_RELEASE = "http://fun.dingsns.com";


    private static final String URL_BASE_DEBUG = " http://dev.sincebest.com:81";
    private static final String URL_BASE_CHAT_DEBUG = " http://dev.sincebest.com:81";
    private static final String URL_BASE_PAY_DEBUG = " http://dev.sincebest.com:81";
    private static final String URL_BASE_GAME_DEBUG = " http://dev.sincebest.com:81";
    private static final String URL_BASE_H5_DEBUG = " http://dev.sincebest.com:81";
    private static final String URL_BASE_FUN_DEBUG = " http://dev.sincebest.com:81";

    private static String URL_HELPCENTER_PATH = "/license.html";//帮助中心
    private static String URL_LICENSE_PATH = "/about_us/license.html";//用户协议
    private static String URL_ANNOUNCEMENT_PATH = "/about_us/announcement.html";//风险提示
    private static String URL_GARDEN_PATH = "/base/garden.html";
    private static String URL_MY_LEVEL_PATH = "/level/index.html?anchorId=";
    private static String URL_GET_DAILY_TASK_PATH = "/taskList/index.html";
    private static String URL_GET_CONSLUTIM_PATH = "/conslut.html";
    public static String URL_BASE;
    public static String URL_BASE_CHAT;
    public static String URL_BASE_PAY;
    public static String URL_BASE_GAME;
    public static String URL_BASE_GARDEN;
    public static String URL_BASE_FUN;

    public static String URL_LICENSE;
    public static String URL_ANNOUNCEMENT;
    public static String URL_HELPCENTER;
    public static String URL_CONSLUTIM;
    public static String URL_GARDEN;
    public static String URL_MY_LEVEL;
    public static String URL_GET_DAILY_TASK;


    static {
        initBaseUrl();
    }


    public static void initBaseUrl() {
        if (IS_DEBUG) {
            setBaseUrl(URL_BASE_DEBUG, URL_BASE_CHAT_DEBUG, URL_BASE_PAY_DEBUG,
                    URL_BASE_H5_DEBUG, URL_BASE_FUN_DEBUG);
        } else {
            setBaseUrl(URL_BASE_RELEASE, URL_BASE_CHAT_RELEASE, URL_BASE_PAY_RELEASE,
                    URL_BASE_H5_RELEASE, URL_BASE_FUN_RELEASE);
        }
    }

    public static void setBaseUrl(String api, String chat, String pay, String h5, String fun) {
        if (api == null || chat == null || pay == null || h5 == null || fun == null) {
            return;
        }
        URL_BASE = api;
        URL_BASE_CHAT = chat;
        URL_BASE_PAY = pay;
        URL_BASE_FUN = fun;
        URL_BASE_GAME = fun;

        URL_GARDEN = h5 + URL_GARDEN_PATH;
        URL_LICENSE = h5 + URL_LICENSE_PATH;
        URL_ANNOUNCEMENT = h5 + URL_ANNOUNCEMENT_PATH;
        URL_MY_LEVEL = h5 + URL_MY_LEVEL_PATH;
        URL_HELPCENTER = h5 + URL_HELPCENTER_PATH;
        URL_HELPCENTER = h5 + URL_HELPCENTER_PATH;
        URL_GET_DAILY_TASK = h5 + URL_GET_DAILY_TASK_PATH;
        URL_CONSLUTIM=h5+URL_GET_CONSLUTIM_PATH;
        IS_DEBUG = !api.contains(URL_BASE_RELEASE);

        Log.i("DEBUG", "isdebug--->" + IS_DEBUG);
    }


}
