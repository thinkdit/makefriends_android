package com.hyphenate.easeui.model;

/**
 * Created by hx_Alex on 2017/5/30.
 * 时间总线回调参数
 */

public class EventBusModel {
    private String action;//标识回调内容，以便回调方法准确处理回调事件
    private Object Data;//回调携带内容

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Object getData() {
        return Data;
    }

    public void setData(Object data) {
        Data = data;
    }
}
