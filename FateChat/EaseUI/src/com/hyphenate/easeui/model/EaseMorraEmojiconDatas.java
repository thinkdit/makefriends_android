package com.hyphenate.easeui.model;

import android.util.Log;

import com.hyphenate.easeui.R;
import com.hyphenate.easeui.domain.EaseEmojicon;
import com.hyphenate.easeui.domain.EaseEmojicon.Type;
import com.hyphenate.easeui.domain.EaseEmojiconGroupEntity;
import com.hyphenate.easeui.utils.EaseSmileUtils;

import java.util.Arrays;
import java.util.Random;

/***
 * 猜拳图库
 */
public class EaseMorraEmojiconDatas {
    
    private static String[] emojis = new String[]{
            EaseSmileUtils.s_7,
            EaseSmileUtils.s_8,
            EaseSmileUtils.s_9,
    };
    
    private static int[] icons = new int[]{
            R.drawable.s7,
            R.drawable.s8,
            R.drawable.s9,
    };
    
    
    private static final EaseEmojiconGroupEntity DATA = createDataEntity();
    
    public static EaseEmojicon[] createData(){
        EaseEmojicon[] datas = new EaseEmojicon[icons.length];
        for(int i = 0; i < icons.length; i++){
            datas[i] = new EaseEmojicon(icons[i], emojis[i], Type.NORMAL);
        }
        return datas;
    }

    public static EaseEmojiconGroupEntity createDataEntity(){
        EaseEmojiconGroupEntity emojiconGroupEntity = new EaseEmojiconGroupEntity();
        EaseEmojicon[] datas = new EaseEmojicon[icons.length];
        for(int i = 0; i < icons.length; i++){
            datas[i] = new EaseEmojicon(icons[i], null, Type.BIG_EXPRESSION);
            datas[i].setBigIcon(icons[i]);
            datas[i].setName("动画表情"+ (i));
            datas[i].setIdentityCode("cm"+ i);
        }
        emojiconGroupEntity.setEmojiconList(Arrays.asList(datas));
        emojiconGroupEntity.setIcon(R.drawable.s_1);
        emojiconGroupEntity.setType(Type.BIG_EXPRESSION);
        return emojiconGroupEntity;
    }
    
    
    
    
    public static EaseEmojicon[] getData(){
        EaseEmojicon easeEmojicon = new EaseEmojicon(icons[1], emojis[1], Type.BIG_EXPRESSION);//默认只给一张图
        EaseEmojicon[] datas = {easeEmojicon};
        return  datas;
    }

    /***
     * 获取发送出去的图片
     * 随机发送一张
     * @return
     */
    public  static  EaseEmojicon getSendData()
    {
        Random random=new Random();
        int i = random.nextInt(icons.length);
        Log.e("猜拳","本次猜拳结果为:"+(i));
        EaseEmojicon easeEmojicon = new EaseEmojicon(icons[i], null, Type.BIG_EXPRESSION);//发送出去的大小
        easeEmojicon.setName("动画表情");
        easeEmojicon.setBigIcon(icons[i]);
        easeEmojicon.setIdentityCode("cm"+i);
        return easeEmojicon;
    }
    
}
