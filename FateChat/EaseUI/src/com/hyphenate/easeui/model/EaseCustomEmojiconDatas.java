package com.hyphenate.easeui.model;

import android.util.Log;

import com.hyphenate.easeui.R;
import com.hyphenate.easeui.domain.EaseEmojicon;
import com.hyphenate.easeui.domain.EaseEmojicon.Type;
import com.hyphenate.easeui.domain.EaseEmojiconGroupEntity;
import com.hyphenate.easeui.utils.EaseSmileUtils;

import java.util.Arrays;
import java.util.Random;

/***
 * 摇撒子图库
 */
public class EaseCustomEmojiconDatas {
    
    private static String[] emojis = new String[]{
            EaseSmileUtils.s_1,
            EaseSmileUtils.s_2,
            EaseSmileUtils.s_3,
            EaseSmileUtils.s_4,
            EaseSmileUtils.s_5,
            EaseSmileUtils.s_6
    };
    
    private static int[] icons = new int[]{
            R.drawable.s_1,
            R.drawable.s_2,
            R.drawable.s_3,
            R.drawable.s_4,
            R.drawable.s_5,
            R.drawable.s_6
    };
    
    
    private static final EaseEmojiconGroupEntity DATA = createDataEntity();
    
    public static EaseEmojicon[] createData(){
        EaseEmojicon[] datas = new EaseEmojicon[icons.length];
        for(int i = 0; i < icons.length; i++){
            datas[i] = new EaseEmojicon(icons[i], emojis[i], Type.NORMAL);
        }
        return datas;
    }

    public static EaseEmojiconGroupEntity createDataEntity(){
        EaseEmojiconGroupEntity emojiconGroupEntity = new EaseEmojiconGroupEntity();
        EaseEmojicon[] datas = new EaseEmojicon[icons.length];
        for(int i = 0; i < icons.length; i++){
            datas[i] = new EaseEmojicon(icons[i], null, Type.BIG_EXPRESSION);
            datas[i].setBigIcon(icons[i]);
            datas[i].setName("动画表情"+ (i));
            datas[i].setIdentityCode("em"+ i);
        }
        emojiconGroupEntity.setEmojiconList(Arrays.asList(datas));
        emojiconGroupEntity.setIcon(R.drawable.s_1);
        emojiconGroupEntity.setType(Type.BIG_EXPRESSION);
        return emojiconGroupEntity;
    }
    
    
    
    
    public static EaseEmojicon[] getData(){
        EaseEmojicon easeEmojicon = new EaseEmojicon(icons[5], emojis[5], Type.BIG_EXPRESSION);//默认只给一张图
        EaseEmojicon[] datas = {easeEmojicon};
        return  datas;
    }

    /***
     * 获取发送出去的图片
     * 随机发送一张
     * @return
     */
    public  static  EaseEmojicon getSendData()
    {
        Random random=new Random();
        int i = random.nextInt(icons.length);
        Log.e("摇撒子","本次大小为:"+(i+1));
        EaseEmojicon easeEmojicon = new EaseEmojicon(icons[i], null, Type.BIG_EXPRESSION);//发送出去的大小
        easeEmojicon.setName("动画表情");
        easeEmojicon.setBigIcon(icons[i]);
        easeEmojicon.setIdentityCode("em"+i);
        return easeEmojicon;
    }
    
}
