package com.hyphenate.easeui.model;

import java.io.Serializable;

/**
 * Created by hx_Alex on 2017/7/19.
 */

public class SensitiveBean  implements Serializable {

    /**
     * id : 9
     * url : http://makefriends-dev.oss-cn-hangzhou.aliyuncs.com/pub/20170718112254458_2563.zip
     * createTime : 1500348204000
     */

    private int id;
    private String url;
    private long createTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }
}
