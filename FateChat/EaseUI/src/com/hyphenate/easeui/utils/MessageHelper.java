package com.hyphenate.easeui.utils;

import com.alibaba.fastjson.JSON;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMImageMessageBody;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.chat.EMTextMessageBody;
import com.hyphenate.chat.EMVoiceMessageBody;
import com.hyphenate.easeui.EaseConstant;
import com.hyphenate.easeui.domain.ChatUserEntity;
import com.hyphenate.easeui.domain.LocalImageContent;
import com.hyphenate.easeui.domain.LocalMessage;
import com.hyphenate.easeui.domain.LocalSession;
import com.hyphenate.easeui.domain.LocalVoiceContent;
import com.hyphenate.easeui.manager.ForbiddenWordsManger;
import com.hyphenate.exceptions.HyphenateException;

/**
 * Created by leaves on 2017/4/7.
 */

public class MessageHelper {

//    private static final int MIL_SEC = 1000;

    public static LocalMessage ServerToLocalMsg(EMMessage msg) {
        LocalMessage localMessage = new LocalMessage();
        try {
            int userId = msg.getIntAttribute(EaseConstant.MESSAGE_ATTRIBUTE_USERID);
            localMessage.setUserId(userId);

        } catch (HyphenateException e) {
            e.printStackTrace();
        }
        localMessage.setKeFu(msg.getFrom());
        localMessage.setMsgId(msg.getMsgId());
        if (msg.status() == EMMessage.Status.SUCCESS) {
            localMessage.setStatus(0);
        } else if (msg.status() == EMMessage.Status.FAIL) {
            localMessage.setStatus(1);
        } else if (msg.status() == EMMessage.Status.INPROGRESS) {
            localMessage.setStatus(2);
        }

        localMessage.setCreate(msg.getMsgTime());
        if (msg.getType() == EMMessage.Type.TXT) {
            //如果是大图
            if(msg.getBooleanAttribute(EaseConstant.MESSAGE_ATTR_IS_BIG_EXPRESSION, false))
            {
                String emojiconId = msg.getStringAttribute(EaseConstant.MESSAGE_ATTR_EXPRESSION_ID, null);
                localMessage.setContent(emojiconId);
                localMessage.setType(LocalMessage.LOCAL_MESSAGE_BIGEXP);
            }else {
                EMTextMessageBody txtBody = (EMTextMessageBody) msg.getBody();
                if (msg.direct()== EMMessage.Direct.RECEIVE)
                {
                    String message = filterMessage(txtBody.getMessage());
                    localMessage.setContent(message);
                }else
                {
                    localMessage.setContent(txtBody.getMessage());
                }
                localMessage.setType(LocalMessage.LOCAL_MESSAGE_TXT);
            }
          
        } else if (msg.getType() == EMMessage.Type.IMAGE) {
            EMImageMessageBody imgBody = (EMImageMessageBody) msg.getBody();
            String path = imgBody.thumbnailLocalPath();
            String path2 = imgBody.getRemoteUrl();
            LocalImageContent localImage = new LocalImageContent();
            localImage.setRemoteUrl(path2);
            localImage.setLocalPath(path);
            localMessage.setContent(JSON.toJSONString(localImage));
            localMessage.setType(LocalMessage.LOCAL_MESSAGE_IMAGE);
        }else if (msg.getType() == EMMessage.Type.VOICE) {
            EMVoiceMessageBody voiceBody = (EMVoiceMessageBody) msg.getBody();
            String path = voiceBody.getLocalUrl();
            String path2 = voiceBody.getRemoteUrl();
            LocalVoiceContent localVoice = new LocalVoiceContent();
            localVoice.setRemoteUrl(path2);
            localVoice.setLocalPath(path);
            localVoice.setLength(voiceBody.getLength());
            localMessage.setContent(JSON.toJSONString(localVoice));
            localMessage.setType(LocalMessage.LOCAL_MESSAGE_VOICE);
        }
        if (msg.getFrom() != null && msg.getFrom().equalsIgnoreCase(EMClient.getInstance()
                .getCurrentUser())) {
            localMessage.setDirection(LocalMessage.DIRECTION_SEND);
        } else {
            localMessage.setDirection(LocalMessage.DIRECTION_RECV);
        }
        return localMessage;
    }

    public static EMMessage localToServerMsg(LocalMessage msg) {
        EMMessage serverMsg = null;
        if (msg.getType() == LocalMessage.LOCAL_MESSAGE_TXT) {
            serverMsg = EMMessage.createTxtSendMessage(msg.getContent(), msg.getKeFu());
        } else if (msg.getType() == LocalMessage.LOCAL_MESSAGE_IMAGE) {

            try {
                LocalImageContent localImageContent = JSON.parseObject(msg.getContent(),
                        LocalImageContent.class);
                serverMsg = EMMessage.createImageSendMessage(localImageContent.getLocalPath(),
                        true, msg.getKeFu());
                EMImageMessageBody imgBody = (EMImageMessageBody) serverMsg.getBody();
                imgBody.setRemoteUrl(localImageContent.getRemoteUrl());
                serverMsg.addBody(imgBody);

            } catch (Exception e) {

            }


        }else if (msg.getType() == LocalMessage.LOCAL_MESSAGE_VOICE) {
            LocalVoiceContent localImageContent = JSON.parseObject(msg.getContent(),
                    LocalVoiceContent.class);
            serverMsg=EMMessage.createVoiceSendMessage(localImageContent.getLocalPath(),localImageContent.getLength(),msg.getKeFu());
            

        } else if(msg.getType() == LocalMessage.LOCAL_MESSAGE_BIGEXP){
            serverMsg=EaseCommonUtils.createExpressionMessage(msg.getKeFu(),"图片表情",msg.getContent());

        }else {

            return null;
        }
        if (serverMsg == null) {
            return null;
        }
        if (msg.getStatus() == 0) {
            serverMsg.setStatus(EMMessage.Status.SUCCESS);
        } else if (msg.getStatus() == 1) {
            serverMsg.setStatus(EMMessage.Status.FAIL);
        } else if (msg.getStatus() == 2) {
            serverMsg.setStatus(EMMessage.Status.INPROGRESS);
        }
        serverMsg.setMsgTime(msg.getCreate());
        serverMsg.setFrom(msg.getKeFu());
        if (msg.getDirection() == LocalMessage.DIRECTION_RECV) {
            serverMsg.setDirection(EMMessage.Direct.RECEIVE);
        } else {
            serverMsg.setDirection(EMMessage.Direct.SEND);
        }
        return serverMsg;
    }

    public static LocalSession ServerToLocalSession(EMMessage msg) {
        LocalSession session = new LocalSession();
        try {
            int userId = msg.getIntAttribute(EaseConstant.MESSAGE_ATTRIBUTE_USERID);
            session.setUserId(userId);
            String avator = msg.getStringAttribute(EaseConstant.EXTRA_USER_AVATOR);
            session.setAvatar(avator);
            String nickName = msg.getStringAttribute(EaseConstant.EXTRA_USER_NICKNAME);
            session.setNickName(nickName);
        } catch (HyphenateException e) {
            e.printStackTrace();
        }
        if(msg.direct()== EMMessage.Direct.RECEIVE) {
            session.setKeFu(msg.getFrom());
        }else if(msg.direct()== EMMessage.Direct.SEND)
        {
            session.setKeFu(msg.getTo());
        }
        session.setUnReadCount(1);

        if (msg.getType() == EMMessage.Type.TXT) {
            session.setLastMsgType(LocalMessage.LOCAL_MESSAGE_TXT);
            EMTextMessageBody txtBody = (EMTextMessageBody) msg.getBody();
            if (msg.direct()== EMMessage.Direct.RECEIVE)
            {
                String message = filterMessage(txtBody.getMessage());
                session.setLastMsgContent(message);
            }else
            {
                session.setLastMsgContent(txtBody.getMessage());
            }
        } else if (msg.getType() == EMMessage.Type.IMAGE) {
            session.setLastMsgType(LocalMessage.LOCAL_MESSAGE_IMAGE);
            EMImageMessageBody imgBody = (EMImageMessageBody) msg.getBody();
            session.setLastMsgContent(imgBody.getLocalUrl());
        }else if (msg.getType() == EMMessage.Type.VOICE) {
            session.setLastMsgType(LocalMessage.LOCAL_MESSAGE_VOICE);
            EMVoiceMessageBody voiceMessageBody= (EMVoiceMessageBody) msg.getBody();
            session.setLastMsgContent(voiceMessageBody.getLocalUrl());
        }
        session.setLasttime(msg.getMsgTime());
        return session;
    }

    public static LocalSession ServerToLocalSession(EMMessage msg, ChatUserEntity entity) {
        LocalSession session = new LocalSession();
        session.setUserId(entity.getToUserID());
        session.setAvatar(entity.getToAvator());
        session.setNickName(entity.getToNickName());
        if(msg.direct()== EMMessage.Direct.RECEIVE) {
            session.setKeFu(msg.getFrom());
        }else if(msg.direct()== EMMessage.Direct.SEND)
        {
            session.setKeFu(msg.getTo());
        }
        session.setUnReadCount(1);

        if (msg.getType() == EMMessage.Type.TXT) {
            session.setLastMsgType(LocalMessage.LOCAL_MESSAGE_TXT);
            EMTextMessageBody txtBody = (EMTextMessageBody) msg.getBody();

            session.setLastMsgContent(txtBody.getMessage());

            if (msg.direct()== EMMessage.Direct.RECEIVE)
            {
                String message = filterMessage(txtBody.getMessage());
                session.setLastMsgContent(message);
            }else
            {
                session.setLastMsgContent(txtBody.getMessage());
            }

            
        } else if (msg.getType() == EMMessage.Type.IMAGE) {
            session.setLastMsgType(LocalMessage.LOCAL_MESSAGE_IMAGE);
            EMImageMessageBody imgBody = (EMImageMessageBody) msg.getBody();
            session.setLastMsgContent(imgBody.getLocalUrl());
        }else if (msg.getType() == EMMessage.Type.VOICE) {
            session.setLastMsgType(LocalMessage.LOCAL_MESSAGE_VOICE);
            EMVoiceMessageBody voiceMessageBody= (EMVoiceMessageBody) msg.getBody();
            session.setLastMsgContent(voiceMessageBody.getLocalUrl());
        }

        session.setLasttime(msg.getMsgTime());
        return session;
    }

    public static String filterMessage(String message)
    {

        ForbiddenWordsManger manger = ForbiddenWordsManger.getInstance(null);
        if(manger==null)
        {
            return message;
        }
        return   manger.filterForbidden(message);
    }
}
