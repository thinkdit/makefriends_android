package com.hyphenate.easeui.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.hyphenate.chat.EMImageMessageBody;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.chat.EMTextMessageBody;
import com.hyphenate.chat.EMVoiceMessageBody;
import com.hyphenate.easeui.EaseConstant;
import com.hyphenate.easeui.domain.ChatUserEntity;
import com.hyphenate.easeui.domain.LocalMessage;
import com.hyphenate.easeui.domain.LocalSession;
import com.hyphenate.easeui.manager.ForbiddenWordsManger;
import com.hyphenate.easeui.model.EventBusModel;
import com.hyphenate.easeui.ui.EaseChatFragment;
import com.hyphenate.exceptions.HyphenateException;
import com.xindian.fatechat.domain.DaoMaster;
import com.xindian.fatechat.domain.DaoSession;
import com.xindian.fatechat.domain.LocalMessageDao;
import com.xindian.fatechat.domain.LocalSessionDao;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.greendao.query.QueryBuilder;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by leaves on 2017/4/5.
 */

public class DBHelper {
    private static DBHelper instance;
    private Context mContext;
    private int mUserId;
    private DaoMaster.DevOpenHelper mHelper;
    private SQLiteDatabase db;
    private DaoMaster mDaoMaster;
    private DaoSession mDaoSession;

    public static synchronized DBHelper intance() {
        if (instance == null) {
            instance = new DBHelper();
        }
        return instance;
    }

    boolean isInit = false;

    public boolean isInit() {
        return isInit;
    }
    @Subscribe
    public void init(Context context, int userId) {
        if (isInit) {
            return;
        }
        mContext = context;
        isInit = true;
        mUserId=userId;
        setDatabase(context, userId);
    
    }
    @Subscribe
    public void reset() {
        isInit = false;
        if (mHelper != null) {
            mHelper.close();
            mUserId=0;//还原userId
        }
        mHelper = null;
     
    }

    /**
     * 设置greenDao
     */
    private void setDatabase(Context context, int userId) {
        // 通过 DaoMaster 的内部类 DevOpenHelper，你可以得到一个便利的 SQLiteOpenHelper 对象。
        // 可能你已经注意到了，你并不需要去编写「CREATE TABLE」这样的 SQL 语句，因为 greenDAO 已经帮你做了。
        // 注意：默认的 DaoMaster.DevOpenHelper 会在数据库升级时，删除所有的表，意味着这将导致数据的丢失。
        // 所以，在正式的项目中，你还应该做一层封装，来实现数据库的安全升级。
        Log.e("DDDDDD", "setDatabase---------------------------------userId=" + userId);
        mHelper = new DaoMaster.DevOpenHelper(context, userId + ".db", null);
        db = mHelper.getWritableDatabase();
        // 注意：该数据库连接属于 DaoMaster，所以多个 LocalSession 指的是相同的数据库连接。
        mDaoMaster = new DaoMaster(db);
        mDaoSession = mDaoMaster.newSession();
    }

    public DaoSession getDaoSession(){
        /***
         * fix bug java.lang.NullPointerException 2017.5.6
         * 存在初始化后session=null的情况
         */
        try {
        if(!isInit)
        {
            
            throw new SessionException("must init DBHelper");
        }
         else 
        {
            if(mDaoSession==null && mContext!=null && mUserId!=0)
            {
                //重新设置一次DB
                setDatabase(mContext,mUserId);
            }
        }   
        } catch (SessionException e) {
            e.printStackTrace();
        }
        return mDaoSession;
    }

    public void insertLocalMessageDao(LocalMessage localMessage) {
        mDaoSession.getLocalMessageDao().insertOrReplace(localMessage);
    }

    public void insertLocalSessionDao(LocalSession session) {
        mDaoSession.getLocalSessionDao().insertOrReplace(session);
    }

    public void insertLocalMessageDao(List<LocalMessage> sessions) {
        mDaoSession.getLocalMessageDao().insertOrReplaceInTx(sessions);
    }

    public void insertLocalMessageDao(LocalMessage[] sessions) {
        for (LocalMessage session : sessions) {
            mDaoSession.getLocalMessageDao().insertOrReplaceInTx(session);
        }
    }

    public void deleteLocalMessageDao(LocalMessage localMessage) {
        mDaoSession.getLocalMessageDao().delete(localMessage);
    }


    public void insertLocalSessionDao(List<LocalSession> sessions) {
        mDaoSession.getLocalSessionDao().insertOrReplaceInTx(sessions);
    }

    public void insertLocalSessionDao(LocalSession[] sessions) {
        mDaoSession.getLocalSessionDao().insertOrReplaceInTx(sessions);
        long t = 1491637971547l;
    }

    public List<EMMessage> getHistoryMessageDao(int userID, long lastTime, int count) {
        //fix bug 2017.5.16 Attempt to invoke virtual method 'com.xindian.fatechat.domain.LocalMessageDao com.xindian.fatechat.domain.b.b()' on a null object reference
        //com.hyphenate.easeui.utils.DBHelper.getHistoryMessageDao(SourceFile:114)
//        List<LocalMessage> all = mDaoSession.getLocalMessageDao().loadAll();
        List<EMMessage> messages = new ArrayList<>();

        if(getDaoSession().getLocalMessageDao()!=null){
            QueryBuilder<LocalMessage> queryBuilder = getDaoSession().getLocalMessageDao().queryBuilder();
            queryBuilder.where(queryBuilder.and(LocalMessageDao.Properties.UserId.eq(userID),
                    LocalMessageDao.Properties.Create.lt(lastTime))).orderDesc(LocalMessageDao
                    .Properties.Create).limit(count);
            List<LocalMessage> list = queryBuilder.list();
            for (LocalMessage localMsg : list) {
                EMMessage message = MessageHelper.localToServerMsg(localMsg);
                if (message != null) {
                    messages.add(0, message);
                }
            }
        }

        return messages;
    }

    public LocalSession getSessionByUserId(int userId) {
        QueryBuilder<LocalSession> queryBuilder = mDaoSession.getLocalSessionDao().queryBuilder();
        return queryBuilder.where(LocalSessionDao.Properties.UserId.eq(userId)).unique();
    }

    public SQLiteDatabase getDb() {
        return db;
    }

    public void onNewMessageAddSessionCount(EMMessage msg) {
        LocalMessage msesage = MessageHelper.ServerToLocalMsg(msg);
        LocalSession session = getSessionByUserId(msesage.getUserId());
        if (session == null) {
            session = MessageHelper.ServerToLocalSession(msg);
        } else {
            session.setUnReadCount(session.getUnReadCount() + 1);
            session.setLasttime(msg.getMsgTime());
            String avator = null;
            try {
                avator = msg.getStringAttribute(EaseConstant.EXTRA_USER_AVATOR);
                session.setAvatar(avator);
                String nickName = msg.getStringAttribute(EaseConstant.EXTRA_USER_NICKNAME);
                session.setNickName(nickName);
            } catch (HyphenateException e) {
                e.printStackTrace();
            }

            if (msg.getType() == EMMessage.Type.TXT) {
                EMTextMessageBody txtBody = (EMTextMessageBody) msg.getBody();
                if(msg.direct()== EMMessage.Direct.RECEIVE)
                {
                    session.setLastMsgContent(msesage.getContent()); 
                }else
                {
                    session.setLastMsgContent(txtBody.getMessage());
                }
                session.setLastMsgType(LocalMessage.LOCAL_MESSAGE_TXT);
            } else if (msg.getType() == EMMessage.Type.IMAGE) {
                EMImageMessageBody imgBody = (EMImageMessageBody) msg.getBody();
                session.setLastMsgContent(imgBody.getLocalUrl());
                session.setLastMsgType(LocalMessage.LOCAL_MESSAGE_IMAGE);
            } else if (msg.getType() == EMMessage.Type.VOICE) {
                EMVoiceMessageBody voiceMessageBody = (EMVoiceMessageBody) msg.getBody();
                session.setLastMsgContent(voiceMessageBody.getLocalUrl());
                session.setLastMsgType(LocalMessage.LOCAL_MESSAGE_VOICE);
            }
        }
        insertLocalMessageDao(msesage);
        Log.i("IM_DEBUG","insert-->TO-->"+msesage.getUserId());

        insertLocalSessionDao(session);
        Log.i("IM_DEBUG","insert---session-->TO-->"+msesage.getUserId());
    }

    public void onNewMessage(EMMessage msg) {
        //收到的消息，将对方的Id作为唯一ID保存到数据库
        LocalMessage msesage = MessageHelper.ServerToLocalMsg(msg);
        LocalSession session = getSessionByUserId(msesage.getUserId());
        if (session == null) {
            session = MessageHelper.ServerToLocalSession(msg);
            session.setUnReadCount(0);
        } else {
            session.setLasttime(msg.getMsgTime());
            if (msg.getType() == EMMessage.Type.TXT) {
                EMTextMessageBody txtBody = (EMTextMessageBody) msg.getBody();
                session.setLastMsgContent(txtBody.getMessage());
                session.setLastMsgType(LocalMessage.LOCAL_MESSAGE_TXT);


            } else if (msg.getType() == EMMessage.Type.IMAGE) {
                EMImageMessageBody imgBody = (EMImageMessageBody) msg.getBody();
                session.setLastMsgContent(imgBody.getLocalUrl());
                session.setLastMsgType(LocalMessage.LOCAL_MESSAGE_IMAGE);
            }else if (msg.getType() == EMMessage.Type.VOICE) {
                EMVoiceMessageBody voiceMessageBody = (EMVoiceMessageBody) msg.getBody();
                session.setLastMsgContent(voiceMessageBody.getLocalUrl());
                session.setLastMsgType(LocalMessage.LOCAL_MESSAGE_VOICE);
            }
        }
        insertLocalMessageDao(msesage);
        Log.i("IM_DEBUG","insert-->TO-->"+msesage.getUserId());

        insertLocalSessionDao(session);
        Log.i("IM_DEBUG","insert---session-->TO-->"+msesage.getUserId());
    }

    public void onNewMessage(EMMessage msg, ChatUserEntity entity) {
        //自己发的消息，将对方的Id作为唯一ID保存到数据库
        msg.setAttribute(EaseConstant.MESSAGE_ATTRIBUTE_USERID,entity.getToUserID());

        LocalMessage msesage = MessageHelper.ServerToLocalMsg(msg);
        LocalSession session = getSessionByUserId(msesage.getUserId());
        if (session == null) {
            session = MessageHelper.ServerToLocalSession(msg, entity);
            session.setUnReadCount(0);
        } else {
            session.setLasttime(msg.getMsgTime());
            if (msg.getType() == EMMessage.Type.TXT) {
                EMTextMessageBody txtBody = (EMTextMessageBody) msg.getBody();
                session.setLastMsgContent(txtBody.getMessage());
                session.setLastMsgType(LocalMessage.LOCAL_MESSAGE_TXT);
            } else if (msg.getType() == EMMessage.Type.IMAGE) {
                EMImageMessageBody imgBody = (EMImageMessageBody) msg.getBody();
                session.setLastMsgContent(imgBody.getLocalUrl());
                session.setLastMsgType(LocalMessage.LOCAL_MESSAGE_IMAGE);
            }else if (msg.getType() == EMMessage.Type.VOICE) {
                EMVoiceMessageBody voiceMessageBody = (EMVoiceMessageBody) msg.getBody();
                session.setLastMsgContent(voiceMessageBody.getRemoteUrl());
                session.setLastMsgType(LocalMessage.LOCAL_MESSAGE_VOICE);
            }
        }
        insertLocalMessageDao(msesage);
        Log.i("IM_DEBUG","insert-->TO-->"+msesage.getUserId());

        insertLocalSessionDao(session);
        Log.i("IM_DEBUG","insert---session-->TO-->"+msesage.getUserId());

        EventBusModel model = new EventBusModel();
        model.setAction(EaseChatFragment.TAG);
        EventBus.getDefault().post(model);
    }

    public void deleteSession(LocalSession session) {
        mDaoSession.getLocalSessionDao().delete(session);
        QueryBuilder<LocalMessage> queryBuilder = mDaoSession.getLocalMessageDao().queryBuilder();
        queryBuilder.where(LocalMessageDao.Properties.UserId.eq(session.getUserId()));
        queryBuilder.buildDelete();
    }
    
    class  SessionException extends Exception
    {
        public SessionException(String msg)
        {
            super(msg);
        }
    }

    public  String filterMessage(String message)
    {

        ForbiddenWordsManger manger = ForbiddenWordsManger.getInstance(mContext);
        if(manger==null)
        {
            return message;
        }
        return   manger.filterForbidden(message);
    }
}
