package com.hyphenate.easeui.utils;


import com.hyphenate.util.TimeInfo;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.hyphenate.util.DateUtils.getTodayStartAndEndTime;
import static com.hyphenate.util.DateUtils.getYesterdayStartAndEndTime;

public class DateUtil {

	/**
	 * 返回数据格式:今天9:41,昨天天9:41
	 * @param oldTime
	 * @param newTime
     * @return
     */
	public static String getTimeStr(long oldTime, long newTime) {
		try{
			StringBuilder sb = new StringBuilder();
			Date oldDate = new Date(oldTime);
			Date newDate = new Date(newTime);
			int oldDateDay = oldDate.getDay();
			int newDateDay = newDate.getDay();
			if(newDateDay-oldDateDay>0){
				sb.append("昨天");
			}else{
				sb.append("今天");
			}

			int oldHour = newDate.getHours();
			int minute = newDate.getMinutes();
			sb.append(oldHour+":");
			if(minute>9){
				sb.append(minute);
			}else{
				sb.append("0"+minute);
			}

			return sb.toString();

		}catch (Exception e){
			e.printStackTrace();
		}

		return "";

	}

	public  static  String getInterval(String createtime) { //传入的时间格式必须类似于2012-8-21 17:53:20这样的格式  
		String interval = null;

		SimpleDateFormat sd = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		ParsePosition pos = new ParsePosition(0);
		Date d1 = (Date) sd.parse(createtime, pos);

		//用现在距离1970年的时间间隔new Date().getTime()减去以前的时间距离1970年的时间间隔d1.getTime()得出的就是以前的时间与现在时间的时间间隔  
		long time =  new Date().getTime()-d1.getTime() ;// 得出的时间间隔是毫秒  
		//time/1000==秒 2000/1000=2秒
		if(time/1000 < 10 && time/1000 >= 0) {
			//如果时间间隔小于10秒则显示“刚刚”time/10得出的时间间隔的单位是秒  
			interval ="刚刚";

		} else if(time/3600000 < 24 && time/3600000 >= 1) {
			//如果时间间隔小于24小时则显示多少小时前  
			int h = (int) (time/3600000);//得出的时间间隔的单位是小时
			interval = h + "小时前";

		} else if(time/60000 < 60 && time/60000 > 0) {
			//如果时间间隔小于60分钟则显示多少分钟前  
			int m = (int) ((time%3600000)/60000);//得出的时间间隔的单位是分钟  
			interval = m + "分钟前";

		} else if(time/1000 < 60 && time/1000 > 0) {
			//如果时间间隔小于60秒则显示多少秒前  
			int se = (int) ((time%60000)/1000);
			interval = se + "秒前";

		}else {
			//大于24小时，则显示正常的时间，但是不显示秒  
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

			ParsePosition pos2 = new ParsePosition(0);
			Date d2 = (Date) sdf.parse(createtime,pos2);
			interval = sdf.format(d2);
		}
		return interval;
	}

	public static String getTimestampString(Date date) {
		String formatString=null;
		//如果时间是今天的
		if(isSameDay(date)){
			formatString="HH:mm";
			
		}else if(isYesterday(date)){
		//如果时间为昨天的
			formatString=" 昨天 HH:mm";
		}else {
			//否则显示至月份
			formatString="M月d日 HH:mm";
		}
		SimpleDateFormat format=new SimpleDateFormat(formatString);
		String formatTime = format.format(date);
		return  formatTime!=null?formatTime:"未知时间";
	}
	
	private static  boolean isYesterday(Date date)	
	{
		if(date==null) return false;
		TimeInfo var2 = getYesterdayStartAndEndTime();
		return  date.getTime()>var2.getStartTime() && date.getTime()<var2.getEndTime();
	}

	private static boolean isSameDay(Date date) {
		if(date==null) return false;
		TimeInfo var2 = getTodayStartAndEndTime();
		return date.getTime() > var2.getStartTime() && date.getTime() < var2.getEndTime();
	}

}