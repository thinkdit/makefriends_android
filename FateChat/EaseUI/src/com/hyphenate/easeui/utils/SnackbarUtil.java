package com.hyphenate.easeui.utils;

import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

/**
 * Created by hx_Alex on 2017/3/13.
 */

public class SnackbarUtil {
    private  static Snackbar snackbar;
    private static  SnackbarUtil snackbarUtil;
    public static  SnackbarUtil  makeSnackBar(View view, String contentText, int duration)
    {
        if(view==null) return null;
        if (snackbarUtil==null)
        {
            snackbarUtil=new SnackbarUtil();
        }

        snackbarUtil.snackbar= Snackbar.make(view,contentText,duration);
        return snackbarUtil;
    }

    /***
     * 设置snackbar的messagetext 颜色
     * @param color
     */
    public static SnackbarUtil setMeessTextColor(int color)
    {
        if(snackbarUtil.snackbar==null) return null;
        View view = snackbarUtil.snackbar.getView();
        ((TextView)view.findViewById(android.support.design.R.id.snackbar_text)).setTextColor(color);
        return snackbarUtil;
    }
    public static  void show()
    {
        if(snackbarUtil.snackbar==null) return;
            
            snackbarUtil.snackbar.show();
    }
   
}
