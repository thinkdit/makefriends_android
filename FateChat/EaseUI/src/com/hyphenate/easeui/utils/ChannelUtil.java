package com.hyphenate.easeui.utils;

import android.content.Context;
import android.content.pm.PackageManager;

/**
 * 渠道工具类
 * Created by admin on 2016/4/20.
 */
public class ChannelUtil {
    public static final String CHANNEL_FIRST_TAG = "_first";//首发标记
    public static final String DEFAULT_CHANNEL = "freeChannel";
    public static boolean isFirstChannel = false;

    private static String channel = null;

    public static String getChannel(Context context) {
        try {
           Object objCannel= context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA).metaData.get("UMENG_CHANNEL");
            if(objCannel!=null)
            {
                if(objCannel instanceof Integer)
                {
                    channel=String.valueOf(objCannel);
                }else if(objCannel instanceof String){
                    channel=(String)objCannel;
                }
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            channel=DEFAULT_CHANNEL;
        }
        if(channel==null ||channel.equals(""))
        {
            channel=DEFAULT_CHANNEL;
        }
        return channel;
    }
}
