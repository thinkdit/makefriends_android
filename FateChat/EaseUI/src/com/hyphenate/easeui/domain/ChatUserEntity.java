package com.hyphenate.easeui.domain;

import java.io.Serializable;

/**
 * Created by leaves on 2017/4/14.
 */

public class ChatUserEntity implements Serializable {
    private int myUserID;
    private String myNickName;
    private String myAvator;
    private int toUserID;
    private String toNickName;
    private String toAvator;
    private String toIdentify;
    private boolean isVip = false;
    private boolean isRealUser = false;

    public boolean isRealUser() {
        return isRealUser;
    }

    public void setRealUser(boolean realUser) {
        isRealUser = realUser;
    }

    public boolean isVip() {
        return isVip;
    }

    public void setVip(boolean vip) {
        isVip = vip;
    }

    public int getMyUserID() {
        return myUserID;
    }

    public void setMyUserID(int myUserID) {
        this.myUserID = myUserID;
    }

    public String getMyNickName() {
        return myNickName;
    }

    public void setMyNickName(String myNickName) {
        this.myNickName = myNickName;
    }

    public String getMyAvator() {
        return myAvator;
    }

    public void setMyAvator(String myAvator) {
        this.myAvator = myAvator;
    }

    public int getToUserID() {
        return toUserID;
    }

    public void setToUserID(int toUserID) {
        this.toUserID = toUserID;
    }

    public String getToNickName() {
        return toNickName;
    }

    public void setToNickName(String toNickName) {
        this.toNickName = toNickName;
    }

    public String getToAvator() {
        return toAvator;
    }

    public void setToAvator(String toAvator) {
        this.toAvator = toAvator;
    }

    public String getToIdentify() {
        return toIdentify;
    }

    public void setToIdentify(String toIdentify) {
        this.toIdentify = toIdentify;
    }
}
