package com.hyphenate.easeui.domain;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Unique;

/**
 * Created by leaves on 2017/4/5.
 */
@Entity
public class LocalSession {
    private int lastMsgType;//消息类型 1:文本;2:图片,3.语音 4:跳转H5
    private String lastMsgContent;//内容
    private long lasttime;//最后发送时间
    private long createtime;//创建时间
    @Id
    @Unique
    private Long userId;//对方id
    private String keFu;//客服标识
    private String avatar;//头像
    private String nickName;
    private int unReadCount;
    
    public void setUserId(int userId) {
        this.userId = Long.valueOf(userId);
    }
    public int getUserIdInt() {
        return this.userId.intValue();
    }


    public int getUnReadCount() {
        return this.unReadCount;
    }

    public void setUnReadCount(int unReadCount) {
        this.unReadCount = unReadCount;
    }

    public String getNickName() {
        return this.nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAvatar() {
        return this.avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getKeFu() {
        return this.keFu;
    }

    public void setKeFu(String keFu) {
        this.keFu = keFu;
    }

    public Long getUserId() {
        return this.userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public long getCreatetime() {
        return this.createtime;
    }

    public void setCreatetime(long createtime) {
        this.createtime = createtime;
    }

    public long getLasttime() {
        return this.lasttime;
    }

    public void setLasttime(long lasttime) {
        this.lasttime = lasttime;
    }

    public String getLastMsgContent() {
        return this.lastMsgContent;
    }

    public void setLastMsgContent(String lastMsgContent) {
        this.lastMsgContent = lastMsgContent;
    }

    public int getLastMsgType() {
        return this.lastMsgType;
    }

    public void setLastMsgType(int lastMsgType) {
        this.lastMsgType = lastMsgType;
    }

  

    
    public LocalSession() {
        
    }
    @Generated(hash = 1408531500)
    public LocalSession(int lastMsgType, String lastMsgContent, long lasttime, long createtime,
            Long userId, String keFu, String avatar, String nickName, int unReadCount) {
        this.lastMsgType = lastMsgType;
        this.lastMsgContent = lastMsgContent;
        this.lasttime = lasttime;
        this.createtime = createtime;
        this.userId = userId;
        this.keFu = keFu;
        this.avatar = avatar;
        this.nickName = nickName;
        this.unReadCount = unReadCount;
    }


}
