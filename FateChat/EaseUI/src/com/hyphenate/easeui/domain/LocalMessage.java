package com.hyphenate.easeui.domain;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Transient;
import org.greenrobot.greendao.annotation.Unique;

/**
 * Created by leaves on 2017/4/5.
 */
@Entity
public class LocalMessage {
    @Transient
    public static final int DIRECTION_SEND = 0;
    @Transient
    public static final int DIRECTION_RECV = 1;

    @Transient
    public static final int LOCAL_MESSAGE_TXT = 1;
    @Transient
    public static final int LOCAL_MESSAGE_IMAGE = 2;
    @Transient
    public static final int LOCAL_MESSAGE_VOICE = 3;

    @Transient
    public static final int LOCAL_MESSAGE_BIGEXP = 4;

    private int type;//消息类型 1:文本;2:图片
    private String content;//内容
    private int status; //-1:发送失败；0发送成功;1:发送中
    private long create;//发送时间
    private int userId;//对方id
    private String keFu;//客服标识
    private String avatar;//头像
    private int direction;//消息方向 1:发送;0:接收

    @Id
    @Unique
    private String msgId;
    public String getMsgId() {
        return this.msgId;
    }
    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }
    public int getDirection() {
        return this.direction;
    }
    public void setDirection(int direction) {
        this.direction = direction;
    }
    public String getAvatar() {
        return this.avatar;
    }
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
    public String getKeFu() {
        return this.keFu;
    }
    public void setKeFu(String keFu) {
        this.keFu = keFu;
    }
    public int getUserId() {
        return this.userId;
    }
    public void setUserId(int userId) {
        this.userId = userId;
    }
    public long getCreate() {
        return this.create;
    }
    public void setCreate(long create) {
        this.create = create;
    }
    public int getStatus() {
        return this.status;
    }
    public void setStatus(int status) {
        this.status = status;
    }
    public String getContent() {
        return this.content;
    }
    public void setContent(String content) {
        this.content = content;
    }
    public int getType() {
        return this.type;
    }
    public void setType(int type) {
        this.type = type;
    }
    @Generated(hash = 1815453811)
    public LocalMessage(int type, String content, int status, long create,
            int userId, String keFu, String avatar, int direction, String msgId) {
        this.type = type;
        this.content = content;
        this.status = status;
        this.create = create;
        this.userId = userId;
        this.keFu = keFu;
        this.avatar = avatar;
        this.direction = direction;
        this.msgId = msgId;
    }
    @Generated(hash = 1157607124)
    public LocalMessage() {
    }


}
