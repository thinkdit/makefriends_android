package com.hyphenate.easeui.domain;

/**
 * Created by leaves on 2017/4/26.
 */

public class LocalVoiceContent {
    private String localPath;
    private String remoteUrl;
    private int length;
    public String getLocalPath() {
        return localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }

    public String getRemoteUrl() {
        return remoteUrl;
    }

    public void setRemoteUrl(String remoteUrl) {
        this.remoteUrl = remoteUrl;
    }
    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }
}
