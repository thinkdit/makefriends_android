package com.hyphenate.easeui.domain;

/**
 * Created by leaves on 2017/4/26.
 */

public class LocalImageContent {
    private String localPath;
    private String remoteUrl;

    public String getLocalPath() {
        return localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }

    public String getRemoteUrl() {
        return remoteUrl;
    }

    public void setRemoteUrl(String remoteUrl) {
        this.remoteUrl = remoteUrl;
    }
}
