package com.hyphenate.easeui.widget.chatrow;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.easeui.EaseConstant;
import com.hyphenate.easeui.R;
import com.hyphenate.easeui.controller.EaseUI;
import com.hyphenate.easeui.domain.EaseEmojicon;

/**
 * big emoji icons
 *
 */
public class EaseChatRowBigExpression extends EaseChatRowText{

    private ImageView imageView;


    private static int[] icons = new int[]{
            R.drawable.s_1,
            R.drawable.s_2,
            R.drawable.s_3,
            R.drawable.s_4,
            R.drawable.s_5,
            R.drawable.s_6,
            R.drawable.s7,
            R.drawable.s8,
            R.drawable.s9  
    };



    public EaseChatRowBigExpression(Context context, EMMessage message, int position, BaseAdapter adapter) {
        super(context, message, position, adapter);
    }
    
    @Override
    protected void onInflateView() {
        inflater.inflate(message.direct() == EMMessage.Direct.RECEIVE ? 
                R.layout.ease_row_received_bigexpression : R.layout.ease_row_sent_bigexpression, this);
    }

    @Override
    protected void onFindViewById() {
        percentageView = (TextView) findViewById(R.id.percentage);
        imageView = (ImageView) findViewById(R.id.image);
    }


    @Override
    public void onSetUpView() {
        String emojiconId = message.getStringAttribute(EaseConstant.MESSAGE_ATTR_EXPRESSION_ID, null);
        EaseEmojicon emojicon = null;
        if(EaseUI.getInstance().getEmojiconInfoProvider() != null){
            emojicon =  EaseUI.getInstance().getEmojiconInfoProvider().getEmojiconInfo(emojiconId);
        }
        if(emojicon != null){

            if(emojicon.getBigIcon() != 0) {
                if (emojicon.getBigIcon() == R.drawable.s_1) {
                    imageView.setBackgroundResource(R.drawable.anim_s1);
                } else if (emojicon.getBigIcon() == R.drawable.s_2) {
                    imageView.setBackgroundResource(R.drawable.anim_s2);

                } else if (emojicon.getBigIcon() == R.drawable.s_3) {
                    imageView.setBackgroundResource(R.drawable.anim_s3);

                } else if (emojicon.getBigIcon() == R.drawable.s_4) {
                    imageView.setBackgroundResource(R.drawable.anim_s4);

                } else if (emojicon.getBigIcon() == R.drawable.s_5) {
                    imageView.setBackgroundResource(R.drawable.anim_s5);
                    
                } else if (emojicon.getBigIcon() == R.drawable.s_6) {
                    imageView.setBackgroundResource(R.drawable.anim_s6);
                    
                } else if (emojicon.getBigIcon() == R.drawable.s7) {
                    imageView.setBackgroundResource(R.drawable.anim_f1);
                    
                } else if (emojicon.getBigIcon() == R.drawable.s8) {
                    imageView.setBackgroundResource(R.drawable.anim_f2);
                    
                } else if (emojicon.getBigIcon() == R.drawable.s9) {
                    imageView.setBackgroundResource(R.drawable.anim_f3);
                }

                AnimationDrawable animationDrawable = (AnimationDrawable) imageView.getBackground();
                animationDrawable.start();

            } else if(emojicon.getBigIconPath() != null){

                Glide.with(activity).load(emojicon.getBigIconPath()).placeholder(R.drawable.ease_default_expression).into(imageView);
            }else{
                imageView.setImageResource(R.drawable.ease_default_expression);
            }
        }

        handleTextMessage();
    }
}
