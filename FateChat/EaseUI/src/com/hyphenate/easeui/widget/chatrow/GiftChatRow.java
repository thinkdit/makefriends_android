package com.hyphenate.easeui.widget.chatrow;

import android.content.Context;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.chat.EMTextMessageBody;
import com.hyphenate.easeui.R;
import com.hyphenate.easeui.ui.EaseChatFragment;

/**
 * 礼物聊天栏目
 *
 */
public class GiftChatRow extends EaseChatRow{
    @Override
    protected void onUpdateView() {

    }

    @Override
    protected void onBubbleClick() {

    }

    private ImageView imageView;
    private TextView sendTextView;
    private TextView reciveTextView;
    private TextView numTextView;
    private TextView danTextView;


    public GiftChatRow(Context context, EMMessage message, int position, BaseAdapter adapter) {
        super(context, message, position, adapter);
    }

    @Override
    protected void onInflateView() {
        inflater.inflate(R.layout.ease_row_gift, this);
    }

    @Override
    protected void onFindViewById() {
        imageView = (ImageView) findViewById(R.id.im_gift);
        sendTextView = (TextView) findViewById(R.id.tv_sendname);
        reciveTextView = (TextView) findViewById(R.id.tv_revicename);
        numTextView = (TextView) findViewById(R.id.tv_num);
        danTextView = (TextView) findViewById(R.id.tv_dan);
    }


    @Override
    public void onSetUpView() {
        try {
            //当前记录
            if (message.getStringAttribute(EaseChatFragment.ATRR_GIFT_URL) != null) {
                Glide.with(activity).load(message.getStringAttribute(EaseChatFragment.ATRR_GIFT_URL)).into(imageView);
            }
            numTextView.setText(message.getIntAttribute(EaseChatFragment.ATRR_GIFT_NUM)+"");
            danTextView.setText(message.getStringAttribute(EaseChatFragment.ATRR_MESSAGE_DAN));
            sendTextView.setText(message.getStringAttribute(EaseChatFragment.ATRR_MESSAGE_SEND)+"送给");
            reciveTextView.setText(message.getStringAttribute(EaseChatFragment.ATRR_MESSAGE_RECIVE));
        } catch (Exception e){//历史记录
            EMTextMessageBody txtBody = (EMTextMessageBody) message.getBody();
            String msgStr = txtBody.getMessage();
            if(msgStr!=null && msgStr.startsWith("*$#")){
                msgStr = msgStr.replace("*$#","");
                //将礼物消息数据解析出来
                String[]atrrStrs = msgStr.split("\\^");
                if(atrrStrs!=null && atrrStrs.length>=6){
                    if (atrrStrs[1] != null) {
                        Glide.with(activity).load(atrrStrs[1]).into(imageView);
                    }
                    numTextView.setText(atrrStrs[2]);
                    danTextView.setText(atrrStrs[3]);
                    sendTextView.setText(atrrStrs[4]+"送给");
                    reciveTextView.setText(atrrStrs[5]);
                }
            }
        }


//        handleTextMessage();
    }
}