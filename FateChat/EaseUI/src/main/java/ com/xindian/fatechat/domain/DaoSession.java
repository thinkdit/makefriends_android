package  com.xindian.fatechat.domain;

import java.util.Map;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.AbstractDaoSession;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.identityscope.IdentityScopeType;
import org.greenrobot.greendao.internal.DaoConfig;

import com.hyphenate.easeui.domain.LocalMessage;
import com.hyphenate.easeui.domain.LocalSession;

import  com.xindian.fatechat.domain.LocalMessageDao;
import  com.xindian.fatechat.domain.LocalSessionDao;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.

/**
 * {@inheritDoc}
 * 
 * @see org.greenrobot.greendao.AbstractDaoSession
 */
public class DaoSession extends AbstractDaoSession {

    private final DaoConfig localMessageDaoConfig;
    private final DaoConfig localSessionDaoConfig;

    private final LocalMessageDao localMessageDao;
    private final LocalSessionDao localSessionDao;

    public DaoSession(Database db, IdentityScopeType type, Map<Class<? extends AbstractDao<?, ?>>, DaoConfig>
            daoConfigMap) {
        super(db);

        localMessageDaoConfig = daoConfigMap.get(LocalMessageDao.class).clone();
        localMessageDaoConfig.initIdentityScope(type);

        localSessionDaoConfig = daoConfigMap.get(LocalSessionDao.class).clone();
        localSessionDaoConfig.initIdentityScope(type);

        localMessageDao = new LocalMessageDao(localMessageDaoConfig, this);
        localSessionDao = new LocalSessionDao(localSessionDaoConfig, this);

        registerDao(LocalMessage.class, localMessageDao);
        registerDao(LocalSession.class, localSessionDao);
    }
    
    public void clear() {
        localMessageDaoConfig.getIdentityScope().clear();
        localSessionDaoConfig.getIdentityScope().clear();
    }

    public LocalMessageDao getLocalMessageDao() {
        return localMessageDao;
    }

    public LocalSessionDao getLocalSessionDao() {
        return localSessionDao;
    }

}
