package com.xindian.fatechat.ui.home.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.xindian.fatechat.R;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.ui.home.model.CommentBean;
import com.xindian.fatechat.ui.user.UserDetailsActivity;
import com.xindian.fatechat.util.GlideRoundTransform;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.switfpass.pay.MainApplication.getContext;


/**
 * Created by qf on 2016/11/8.
 */
public class CommentAdapter extends BaseAdapter {
    private final LayoutInflater inflater;
    List<CommentBean> data;
    Context context;
    public static final String TAG = "testVoice";

    public void addData(List<CommentBean> data) {
        if(this.data == null){
            this.data = data;
        }else{
            this.data.addAll(data);
        }

        notifyDataSetChanged();
    }

    public void setData(List<CommentBean> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public CommentAdapter(List<CommentBean> data, Context context) {
        this.data = data;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        if(data!=null){
            return data.size();
        }else{
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            convertView =inflater.inflate(R.layout.comment_item_layout, parent, false);
            holder=new Holder(convertView);
            convertView.setTag(holder);
        }else{
            holder= (Holder) convertView.getTag();
        }
        CommentBean bean = data.get(position);
        showComments(holder, bean);
        if(position == getCount() -1){
            holder.tvBottomLine.setVisibility(View.GONE);
        }else{
            holder.tvBottomLine.setVisibility(View.VISIBLE);
        }

        return convertView;
    }

    private void showComments(final Holder holder, final CommentBean bean) {
        Glide.with(holder.sdvUserHead.getContext()).load(bean.getHeadUrl())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .transform(new GlideRoundTransform(holder.sdvUserHead.getContext(), (int)context.getResources().getDimension(R.dimen.image_aspect_40dp)))
                .into(holder.sdvUserHead);
        holder.tvUserName.setText(bean.getNickName());
        holder.tvCommentText.setText(bean.getCommentContent());
        holder.tvLevel.setText("Lv"+bean.getUserGrade());
        //头像
        holder.sdvUserHead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sex = UserInfoManager.getManager(getContext()).getSex();
                String sexOther = bean.getSex();

                if(sex!=null && sexOther!=null && !sex.equals(sexOther)){//异性
                    goToUserDetail(context,(int)bean.getUserId());
                }else{
                    Toast.makeText(context,R.string.only_sex_look,Toast.LENGTH_SHORT).show();
                }

            }
        });
    }


    private void goToUserDetail(Context context, int userID) {
        UserDetailsActivity.gotoUserDetails(context, userID, null);
    }

    static class Holder {
        @BindView(R.id.sdvUserHead)
        ImageView sdvUserHead;
        @BindView(R.id.tvUserName)
        TextView tvUserName;
        @BindView(R.id.tvCommentText)
        TextView tvCommentText;
        @BindView(R.id.tvTimeText)
        TextView tvTimeText;
        @BindView(R.id.tvLevel)
        TextView tvLevel;
        @BindView(R.id.tvBottomLine)
        TextView tvBottomLine;

        Holder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
