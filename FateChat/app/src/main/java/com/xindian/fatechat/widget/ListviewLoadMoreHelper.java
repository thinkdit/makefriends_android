package com.xindian.fatechat.widget;

import android.widget.AbsListView;
import android.widget.ListView;

/**
 * Created by zjzhu on 16-7-4.
 * list view 加载更多帮助类实现
 */
public class ListviewLoadMoreHelper {

    private enum Status {
        READY, LOADING, ERROR, END
    }

    private static final int START_LOAD_OFFSET = 2;

    private ListView mListView;
    private ILoadMoreListListener mListener;
    private AbsListView.OnScrollListener mOnScrollListener;
    private LoadMoreFooterView mMoreFooterView;
    private boolean mHasMore;
    private Status mStatus;

    public ListviewLoadMoreHelper(ListView listView, ILoadMoreListListener listener) {
        this(listView, listener, null);
    }

    public ListviewLoadMoreHelper(ListView listView, ILoadMoreListListener listener, AbsListView
            .OnScrollListener scrollListener) {
        mListView = listView;
        mListener = listener;
        mOnScrollListener = scrollListener;
        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (mOnScrollListener != null) {
                    mOnScrollListener.onScrollStateChanged(view, scrollState);
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount,
                                 int totalItemCount) {
                if (mOnScrollListener != null) {
                    mOnScrollListener.onScroll(view, firstVisibleItem, visibleItemCount,
                        totalItemCount);
            }
                checkScroll(firstVisibleItem, visibleItemCount, totalItemCount);
            }
        });
    }

    private void checkScroll(int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (mListView.getAdapter() != null && mListView.getAdapter().getCount() > 1 &&
                firstVisibleItem >= 0 && totalItemCount > START_LOAD_OFFSET && firstVisibleItem +
                visibleItemCount >= (totalItemCount - START_LOAD_OFFSET)) {
            if (mStatus == Status.READY && mHasMore) {
                mStatus = Status.LOADING;
                addFooterView();
                mMoreFooterView.showLoading();
                mListener.onLoadMore();
            }
        }
    }

    private void addFooterView() {
    
        if (mMoreFooterView == null) {
            mMoreFooterView = LoadMoreFooterView.getLoadMoreFooterView(mListView.getContext());
            mMoreFooterView.setOnRetryLoadListener(() -> {
                mMoreFooterView.showLoading();
                if (mListener != null) {
                    mListener.onLoadMore();
                }
            });
        }
        if (mListView.getFooterViewsCount() == 0) {
            mListView.addFooterView(mMoreFooterView);
        }
      
    }

    private void removeFooterView() {
       
        if (mMoreFooterView != null && mListView.getFooterViewsCount() > 0) {
            mListView.removeFooterView(mMoreFooterView);
        }
       
    }
   
    public void onLoadCompleted(boolean hasMore) {
        mHasMore = hasMore;
        if (mHasMore) {
            removeFooterView();
            mStatus = Status.READY;
        } else {
            mStatus = Status.END;
            if (mListView.getAdapter() != null && mListView.getAdapter().getCount() > 3) {
                addFooterView();
                mMoreFooterView.showEnd();
            } else {
                removeFooterView();
            }
        }
    }

    public void onLoadError() {
        if (mMoreFooterView != null) {
            mStatus = Status.ERROR;
            mMoreFooterView.showError();
        }
    }

    public interface ILoadMoreListListener {
        public void onLoadMore();
    }

    public LoadMoreFooterView getmMoreFooterView() {
        return mMoreFooterView;
    }

}
