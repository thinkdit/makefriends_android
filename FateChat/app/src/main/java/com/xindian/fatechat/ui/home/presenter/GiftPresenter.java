package com.xindian.fatechat.ui.home.presenter;

import android.content.Context;
import android.os.RemoteException;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.parser.Feature;
import com.xindian.fatechat.R;
import com.xindian.fatechat.common.BaseModel;
import com.xindian.fatechat.common.BasePresenter;
import com.xindian.fatechat.common.FateChatApplication;
import com.xindian.fatechat.common.ResultModel;
import com.xindian.fatechat.ui.home.model.ActivityModel;
import com.xindian.fatechat.ui.home.model.GiftListModel;
import com.xindian.fatechat.ui.home.model.PictureModel;
import com.xindian.fatechat.widget.MyProgressDialog;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 礼物
 */
public class GiftPresenter extends BasePresenter {
    private final String URL_GIFT = "/gift/list";
    private final String URL_GIFT_SEND = "/gift/gift";//送礼
    private MyProgressDialog mDialog;


    public GiftPresenter(Context mContext){
        mDialog = new MyProgressDialog(mContext);
    }

    //TODO 加载框
    public void getGiftList() {
        HashMap<String, Object> data = new HashMap<>();
        get(getUrl(URL_GIFT), data, null);
//        mDialog.show();
    }

    public void sendGiftList(long giftId,int giftNum,String giverId,String receiverId) {
        HashMap<String, Object> data = new HashMap<>();
        data.put("giftId",giftId);
        data.put("giftNum",giftNum);
        data.put("giverUserId",giverId);
        data.put("receiverUserId",receiverId);
        post(getUrl(URL_GIFT_SEND), data, null);
//        mDialog.show();
    }


    @Override
    public Object asyncExecute(String url, ResultModel resultModel) {
        if (url.contains(URL_GIFT)) {
            return JSON.parseArray(resultModel.getData(), GiftListModel.class);
        }else if(url.contains(URL_GIFT_SEND)){
            return JSON.parseObject(resultModel.getData(), Boolean.class);
        }
        return null;
    }

    @Override
    public void onError(String url, Exception e) {
        super.onError(url, e);
    }


    @Override
    public void onFailure(String url, ResultModel resultModel) {
        super.onFailure(url,resultModel);
//        mDialog.dismiss();
        if (url.contains(URL_GIFT)) {

        }else if(url.contains(URL_GIFT_SEND)){
            Toast.makeText(FateChatApplication.getInstance(),resultModel.getMessage(),Toast.LENGTH_SHORT).show();
            if(mOnGiftLisener!=null){
                mOnGiftLisener.onSendCiftErr(resultModel.getCode());
            }
        }
    }

    @Override
    public void onSucceed(String url, ResultModel resultModel) throws JSONException,
            RemoteException {
//        mDialog.dismiss();
        if (url.contains(URL_GIFT)) {
            if(mOnGiftLisener!=null){
                mOnGiftLisener.onGetCiftListSuccess((ArrayList<GiftListModel>)resultModel.getDataModel());
            }
        }else if(url.contains(URL_GIFT_SEND)){
            if(mOnGiftLisener!=null){
                mOnGiftLisener.onSendCiftSuccess();
            }
        }
    }

    private OnGiftLisener mOnGiftLisener;

    public OnGiftLisener getmOnGiftLisener() {
        return mOnGiftLisener;
    }

    public void setmOnGiftLisener(OnGiftLisener mOnGiftLisener) {
        this.mOnGiftLisener = mOnGiftLisener;
    }

    public interface OnGiftLisener {
        void onGetCiftListSuccess(List<GiftListModel> list);
        void onSendCiftSuccess();
        void onSendCiftErr(String errCode);
    }

}
