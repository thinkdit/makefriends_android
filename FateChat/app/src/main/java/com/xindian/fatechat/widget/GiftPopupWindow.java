package com.xindian.fatechat.widget;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.view.PagerAdapter;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.hyphenate.chat.EMMessage;
import com.hyphenate.easeui.domain.ChatUserEntity;
import com.hyphenate.easeui.ui.EaseChatFragment;
import com.xindian.fatechat.R;
import com.xindian.fatechat.databinding.PupoGiftBinding;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.ui.home.model.GiftListModel;
import com.xindian.fatechat.ui.home.presenter.GiftPresenter;
import com.xindian.fatechat.ui.user.DiamodnsCashActivity;
import com.xindian.fatechat.ui.user.model.BalanceBean;
import com.xindian.fatechat.ui.user.presenter.BlancePresenter;
import com.xindian.fatechat.util.DisplayUtil;
import com.xindian.fatechat.widget.adapter.GiftPortListAdapter;

import java.util.ArrayList;
import java.util.List;

import static com.hyphenate.easeui.ui.EaseChatFragment.ATRR_GIFT_NUM;
import static com.hyphenate.easeui.ui.EaseChatFragment.ATRR_GIFT_URL;
import static com.hyphenate.easeui.ui.EaseChatFragment.ATRR_MESSAGE_DAN;
import static com.hyphenate.easeui.ui.EaseChatFragment.ATRR_MESSAGE_RECIVE;
import static com.hyphenate.easeui.ui.EaseChatFragment.ATRR_MESSAGE_SEND;
import static com.hyphenate.easeui.ui.EaseChatFragment.ATRR_MESSAGE_TYPE;
import static com.hyphenate.easeui.ui.EaseChatFragment.MESSAGE_TYPE_GIFT;


/**
 * 送礼弹窗
 */

public class GiftPopupWindow extends PopupWindow{
    private Activity mContext;


    private PupoGiftBinding mPupoGiftBinding;
    private GiftAdapter mGiftAdapter;
    EaseChatFragment easeChatFragment;

    private int sendCount = 0;

    public EaseChatFragment getEaseChatFragment() {
        return easeChatFragment;
    }

    public void setEaseChatFragment(EaseChatFragment easeChatFragment) {
        this.easeChatFragment = easeChatFragment;
    }

    public void setmChatUserEntity(ChatUserEntity mChatUserEntity) {
        this.mChatUserEntity = mChatUserEntity;
    }

    private ChatUserEntity mChatUserEntity;
    public GiftPopupWindow(Activity context, ChatUserEntity chatUserEntity){
        this.mContext = context;
        this.mChatUserEntity = chatUserEntity;
        this.mPupoGiftBinding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.pupo_gift, null, false);

        init();
        getList();
        getBalance();
    }

    private void init(){
        setAnimationStyle(R.style.popup_show_Animation);
        // 设置弹出窗体的宽和高
        setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        // 设置弹出窗体可点击
        setFocusable(true);
        setBackgroundDrawable(new ColorDrawable(0x00000000));
        //解决华为手机window被navigationBar挡住
        setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        setContentView(mPupoGiftBinding.getRoot());
//        setOnDismissListener(new OnDismissListener() {
//            @Override
//            public void onDismiss() {
//                WindowManager.LayoutParams lp= mContext.getWindow().getAttributes();
//                lp.alpha = 1.0f;
//            }
//        });

        mPupoGiftBinding.emptyPager.setAdapter(mGiftAdapter = new GiftAdapter());

        mPupoGiftBinding.buSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mGiftListModel == null){
                    Toast.makeText(mContext,R.string.send_select_gift_frist,Toast.LENGTH_SHORT).show();
                    return;
                }

                int uId = UserInfoManager.getManager(mContext).getUserId();
                sendGift(mGiftListModel.getId(),sendCount,uId+"",mChatUserEntity.getToUserID()+"");

            }
        });
        mPupoGiftBinding.buAddMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int balance=0;
                Intent i = new Intent(mContext, DiamodnsCashActivity.class);
                i.putExtra(DiamodnsCashActivity.DIAMODNSBALANCE,balance);
                mContext.startActivity(i);
            }
        });

        mPupoGiftBinding.numText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPupoWindows();
            }
        });

    }

    @Override
    public void showAtLocation(View parent, int gravity, int x, int y) {
        super.showAtLocation(parent, gravity, x, y);
//        WindowManager.LayoutParams lp= mContext.getWindow().getAttributes();
//        lp.alpha = 0.5f;
    }

    GiftNumPopupWindow mGiftNumPopupWindow;
    private void showPupoWindows(){
        if(mGiftNumPopupWindow == null){
            mGiftNumPopupWindow = new GiftNumPopupWindow(mContext);
            mGiftNumPopupWindow.setOnGiftNumSelecedListener(new GiftNumPopupWindow.OnGiftNumSelecedListener() {
                @Override
                public void onNumSeleced(int num) {
                    sendCount = num;
                    mPupoGiftBinding.numText.setText(sendCount+"");
                    if(mGiftListModel!=null){
                        int needMoney = (int)(mGiftListModel.getPrice()*sendCount);
                        mPupoGiftBinding.conNeedMoney.setText(needMoney+"");
                    }
                }
            });
        }

        mGiftNumPopupWindow.showAtLocation(mContext.getWindow().getDecorView(), Gravity.BOTTOM, DisplayUtil.dip2px(mContext,50),0);
    }

    private void getPre(){
        if(mGiftPresenter == null){
            mGiftPresenter = new GiftPresenter(mContext);
            mGiftPresenter.setmOnGiftLisener(new GiftPresenter.OnGiftLisener() {
                @Override
                public void onGetCiftListSuccess(List<GiftListModel> list) {
                    setData(list);
                }

                @Override
                public void onSendCiftSuccess() {
                    //发送礼物
                    sendGiftIm(mChatUserEntity);
                    //刷新余额
                    getBalance();
                    Toast.makeText(mContext,"礼物发送成功!",Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSendCiftErr(String errCode) {
                    if(errCode != null && errCode.equals("GF0001")){//余额不足
                        int balance=0;
                        Intent i = new Intent(mContext, DiamodnsCashActivity.class);
                        i.putExtra(DiamodnsCashActivity.DIAMODNSBALANCE,balance);
                        mContext.startActivity(i);
                    }
                }
            });
        }
    }

    private void getList(){
        getPre();
        mGiftPresenter.getGiftList();
    }

    private void sendGift(long giftId,int giftNum,String giverId,String receiverId){
        getPre();
        mGiftPresenter.sendGiftList(giftId,giftNum,giverId,receiverId);
    }

    int mBalanceNum = 0;
    BlancePresenter mBlancePresenter;
    private void getBalance(){
        if(mBlancePresenter == null){
            mBlancePresenter = new BlancePresenter(new BlancePresenter.IGetBalance() {
                @Override
                public void onGetBalanceSuccess(BalanceBean model) {
                    if(model!=null){
                        mBalanceNum = model.getDiamonds();
                        mPupoGiftBinding.myMoney.setText(mBalanceNum+"");
                    }
                }
            });
        }
        mBlancePresenter.getBalance();
    }

    private void sendGiftIm(ChatUserEntity chatUserEntity){
        if(chatUserEntity == null || easeChatFragment == null || mGiftListModel == null){
            return;
        }
        //将礼物消息数据封装起来
        String giftContent = "*$#"+MESSAGE_TYPE_GIFT+"^"+mGiftListModel.getUrl()+"^"+sendCount+"^"+mGiftListModel.getUnit()+"^"+chatUserEntity.getMyNickName()+"^"+chatUserEntity.getToNickName();
        EMMessage message = EMMessage.createTxtSendMessage(giftContent, mChatUserEntity.getToIdentify());
        // 增加自己特定的属性
        message.setAttribute(ATRR_MESSAGE_TYPE,MESSAGE_TYPE_GIFT);
        message.setAttribute(ATRR_GIFT_URL,mGiftListModel.getUrl() );
        message.setAttribute(ATRR_GIFT_NUM, sendCount);
        message.setAttribute(ATRR_MESSAGE_DAN, mGiftListModel.getUnit());
        message.setAttribute(ATRR_MESSAGE_SEND, chatUserEntity.getMyNickName());
        message.setAttribute(ATRR_MESSAGE_RECIVE, chatUserEntity.getToNickName());

        if(easeChatFragment!=null){
            easeChatFragment.sendMessage(message,false);
        }
    }

    private static final int CHILD_COUNT = 8;
    private int mPageCount = 0;
    private GiftPresenter mGiftPresenter;
    private List<List<GiftListModel>> mDataList = new ArrayList<>();
    public void setData(List<GiftListModel> gifts) {
        int page = mPupoGiftBinding.emptyPager.getCurrentItem();
        mDataList.clear();
        mPageCount = 0;
        if (gifts != null && gifts.size() > 0) {
            mPageCount = gifts.size() / CHILD_COUNT;
            if (gifts.size() % CHILD_COUNT > 0) {
                mPageCount += 1;
            }
            for (int i = 0; i < mPageCount; i++) {
                int start = CHILD_COUNT * i;
                int end = start + CHILD_COUNT;
                if (end > gifts.size()) {
                    end = gifts.size();
                }
                mDataList.add(gifts.subList(start, end));
            }
        }
        mPupoGiftBinding.emptyPager.setAdapter(null);
        mPupoGiftBinding.emptyPager.removeAllViews();
        mPupoGiftBinding.emptyPager.setAdapter(mGiftAdapter);
        mGiftAdapter.notifyDataSetChanged();
        if (mPageCount == 1) {
            mPupoGiftBinding.smartTabIcon.setVisibility(View.GONE);
        } else {
            mPupoGiftBinding.smartTabIcon.setVisibility(View.VISIBLE);
            mPupoGiftBinding.smartTabIcon.setViewPager(mPupoGiftBinding.emptyPager);
        }
        if (page >= 0 && page < mPageCount) {
            mPupoGiftBinding.emptyPager.setCurrentItem(page);
        }
    }

    private List<GiftListModel> getGifts(int page) {
        return mDataList.get(page);
    }

    GiftPortListAdapter mGiftListAdapter;
    GiftListModel mGiftListModel;
    private class GiftAdapter extends PagerAdapter implements AbsListView.OnItemClickListener {

        @Override
        public int getCount() {
            return mPageCount;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            GridView gridView = new GridView(mContext);
            gridView.setNumColumns(4);
            gridView.setSelector(new ColorDrawable(Color.TRANSPARENT));
            gridView.setOnItemClickListener(this);
            GiftPortListAdapter adapter = new GiftPortListAdapter(mContext);
            gridView.setAdapter(adapter);
            adapter.setData(getGifts(position));
            container.addView(gridView);
            return gridView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            mGiftListAdapter = (GiftPortListAdapter) parent.getAdapter();
            mGiftListAdapter.setmSelectedPosition(position);
            mGiftListModel = mGiftListAdapter.getItem(position);
            if(sendCount==0){
                sendCount = 1;
            }
            mPupoGiftBinding.numText.setText(sendCount+"");
            int needMoney = (int)(mGiftListModel.getPrice()*sendCount);
            mPupoGiftBinding.conNeedMoney.setText(needMoney+"");
        }
    }
}
