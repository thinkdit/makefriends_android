package com.xindian.fatechat.ui.user.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.widget.ImageView;
import android.widget.TextView;

import com.xindian.fatechat.R;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.ui.home.fragment.MainActivity;
import com.xindian.fatechat.ui.login.SplashActivity;
import com.xindian.fatechat.ui.user.SetActivity;
import com.xindian.fatechat.ui.user.model.User;
import com.xindian.fatechat.util.ActivityStack;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by hx_Alex on 2017/7/20.
 */

public class PunishDialog extends AlertDialog {
    public  static  final String  ACTION="PunishDialog";
    public static final String NOTICE="notice";
    public static final String CLOSE_ACCOUNT="close_account";
    private static final int DEFALUT_TIME_COUNT=10;
    private TextView txt_message,txt_content_hint,txt_time_hint,txt_open,tv_normal_dialog_msg;
    private ImageView noticeImage;
    private User user;
    private Context context;
    private String msg;
    private String punishType;
    private String openTime="0";
    private int timeCount;
    private CloseDialogHandler handler;
    public PunishDialog(Context context) {
        super(context, R.style.FullScreen_dialog);
        setCanceledOnTouchOutside(false);
        setCancelable(false);
        this.context=context;
        EventBus.getDefault().register(this);
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
    public void setPunishType(String punishType) {
        this.punishType = punishType;
    }
    public void setOpenTime(String openTime) {
        this.openTime = openTime;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_punish);
        txt_content_hint= (TextView) findViewById(R.id.txt_content_hint);
        txt_message=(TextView)findViewById(R.id.tv_normal_dialog_text);
        txt_time_hint= (TextView) findViewById(R.id.tv_normal_dialog_bu);
        txt_open= (TextView) findViewById(R.id.txt_open);
        tv_normal_dialog_msg= (TextView) findViewById(R.id.tv_normal_dialog_msg);
        txt_content_hint.setText(punishType.equals(NOTICE)?context.getResources().getString(R.string.notice):punishType.equals(CLOSE_ACCOUNT)?
                context.getResources().getString(R.string.close_account):"处理结果");
        if(msg!=null)
        {
            tv_normal_dialog_msg.setText(context.getResources().getString(R.string.punish_msg,msg));
        }
        timeCount=DEFALUT_TIME_COUNT;
        timeCloseDialog();

        handleCloseAccount();
    }


    @NonNull
    @Override
    public Bundle onSaveInstanceState() {
        return null;
    }

    public void handleCloseAccount()
    {
        if(punishType.equals(CLOSE_ACCOUNT))
        {
                int tOpen = Integer.parseInt(openTime);
                long cm= System.currentTimeMillis()+ 86400000*tOpen;
               Date openDate=new Date(cm);
               SimpleDateFormat df=new SimpleDateFormat("yy年MM月dd");
              String format = df.format(openDate);
            if(tOpen<300)
            {
                txt_open.setText(context.getResources().getString(R.string.punish_open,format));
            }else 
            {
                txt_open.setText("永久封停");
            }
         
        }
    }
    
    
    
    public void timeCloseDialog()
    {
        handler=new CloseDialogHandler();
        handler.sendEmptyMessageDelayed(0,1000);
        txt_time_hint.setText(context.getResources().getString(R.string.close_dialog,timeCount));
    }


    @Override
    public void show() {
        Activity topActivity = ActivityStack.getInstance().getTopActivity();
        //
        if(topActivity instanceof SplashActivity) {
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    show();
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            };
            runnable.run();
        }
        super.show();
    }

    class CloseDialogHandler extends Handler
    {
        @Override
        public void handleMessage(Message msg) {
            if(timeCount==0)
            {
                if(punishType.equals(CLOSE_ACCOUNT))
                {
                    //退出账号
                    UserInfoManager.getManager(context).loginOut(context);
                }
                dismiss();
               
            }else {
                timeCount = --timeCount;
                txt_time_hint.setText(context.getResources().getString(R.string.close_dialog,timeCount));
                sendEmptyMessageDelayed(0, 1000);
            }
        }
    }





    @Override
    public void dismiss() {
        super.dismiss();
        EventBus.getDefault().unregister(this);
    }

    /***
     * 退出回调
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void loginOutCallBack(String msg){
        if(msg.equals(UserInfoManager.IMLOGINOUTSUCCESS))
        {
            Intent i=new Intent(context, SplashActivity.class);
            i.putExtra(SetActivity.TAG,SplashActivity.BYLOGINOUT);
             context.startActivity(i);
            EventBus.getDefault().post(MainActivity.ONLOGINOUTBYSET);//发送消息通知首页用户已经退出登录，finsh掉自己
            dismiss();
        }
    }
}
