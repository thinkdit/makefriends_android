package com.xindian.fatechat.ui.user.model;

import com.xindian.fatechat.common.BaseModel;

/**
 * Created by hx_Alex on 2017/7/14.
 */

public class GiftRecordBean extends BaseModel {

    /**
     * id : 1
     * giverUserId : 222
     * receiverUserId : 177
     * giftId : 1
     * giftName : 么么哒
     * giftUrl : http://img.dingsns.com/pub/20161015082221987_5763.png
     * giverUserName : 哈哈哈
     * receiverUserName : 路
     * giftPrice : 1
     * giftNum : 2
     * totalPrice : 2
     * createTime : 1499842543000
     */

    private int id;
    private int giverUserId;
    private int receiverUserId;
    private int giftId;
    private String giftName;
    private String giftUrl;
    private String giverUserName;
    private String receiverUserName;
    private int giftPrice;
    private int giftNum;
    private int totalPrice;
    private long createTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGiverUserId() {
        return giverUserId;
    }

    public void setGiverUserId(int giverUserId) {
        this.giverUserId = giverUserId;
    }

    public int getReceiverUserId() {
        return receiverUserId;
    }

    public void setReceiverUserId(int receiverUserId) {
        this.receiverUserId = receiverUserId;
    }

    public int getGiftId() {
        return giftId;
    }

    public void setGiftId(int giftId) {
        this.giftId = giftId;
    }

    public String getGiftName() {
        return giftName;
    }

    public void setGiftName(String giftName) {
        this.giftName = giftName;
    }

    public String getGiftUrl() {
        return giftUrl;
    }

    public void setGiftUrl(String giftUrl) {
        this.giftUrl = giftUrl;
    }

    public String getGiverUserName() {
        return giverUserName;
    }

    public void setGiverUserName(String giverUserName) {
        this.giverUserName = giverUserName;
    }

    public String getReceiverUserName() {
        return receiverUserName;
    }

    public void setReceiverUserName(String receiverUserName) {
        this.receiverUserName = receiverUserName;
    }

    public int getGiftPrice() {
        return giftPrice;
    }

    public void setGiftPrice(int giftPrice) {
        this.giftPrice = giftPrice;
    }

    public int getGiftNum() {
        return giftNum;
    }

    public void setGiftNum(int giftNum) {
        this.giftNum = giftNum;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }
}
