package com.xindian.fatechat.ui.base.presenter;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.xindian.fatechat.R;

/**
 * 标题处理p类
 */
public class TitlePresenter {
    private View mView;
    private ImageView mIvBack;
    private TextView mTvTitle;
    private Button mBtnRight;
    private ImageButton mIbtnRight;
    private Button mSecondaryBtn;

    public TitlePresenter(View view, boolean disPlayHomeAsUp, View.OnClickListener leftClickListener) {
        mView = view;
        mIvBack = (ImageView) view.findViewById(R.id.iv_title_bar_back);
        mTvTitle = (TextView) view.findViewById(R.id.tv_toolbar_title);
        mBtnRight = (Button) view.findViewById(R.id.btn_toolbar_right);
        mIbtnRight = (ImageButton) view.findViewById(R.id.ibtn_toolbar_right);
        mIvBack.setVisibility(disPlayHomeAsUp ? View.VISIBLE : View.INVISIBLE);
        if (disPlayHomeAsUp) {
            mIvBack.setOnClickListener(leftClickListener);
        }
    }


    public void initToolBarRightMenu(String text, View.OnClickListener listener) {
        mBtnRight.setText(text);
        mBtnRight.setOnClickListener(listener);
        mBtnRight.setVisibility(View.VISIBLE);
    }

    public void initToolBarSecondaryRightMenu(String text, View.OnClickListener listener) {
        if (mSecondaryBtn != null) {
            mSecondaryBtn.setText(text);
            if (listener != null) {
                mSecondaryBtn.setOnClickListener(listener);
            }
            mSecondaryBtn.setVisibility(View.VISIBLE);
        }
    }

    public void initToolBarRightMenu(Drawable drawable, View.OnClickListener listener) {
        mIbtnRight.setImageDrawable(drawable);
        mIbtnRight.setOnClickListener(listener);
        mIbtnRight.setVisibility(View.VISIBLE);
    }

    public void setToolBarRightMenuVisibility(boolean visible) {
        if (mBtnRight != null) {
            mBtnRight.setVisibility(visible ? View.VISIBLE : View.GONE);
        }
    }

}
