package com.xindian.fatechat.common;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;

/**
 * 全局常量
 */

public class Constant {
    public static final String KEY_UEMNG = "59239b3c766613193b0000e2";
    public static final String IMAGE_100 = "?x-oss-process=image/resize,m_fixed,h_100,w_100";
    public static final String IMAGE_200 = "?x-oss-process=image/resize,m_fixed,h_200,w_200";

    /***
     * 检查本机是否装有该包名应用
     * @param context
     * @param packageName
     * @return
     */
    public static boolean checkApkExist(Context context, String packageName) {
        if (packageName == null || "".equals(packageName))
            return false;
        try {
            ApplicationInfo info = context.getPackageManager().getApplicationInfo(packageName,
                    PackageManager.GET_UNINSTALLED_PACKAGES);
            
            return info!=null?true:false;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }
}
