package com.xindian.fatechat.common;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.thinkdit.lib.util.StringUtil;
import com.xindian.fatechat.R;
import com.xindian.fatechat.pay.model.ProductBean;
import com.xindian.fatechat.ui.user.model.User;
import com.xindian.fatechat.util.GlideRoundTransform;
import com.xindian.fatechat.util.GlideRoundTransformv2;
import com.xindian.fatechat.util.GlideRoundTransformv3;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import jp.wasabeef.glide.transformations.BlurTransformation;

/**
 * Created by qiuda on 16/6/13.
 */
public class MVVPSetters {

    @BindingAdapter({"image", "defaultImage"})
    public static void imageLoader(ImageView view, String url, Drawable defaultImage) {
        if(getActivityFromView(view)!=null &&  getActivityFromView(view) instanceof Activity && !assertNotDestroyed(getActivityFromView(view)))
        {
            loadImage(Glide.with(view.getContext()),url,view,defaultImage);
        }
    }

    @BindingAdapter({"image", "aspect"})
    public static void imageLoader(ImageView view, String url, float aspect) {
        Glide.with(view.getContext()).load(url).diskCacheStrategy(DiskCacheStrategy.ALL)
                .transform(new GlideRoundTransform(view.getContext(), (int) aspect)).centerCrop()
                .into(view);
    }

    //高斯模糊
    @BindingAdapter({"image", "gs"})
    public static void imageLoaderGS(ImageView view, String url, float gs) {
        if(gs==0)
        {

            Glide.with(view.getContext()).load(url).diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop().into(view);
            return;
        }

        Glide.with(view.getContext()).load(url).diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                // 设置高斯模糊
                .bitmapTransform(new BlurTransformation(view.getContext(), 14, (int) gs))
                .into(view);
    }

    @BindingAdapter({"image", "aspect", "dontAnimate"})
    public static void imageLoader(ImageView view, String url, float aspect, boolean dontAnimate) {
        if (dontAnimate) {
            Glide.with(view.getContext()).load(url).diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontAnimate().centerCrop().transform(new GlideRoundTransform(view.getContext
                    (), (int) aspect)).into(view);
        } else {
            Glide.with(view.getContext()).load(url).centerCrop().diskCacheStrategy
                    (DiskCacheStrategy.ALL).transform(new GlideRoundTransform(view.getContext(),
                    (int) aspect)).into(view);
        }
    }

    @BindingAdapter({"image", "errorImage", "aspect", "dontAnimate"})
    public static void imageLoader(ImageView view, String url, Drawable errorImage, float aspect,
                                   boolean dontAnimate) {
        if (dontAnimate) {
            Glide.with(view.getContext()).load(url).diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontAnimate().centerCrop().transform(new GlideRoundTransform(view.getContext
                    (), (int) aspect)).error(errorImage).into(view);
        } else {
            Glide.with(view.getContext()).load(url).diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop().transform(new GlideRoundTransform(view.getContext(), (int)
                    aspect)).error(errorImage).into(view);
        }
    }

    @BindingAdapter({"image", "defaultImage", "aspect"})
    public static void imageLoader(ImageView view, String url, Drawable defaultImage, float
            aspect) {
        try {
            if(getActivityFromView(view)!=null &&  getActivityFromView(view) instanceof Activity && !assertNotDestroyed(getActivityFromView(view)))
            {
                loadImage(Glide.with(view.getContext()),view,url,defaultImage,aspect);
            }

        } catch (IllegalArgumentException e) {

        }
    }

    @BindingAdapter({"image", "radius"})
    public static void roundImageLoader(ImageView view, String url, int radius) {
        Glide.with(view.getContext()).load(url).diskCacheStrategy(DiskCacheStrategy.ALL)
                .transform(new GlideRoundTransformv2(view.getContext(), radius)).into(view);
    }

    @BindingAdapter({"image", "radius", "defaultImage"})
    public static void roundImageLoader(ImageView view, String url, int radius, Drawable
            defaultImage) {
        Glide.with(view.getContext()).load(url).diskCacheStrategy(DiskCacheStrategy.ALL).error
                (defaultImage).transform(new GlideRoundTransformv2(view.getContext(), radius))
                .into(view);
    }

    @BindingAdapter({"image", "radius", "direction", "defaultImage"})
    public static void topOrBottomRoundImageLoader(ImageView view, String url, int radius, int
            direction, Drawable defaultImage) {
        Glide.with(view.getContext()).load(url).diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(defaultImage).
                transform(new GlideRoundTransformv3(view.getContext(), radius, direction)).into
                (view);
    }

    @BindingAdapter({"image"})
    public static void imageLoader(ImageView view, String url) {
        Glide.with(view.getContext()).load(url).diskCacheStrategy(DiskCacheStrategy.ALL).into(view);
    }

    @BindingAdapter({"age"})
    public static void setAge(TextView view, int age) {
        view.setText(age + "岁");
    }

    @BindingAdapter({"price"})
    public static void setPrice(TextView view, double price) {
        view.setText(price + "钻石");
    }

    static String male = "1";
    static String female = "2";

    @BindingAdapter({"item_re_sex_bg"})
    public static void setSexIcon(LinearLayout view, String sex) {
        if (sex.equalsIgnoreCase(female)) {
            view.setBackgroundResource(R.drawable.shape_txt_half_circle_pink_gril);
        } else {
            view.setBackgroundResource(R.drawable.shape_txt_half_circle_pink_boy);
        }

    }

    @BindingAdapter({"item_re_sex_bg"})
    public static void setSexIcon(TextView view, String sex) {
        if (sex.equalsIgnoreCase(female)) {
            view.setBackgroundResource(R.drawable.shape_txt_half_circle_pink_gril);
        } else {
            view.setBackgroundResource(R.drawable.shape_txt_half_circle_pink_boy);
        }

    }
    @BindingAdapter({"sex"})
    public static void setSex(TextView view, String sex) {
        if(sex==null) return;
        view.setText(sex.equals("1")?view.getContext().getString(R.string.male):view.getContext().getString(R.string.female));
    }
    @BindingAdapter({"photo"})
    public static void setphoto(TextView view, int num) {

        view.setText(num+"张照片");
    }

    @BindingAdapter({"userPhotoNum"})
    public static void setuserPhotoNum(TextView view, int num) {

        view.setText("相册"+num);
    }

    @BindingAdapter({"attentionCount"})
    public static void setAttentionCount(TextView view, String num) {
        if(num==null)
        {
            view.setText("关注0");
        }
        else
        {
            view.setText("关注"+num);
        }

    }
    @BindingAdapter({"lastLoginTime"})
    public static void setLastLoginTime(TextView view, long time) {
        if(view.getText().toString().contains(":") ) return;
        if(time==0) {
            //随机在当天生成一个时间显示
            Random random=new Random();
            long l = random.nextInt(7200000);
            time=  System.currentTimeMillis() - l;
        };
        SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date loginDate=new Date(time);
        String formatDate = format.format(loginDate);
        view.setText(formatDate);
    }

    @BindingAdapter({"dateFormat"})
    public static void setdateFormat(TextView view, long time) {
        if(time==0 ) return;
        SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd");
        Date loginDate=new Date(time);
        String formatDate = format.format(loginDate);
        view.setText(formatDate);
    }

    @BindingAdapter({"isVip","info"})
    public static void setContactInformation(TextView view,boolean isVip,String info) {
        if( isVip)
        {
            view.setText(info);
        }
        else
        {
            view.setText("仅对钻石VIP开放");
        }
    }
    @BindingAdapter({"vip","user"})
    public static void setIsVip(TextView view, String userLevel, User user) {
        if (user == null || user.getUserLevel()==null) return;
        if (userLevel.equals("2")) {
            SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd");
            String endTime=null;
            if(user.getUserAuth()!=null && user.getUserAuth().getEndTime()!=0) {
                Date date = new Date(user.getUserAuth().getEndTime());
                endTime=format.format(date);
            }
            view.setText(endTime!=null? endTime+ "到期":"");
        } else {
            view.setText(view.getContext().getString(R.string.un_vip_hint));
        }
    }


    @BindingAdapter({"heightStart", "heightEnd"})
    public static void setCaseFriendHeight(TextView view, String heightStart, String heightEnd) {
        if (heightStart != null && heightEnd != null) {
            view.setText(heightStart + "cm-" + heightEnd+"cm");
        } else if (heightStart != null && heightEnd == null) {
            view.setText(heightStart + "cm以上");
        } else if (heightStart == null && heightEnd == null) {
            view.setText("不限");
        }
    }

    @BindingAdapter({"ageStart", "ageEnd"})
    public static void setCaseFriendAge(TextView view, int ageStart, int ageEnd) {
        if (ageStart != 0 && ageEnd != 0) {
            view.setText(ageStart + "岁-" + ageEnd + "岁");
        } else if (ageStart != 0 && ageEnd == 0) {
            view.setText(ageStart + "岁以上");
        } else if (ageStart == 0 && ageEnd == 0) {
            view.setText("不限");
        }
    }

    @BindingAdapter({"height"})
    public static void setUserHeight(TextView view, String height) {
        if(height==null) return;
        view.setText(height+"cm");
    }

    @BindingAdapter({"weghit"})
    public static void setUserWeghit(TextView view, String weghit) {
        if(weghit==null) return;
        view.setText(weghit+"kg");
    }


    @BindingAdapter({"setProfession"})
    public static void setUserProfession(TextView view, String profession) {
        if(StringUtil.isNullorEmpty(profession)){
            view.setText("自由职业");
        } else {
            view.setText(profession);
        }
    }

    @BindingAdapter({"productBeanList","position"})
    public  static void setDiamodns(TextView view, List bean,int position) {
        if(bean!=null &&bean.size()>position)
        {

            List<ProductBean> data=(List<ProductBean>) bean;
            view.setText(view.getContext().getResources().getString(R.string.diamodns,data.get(position).getDay()));
            if(view.getVisibility()==View.GONE)
            {
                view.setVisibility(View.VISIBLE);
            }
        }else
        {
            view.setVisibility(View.GONE);
        }
    }

    @BindingAdapter({"productBeanList","position"})
    public  static void setDiamodnsMoney(Button view, List bean, int position) {

        if(bean!=null &&bean.size()>position)
        {
            List<ProductBean> data=(List<ProductBean>) bean;
            int money = data.get(position).getMoney();
            Double d_money = Double.valueOf(String.valueOf(money));
            DecimalFormat df   =new   DecimalFormat("#.00");
            view.setText(view.getContext().getResources().getString(R.string.diamodns_money, df.format(d_money)));
            if(view.getVisibility()==View.GONE)
            {
                view.setVisibility(View.VISIBLE);
            }
        }else
        {
            view.setVisibility(View.GONE);
        }
    }


    @BindingAdapter({"isVip","text"})
    public  static void setTextVaguebg(TextView view, boolean isVip,String text) 
    {
        if(!isVip)
        {
            int length = text.length();
            StringBuffer vagueText=new StringBuffer();
            for(int i=0;i<length;i++)
            {
                vagueText.append("*");
            }
            view.setText(vagueText);
        }else

        {
           view.setText(text);
        }

    }

    @BindingAdapter({"basicinfo"})
    public static void setBasicInfo(TextView view, User user)
    {
        if(user==null)
        {

        }
        String sex=user.getSex().equals("1")?view.getContext().getString(R.string.male):view.getContext().getString(R.string.female);
        String age=user.getAge()!=0?user.getAge()+"岁":null;
        String height=user.getHeight()!=null?user.getHeight()+"cm":null;
        String city=user.getAddress();
        StringBuffer content=new StringBuffer();
        content.append(sex!=null?sex:"");
        content.append(age!=null?","+age:"");
        content.append(height!=null?","+height:"");
        content.append(city!=null?","+city:"");
        view.setText(content);
    }

    /***
     * 推荐列表显示是否在线
     * @param view
     * @param isOnline
     */
    @BindingAdapter({"isOnline"})
    public static void isOnline(TextView view, boolean isOnline)
    {
      if(isOnline)
      {
          view.setText("在线");
          view.setBackgroundResource(R.drawable.shape_bg_red_arc);
      }else {

          view.setText("离线");
          view.setBackgroundResource(R.drawable.shape_bg_black_arc);
      }
    }

    /***
     * glide图像加载推荐使用RequestManager进行加载管理避免加载资源与Context生命周期不一致
     * @param glide
     * @param url
     * @param view
     * @param defaultImage
     */
    public  static void loadImage(RequestManager glide, String url, ImageView view,Drawable defaultImage) {
        glide.load(url).diskCacheStrategy(DiskCacheStrategy.ALL).error
                (defaultImage).into(view);

    }

    public  static void loadImage(RequestManager glide, ImageView view, String url, Drawable defaultImage, float
            aspect) {
        glide.load(url).diskCacheStrategy(DiskCacheStrategy.ALL).error
                (defaultImage).centerCrop().transform(new GlideRoundTransform(view.getContext(),(int)aspect)).into(view);
    }


    /**
     * 解决View的上下文被TintContextWrapper包了一层，无法找到Activity的问题
     * @return host activity; or null if not available
     */
    public static Activity getActivityFromView(View view) {
        if(view ==null) return null;
        Context context = view.getContext();
        while (context instanceof ContextWrapper) {
            if (context instanceof Activity) {
                return (Activity) context;
            }
            context = ((ContextWrapper) context).getBaseContext();
        }
        return null;
    }

    private static boolean assertNotDestroyed(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1 && activity.isDestroyed()) {
            return true;
        }
        return false;
    }
}
