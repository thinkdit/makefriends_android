package com.xindian.fatechat.ui.home.presenter;

import android.os.RemoteException;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.parser.Feature;
import com.xindian.fatechat.R;
import com.xindian.fatechat.common.BaseModel;
import com.xindian.fatechat.common.BasePresenter;
import com.xindian.fatechat.common.FateChatApplication;
import com.xindian.fatechat.common.ResultModel;
import com.xindian.fatechat.ui.home.model.RulesModel;

import org.json.JSONException;

import java.util.HashMap;

/**
 * 举报
 */

public class ReportPresenter extends BasePresenter {
    private final String URL_REPORT = "/auth/allegations";

    public void report(String fromId,String fromName,String toId,String toName,String toHead,String reportStr) {
        HashMap<String, Object> data = new HashMap<>();
        data.put("informant",fromName);
        data.put("informantUid",fromId);
        data.put("informants",toName);
        data.put("informantsHead",toHead);
        data.put("informantsUid",toId);
        data.put("type",reportStr);
        post(getUrl(URL_REPORT), data, null);
    }


    @Override
    public Object asyncExecute(String url, ResultModel resultModel) {
        if (url.contains(URL_REPORT)) {
            return JSON.parseObject(resultModel.getData(), BaseModel.class, Feature
                    .IgnoreNotMatch);
        }
        return null;
    }

    @Override
    public void onError(String url, Exception e) {
        super.onError(url, e);
    }


    @Override
    public void onFailure(String url, ResultModel resultModel) {
        super.onFailure(url,resultModel);
        if (url.contains(URL_REPORT)) {
        }
    }

    @Override
    public void onSucceed(String url, ResultModel resultModel) throws JSONException,
            RemoteException {
        if (url.contains(URL_REPORT)) {
            Toast.makeText(FateChatApplication.getInstance(), R.string.re_su,Toast.LENGTH_SHORT).show();
        }
    }

}
