package com.xindian.fatechat.pay.wxpay.bbnpay;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Toast;

/**
 * Created by hx_Alex on 2017/5/12.
 */

public class BbnPayh5Activity  extends Activity implements View.OnClickListener {
    private String payurl = "";
    private WebView webView;
    private String backurl = "https://payh5.bbnpay.com/html/pay_return_wx.php";
    private String back_str = "";

    public BbnPayh5Activity() {
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(this.goodNet()) {
            this.payurl = this.getIntent().getStringExtra("payurl");
            this.attachAnotherLayout(this.payurl);
        }

        this.back_str =BbnPay.get_back();
    }

    private void attachAnotherLayout(String url) {
        LinearLayout theLayout = new LinearLayout(this);
        theLayout.setOrientation(LinearLayout.VERTICAL);
        this.webView = new WebView(this);
        this.webView.getSettings().setJavaScriptEnabled(true);
        this.webView.addJavascriptInterface(new AndroidJavaScript(this, this.webView), "AndroidFunction");
        this.webView.loadUrl(url);
        this.webView.setWebViewClient(new BbnPayh5Activity.Callback());
        RadioGroup.LayoutParams lp = new RadioGroup.LayoutParams(-1, -1);
        this.webView.setLayoutParams(lp);
        theLayout.addView(this.webView);
        this.addContentView(theLayout, lp);
    }

    public void onClick(View v) {
    }

    public void finish_activity() {
        this.finish();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == 4) {
            this.finish();
            return true;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }

    public boolean goodNet() {
        ConnectivityManager manager = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkinfo = manager.getActiveNetworkInfo();
        if(networkinfo != null && networkinfo.isAvailable()) {
            return true;
        } else {
            (new AlertDialog.Builder(this)).setMessage(" 请先打开网络。").setPositiveButton("Ok", new android.content.DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    BbnPayh5Activity.this.finish();
                }
            }).show();
            return false;
        }
    }

    private class Callback extends WebViewClient {
        private Callback() {
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Intent intent;
            if(url.startsWith("weixin://wap/pay?")) {
                try {
                    intent = new Intent();
                    intent.setAction("android.intent.action.VIEW");
                    intent.setData(Uri.parse(url));
                    BbnPayh5Activity.this.startActivity(intent);
                } catch (Exception var4) {
                    Toast.makeText(BbnPayh5Activity.this, "微信支付仅支持6.0.2 及以上版本，请将微信更新至最新版本", Toast.LENGTH_SHORT).show();
                }

                return true;
            } else if(url.startsWith("tel:")) {
                intent = new Intent("android.intent.action.VIEW", Uri.parse(url));
                BbnPayh5Activity.this.startActivity(intent);
                return true;
            } else {
                if(url.equals("http://payh5.tntsdk.com/html/back-ios-app")) {
                    BbnPayh5Activity.this.finish_activity();
                } else {
                    url.equals(BbnPayh5Activity.this.backurl);
                }

                return super.shouldOverrideUrlLoading(view, url);
            }
        }

        @Override
        public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
            return super.shouldInterceptRequest(view, request);
        }
    }

     
}
