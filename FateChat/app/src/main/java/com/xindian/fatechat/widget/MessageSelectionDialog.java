package com.xindian.fatechat.widget;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.wx.wheelview.adapter.ArrayWheelAdapter;
import com.wx.wheelview.widget.WheelView;
import com.xindian.fatechat.R;
import com.xindian.fatechat.ui.user.data.UserData;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;


/**
 * Created by hx_Alex on 2017/3/28.
 * 常用选择器控件Dialog
 */

public class MessageSelectionDialog extends AlertDialog implements WheelView.OnWheelItemSelectedListener<String>{
    public static final  String LIVE_SELECTION="LIVE_SELECTION";//居住地选择
    public static final  String EDIT_SELECTION="EDIT_SELECTION";//输入框
    public static final  String EDIT_VIP_SELECTION="EDIT_VIP_SELECTION";//可以选择对谁开放的输入框
    public static final  String DATE_SELECTION="DATE_SELECTION";//可以选择对谁开放的输入框
    public static final  String AGE_SCOPE_SELECTION="AGE_SCOPE_SELECTION";//年龄范围选择
    public static final  String HEGHIT_SCOPE_SELECTION="HEGHIT_SCOPE_SELECTION";//年龄范围选择
    private  String title;
    private TextView titleView;
    private Button btn_ok,btn_cancel;
    private EditText nomalEditText;
    private LinearLayout selectionSingleton,selectionDouble,selectionThree;
    private  String useKind;
    private WheelView mainWheelView,belongWheelView,selectionWheelView,threeWheelView,threeMainWheelView,threeBelongView;
    private RadioGroup rgInfoOpenSelect;
    private  onSaveListener mListener;
    private  String selectContent;
    private  String selectContent2;
    private   Context context;
    private  List dataList;
    public MessageSelectionDialog(Context context,String title,String useKind,onSaveListener mListener) {
        super(context);
        this.title=title;
        this.context=context;
        this.useKind=useKind;
        this.mListener=mListener;
    }

    public void setMaxEditLen(int len){
        nomalEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(len)});
    }

    public MessageSelectionDialog(Context context,List dataList,String title,onSaveListener mListener) {
        super(context);
        this.title=title;
        this.context=context;
        this.dataList=dataList;
        this.mListener=mListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_selection_dialog);
        intiView();
        titleView.setText(title);
        
    }

    private void intiView() {
        getWindow().setBackgroundDrawable(context.getResources().getDrawable(R.color.transparent));
        titleView= (TextView) findViewById(R.id.txt_title);
        nomalEditText= (EditText) findViewById(R.id.edit_message);
        selectionSingleton= (LinearLayout) findViewById(R.id.singleton_selection);
        selectionDouble= (LinearLayout) findViewById(R.id.singleton_double);
        selectionThree= (LinearLayout) findViewById(R.id.singleton_selection_three);
        btn_ok= (Button) findViewById(R.id.btn_ok);
        btn_cancel= (Button) findViewById(R.id.btn_cancel);
        mainWheelView= (WheelView) findViewById(R.id.main_wheel_view);
        belongWheelView=(WheelView) findViewById(R.id.belong_wheel_view);
        threeWheelView= (WheelView) findViewById(R.id.three_wheel_view);
        selectionWheelView= (WheelView) findViewById(R.id.selection_wheel_view);
        threeMainWheelView= (WheelView) findViewById(R.id.three_main_wheel_view);
        threeBelongView= (WheelView) findViewById(R.id.three_belong_wheel_view);
        rgInfoOpenSelect= (RadioGroup) findViewById(R.id.info_open_select);
        
        intiWheelView(mainWheelView);
        intiWheelView(belongWheelView);
        intiWheelView(selectionWheelView);
        intiWheelView(threeWheelView);
        intiWheelView(threeMainWheelView);
        intiWheelView(threeBelongView);
        
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(useKind==EDIT_SELECTION)
                {
                    selectContent=nomalEditText.getText().toString().trim();
                } else  if(useKind==LIVE_SELECTION) {
                    if(((String) mainWheelView.getSelectionItem()).contains("不限"))
                    {
                        selectContent="不限";
                    }
                 selectContent=belongWheelView.getSelectionItem().equals("不限")?(String)mainWheelView.getSelectionItem():(String)mainWheelView.getSelectionItem()+"-"+belongWheelView.getSelectionItem();
                } else  if(useKind==EDIT_VIP_SELECTION) {
                    selectContent=nomalEditText.getText().toString().trim();
                    selectContent2=  context.getString(R.string.open_vip);
                }else  if(useKind==DATE_SELECTION) {
                    selectContent=(String)threeMainWheelView.getSelectionItem()+"-"+threeBelongView.getSelectionItem()+"-"+threeWheelView.getSelectionItem();
                } else  if(useKind==AGE_SCOPE_SELECTION) {
                    selectContent=(String) mainWheelView.getSelectionItem();
                    if(selectContent.equals("不限"))
                    {
                        mListener.onSave(selectContent,selectContent2);
                        dismiss();
                        return;
                    }
                    if(belongWheelView.getSelectionItem().equals("不限")) {
                        selectContent = (String) mainWheelView.getSelectionItem() + "以上";
                    }else 
                    {
                        selectContent = (String) mainWheelView.getSelectionItem() + "-"+belongWheelView.getSelectionItem();
                    }
                } else  if(useKind==HEGHIT_SCOPE_SELECTION) {
                    selectContent=(String) mainWheelView.getSelectionItem();
                    if(selectContent.equals("不限"))
                    {
                        mListener.onSave(selectContent,selectContent2);
                        dismiss();
                        return;
                    }
                    if(belongWheelView.getSelectionItem().equals("不限")) {
                        selectContent = (String) mainWheelView.getSelectionItem() + "以上";
                    }else
                    {
                        selectContent = (String) mainWheelView.getSelectionItem() + "-"+belongWheelView.getSelectionItem();
                    }
                }
                
                mListener.onSave(selectContent,selectContent2);
                dismiss();
            }
        });
        intiContentView();
    }
    
    private  void intiWheelView(WheelView view)
    {
        view.setStyle(new MessageSelectionStyle(context));
        view.setWheelAdapter(new ArrayWheelAdapter(context)); // 文本数据源
        view.setSkin(WheelView.Skin.Holo);
        view.setWheelSize(3);
        view.setOnWheelItemSelectedListener(this);
    }
    
    private  void intiContentView()
    {
        if(useKind==LIVE_SELECTION)
        {
            nomalEditText.setVisibility(View.GONE);
            selectionDouble.setVisibility(View.VISIBLE);
            mainWheelView.setWheelData(UserData.getProvince());
            belongWheelView.setWheelData(UserData.getProvince());//默认给一个数据，不然效果出不来，日
            Map<String, String[]> cityMap = UserData.getCityMap();
            mainWheelView.setOnWheelItemSelectedListener(new WheelView.OnWheelItemSelectedListener() {
                @Override
                public void onItemSelected(int position, Object o) {
                    ArrayList<String> cityList=new ArrayList<String>();
                    for(String s:cityMap.get(o))
                    {
                        cityList.add(s);
                    }
                    belongWheelView.setWheelData(cityList);
                }
            });
            
        } else if(useKind==EDIT_SELECTION){
            
        } else if(useKind==EDIT_VIP_SELECTION){
           
        }else if(useKind==DATE_SELECTION){
            nomalEditText.setVisibility(View.GONE);
            selectionThree.setVisibility(View.VISIBLE);
            threeMainWheelView.setWheelData(UserData.getYear());
            threeBelongView.setWheelData(UserData.getMonth());
            threeWheelView.setWheelData(UserData.getDay());
        }else if(useKind==AGE_SCOPE_SELECTION) {
            nomalEditText.setVisibility(View.GONE);
            selectionDouble.setVisibility(View.VISIBLE);
            List<String> agelist = UserData.getAge();
            agelist.add(0,"不限");
            mainWheelView.setWheelData(agelist);
            belongWheelView.setWheelData(agelist);//默认给一个数据，不然效果出不来，日
            mainWheelView.setOnWheelItemSelectedListener(new WheelView.OnWheelItemSelectedListener() {
                @Override
                public void onItemSelected(int position, Object o) {
                    List<String> age = new ArrayList<String>();
                    age.add(0,"不限");
                    if(position==0){
                        belongWheelView.setWheelData(age);
                        return;
                    }
                    Collection<String> c=UserData.getAge();
                    age.addAll(c);
                    for(int i=0;i<position;i++)
                    {
                        age.remove(1);
                    }
                    belongWheelView.setWheelData(age);
                }
            });
        }else if(useKind==HEGHIT_SCOPE_SELECTION){
            nomalEditText.setVisibility(View.GONE);
            selectionDouble.setVisibility(View.VISIBLE);
            List<String> heightlist = UserData.getHeightList();
            heightlist.add(0,"不限");
            mainWheelView.setWheelData(heightlist);
            belongWheelView.setWheelData(heightlist);//默认给一个数据，不然效果出不来，日
            mainWheelView.setOnWheelItemSelectedListener(new WheelView.OnWheelItemSelectedListener() {
                @Override
                public void onItemSelected(int position, Object o) {
                    List<String> height = new ArrayList<String>();
                    height.add(0,"不限");
                    if(position==0){
                        belongWheelView.setWheelData(height);
                        return;
                    }
                    Collection<String> c=UserData.getHeightList();
                    height.addAll(c);
                    for(int i=0;i<position;i++)
                    {
                        height.remove(1);
                    }
                    belongWheelView.setWheelData(height);
                }
            });
        } else
        {
            nomalEditText.setVisibility(View.GONE);
            selectionSingleton.setVisibility(View.VISIBLE);
            selectionWheelView.setWheelAdapter(new ArrayWheelAdapter(context));
            selectionWheelView.setSkin(WheelView.Skin.Holo);
            selectionWheelView.setWheelSize(3);
            selectionWheelView.setWheelData(dataList);
        }
    }

    /***
     * 选中选择回调
     * @param position
     * @param o
     */
    @Override
    public void onItemSelected(int position, String o) {
        selectContent=o;
    }


    public class MessageSelectionStyle extends WheelView.WheelViewStyle
    {
        private  Context context;

        public MessageSelectionStyle(Context context) {
            this.context = context;
           holoBorderColor= Color.parseColor("#FD6B8F");  // holo样式边框颜色
        }
    }
    
    public  interface  onSaveListener
    {
        void onSave(String content,String content2);
    }

}
