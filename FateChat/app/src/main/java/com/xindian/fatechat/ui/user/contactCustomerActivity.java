package com.xindian.fatechat.ui.user;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.xindian.fatechat.R;
import com.xindian.fatechat.databinding.ActivityContactCustomerBinding;
import com.xindian.fatechat.ui.base.BaseActivity;

public class contactCustomerActivity extends BaseActivity {
private ActivityContactCustomerBinding mBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
     mBinding= DataBindingUtil.setContentView(this, R.layout.activity_contact_customer);
        initToolBar();
    }
}
