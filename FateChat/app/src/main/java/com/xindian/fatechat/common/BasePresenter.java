package com.xindian.fatechat.common;

import android.content.Context;
import android.os.RemoteException;
import android.widget.Toast;

import com.thinkdit.lib.base.IRequestCallback;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.util.XTHttpUtil;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;

import java.util.Map;

/**
 * Created by qiuda on 16/6/6.
 */
public class BasePresenter implements IRequestCallback<ResultModel> {
    private static final String ERROR_TOKEN_INVALID = "PP13";
    public static final String CLOSE_DIALOG="close_dialog";
    public final String TAG = this.getClass().getSimpleName();
    public String getUrl(String path) {
        return UrlConstant.URL_BASE + path;
    }

    public String getChatUrl(String path) {
        return UrlConstant.URL_BASE_CHAT + path;
    }

    public String getPayUrl(String path) {
        return UrlConstant.URL_BASE_PAY + path;
    }

    public String getGameUrl(String path) {
        return UrlConstant.URL_BASE_GAME + path;
    }

    public String getFunUrl(String path) {
        return UrlConstant.URL_BASE_FUN + path;
    }

    @Override
    public Object asyncExecute(String url, ResultModel resultModel) {
        return null;
    }

    @Override
    public void onSucceed(String url, ResultModel resultModel) throws JSONException, RemoteException {
        EventBus.getDefault().post(CLOSE_DIALOG);
    }

    @Override
    public void onError(String url, Exception e) {

    }

    @Override
    public void onFailure(String url, ResultModel resultModel) {
        //默认拦截错误 如果错误之后不需要跳转页面可以不重写
        /***
         * 如果弹出网络加载框，当错误时需要取消的，可super.onFailure
         */
        interceptFailure(FateChatApplication.getInstance(), resultModel);
        EventBus.getDefault().post(CLOSE_DIALOG);
    }

    public void post(String url, Map<String, Object> param, Object tag) {
        XTHttpUtil.getInstance().doPost(this, url, param, tag);
    }


    /**
     * post 请求 重载方法
     * @param url  url地址
     * @param singleRequest 是否单一请求
     * @param param 参数
     * @param tag tag
     */
    public void post(String url, boolean singleRequest, Map<String, Object> param, Object tag) {
        XTHttpUtil.getInstance().doPost(this, url, singleRequest, param, tag);
    }

    public void post(String url, Map<String, Object> param,boolean isNotJsonParam,boolean isNormalRespose, Object tag) {
        XTHttpUtil.getInstance().doPost(this, url, param, isNotJsonParam,isNormalRespose,tag);
    }


    public void get(String url, Map<String, Object> param, Object tag) {
        XTHttpUtil.getInstance().doGet(this, url, param, tag);
    }

    /**
     * 是不是拦截网络失败  token失败需要拦截
     *
     * @param context
     * @param resultModel
     * @return
     */
    public static boolean interceptFailure(Context context, ResultModel resultModel) {
        //无效的会话token参数
        if(resultModel.getCode().equals(ERROR_TOKEN_INVALID))
        {
        
            Toast.makeText(context,resultModel.getMessage(),Toast.LENGTH_LONG).show();
            UserInfoManager.getManager(context).loginOut(context);
        }
        return false;
    }


    
}
