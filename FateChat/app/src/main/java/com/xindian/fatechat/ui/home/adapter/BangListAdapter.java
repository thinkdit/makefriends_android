package com.xindian.fatechat.ui.home.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.xindian.fatechat.R;
import com.xindian.fatechat.ui.home.model.BangBean;
import com.xindian.fatechat.ui.home.presenter.BangListPresenter;
import com.xindian.fatechat.util.DisplayUtil;
import com.xindian.fatechat.util.GlideRoundTransform;

import java.util.List;


/**
 * 榜单
 */
public class BangListAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private List<BangBean> mDatas;

    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BangListAdapter(Context context) {
        mContext = context;
        mLayoutInflater = LayoutInflater.from(mContext);
    }

    public List<BangBean> getmDatas() {
        return mDatas;
    }

    public void setDatas(List<BangBean> datas) {
        mDatas = datas;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if (mDatas != null) {
            return mDatas.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        BindingViewHolder holder;
        if(convertView ==null){
            convertView = mLayoutInflater.inflate(R.layout.item_bang, parent, false);
            holder = new BindingViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (BindingViewHolder)convertView.getTag();
        }

        BangBean bangBean2 = mDatas.get(position);
        //头像
        Glide.with(mContext).load(bangBean2.getUserInfo().getHeadimg()).diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .transform(new GlideRoundTransform(mContext, DisplayUtil.dip2px(mContext,40)))
                .into(holder.headImage);
        //名字
        holder.nameText.setText(bangBean2.getUserInfo().getNickName());
        //排名
        holder.numText.setText(bangBean2.getRank()+"");
        //钻石
        String zuanshiStr = "";
        if(type!=null && (type.equals(BangListPresenter.TYPE_TYPE_FEMALE_RECIVE) || type.equals(BangListPresenter.TYPE_TYPE_MALE_RECIVE))){
            zuanshiStr = "收到";
        }else{
            zuanshiStr = "送出";
        }
        zuanshiStr = zuanshiStr+bangBean2.getDiamonds()+"颗钻石的礼物";
        holder.detailText.setText(zuanshiStr);

        if(position == 0){
            holder.topText.setVisibility(View.VISIBLE);
        }else{
            holder.topText.setVisibility(View.GONE);
        }
        if(position == getCount()-1){
            holder.bottomText.setVisibility(View.VISIBLE);
            holder.line_gray.setVisibility(View.GONE);
        }else{
            holder.bottomText.setVisibility(View.GONE);
            holder.line_gray.setVisibility(View.VISIBLE);
        }

        return convertView;
    }

    public class BindingViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView numText;
        private TextView nameText;
        private TextView detailText;
        private TextView topText;
        private TextView bottomText;
        private TextView line_gray;
        private ImageView headImage;

        public BindingViewHolder(View root) {
            super(root);
            numText = (TextView) root.findViewById(R.id.iv_top_num);
            nameText = (TextView) root.findViewById(R.id.iv_top_name);
            detailText = (TextView) root.findViewById(R.id.iv_top_detail);
            topText = (TextView) root.findViewById(R.id.top_bg);
            bottomText = (TextView) root.findViewById(R.id.bottom_bg);
            line_gray = (TextView) root.findViewById(R.id.line_gray);
            headImage = (ImageView) root.findViewById(R.id.iv_avatar);
        }

        @Override
        public void onClick(View v) {
        }
    }
}
