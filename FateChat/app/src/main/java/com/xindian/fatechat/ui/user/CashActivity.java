package com.xindian.fatechat.ui.user;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;

import android.graphics.Color;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.ListViewAutoScrollHelper;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;


import com.hyphenate.easeui.utils.SnackbarUtil;

import com.xindian.fatechat.R;
import com.xindian.fatechat.common.Constant;
import com.xindian.fatechat.databinding.ActivityCashBinding;
import com.xindian.fatechat.manager.PayTypeManager;
import com.xindian.fatechat.manager.UmengEventManager;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.pay.SelectPayWindow;
import com.xindian.fatechat.pay.model.ProductBean;
import com.xindian.fatechat.ui.base.BaseActivity;
import com.xindian.fatechat.ui.user.adapter.UserCashListAdapter;
import com.xindian.fatechat.ui.user.data.CashListStaticData;
import com.xindian.fatechat.ui.user.model.PayTypeBean;
import com.xindian.fatechat.ui.user.presenter.CashPresenter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.beecloud.BCPay;

import static com.alibaba.sdk.android.oss.common.HttpMethod.HEAD;

public class CashActivity extends BaseActivity implements CashPresenter.onGetProductListListener,
        View.OnClickListener,CashPresenter.onPayTypeInfoListener {

    public static final String  ENTRYSOURCE="source";
    private ActivityCashBinding mBinding;
    private ListViewAutoScrollHelper scrolHelper;
    private UserCashListAdapter adapter;
    private CashPresenter cashPresenter;
    private List<ProductBean> productBeanList;
    private SelectPayWindow selectPaywindow;


    private boolean isReport=true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_cash);
        initToolBar();
        intiBeeCloudWxPay();
        cashPresenter = new CashPresenter(this, this);
        cashPresenter.setOnPayTypeInfoListener(this);

        if (UserInfoManager.getManager(this).getUserInfo() != null) {
            cashPresenter.requestGetProductList(UserInfoManager.getManager(this).getUserInfo());
        }

    }

    

    private void intiBeeCloudWxPay() {
        BCPay.initWechatPay(this, getResources().getString(R.string.wx_app_id));
    }

    
    

    private void intiView()  {
        if (productBeanList == null || productBeanList.size() < 2)
        {
            Toast.makeText(this,"充值金额获取失败，请重新尝试！",Toast.LENGTH_LONG).show();
            return;
        }
        //初始化选择金额样式
        mBinding.btnSelect.setOnClickListener(this);
        mBinding.btnSelect1.setOnClickListener(this);
        mBinding.btnSelect2.setOnClickListener(this);
        mBinding.btnSelect.setSelected(true);
        mBinding.btnSelect1.setSelected(false);
        mBinding.btnSelect2.setSelected(false);
        mBinding.setDiamondsVipPrice("￥" + productBeanList.get(1).getMoney()+"(" + productBeanList.get
              (1).getDay() + "天)");
        mBinding.setDiamondsVipDay(productBeanList.get(1).getRemarks());
        
        mBinding.setGoldVipPrice("￥" + productBeanList.get(0).getMoney()+"(" + productBeanList.get
                (0).getDay() + "天)");
        mBinding.setGoldVipDay(productBeanList.get(0).getRemarks());

        mBinding.setSupVipPrice("￥" + productBeanList.get(2).getMoney()+"(" + productBeanList.get
                (2).getDay() + "天)");
        mBinding.setSupVipDay(productBeanList.get(2).getRemarks());

        mBinding.btnCash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goCash();
            }
        });

        intiLsitView();
        showCashLayout();
    }

    /***
     * 初始化滚动列表
     */
    public void intiLsitView() {
        
        ArrayList<HashMap<String, String>> cashContentListData = CashListStaticData
                .getCashContentListData(this,productBeanList.get(0).getMoney(),productBeanList.get(1).getMoney());
        adapter = new UserCashListAdapter(this, cashContentListData);
        mBinding.userCashList.setAdapter(adapter);
        mBinding.userCashList.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        mBinding.userCashList.setSelection(adapter.getCount() - 1);
        scrolHelper = new ListViewAutoScrollHelper(mBinding.userCashList);
        ListScrolhandler handler = new ListScrolhandler();
        ListScrolThread thread = new ListScrolThread(handler);
        thread.start();
        mBinding.scrollView.scrollTo(0,0);
    }

    private void showCashLayout() {
       ObjectAnimator.ofFloat(mBinding.layoutSelectCash,"alpha",0.2f,1).setDuration(800).start();
        mBinding.layoutSelectCash.setVisibility(View.VISIBLE);
    }


    @Override
    public void onClick(View v) {
      
        if (v == mBinding.btnSelect1 && productBeanList != null &&
                productBeanList.size() >= 3) {
            mBinding.btnSelect1.setSelected(true);
            mBinding.btnSelect2.setSelected(false);
            mBinding.btnSelect.setSelected(false);
        } else if (v == mBinding.btnSelect2 && productBeanList != null &&
                productBeanList.size() >= 3) {
            mBinding.btnSelect2.setSelected(true);
            mBinding.btnSelect1.setSelected(false);
            mBinding.btnSelect.setSelected(false);
        }else if (v == mBinding.btnSelect && productBeanList != null &&
                productBeanList.size() >= 3) {
            mBinding.btnSelect.setSelected(true);
            mBinding.btnSelect1.setSelected(false);
            mBinding.btnSelect2.setSelected(false);
        }
     
    }
    
    public void onClickConsultkefu(View view)
    {
        if(Constant.checkApkExist(this,"com.tencent.mobileqq"))
        {
            try {
                startActivity
                        (new Intent
                                (Intent.ACTION_VIEW, 
                                        Uri.parse("mqqwpa://im/chat?chat_type=wpa&uin="+getResources().getString(R.string.contact_qq)+"&version=1")));
            }catch (Exception e)
            {
                Toast.makeText(this,"很抱歉,您的设备暂时不支持此项功能.您可手动添加客服咨询。",Toast.LENGTH_LONG).show();
            }
        

        }else 
        {
            Toast.makeText(this,"本机未安装QQ应用",Toast.LENGTH_SHORT).show();
        }
    }
    
    public  void goCash()
    {
        if(selectPaywindow!=null && selectPaywindow.isShow()) return;
        
        ProductBean productBean = null;
        if(mBinding.btnSelect1.isSelected()&& !mBinding.btnSelect2.isSelected() && !mBinding.btnSelect.isSelected()&& productBeanList != null &&
                productBeanList.size() >= 3)
        {
            productBean = productBeanList.get(1);
        }else if (mBinding.btnSelect2.isSelected()&& !mBinding.btnSelect1.isSelected()&& !mBinding.btnSelect.isSelected() && productBeanList != null &&
                productBeanList.size() >= 3){
            productBean = productBeanList.get(0);
        }else if (mBinding.btnSelect.isSelected()&& !mBinding.btnSelect1.isSelected()&&  !mBinding.btnSelect2.isSelected() && productBeanList != null &&
                productBeanList.size() >= 3){
            productBean = productBeanList.get(2);
        }
        /***
         * window多次实例化存在对象堆积
         */
        if(selectPaywindow==null) {
            selectPaywindow = new SelectPayWindow(this, productBean);
        }else 
        {
            selectPaywindow.setProductBean(productBean);
        }
        selectPaywindow.show();
        UmengEventManager.getInstance(this).reportClickPay(productBean.getMoney()+"元");
    }


   
    
    
//    @Override
//    public void onPayTypeInfoSuccess(List<PayTypeBean> payTypeBeanList) {
//        for(PayTypeBean bean:payTypeBeanList)
//        {
//            if(bean.getType()==PayTypeManager.PAY_WX) {
//                PayTypeManager.getInstance().setWxPayChannel(PayTypeManager.WXPayChannel.valueOf(bean.getCode()));
//            }else if(bean.getType()==PayTypeManager.PAY_ZFB)
//            {
//                PayTypeManager.getInstance().setZfbPayChannel(PayTypeManager.ZFBPayChannel.valueOf(bean.getCode()));
//            } 
//        }
//
//        if(mBinding.layoutSelectCash.getVisibility()!=View.VISIBLE)
//        {
//            intiView();
//        }
//    }
    /***
     * 获取支付渠道成功后初始化充值界面
     * @param payTypeBeanList
     */
    @Override
    public void onPayTypeInfoSuccess(PayTypeBean bean) {
        if(bean.getType()==PayTypeManager.PAY_WX) {
            PayTypeManager.getInstance().setWxPayChannel(PayTypeManager.WXPayChannel.valueOf(bean.getCode()));
        }else if(bean.getType()==PayTypeManager.PAY_ZFB)
        {
            PayTypeManager.getInstance().setZfbPayChannel(PayTypeManager.ZFBPayChannel.valueOf(bean.getCode()));
        }

        if(mBinding.layoutSelectCash.getVisibility()!=View.VISIBLE)
        {
            intiView();
        }
    }

    @Override
    public void onPayTypeInfoError(String msg) {



        SnackbarUtil.makeSnackBar(mBinding.getRoot(), msg,
                Toast.LENGTH_SHORT).setMeessTextColor(Color.WHITE).show();

    }

    class ListScrolhandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            scrolHelper.scrollTargetBy(0, -1);
        }
    }

    class ListScrolThread extends Thread {
        ListScrolhandler handler;

        public ListScrolThread(ListScrolhandler handler) {
            this.handler = handler;
        }

        @Override
        public void run() {

            while (true) {
                handler.sendEmptyMessage(1);
                try {
                    sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onGetProductListSuccess(List<ProductBean> productBeanList) {
        this.productBeanList = productBeanList;

        cashPresenter.getPayType(PayTypeManager.PAY_WX);
        cashPresenter.getPayType(PayTypeManager.PAY_ZFB);

        cashPresenter.getPayTypeChanneid();

    }

    @Override
    public void onGetProductListError(String msg) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("充值刷新","刷新用户数据");
        UserInfoManager.getManager(this).refreshUserInfo();
        String source = getIntent().getStringExtra(ENTRYSOURCE);
        if(isReport&& source!=null)
        {
            UmengEventManager.getInstance(this).reportGoCash(source);
            isReport=false;
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(selectPaywindow!=null) {
            selectPaywindow.unRegisterBroadCastReceiverWithWxPay();
        }

    }


}
