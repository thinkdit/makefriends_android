package com.xindian.fatechat.widget;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.thinkdit.lib.util.SharePreferenceUtils;
import com.xindian.fatechat.R;
import com.xindian.fatechat.manager.UserInfoManager;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 社区公告dailog
 * Created by qiuda on 16/7/9.
 */
public class ComRemindDialog extends Dialog implements View.OnClickListener{
    private Context mContext;
    private iDialogCallback mBtnCancelCallback;
    private iDialogCallback mBtnOkCallback;
    private iDialogCancelCallback mIDialogCancelCallback;

    private View rootView;
    @BindView(R.id.tv_cancel_dialog_bu)
    TextView tv_cancel_dialog_bu;
    @BindView(R.id.tv_cancel_close_bu)
    ImageView tv_cancel_close_bu;

    public ComRemindDialog(Context context) {
        super(context,R.style.Dialog_Common);
        mContext = context;
        creat();
    }

    /**
     * 创建对话框
     *
     */
    private void creat() {
        rootView = LayoutInflater.from(mContext).inflate(R.layout.layout_cancel_reason_dialog,null);
        setContentView(rootView);
        ButterKnife.bind(this,rootView);
        setCancelable(true);
        setCanceledOnTouchOutside(true);
        tv_cancel_close_bu.setOnClickListener(this);
        tv_cancel_dialog_bu.setOnClickListener(this);
    }

    public void setIDialogCancelCallback(iDialogCancelCallback IDialogCancelCallback) {
        mIDialogCancelCallback = IDialogCancelCallback;
    }

    public void setmBtnCancelCallback(iDialogCallback mBtnCancelCallback) {
        this.mBtnCancelCallback = mBtnCancelCallback;
    }

    public void setmBtnOkCallback(iDialogCallback mBtnOkCallback) {
        this.mBtnOkCallback = mBtnOkCallback;
    }


    @Override
    public void onClick(View v) {
            if (v == tv_cancel_dialog_bu) {
                dismiss();
            } else if (v == tv_cancel_close_bu) {
                dismiss();
            }
        int userId = UserInfoManager.getManager(mContext).getUserId();
        SharePreferenceUtils.putBoolean(mContext, SharePreferenceUtils.KEY_HAS_REM_COM + userId, true);
    }

    /**
     * 显示对话框
     *
     * @param title 标题
     * @param text  内容
     * @param image 图片
     * @param btn1  OK按钮文字
     * @param btn2  Cancel 按钮文字
     */
    public void show(String title, String image, String text, String btn1, String btn2) {
    }

    public interface iDialogCancelCallback {
        void onCanceled();
    }

    public interface iDialogCallback {

        /**
         * 添加监听
         *
         * @return 是否关闭对话框
         */
        boolean onBtnClicked(String reasonStr);

    }
}
