package com.xindian.fatechat.util;

import android.content.Context;
import android.util.Log;
import android.widget.EditText;

import com.hyphenate.EMCallBack;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.easeui.EaseConstant;
import com.hyphenate.easeui.domain.ChatUserEntity;
import com.xindian.fatechat.DemoHelper;
import com.xindian.fatechat.R;
import com.xindian.fatechat.manager.SharePreferenceManager;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.ui.login.LoginDialog;
import com.xindian.fatechat.ui.user.model.User;

/**
 * Created by leaves on 2017/4/14.
 */

public class IMUtil {

    //打招呼
    public static void sendHellMessage(Context context, User user) {
        boolean isRealUser = false;
        if (user.getUserType().equals(User.REALUSER)) {
            isRealUser = true;
        }
        ChatUserEntity entity = DemoHelper.getInstance().getChatUserEntity(isRealUser, user.getUserId());
        EMMessage message = EMMessage.createTxtSendMessage(context.getString(R.string.str_hello),
                entity.getToIdentify());


        if(isRealUser){
            message.setAttribute(EaseConstant.MESSAGE_ATTRIBUTE_USERID, UserInfoManager.getManager(context).getUserInfo().getUserId());
        }else {
            message.setAttribute(EaseConstant.MESSAGE_ATTRIBUTE_USERID, user.getUserId());
        }
        message.setAttribute(EaseConstant.MESSAGE_ATTRIBUTE_USERID, UserInfoManager.getManager(context).getUserInfo().getUserId());
        message.setAttribute(EaseConstant.EXTRA_USER_AVATOR, UserInfoManager.getManager(context).getUserInfo().getHeadimg());
        message.setAttribute(EaseConstant.EXTRA_USER_NICKNAME, UserInfoManager.getManager(context).getUserInfo().getNickName());
        message.setAttribute(EaseConstant.EXTRA_USER_MSGTYPE, 2);
        SharePreferenceManager.getManager(context).addNewUserId(user.getUserId());
        message.setMessageStatusCallback(new EMCallBack() {
            @Override
            public void onSuccess() {
                Log.i("IMMSG","--->us");
            }

            @Override
            public void onError(int i, String s) {
                Log.i("IMMSG","--->err:"+s);
            }

            @Override
            public void onProgress(int i, String s) {
                Log.i("IMMSG","--->pro:"+s);
            }
        });
        //send message
        EMClient.getInstance().chatManager().sendMessage(message);
    }

    /***
     * 检查是否登录
     *
     * @return
     */
    public static boolean checkIsLogin(Context context) {
        if (!UserInfoManager.getManager(context).isLogin()) {
            onClickLogin(context, null);
            return false;
        }
        return true;
    }

    /***
     * 检查是否登录
     *
     * @return
     */
    public static boolean checkIsLogin(Context context, LoginDialog.onLoginListener listener) {
        if (!UserInfoManager.getManager(context).isLogin()) {
            onClickLogin(context, listener);
            return false;
        }
        return true;
    }

    public static void onClickLogin(Context context, LoginDialog.onLoginListener listener) {
        LoginDialog dialog;
        dialog = new LoginDialog(context, "登录方可体验更多内容");
        dialog.setOnLoginListener(new LoginDialog.onLoginListener() {
            @Override
            public void LoginSucceed(User user) {
                UserInfoManager.getManager(context).addHeadInfoToUser(user,false);
                if (listener != null) {
                    listener.LoginSucceed(user);
                }
            }

            @Override
            public void LoginError(String msg) {

            }
        });
        dialog.setView(new EditText(context));
        dialog.show();
    }
}
