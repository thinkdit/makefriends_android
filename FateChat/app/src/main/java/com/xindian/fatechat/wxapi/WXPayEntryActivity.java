package com.xindian.fatechat.wxapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tencent.mm.opensdk.constants.ConstantsAPI;
import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.xindian.fatechat.R;
import com.xindian.fatechat.manager.UmengEventManager;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.pay.model.PrePayInfoBean;
import com.xindian.fatechat.pay.presenter.PayPresenter;
import com.xindian.fatechat.pay.wxpay.WxpayPresenter;

import org.greenrobot.eventbus.EventBus;

import static com.xindian.fatechat.R.id.status;


public class WXPayEntryActivity extends Activity implements IWXAPIEventHandler
{
	
    public static final String TAG = "MicroMsg.SDKSample.WXPayEntryActivity";
    public static final String PAY_SUCCESS = "Success";
    private RelativeLayout mLayout;
    private TextView txt_reslut;
    private TextView tvOrderNo, tvOrderTime, tvMoney;
    private IWXAPI api;
    private PayPresenter mPersenter;
    @Override
    public void onCreate(Bundle savedInstanceState)
    { 
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wx_pay_results);
        txt_reslut= (TextView) findViewById(R.id.txt_reslut);
        mPersenter=new PayPresenter(this, new PayPresenter.onWxPrePayInfoListener() {
            @Override
            public void onGetPrePayInfoSuccess(PrePayInfoBean bean) {
                
            }

            @Override
            public void onGetPrePayInfoError(String msg) {

            }
        });
        api = WXAPIFactory.createWXAPI(this,getResources().getString(R.string.wx_app_id));//appid需换成商户自己开放平台appid getString(R.string.wx_app_id)
        api.handleIntent(getIntent(), this);
    }
    
    @Override
    protected void onNewIntent(Intent intent)
    {
        super.onNewIntent(intent);
        setIntent(intent);
    }
    
    @Override
    public void onReq(BaseReq req)
    {
    }
    
    @Override
    public void onResp(BaseResp resp)
    {
        String reportMsg="";
        if (resp.getType() == ConstantsAPI.COMMAND_PAY_BY_WX)
        {
            // resp.errCode == -1 原因：支付错误,可能的原因：签名错误、未注册APPID、项目设置APPID不正确、注册的APPID与设置的不匹配、其他异常等
            // resp.errCode == -2 原因 用户取消,无需处理。发生场景：用户不支付了，点击取消，返回APP
            if (resp.errCode == 0) // 支付成功
            {
                Toast.makeText(this, "支付成功", Toast.LENGTH_SHORT).show();
                txt_reslut.setText("支付成功");
                ReslutMsg msg=new ReslutMsg();
                msg.setTag(TAG);
                msg.setMsg(PAY_SUCCESS);
                EventBus.getDefault().post(msg);
                reportMsg="支付成功,等待服务回调";
                finish();
            }
            else if(resp.errCode==-1)
            {
                Toast.makeText(this, "支付错误,错误 code:" + resp.errCode , Toast.LENGTH_SHORT)
                        .show();
                txt_reslut.setText("支付错误");
                reportMsg="支付错误";
                finish();
            }
            else {  
                Toast.makeText(this, "用户取消支付", Toast.LENGTH_SHORT)
                        .show();
                txt_reslut.setText("支付取消");
                reportMsg="支付取消";
                finish();
            }
            if(WxpayPresenter.getPid()!=0||WxpayPresenter.getOrderNum()!=null) {
                mPersenter.reportPayInfo(WxpayPresenter.getPid(), reportMsg, WxpayPresenter.getOrderNum(), UserInfoManager.getManager(this).getUserId());
            }
            UmengEventManager.getInstance(this).reportPayReslut("客户端回调-微信"+reportMsg);
        }
    }
    
    
    public class ReslutMsg
    {
        private String tag;
        private String msg;

        public String getTag() {
            return tag;
        }

        public void setTag(String tag) {
            this.tag = tag;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }
    }
}