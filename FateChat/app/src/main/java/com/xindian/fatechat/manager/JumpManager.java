package com.xindian.fatechat.manager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.ViewGroup;

import com.hyphenate.easeui.EaseConstant;
import com.hyphenate.easeui.domain.ChatUserEntity;
import com.thinkdit.lib.util.NetworkUtils;
import com.xindian.fatechat.ui.ChatActivity;
import com.xindian.fatechat.ui.home.model.RulesModel;
import com.xindian.fatechat.ui.user.model.User;
import com.xindian.fatechat.widget.StarTDialog;

/**
 * Created by leaves on 2017/4/28.
 */

public class JumpManager {


    /****
     * 进入聊天界面
     * 接入互聊后需要区分
     * @param context
     * @param user
     */
    public static void gotoChat(Context context, ChatUserEntity user) {
        if (!NetworkUtils.isNetworkAvailable(context)) {
            if (context instanceof Activity) {
                Activity activity = (Activity) context;
                showNoNetDialog(context, (ViewGroup) activity.getWindow().getDecorView());
            }
            return;
        }
        boolean isShow = checkIsShowGiftButton(context);
        Intent intent = new Intent(context, ChatActivity.class);
        Bundle mBundle = new Bundle();
        mBundle.putSerializable(EaseConstant.EXTRA_CHAT_USER, user);
        mBundle.putBoolean(EaseConstant.IS_SHOW_GIFT_BUTTON, isShow);
        intent.putExtras(mBundle);
        context.startActivity(intent);
    }

    public static void showNoNetDialog(Context context, ViewGroup group) {
        StarTDialog dialog = new StarTDialog(context, group);
        dialog.setBtnOkCallback(new StarTDialog.iDialogCallback() {
            @Override
            public boolean onBtnClicked() {
                Intent wifiSettingsIntent = new Intent("android.settings.WIFI_SETTINGS");
                context.startActivity(wifiSettingsIntent);
                return true;
            }
        });
        dialog.show("提示", "请打开网络", "确定", "取消");
    }

    private static boolean checkIsShowGiftButton(Context context)
    {
        if(context==null) return false;
        User user=UserInfoManager.getManager(context).getUserInfo();
        RulesModel mRuleMode = RulesManager.instance(context).mRuleMode;
        if(user.isVip())
        {
            return true;
        }
        if(mRuleMode!=null && mRuleMode.getPushButton()!=null && mRuleMode.getPushButton().isGiftIcon())
        {
            return true;
        }
        return false;
    }

}
