package com.xindian.fatechat.ui.user.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.xindian.fatechat.R;
import com.xindian.fatechat.databinding.ItemPhotoBinding;
import com.xindian.fatechat.ui.user.model.PhotoInfoBean;

import java.util.List;

/**
 * Created by hx_Alex on 2017/4/4.
 */

public class UserPhotoListAdapter extends RecyclerView.Adapter<UserPhotoListAdapter.ItemPhotoViewHodler> {
    private List<PhotoInfoBean> photoList;
    private Context context;
    private LayoutInflater inflater;
    private  OnItemClickListener mOnItemClickListener;
    public UserPhotoListAdapter(Context context, List<PhotoInfoBean> photoList) {
        this.context = context;
        this.photoList = photoList;
        inflater=LayoutInflater.from(context);
    }

    @Override
    public ItemPhotoViewHodler onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemPhotoBinding mBinding= DataBindingUtil.inflate(inflater, R.layout.item_photo,parent,false);
        return new ItemPhotoViewHodler(mBinding,mOnItemClickListener) ;
    }

    @Override
    public void onBindViewHolder(ItemPhotoViewHodler holder, int position) {
        //第一位默认为从相册选取入口图片
            holder.getmBinding().imgAdd.setVisibility(View.GONE);
            holder.getmBinding().imgPhoto.setVisibility(View.VISIBLE);
            holder.getmBinding().setPhoto(photoList.get(position-1).getImageUrl());
            holder.getmBinding().setId(photoList.get(position-1).getId());
    }

    @Override
    public int getItemCount() {

        return photoList.size();
    }



    public OnItemClickListener getmOnItemClickListener() {
        return mOnItemClickListener;
    }

    public void setmOnItemClickListener(OnItemClickListener mOnItemClickListener) {
        this.mOnItemClickListener = mOnItemClickListener;
    }
    
    /***
     * item点击事件
     */
    public  interface  OnItemClickListener
    {
       void onItemClickListener(View view, int position, Object value, int id);
    }
    /***
     * viewHodler
     */
    public class ItemPhotoViewHodler extends RecyclerView.ViewHolder implements View.OnClickListener {
        private  ItemPhotoBinding mBinding;
        private OnItemClickListener mItemClickListener;
        public ItemPhotoViewHodler(ItemPhotoBinding mBinding,OnItemClickListener mItemClickListener) {
            super(mBinding.getRoot());
            this.mBinding=mBinding;
            this.mItemClickListener=mItemClickListener;
            mBinding.imgPhoto.setOnClickListener(this);
            mBinding.imgAdd.setOnClickListener(this);
        }

        public ItemPhotoBinding getmBinding() {
            return mBinding;
        }

        
        @Override
        public void onClick(View v) {
                mItemClickListener.onItemClickListener(v, getAdapterPosition(), mBinding.getPhoto(), photoList.get(getAdapterPosition()).getId());
        }
    }
}

