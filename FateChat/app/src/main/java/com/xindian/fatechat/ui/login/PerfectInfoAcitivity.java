package com.xindian.fatechat.ui.login;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.hyphenate.easeui.utils.SnackbarUtil;
import com.thinkdit.lib.base.BaseApplication;
import com.thinkdit.lib.util.DeviceInfoUtil;
import com.thinkdit.lib.util.SharePreferenceUtils;
import com.thinkdit.lib.util.StringUtil;
import com.xindian.fatechat.R;
import com.xindian.fatechat.databinding.ActivityPerfectInfoAcitivityBinding;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.ui.base.BaseActivity;
import com.xindian.fatechat.ui.login.presenter.AccountPresenter;
import com.xindian.fatechat.ui.user.data.UserData;
import com.xindian.fatechat.ui.user.model.User;
import com.xindian.fatechat.util.imageSelectorUtil;
import com.xindian.fatechat.widget.MessageSelectionDialog;
import com.xindian.fatechat.widget.StarTDialog;
import com.yancy.imageselector.ImageSelector;
import com.yancy.imageselector.ImageSelectorActivity;

import java.util.List;
import java.util.Random;


public class PerfectInfoAcitivity extends BaseActivity implements MessageSelectionDialog
        .onSaveListener, AccountPresenter.
        OnRegisterListener, AccountPresenter.OnLoginListener, AccountPresenter
        .IAvatarUploadedListener {
    private static final int OCCUPATION = 1;
    private static final int EDUCATION = 2;
    private static final int INCOME = 3;
    private static final int HEGHIT = 4;
    private static final int MARRIAGE = 5;
    public static final String KEY_DEVICEID="key_deviceId";
    private int intiPassword;
    private ActivityPerfectInfoAcitivityBinding mBinding;
    private imageSelectorUtil imageSelectorUtil;
    private int openDialog;
    private User user;
    private AccountPresenter accountPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        User extraUser = (User) getIntent().getSerializableExtra(RegisterActivity.USERINFO);
        accountPresenter = new AccountPresenter(this);
        accountPresenter.setAvatarUploadedListener(this);
        accountPresenter.setOnLoginListener(this);
        accountPresenter.setRegisterListren(this);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_perfect_info_acitivity);
        initToolBar();
        if (extraUser != null) {
            user = extraUser;
        } else {
            user = new User();
        }
        //默认初始值
        user.setProfession("企业职工");
        user.setQualifications("大专");
        user.setRevenue("2000-5000");
        user.setHeight(user.getSex().equals("1") ? "175cm" : "168cm");
        user.setMarriage("未婚");
        mBinding.setUser(user);
        imageSelectorUtil = new imageSelectorUtil(this);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            if (requestCode == ImageSelector.IMAGE_REQUEST_CODE) {
                // Get Image Path List
                List<String> pathList = data.getStringArrayListExtra(ImageSelectorActivity
                        .EXTRA_RESULT);
                String path = pathList.get(pathList.size() - 1);
                accountPresenter.uploadAvatar(path);
            }
        }
    }

    public void onClickSelectOccupation(View v) {
        showMessageSelectionDialog(OCCUPATION, UserData.getOccupationList(), "工作情况");
    }

    public void onClickSelectEducation(View v) {
        showMessageSelectionDialog(EDUCATION, UserData.getEducationList(), "学历");
    }

    public void onClickSelectIncome(View v) {
        showMessageSelectionDialog(INCOME, UserData.getIncomeList(), "月收入");
    }


    public void onClickSelectHeight(View v) {

        showMessageSelectionDialog(HEGHIT, UserData.getHeightList(), "身高");
    }

    public void onClickSelectMarriage(View v) {
        showMessageSelectionDialog(MARRIAGE, UserData.getMarriageList(), "婚姻状态");
    }

    public void showMessageSelectionDialog(int dialogTag, List<String> dataList, String
            titleString) {
        openDialog = dialogTag;
        MessageSelectionDialog dialog = new MessageSelectionDialog(this, dataList, titleString,
                this);
        dialog.show();
    }


    public void onClickSelectPhoto(View v) {
        imageSelectorUtil.changePortrait();
    }

    @Override
    public void onSave(String content, String content2) {
        if (openDialog == OCCUPATION) {
            mBinding.txtOccupation.setText(content);
        } else if (openDialog == EDUCATION) {

            mBinding.txtEducation.setText(content);
        } else if (openDialog == INCOME) {

            mBinding.txtIncome.setText(content);
        } else if (openDialog == HEGHIT) {

            mBinding.txtHeight.setText(content);
        } else if (openDialog == MARRIAGE) {

            mBinding.txtMarriage.setText(content);
        }
    }

    public void onClickRegister(View v) {
        if (checkData()) {
            onRegister();
        }
    }

    public void onRegister() {
        //随机生成一个密码
        if(checkDeviceIdIsUse()) {
            Random random = new Random();
            int intiPassWord = random.nextInt(999999) + 1;
            this.intiPassword = intiPassWord;
            user.setPassword(String.valueOf(intiPassWord));
            accountPresenter.requestRegister(user);
        }
    }

    public boolean checkData() {

        if (StringUtil.isNullorEmpty(user.getHeadimg())) {
            StarTDialog mStarTDialog = new StarTDialog(PerfectInfoAcitivity.this, (ViewGroup)
                    mBinding.getRoot());

            mStarTDialog.setBtnOkCallback(new StarTDialog.iDialogCallback() {
                @Override
                public boolean onBtnClicked() {
                    onRegister();
                    return true;
                }
            });
            mStarTDialog.show("提示", null, "提交照片将提高您交友成功的概率哟.", "立即注册", "取消");
            return false;
        }

        return true;
    }
    
    public boolean checkDeviceIdIsUse()
    {
        String deviceIdByRemto = UserInfoManager.getManager(this).getDeviceIdByRemto();
        if(deviceIdByRemto==null)
        {
            return true;
        }else {
            SnackbarUtil.makeSnackBar(mBinding.getRoot(),"检测到本设备已注册过有效用户,无法重复注册!", Toast.LENGTH_SHORT).setMeessTextColor(Color.WHITE).show();
        }
        return false;
    }

    @Override
    public void registerSucceed(User user) {
        //注册成功后进行登录操作
        StarTDialog mStarTDialog = new StarTDialog(PerfectInfoAcitivity.this, (ViewGroup)
                mBinding.getRoot());
        String contentText = String.format(new String(getString(R.string.registration_tips)),
                user.getUserId(), intiPassword);
        mStarTDialog.setBtnOkCallback(new StarTDialog.iDialogCallback() {
            @Override
            public boolean onBtnClicked() {
                if (user.getUserId() != 0 && intiPassword != 0) {
                    accountPresenter.requestLogin(String.valueOf(user.getUserId()), String
                            .valueOf(intiPassword));
                }
                return true;
            }
        });


        mStarTDialog.showSingle("提示", null, contentText, "立即登录");
        mStarTDialog.setCancelable(false);
        mStarTDialog.setCanceledOnTouchOutside(false);

    }

    @Override
    public void registerError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAvatarUploadedSucceed(String url) {
        user.setHeadimg(url);
        mBinding.setUser(user);
    }

    @Override
    public void onAvatarUploadedError() {
        
    }

    private  boolean isSaveDeviceId=true;
    @Override
    public void LoginSucceed(User user) {
        //限制单台手机只能注册一次
        String deviceId = DeviceInfoUtil.getDeviceId(BaseApplication.getInstance());
        if(isSaveDeviceId && deviceId!=null)
        {
            SharePreferenceUtils.putString(this, KEY_DEVICEID,deviceId);
        }
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void LoginError(String msg) {
    }


}


