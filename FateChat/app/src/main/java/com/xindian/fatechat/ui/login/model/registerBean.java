package com.xindian.fatechat.ui.login.model;

import com.xindian.fatechat.common.BaseModel;

/**
 * Created by hx_Alex on 2017/3/31.
 * 注册返回的实体bean
 */

public class RegisterBean extends BaseModel {
    private int  userId;//用户基本信息

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
