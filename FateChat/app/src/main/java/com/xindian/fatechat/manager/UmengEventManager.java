package com.xindian.fatechat.manager;

import android.content.Context;

import com.umeng.analytics.MobclickAgent;

import java.util.HashMap;

import static com.switfpass.pay.MainApplication.getContext;

/**
 * Created by hx_Alex on 2017/6/9.
 */

public class UmengEventManager {
    public static final String GOCASH="1";
        public static final String CLICKCASH="2";
        public static final String STARTCASH="3";
        public static final String CASHRESLUT="4";
    private static  UmengEventManager umengEventManager;
    private static Context context;
    private UmengEventManager(){}

    public  static synchronized UmengEventManager getInstance(Context context){
        if(umengEventManager==null){
            umengEventManager=new UmengEventManager();
        }
        UmengEventManager.context=context;
        return umengEventManager;
    }
    
    private   void statisticsEvent(String eventId)
    {
        MobclickAgent.onEvent(context,eventId);
    }

    private  void statisticsEvent(String eventId,HashMap map)
    {
        MobclickAgent.onEvent(context,eventId,map);
    }

    public  void reportGoCash(String enterSource){

        HashMap<String,String> map=new HashMap<>();
        map.put("source",enterSource);
        UmengEventManager.getInstance(getContext()).statisticsEvent(UmengEventManager.GOCASH,map);
    }

    public  void reportClickPay(String PaymentAmount){

        HashMap<String,String> map=new HashMap<>();
        map.put("ClickPay",PaymentAmount);
        UmengEventManager.getInstance(getContext()).statisticsEvent(UmengEventManager.CLICKCASH,map);
    }

    public  void reportStartCash(String payMethod){

        HashMap<String,String> map=new HashMap<>();
        map.put("PayMethod",payMethod);
        UmengEventManager.getInstance(getContext()).statisticsEvent(UmengEventManager.STARTCASH,map);
    }

    public  void reportPayReslut(String payReslut){

        HashMap<String,String> map=new HashMap<>();
        map.put("payReslut",payReslut);
        UmengEventManager.getInstance(getContext()).statisticsEvent(UmengEventManager.CASHRESLUT,map);
    }

}
