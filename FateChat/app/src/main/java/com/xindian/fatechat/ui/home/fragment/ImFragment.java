package com.xindian.fatechat.ui.home.fragment;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMConversation;
import com.hyphenate.easeui.domain.LocalSession;
import com.hyphenate.easeui.manager.MessageLimitManager;
import com.hyphenate.easeui.model.EventBusModel;
import com.hyphenate.easeui.ui.EaseChatFragment;
import com.hyphenate.easeui.utils.DBHelper;
import com.hyphenate.easeui.utils.EaseCommonUtils;
import com.thinkdit.lib.util.SharePreferenceUtils;
import com.xindian.fatechat.R;
import com.xindian.fatechat.databinding.FragmentImBinding;
import com.xindian.fatechat.manager.ISessionManager;
import com.xindian.fatechat.manager.LoginManager;
import com.xindian.fatechat.manager.SharePreferenceManager;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.ui.ChatFragment;
import com.xindian.fatechat.ui.base.BaseFragment_v4;
import com.xindian.fatechat.ui.home.adapter.SessionListAdapter;

import com.xindian.fatechat.ui.login.LoginDialog;
import com.xindian.fatechat.ui.user.MyAttentionActivity;
import com.xindian.fatechat.ui.user.model.User;
import com.xindian.fatechat.widget.RecycleViewDivider;
import com.yanzhenjie.recyclerview.swipe.Closeable;
import com.yanzhenjie.recyclerview.swipe.OnSwipeMenuItemClickListener;
import com.yanzhenjie.recyclerview.swipe.SwipeMenu;
import com.yanzhenjie.recyclerview.swipe.SwipeMenuCreator;
import com.yanzhenjie.recyclerview.swipe.SwipeMenuItem;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hx_Alex on 2017/3/24.
 * 私信fragment
 */

public class ImFragment extends BaseFragment_v4 implements LoginDialog.onLoginListener, OnSwipeMenuItemClickListener {
    public final static String DELETE = "delete";
    public final static String STATEREAD = "stateRead";
    private FragmentImBinding mBinding;
    private SessionListAdapter adapter;
    private final String EDITCHATLIST = "编辑";
    private final String CANCELEDITCHATLIST = "取消";

    public static ImFragment newInstance() {
        ImFragment imFragment = new ImFragment();
        Bundle bundle = new Bundle();
        imFragment.setArguments(bundle);
        return imFragment;
    }

    @Subscribe
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
            Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_im, container, false);
        initView();

        LoginManager.instance(getContext()).registerLoginListerer(this);

        gotoCashActivity(mBinding.getRoot(),"私信页面");

        EventBus.getDefault().register(this);
        mBinding.layoutMyAttention.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), MyAttentionActivity.class);
                startActivity(i);
            }
        });
        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        if (!hidden) {
            refresh();
        }

    }


    private void initView() {
        LinearLayoutManager staggeredGridLayoutManager = new LinearLayoutManager(this.getActivity
                ());
        mBinding.rvList.addItemDecoration(new RecycleViewDivider(this.getActivity(),
                LinearLayoutManager.HORIZONTAL, 1, getResources().getColor(R.color.divide_line)));
        //3.为recyclerView设置布局管理器
        mBinding.rvList.setLayoutManager(staggeredGridLayoutManager);
        //侧滑菜单构建
        SwipeMenuCreator swipeMenuCreator = new SwipeMenuCreator() {
            @Override
            public void onCreateMenu(SwipeMenu swipeLeftMenu, SwipeMenu swipeRightMenu, int viewType) {
                SwipeMenuItem deleteItem = new SwipeMenuItem(ImFragment.this.getContext())
                        .setBackgroundColor(getContext().getResources().getColor(R.color.colorAccent))
                        .setText("删除") // 文字。
                        .setTextColor(Color.WHITE) // 文字的颜色。
                        .setWidth((int) getContext().getResources().getDimension(R.dimen.rghit_menu_width)) // 菜单宽度。
                        .setHeight(WindowManager.LayoutParams.MATCH_PARENT); // 菜单高度。
                swipeRightMenu.addMenuItem(deleteItem); // 在右侧添加一个菜单。
            }
        };
        mBinding.rvList.setSwipeMenuItemClickListener(this);
        mBinding.rvList.setSwipeMenuCreator(swipeMenuCreator);
        adapter = new SessionListAdapter(this.getActivity());
        mBinding.rvList.setAdapter(adapter);
        //初始化toolbar编辑按钮

        mBinding.toolbar.btnToolbarRight.setVisibility(View.VISIBLE);
        mBinding.toolbar.btnToolbarRight.setText(EDITCHATLIST);
        mBinding.toolbar.btnToolbarRight.setTextColor(getResources().getColor(R.color.colorAccent));
        mBinding.toolbar.btnToolbarRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editChatList();
            }
        });
    }

    public void editChatList() {
        EventBusModel model = new EventBusModel();
        if (mBinding.toolbar.btnToolbarRight.getText().equals(EDITCHATLIST)) {
            mBinding.toolbar.btnToolbarRight.setText(CANCELEDITCHATLIST);
            model.setAction(MainActivity.SHOWEDITCHATWINDOW);
            adapter.setShowCheckBox(true);
        } else {
            mBinding.toolbar.btnToolbarRight.setText(EDITCHATLIST);
            model.setAction(MainActivity.HIDEEDITCHATWINDOW);
            adapter.setShowCheckBox(false);
            adapter.getCheckItemList().clear();
        }
        EventBus.getDefault().post(model);
    }


    public void refresh() {
        if (getContext() == null) return;
        if (UserInfoManager.getManager(this.getActivity()).isLogin() && DBHelper.intance().isInit
                ()) {
            List<LocalSession> list = ISessionManager.getManager(this.getActivity())
                    .getSessionList();

            if (adapter == null || list == null) {
                return;
            }
            Log.d("****", "refresh");
            adapter.setDatas(list);
            if (this.getActivity() instanceof MainActivity) {
                MainActivity activity = (MainActivity) this.getActivity();
                int unread = 0;
                if (list != null) {
                    for (LocalSession session : list) {
                        unread = unread + session.getUnReadCount();
                    }
                }
                activity.setUnReadCount(unread);
            }
        } else {
            adapter.setDatas(new ArrayList<>());
            if (this.getActivity() instanceof MainActivity) {
                MainActivity activity = (MainActivity) this.getActivity();
                int unread = 0;
                activity.setUnReadCount(unread);
            }
        }
    }

    /**
     * 首次进入显示同城小秘书Im
     */
    public List<LocalSession> showImConsult(List<LocalSession> list) {
        if (adapter != null && list != null) {
            LocalSession localSession = new LocalSession();
            localSession.setLastMsgType(3);
            localSession.setNickName("同城小秘书");
            localSession.setAvatar("https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1495534559091&di=db908a2d45" +
                    "cdf57dcd9776a93161a877&imgtype=0&src=http%3A" +
                    "%2F%2Fimg.eelly.com%2FG03%2FM00%2F00%2F86%2FpYYBAFPRnhiIerdiAAFjgyDm12oAAA0hQAPdKIAAWOb312.jpg");
            localSession.setLasttime(System.currentTimeMillis());
            User userInfo = UserInfoManager.getManager(getContext()).getUserInfo();
            String sex = userInfo.getSex().equals("1") ? "帅哥" : "美女";
            int id = userInfo.getUserId();
            String msgContent = "亲爱的" + sex + "(ID:" + id + ")" + "恭喜你成为...";
            localSession.setLastMsgContent(msgContent);
            localSession.setUnReadCount(0);
            localSession.setUserId(id);
            list.add(localSession);
        }
        return list;
    }

    @Override
    public void LoginSucceed(User user) {
        if (this.getActivity() == null) return;
        this.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (ImFragment.this.getContext() == null) return;
                refresh();
                SharePreferenceManager.getManager(ImFragment.this.getContext()).reset(ImFragment
                        .this.getActivity());
                MessageLimitManager.instance(ImFragment.this.getContext()).reset();
            }
        });

    }

    @Override
    public void LoginError(String msg) {

    }


    /****
     * 侧滑按钮点击监听
     *
     * @param closeable
     * @param adapterPosition
     * @param menuPosition
     * @param direction
     */
    @Override
    public void onItemClick(Closeable closeable, int adapterPosition, int menuPosition, int direction) {
        deleteChatItem(adapterPosition);
        closeable.smoothCloseMenu();
        refresh();
    }

    //删除对话列表
    private void deleteChatItem(int position) {
        if (adapter == null || adapter.getmDatas() == null) return;
        LocalSession localSession = adapter.getmDatas().get(position);
        if (localSession.getLastMsgType() != 4) {
            DBHelper.intance().deleteSession(adapter.getmDatas().get(position));
        } else {
            int userId = UserInfoManager.getManager(getContext()).getUserId();
            //如果等于3并且执行删除操作，则不再更新以及推送小秘书这条消息
            SharePreferenceUtils.putString(this.getContext(), SharePreferenceUtils.KEY_HAS_SHOW_CONSULT + userId, userId + "");
        }
        adapter.getmDatas().remove(localSession);
        adapter.notifyDataSetChanged();
    }

    private void deleteChatItem(List<LocalSession> sessionList) {
        if (adapter == null || adapter.getmDatas() == null) return;
        for (LocalSession ls : sessionList) {
            if (ls.getLastMsgType() != 4) {
                DBHelper.intance().deleteSession(ls);
            } else {
                int userId = UserInfoManager.getManager(getContext()).getUserId();
                //如果等于3并且执行删除操作，则不再更新以及推送小秘书这条消息
                SharePreferenceUtils.putString(this.getContext(), SharePreferenceUtils.KEY_HAS_SHOW_CONSULT + userId, userId + "");
            }
        }
        adapter.getmDatas().removeAll(sessionList);
        adapter.notifyDataSetChanged();
    }


    @Subscribe
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }


    /***
     * 刷新列表数据
     *
     * @param model
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRefreshEventBusCallBack(EventBusModel model) {
        if (model.getAction().equals(EaseChatFragment.TAG) || model.getAction().equals(ChatFragment.TAG)) {
            refresh();
        }
    }


    /***
     * 操作聊天列表事件回调
     * 事件发送源来自mainActivity
     *
     * @param model
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEditChatListEventBusCallBack(EventBusModel model) {
        if (model.getAction().equals(ImFragment.DELETE)) {
            List<Integer> checkItemList = adapter.getCheckItemList();
            List<LocalSession> localSessionList = new ArrayList<>();
            if (checkItemList != null && checkItemList.size() > 0) {
                for (Integer postion : checkItemList) {
                    localSessionList.add(adapter.getmDatas().get(postion));
                }
                deleteChatItem(localSessionList);
                refresh();
            }
            editChatList();
        } else if (model.getAction().equals(ImFragment.STATEREAD)) {
            List<Integer> checkItemList = adapter.getCheckItemList();
            EMConversation conversation;
            if (checkItemList != null && checkItemList.size() > 0) {
                for (Integer postion : checkItemList) {
                    LocalSession localSession = adapter.getmDatas().get(postion);
                    ISessionManager.getManager(this.getActivity()).cleanSession(localSession.getUserIdInt());
                    conversation = EMClient.getInstance().chatManager().getConversation(localSession
                            .getKeFu(), EaseCommonUtils.getConversationType(), true);
                    if (conversation != null) {
                        conversation.markAllMessagesAsRead();
                    }
                }
                refresh();
            }
            editChatList();
        }

    }


}
