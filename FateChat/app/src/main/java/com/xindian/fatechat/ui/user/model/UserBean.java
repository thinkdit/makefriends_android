package com.xindian.fatechat.ui.user.model;

import com.xindian.fatechat.common.BaseModel;

/**
 * Created by hx_Alex on 2017/3/27.
 */

public class UserBean extends BaseModel {
    private int userId;
    private String userName;
    private int userAge;
    private int photoNum;
    private  String userImg;

    public UserBean() {

    }

    public UserBean(int userId, String userName, int userAge, int photoNum, String userImg) {
        this.userId = userId;
        this.userName = userName;
        this.userAge = userAge;
        this.photoNum = photoNum;
        this.userImg = userImg;
    }

    public String getUserImg() {
        return userImg;
    }

    public void setUserImg(String userImg) {
        this.userImg = userImg;
    }

    public int getUserAge() {
        return userAge;
    }

    public void setUserAge(int userAge) {
        this.userAge = userAge;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
    
    public int getPhotoNum() {
        return photoNum;
    }

    public void setPhotoNum(int photoNum) {
        this.photoNum = photoNum;
    }
}
