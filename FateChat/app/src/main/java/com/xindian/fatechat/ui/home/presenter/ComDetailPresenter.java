package com.xindian.fatechat.ui.home.presenter;

import android.os.RemoteException;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.xindian.fatechat.common.BasePresenter;
import com.xindian.fatechat.common.FateChatApplication;
import com.xindian.fatechat.common.ResultModel;
import com.xindian.fatechat.ui.home.model.JHTuiJianBean;

import org.json.JSONException;

import java.util.HashMap;

/**
 * 社区动态详情
 */
public class ComDetailPresenter extends BasePresenter {
    public static final int PAGE_SIZE = 200;
    private final String URL_GET_DETAIL = "/dynamic/dynamic_details";

    public void getComDetail(String dynamicId,String userId,String token,int page,int pageSize) {
        HashMap<String, Object> data = new HashMap<>();
        data.put("dynamicId",dynamicId);
        data.put("userId",userId);
        data.put("token",token);
        data.put("page",page);
        data.put("pageSize",pageSize);
        post(getUrl(URL_GET_DETAIL), data, null);
    }

    @Override
    public Object asyncExecute(String url, ResultModel resultModel) {
        if (url.contains(URL_GET_DETAIL)) {
            return JSON.parseObject(resultModel.getData(), JHTuiJianBean.class);
        }
        return null;
    }

    @Override
    public void onError(String url, Exception e) {
        super.onError(url, e);
    }


    @Override
    public void onFailure(String url, ResultModel resultModel) {
        super.onFailure(url,resultModel);
        if (url.contains(URL_GET_DETAIL)) {
            if(resultModel.getMessage()!=null){
                Toast.makeText(FateChatApplication.getInstance(), resultModel.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onSucceed(String url, ResultModel resultModel) throws JSONException,
            RemoteException {
        if (url.contains(URL_GET_DETAIL)) {
            if(mOnComLisener !=null){
                mOnComLisener.onGetComDetailSuccess((JHTuiJianBean)resultModel.getDataModel());
            }

        }
    }

    private OnComLisener mOnComLisener;

    public OnComLisener getmOnComLisener() {
        return mOnComLisener;
    }

    public void setmOnComLisener(OnComLisener mOnComLisener) {
        this.mOnComLisener = mOnComLisener;
    }

    public interface OnComLisener {
        void onGetComDetailSuccess(JHTuiJianBean list);
    }

}
