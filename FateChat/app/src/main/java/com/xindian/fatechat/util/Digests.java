/*******************************************************************************
 * Copyright (c) 2005, 2014 springside.github.io
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *******************************************************************************/
package com.xindian.fatechat.util;

import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.SecureRandom;

/**
 * 消息摘要的工具类.
 * 支持SHA-1/MD5这些安全性较高，返回byte[]-?(可用Encodes进一步被编码为Hex, Base64或UrlSafeBase64),支持带salt达到更高的安全-??.
 * 也支持crc32，murmur32这些不追求安全-?-，性能较高，返回int-?.
 * 
 * @author calvin
 */
public class Digests {

	private static final String SHA1 = "SHA-1";
	private static final String MD5 = "MD5";

	private static SecureRandom random = new SecureRandom();

	/**
	 * 对输入字符串进行MD5散列.
	 */
	public static String md5(byte[] input) {
		byte[] digest = digest(input, MD5, null, 1);
		return toHexString(digest);
	}

	public static String toHexString(byte[] digest) {
		StringBuffer hexValue = new StringBuffer();
		for (int i = 0; i < digest.length; i++) {
			int val = ((int) digest[i]) & 0xff;
			if (val < 16)
				hexValue.append("0");
			hexValue.append(Integer.toHexString(val));
		}
		return hexValue.toString();
	}

	/**
	 * 对输入字符串进行MD5散列.
	 */
	public static String md5(String input) {
		return md5(input.getBytes(Charset.defaultCharset()));
	}
	/**
	 * 对字符串进行散列, 支持md5与sha1算法.
	 */
	private static byte[] digest(byte[] input, String algorithm, byte[] salt, int iterations) {
		try {
			MessageDigest digest = MessageDigest.getInstance(algorithm);

			if (salt != null) {
				digest.update(salt);
			}

			byte[] result = digest.digest(input);

			for (int i = 1; i < iterations; i++) {
				digest.reset();
				result = digest.digest(result);
			}
			return result;
		} catch (GeneralSecurityException e) {
			 e.printStackTrace();
		}
		return null;
	}
}
