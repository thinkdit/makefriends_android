package com.xindian.fatechat.util;

import android.content.Context;

import java.net.URLEncoder;

/**
 */
public class MVUrlConstant {

    public static final int ON_JSON_GOT_SUCCESS=10;
    public static final int ON_JSON_GOT_FAIL=30;
    public static final String ON_PAGE_CHANGED="ON_PAGE_CHANGED";
    public static final String ON_REFRESH_START="ON_REFRESH_START";
    public static final String ON_REFRESH_COMPLETED="ON_REFRESH_COMPLETED";
    public static final int UP_FRESH_MESSAGE = 50;
    public static final int DOWN_FRESH_MESSAGE = 70;
    public static final int NO_MORE_MESSAGE = 90;
    public static final int GET_PLAY_MESSAGE = 100;

    public static String getVideoJH = "http://s.budejie.com/topic/list/jingxuan/41/budejie-android-6.6.6/0-" + "%s" + ".json?market=baidu&ver=6.6.6&visiting=&os=6.0&appname=baisibudejie&client=android&udid=352090060095762&mac=02%3A00%3A00%3A00%3A00%3A00";

    public static String getjinghuaDrawString = "http://s.budejie.com/topic/list/jingxuan/10/budejie-android-6.6.6/0-" + "%s" + ".json?market=baidu&ver=6.6.6&visiting=&os=6.0&appname=baisibudejie&client=android&udid=352090060095762&mac=02%3A00%3A00%3A00%3A00%3A00";

    public static String getJHShiPinJson = "http://s.budejie.com/topic/list/jingxuan/41/budejie-android-6.6.6/0-" + "%s" + ".json?market=baidu&ver=6.6.6&visiting=&os=6.0&appname=baisibudejie&client=android&udid=352090060095762&mac=02%3A00%3A00%3A00%3A00%3A00";

    public static String getJHDuanZiJson = "http://s.budejie.com/topic/list/jingxuan/29/budejie-android-6.6.6/0-" + "%s" + ".json?market=baidu&ver=6.6.6&visiting=&os=6.0&appname=baisibudejie&client=android&udid=352090060095762&mac=02%3A00%3A00%3A00%3A00%3A00";

    public static String getJHLengZhiShiJson = "http://s.budejie.com/topic/tag-topic/3176/hot/budejie-android-6.6.6/0-" + "%s" + ".json?market=baidu&ver=6.6.6&visiting=&os=6.0&appname=baisibudejie&client=android&udid=352090060095762&mac=02%3A00%3A00%3A00%3A00%3A00";

    public static String getJHMeiNvJson = "http://s.budejie.com/topic/tag-topic/117/hot/budejie-android-6.6.6/0-20.json?market=baidu&ver=6.6.6&visiting=&os=6.0&appname=baisibudejie&client=android&udid=352090060095762&mac=02%3A00%3A00%3A00%3A00%3A00";

    public static String getJHPaiHangJson = "http://s.budejie.com/topic/list/remen/1/budejie-android-6.6.6/0-" + "%s" + ".json?market=baidu&ver=6.6.6&visiting=&os=6.0&appname=baisibudejie&client=android&udid=352090060095762&mac=02%3A00%3A00%3A00%3A00%3A00";

    public static String getJHSheHuiJson = "http://s.budejie.com/topic/tag-topic/473/hot/budejie-android-6.6.6/0-" + "%s" + ".json?market=baidu&ver=6.6.6&visiting=&os=6.0&appname=baisibudejie&client=android&udid=352090060095762&mac=02%3A00%3A00%3A00%3A00%3A00";

    public static String getJHTuiJianJson = "http://s.budejie.com/topic/list/jingxuan/1/budejie-android-6.6.6/0-" + "%s" + ".json?market=baidu&ver=6.6.6&visiting=&os=6.0&appname=baisibudejie&client=android&udid=352090060095762&mac=02%3A00%3A00%3A00%3A00%3A00";

    public static String getJHWangHongJson = "http://s.budejie.com/topic/tag-topic/3096/hot/budejie-android-6.6.6/0-" + "%s" + ".json?market=baidu&ver=6.6.6&visiting=&os=6.0&appname=baisibudejie&client=android&udid=352090060095762&mac=02%3A00%3A00%3A00%3A00%3A00";

    public static String getJHYouXiJson = "http://s.budejie.com/topic/tag-topic/164/hot/budejie-android-6.6.6/0-" + "%s" + ".json?market=baidu&ver=6.6.6&visiting=&os=6.0&appname=baisibudejie&client=android&udid=352090060095762&mac=02%3A00%3A00%3A00%3A00%3A00";

    public static String getXTShengYinJson = "http://s.budejie.com/topic/list/zuixin/31/budejie-android-6.6.6/0-" + "%s" + ".json?market=baidu&ver=6.6.6&visiting=&os=6.0&appname=baisibudejie&client=android&udid=352090060095762&mac=02%3A00%3A00%3A00%3A00%3A00";

    public static String getXTQuanBuJson = "http://s.budejie.com/topic/list/zuixin/1/budejie-android-6.6.6/0-" + "%s" + ".json?market=baidu&ver=6.6.6&visiting=&os=6.0&appname=baisibudejie&client=android&udid=352090060095762&mac=02%3A00%3A00%3A00%3A00%3A00";

    public static String getXTShiPinJson = "http://s.budejie.com/topic/list/zuixin/41/budejie-android-6.6.6/0-" + "%s" + ".json?market=baidu&ver=6.6.6&visiting=&os=6.0&appname=baisibudejie&client=android&udid=352090060095762&mac=02%3A00%3A00%3A00%3A00%3A00";

    public static String getXTDuanZiJson = "http://s.budejie.com/topic/list/zuixin/29/budejie-android-6.6.6/0-" + "%s" + ".json?market=baidu&ver=6.6.6&visiting=&os=6.0&appname=baisibudejie&client=android&udid=352090060095762&mac=02%3A00%3A00%3A00%3A00%3A00";

    public static String getXTTuPianJson = "http://s.budejie.com/topic/list/zuixin/10/budejie-android-6.6.6/0-" + "%s" + ".json?market=baidu&ver=6.6.6&visiting=&os=6.0&appname=baisibudejie&client=android&udid=352090060095762&mac=02%3A00%3A00%3A00%3A00%3A00";

    public static String getXTWangHongJson = "http://s.budejie.com/topic/tag-topic/3096/new/budejie-android-6.6.6/0-" + "%s" + ".json?market=baidu&ver=6.6.6&visiting=&os=6.0&appname=baisibudejie&client=android&udid=352090060095762&mac=02%3A00%3A00%3A00%3A00%3A00";

    public static String getXTMeiNvJson = "http://s.budejie.com/topic/tag-topic/117/new/budejie-android-6.6.6/0-" + "%s" + ".json?market=baidu&ver=6.6.6&visiting=&os=6.0&appname=baisibudejie&client=android&udid=352090060095762&mac=02%3A00%3A00%3A00%3A00%3A00";

    public static String getXTLengZhiShiJson = "http://s.budejie.com/topic/tag-topic/3176/new/budejie-android-6.6.6/0-" + "%s" + ".json?market=baidu&ver=6.6.6&visiting=&os=6.0&appname=baisibudejie&client=android&udid=352090060095762&mac=02%3A00%3A00%3A00%3A00%3A00";

    public static String getXTYouXiJson = "http://s.budejie.com/topic/tag-topic/164/new/budejie-android-6.6.6/0-" + "%s" + ".json?market=baidu&ver=6.6.6&visiting=&os=6.0&appname=baisibudejie&client=android&udid=352090060095762&mac=02%3A00%3A00%3A00%3A00%3A00";

    public static String getPingLunJson = "http://c.api.budejie.com/topic/comment_list/" + "%s" + "/0/budejie-android-6.6.6/0-20.json";

    //public static String getPingLunJson = "http://s.budejie.com/topic/list/jingxuan/1/budejie-android-6.6.6/0-20.json?market=guanwang&ver=6.6.6&visiting=20170928&os=5.1.1&appname=baisibudejie&client=android&udid=A100004E5AD438&mac=dc%3A6d%3Acd%3Aab%3Aa7%3A66";

//    http://s.budejie.com/topic/list/jingxuan/1/budejie-android-6.6.6/0-19.json?market=baidu&ver=6.6.6&visiting=&os=6.0&appname=baisibudejie&client=android&udid=352090060095762&mac=02%3A00%3A00%3A00%3A00%3A00
//    http://s.budejie.com/topic/list/jingxuan/1/budejie-android-6.6.6/0-19.json?market=baidu&ver=6.6.6&visiting=&os=6.0&appname=baisibudejie&client=android&udid=352090060095762&mac=02%3A00%3A00%3A00%3A00%3A00
    /**
     * 初始化百思Url
     * @param context
     */
    public static void initBSUrls(Context context){
        if(context == null){
            return;
        }

        try {
            String[]urls = new String[]{getVideoJH,getjinghuaDrawString,getJHShiPinJson,getJHDuanZiJson,getJHLengZhiShiJson,
                    getJHMeiNvJson,getJHPaiHangJson,getJHSheHuiJson,getJHTuiJianJson,getJHWangHongJson
                    ,getJHYouXiJson,getXTShengYinJson,getXTQuanBuJson,getXTShiPinJson,getXTDuanZiJson,
                    getXTTuPianJson,getXTWangHongJson,getXTMeiNvJson,getXTLengZhiShiJson,getXTYouXiJson};

            String macStr = "20：30：79：80";
            String osVesion = android.os.Build.VERSION.RELEASE;
            String udid = "20：30：79：80";

            if(macStr != null && osVesion != null && udid!=null){
                for (int i =0;i<urls.length;i++) {
                    String macStrE = URLEncoder.encode(macStr,"UTF-8");
                    String url = urls[i];
                    url = url.replace("mac=02%3A00%3A00%3A00%3A00%3A00","mac="+macStrE);
                    url = url.replace("os=6.0","os="+osVesion);
                    url = url.replace("udid=352090060095762","udid="+udid);

                    switch (i){
                        case 0:
                            getVideoJH = url;
                            break;
                        case 1:
                            getjinghuaDrawString = url;
                            break;
                        case 2:
                            getJHShiPinJson = url;
                            break;
                        case 3:
                            getJHDuanZiJson = url;
                            break;
                        case 4:
                            getJHLengZhiShiJson = url;
                            break;
                        case 5:
                            getJHMeiNvJson = url;
                            break;
                        case 6:
                            getJHPaiHangJson = url;
                            break;
                        case 7:
                            getJHSheHuiJson = url;
                            break;
                        case 8:
                            getJHTuiJianJson = url;
                            break;
                        case 9:
                            getJHWangHongJson = url;
                            break;
                        case 10:
                            getJHYouXiJson = url;
                            break;
                        case 11:
                            getXTShengYinJson = url;
                            break;
                        case 12:
                            getXTQuanBuJson = url;
                            break;
                        case 13:
                            getXTShiPinJson = url;
                            break;
                        case 14:
                            getXTDuanZiJson = url;
                            break;
                        case 15:
                            getXTTuPianJson = url;
                            break;
                        case 16:
                            getXTWangHongJson = url;
                            break;
                        case 17:
                            getXTMeiNvJson = url;
                            break;
                        case 18:
                            getXTLengZhiShiJson = url;
                            break;
                        case 19:
                            getXTYouXiJson = url;
                            break;

                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }


    }

}
