package com.xindian.fatechat.ui.home.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.common.pullrefreshview.PullToRefreshView;
import com.xindian.fatechat.R;
import com.xindian.fatechat.common.Constant;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.ui.base.BaseFragment_v4;
import com.xindian.fatechat.ui.home.SendComActivity;
import com.xindian.fatechat.ui.home.adapter.ComprehensiveAdapter;
import com.xindian.fatechat.ui.home.model.JHTuiJianBean;
import com.xindian.fatechat.ui.home.presenter.ComListPresenter;
import com.xindian.fatechat.widget.player.VideoPlayerHelper;

import java.util.List;

/**
 * 社区
 */
public class ComFragment extends BaseFragment_v4 {


    PullToRefreshView ptrlComprehensive;
    ListView mListView;
    View view;
    private int page = 1;
    private  static final String TAG = "testv";
    private ComprehensiveAdapter comprehensiveAdapter;
    public static final int TYPE_GC = 1;//帖子类型：广场
    public static final int TYPE_MY = 2;//帖子类型：自己的帖子
    private int type = TYPE_GC;//帖子类型：广场或者自己的帖子
    public void setType(int type){
        this.type = type;
        page = 1;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.comprehensive_layout, container, false);
            ptrlComprehensive = ((PullToRefreshView) view.findViewById(R.id.ptrlComprehensive));
            mListView = (ListView)view.findViewById(R.id.rv_List);

            setTitleBar();
            setFrescoGetImagePipelineOnPtrStopScroll();
            comprehensiveAdapter = new ComprehensiveAdapter(getContext(), null);
            if(type == TYPE_MY){
                comprehensiveAdapter.setShowStadus(true);
            }
            mListView.setAdapter(comprehensiveAdapter);
            setPtrListViewRefreshEvent();
        }
        return view;
    }

    String title;
    public void setTitle(String title){
        this.title = title;
    }

    private void setTitleBar(){
        if(this.title == null){
            ((TextView)view.findViewById(R.id.tv_toolbar_title)).setText(R.string.title_com);
        }else {
            ((TextView)view.findViewById(R.id.tv_toolbar_title)).setText(title);
            ((ImageButton)view.findViewById(R.id.iv_title_bar_back)).setVisibility(View.VISIBLE);

        }
        ((ImageButton)view.findViewById(R.id.ibtn_toolbar_right)).setVisibility(View.VISIBLE);
        ((ImageButton)view.findViewById(R.id.ibtn_toolbar_right)).setImageResource(R.drawable.bu_fabu);
        view.findViewById(R.id.ibtn_toolbar_right).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SendComActivity.class);
                startActivity(intent);
            }
        });
        view.findViewById(R.id.iv_title_bar_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
    }

    //设置ptr刷新事件
    private void setPtrListViewRefreshEvent() {
        ptrlComprehensive.setListener(new PullToRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                getDataFromNet(page,ComListPresenter.PAGE_SIZE);
            }

            @Override
            public void onLoadMore() {
                page++;
                getDataFromNet(page,ComListPresenter.PAGE_SIZE);
            }
        });
    }

    //设置fresco在滚动时停止加载数据
    private void setFrescoGetImagePipelineOnPtrStopScroll() {
        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                int curPlayPosition = VideoPlayerHelper.getInstance().getCurrPlayPosition();
                int lastPlayPosition = VideoPlayerHelper.getInstance().getLastPlayPosition();
                if (curPlayPosition != -1 && (curPlayPosition < mListView.getFirstVisiblePosition() ||
                        curPlayPosition > mListView.getLastVisiblePosition())) {
                    VideoPlayerHelper.getInstance().stop();//划出界面则停止播放
                } else if (curPlayPosition == -1 && lastPlayPosition >= mListView.getFirstVisiblePosition()
                        && lastPlayPosition <= mListView.getLastVisiblePosition()) {
                    VideoPlayerHelper.getInstance().stop();//划出界面则停止播放
                }
            }
        });
    }

    public static JHTuiJianBean paresJHTuiJianBean(String json){
        JHTuiJianBean jhTuiJianBean=null;
        try {
            jhTuiJianBean= JSON.parseObject(json, JHTuiJianBean.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  jhTuiJianBean;
    }

    ComListPresenter mComListPresenter;
    //从网络加载数据
    public void getDataFromNet(int page, int pageSize) {

        if(mComListPresenter == null){
            mComListPresenter = new ComListPresenter();
        }
        mComListPresenter.setmOnComLisener(new ComListPresenter.OnComLisener() {
            @Override
            public void onGetComListSuccess(List<JHTuiJianBean> list) {
                //对用户头像进行地址加参
                for(int i=0;i<list.size();i++){
                    JHTuiJianBean user = list.get(i);
                    StringBuffer temp=new StringBuffer(user.getHeadUrl());
                    temp.append(Constant.IMAGE_200);
                    user.setHeadUrl(temp.toString());
                }

                ptrlComprehensive.onFinishLoading();
                if(page == 1){
                    comprehensiveAdapter.setData(list);
                }else{
                    comprehensiveAdapter.addData(list);
                }
            }
        });
        long userId = UserInfoManager.getManager(getActivity()).getUserId();
        String token = UserInfoManager.getManager(getActivity()).getToken();
        if(type == TYPE_GC){
            mComListPresenter.getComList(userId+"",token,page,pageSize);
        }else{
            mComListPresenter.getMyComList(userId+"",token,page,pageSize);
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        getDataFromNet(page,ComListPresenter.PAGE_SIZE);
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //TODO  ButterKnife.unbind(this);
    }

}
