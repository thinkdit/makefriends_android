package com.xindian.fatechat.ui.user.presenter;

import android.content.Context;
import android.os.RemoteException;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.sdk.android.oss.ClientConfiguration;
import com.alibaba.sdk.android.oss.ClientException;
import com.alibaba.sdk.android.oss.OSSClient;
import com.alibaba.sdk.android.oss.ServiceException;
import com.alibaba.sdk.android.oss.callback.OSSCompletedCallback;
import com.alibaba.sdk.android.oss.common.auth.OSSCredentialProvider;
import com.alibaba.sdk.android.oss.common.auth.OSSStsTokenCredentialProvider;
import com.alibaba.sdk.android.oss.internal.OSSAsyncTask;
import com.alibaba.sdk.android.oss.model.DeleteObjectRequest;
import com.alibaba.sdk.android.oss.model.DeleteObjectResult;
import com.alibaba.sdk.android.oss.model.ObjectMetadata;
import com.alibaba.sdk.android.oss.model.PutObjectRequest;
import com.alibaba.sdk.android.oss.model.PutObjectResult;
import com.thinkdit.lib.util.L;
import com.thinkdit.lib.util.StringUtil;
import com.xindian.fatechat.R;
import com.xindian.fatechat.common.BasePresenter;
import com.xindian.fatechat.common.ResultModel;
import com.xindian.fatechat.ui.user.model.AliStsBean;

import org.json.JSONException;

import java.util.HashMap;
import java.util.Map;

/**
 * 文件上传
 */
public class FileUploadPresenter extends BasePresenter {

    public static final int FILE_TYPE_HEAD = 1; //用户头像
    public static final int FILE_TYPE_VIEDO = 2; //动态视频
    public static final int FILE_TYPE_VIEDO_COVER = 3; //动态视频封面
    public static final int FILE_TYPE_PICTURE = 4; //动态图文
    public static final int FILE_TYPE_LIVE_COVER = 5; //直播封面
    public static final int FILE_TYPE_LIVE_TRAILER = 6; //直播预告封面
    public static final int FILE_TYPE_IDCARD = 7; //身份认证照片

    private static final String TAG = "httputil";
    private final String ALI_STS = "/ali-sts/oss-st";

    private IFileUploadedListener mListener;
    private Context mContext;
    private String mLocalFilePath;
    private int mFileType;
    private String mServerFilePath;
    private String mEndpoint;
    private String mDnsEndpoint;
    private OSSClient mOSSClient;
    private OSSAsyncTask mCurrentTask;
    private AliStsBean mAliStsBean;

    
    
    public FileUploadPresenter(Context context, IFileUploadedListener listener) {
        mContext = context;
        mListener = listener;
    }

    public void startUpload(String localFilePath, int fileType) {
        mLocalFilePath = localFilePath;
        mFileType = fileType;
        Map<String, Object> map = new HashMap<>();
        map.put("userId", 100);
        get(getUrl(ALI_STS), map, mContext);
    }

    @Override
    public void onSucceed(String url, ResultModel resultModel) throws JSONException, RemoteException {
        if (url.contains(ALI_STS)) {
            AliStsBean bean = (AliStsBean) resultModel.getDataModel();
            ossUpload(bean, mLocalFilePath);
        }
    }

    @Override
    public Object asyncExecute(String url, ResultModel resultModel) {
        if (url.contains(ALI_STS)) {
            return JSON.parseObject(resultModel.getData(), AliStsBean.class);
        }
        return null;
    }

    @Override
    public void onFailure(String url, ResultModel resultModel) {
        super.onFailure(url, resultModel);
//        Toast.makeText(mContext, resultModel.getMessage(), Toast.LENGTH_LONG).show();
        if (mListener != null) {
            mListener.onFileUploadedFailure();
        }
    }

    public static String getStreamType(int fileType) {
        switch (fileType) {
            case FILE_TYPE_HEAD:
            case FILE_TYPE_VIEDO_COVER:
            case FILE_TYPE_PICTURE:
            case FILE_TYPE_LIVE_COVER:
            case FILE_TYPE_LIVE_TRAILER:
            case FILE_TYPE_IDCARD:
                return "image/jpeg";
            case FILE_TYPE_VIEDO:
                return "video/*";
        }
        return null;
    }

    public static String getFileName(int fileType) {
        switch (fileType) {
            case FILE_TYPE_HEAD:
                return "avatar_" + System.currentTimeMillis() + ".jpg";
            case FILE_TYPE_VIEDO:
                return "video_" + System.currentTimeMillis() + ".mp4";
            case FILE_TYPE_VIEDO_COVER:
                return "cover_" + System.currentTimeMillis() + ".jpg";
            case FILE_TYPE_PICTURE:
                return "pic_" + System.currentTimeMillis() + ".jpg";
            case FILE_TYPE_LIVE_COVER:
                return "live_cover_" + System.currentTimeMillis() + ".jpg";
            case FILE_TYPE_LIVE_TRAILER:
                return "live_trailer_" + System.currentTimeMillis() + ".jpg";
            case FILE_TYPE_IDCARD:
                return "idcard_" + System.currentTimeMillis() + ".jpg";
        }
        return "unkown";
    }

    private String getFilePath() {
        return 1 + "/";
    }


    private void initOss(AliStsBean bean) {
        mAliStsBean = bean;
   
        if(mOSSClient==null)
        {
            OSSCredentialProvider credentialProvider = new OSSStsTokenCredentialProvider(
                    bean.getCredential().getAccessKeyId(), bean.getCredential().getAccessKeySecret(), bean.getCredential().getSecurityToken());
            mEndpoint = formatUrl(bean.getCdnEndpoint().trim(),null);
            ClientConfiguration conf = new ClientConfiguration();
            conf.setConnectionTimeout(20 * 1000); // 连接超时，默认15秒
            conf.setSocketTimeout(20 * 1000); // socket超时，默认15秒
            conf.setMaxConcurrentRequest(8); // 最大并发请求数，默认5个
            conf.setMaxErrorRetry(2); // 失败后最大重试次数，默认2次
            mOSSClient = new OSSClient(mContext.getApplicationContext(), mEndpoint, credentialProvider, conf);
        }
        mDnsEndpoint = formatUrl(bean.getCdnEndpoint().trim(),bean.getBucket());
        mServerFilePath = getFilePath() + getFileName(mFileType);
        
    }

    private String formatUrl(String url, String headHost) {
        if(!StringUtil.isNullorEmpty(headHost)){
            url = headHost+"."+url;
        }
        if (!url.startsWith("http")) {
            url = "http://" + url;
        }
        return url;
    }

    private void ossUpload(AliStsBean bean, String uploadFilePath) {
        initOss(bean);
        // 指定数据类型，没有指定会自动根据后缀名判断
        ObjectMetadata objectMeta = new ObjectMetadata();
        objectMeta.setContentType(getStreamType(mFileType));
        // 构造上传请求
        PutObjectRequest put = new PutObjectRequest(bean.getBucket(), mServerFilePath, uploadFilePath);
        put.setMetadata(objectMeta);
        // 异步上传时可以设置进度回调
        put.setProgressCallback((PutObjectRequest request, long currentSize, long totalSize) -> {
            L.d(TAG, "progress " + (currentSize * 100) / totalSize);
            // 在这里可以实现进度条展现功能
            if (mListener != null) {
                mListener.onFileUploadProgress(currentSize, totalSize);
            }
        });
        mCurrentTask = mOSSClient.asyncPutObject(put, new OSSCompletedCallback<PutObjectRequest, PutObjectResult>() {
            @Override
            public void onSuccess(PutObjectRequest request, PutObjectResult result) {
                if (mListener != null) {
                    String url = mDnsEndpoint + "/" + mServerFilePath;
                    L.d(TAG, "UploadSuccess " + url);
                    mListener.onFileUploadedSuccess(url);
                }
            }

            @Override
            public void onFailure(PutObjectRequest request, ClientException clientExcepion, ServiceException serviceException) {
                // 请求异常
                if (clientExcepion != null) {
                    // 本地异常如网络异常等
                    clientExcepion.printStackTrace();
                }
                if (serviceException != null) {
                    // 服务异常
                    L.e(TAG, serviceException.getErrorCode());
                    L.e(TAG, serviceException.getRequestId());
                    L.e(TAG, serviceException.getHostId());
                    L.e(TAG, serviceException.getRawMessage());
                }
                if (mListener != null) {
                    mListener.onFileUploadedFailure();
                }
                Toast.makeText(mContext, R.string.user_file_upload_error, Toast.LENGTH_LONG).show();
                return;
            }
        });
    }

    public void destroy() {
        mListener = null;
        mOSSClient = null;
        if (mCurrentTask != null && !mCurrentTask.isCompleted()) {
            mCurrentTask.cancel();
        }
        mCurrentTask = null;
    }

    public void deleteObject(String url) {
        if (StringUtil.isNullorEmpty(url)) {
            return;
        }
        int index = url.lastIndexOf("/");
        String fileName = url.substring(index + 1);
        L.d(TAG, "delete " + getFilePath() + fileName);
        // 创建删除请求
        DeleteObjectRequest delete = new DeleteObjectRequest(mAliStsBean.getBucket(),
                getFilePath() + fileName);
        // 异步删除
        mCurrentTask = mOSSClient.asyncDeleteObject(delete, new OSSCompletedCallback<DeleteObjectRequest, DeleteObjectResult>() {
            @Override
            public void onSuccess(DeleteObjectRequest request, DeleteObjectResult result) {
                L.d(TAG, "delete success!");
            }

            @Override
            public void onFailure(DeleteObjectRequest request, ClientException clientExcepion, ServiceException serviceException) {
                // 请求异常
                if (clientExcepion != null) {
                    // 本地异常如网络异常等
                    clientExcepion.printStackTrace();
                }
                if (serviceException != null) {
                    // 服务异常
                    L.e(TAG, "ErrorCode" + serviceException.getErrorCode());
                    L.e(TAG, "RequestId" + serviceException.getRequestId());
                    L.e(TAG, "HostId" + serviceException.getHostId());
                    L.e(TAG, "RawMessage" + serviceException.getRawMessage());
                }
            }

        });
    }

    public interface IFileUploadedListener {
        public void onFileUploadProgress(long currentSize, long totalSize);

        public void onFileUploadedSuccess(String url);

        public void onFileUploadedFailure();
    }
}
