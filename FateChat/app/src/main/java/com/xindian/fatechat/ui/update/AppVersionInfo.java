package com.xindian.fatechat.ui.update;


import com.xindian.fatechat.common.BaseModel;

/**
 * Created by zjzhu on 16-7-11.
 */
public class AppVersionInfo extends BaseModel {

    /**
     * versionNum : 1000000
     * versionName : 1.0.0
     * haveUpgrade : true
     * upgradeVersionNum : 1001000
     * upgradeVersionName : 1.1.0
     * downloadUrl : www.baidu.com
     * upgradeDescription : 分支版本
     * forceUpgrade : false
     */

    private int versionNum;
    private String versionName;
    private boolean haveUpgrade;
    private int upgradeVersionNum;
    private String upgradeVersionName;
    private String downloadUrl;
    private String upgradeDescription;
    private boolean forceUpgrade;

    public int getVersionNum() {
        return versionNum;
    }

    public void setVersionNum(int versionNum) {
        this.versionNum = versionNum;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public boolean isHaveUpgrade() {
        return haveUpgrade;
    }

    public void setHaveUpgrade(boolean haveUpgrade) {
        this.haveUpgrade = haveUpgrade;
    }

    public int getUpgradeVersionNum() {
        return upgradeVersionNum;
    }

    public void setUpgradeVersionNum(int upgradeVersionNum) {
        this.upgradeVersionNum = upgradeVersionNum;
    }

    public String getUpgradeVersionName() {
        return upgradeVersionName;
    }

    public void setUpgradeVersionName(String upgradeVersionName) {
        this.upgradeVersionName = upgradeVersionName;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public String getUpgradeDescription() {
        return upgradeDescription;
    }

    public void setUpgradeDescription(String upgradeDescription) {
        this.upgradeDescription = upgradeDescription;
    }

    public boolean isForceUpgrade() {
        return forceUpgrade;
    }

    public void setForceUpgrade(boolean forceUpgrade) {
        this.forceUpgrade = forceUpgrade;
    }
}
