package com.xindian.fatechat.ui.home;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.thinkdit.lib.util.StringUtil;
import com.xindian.fatechat.R;
import com.xindian.fatechat.common.FateChatApplication;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.ui.base.BaseActivity;
import com.xindian.fatechat.ui.home.model.JHTuiJianBean;
import com.xindian.fatechat.ui.home.presenter.ComEditPresenter;
import com.xindian.fatechat.ui.login.presenter.AccountPresenter;
import com.xindian.fatechat.util.imageSelectorUtil;
import com.xindian.fatechat.widget.ComRemindDialog;
import com.xindian.fatechat.widget.MyProgressDialog;
import com.yancy.imageselector.ImageSelector;
import com.yancy.imageselector.ImageSelectorActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 发送状态
 */

public class SendComActivity extends BaseActivity implements AccountPresenter.IAvatarUploadedListener{

    @BindView(R.id.edit_content)
    EditText editContent;
    @BindView(R.id.edit_image)
    ImageView edit_image;
    @BindView(R.id.bu_send_image)
    ImageView bu_send_image;
    MyProgressDialog myProgressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_send_com);
        ButterKnife.bind(this);

        setTitleBar();
        doRemindCom();
        myProgressDialog = new MyProgressDialog(this);
    }

    private void setTitleBar(){
        ((TextView)findViewById(R.id.tv_toolbar_title)).setText("发帖子");
        initToolBarWithRightMenu("发送", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editCom();
            }
        });
    }

    ComRemindDialog mReComDialog;
    private void doRemindCom() {
//        if (!UserInfoManager.getManager(this).isLogin()) {
//            return;
//        }
//        int userId = UserInfoManager.getManager(this).getUserId();
//        boolean isHasSay = SharePreferenceUtils.getBoolean(this, SharePreferenceUtils.KEY_HAS_REM_COM + userId, false);
//        if (!isHasSay) {
            if (mReComDialog == null) {
                mReComDialog = new ComRemindDialog(SendComActivity.this);
            }
            mReComDialog.show();
//        }

    }

    private AccountPresenter accountPresenter;
    private imageSelectorUtil imageSelectorUtil;
    @OnClick(R.id.bu_send_image)
    void onImageUp(View v)
    {
        if(imageSelectorUtil == null){
            imageSelectorUtil=new imageSelectorUtil(this);
            imageSelectorUtil.delCrop();
        }

        imageSelectorUtil.changePortrait();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            if (requestCode == ImageSelector.IMAGE_REQUEST_CODE) {
                // Get Image Path List
                List<String> pathList = data.getStringArrayListExtra(ImageSelectorActivity.EXTRA_RESULT);
                String path = pathList.get(pathList.size() - 1);

                if(accountPresenter == null){
                    accountPresenter=new AccountPresenter(this);
                    accountPresenter.setAvatarUploadedListener(this);
                }
                accountPresenter.uploadAvatar(path);
                myProgressDialog.show();

            }
        }
    }

    String upImageUrl;
    @Override
    public void onAvatarUploadedSucceed(String url) {
        upImageUrl = url;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Glide.with(edit_image.getContext()).load(upImageUrl)
                        .fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(edit_image);
                myProgressDialog.dismiss();
            }
        });

    }

    @Override
    public void onAvatarUploadedError() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                myProgressDialog.dismiss();
            }
        });
    }


    ComEditPresenter mComListPresenter;
    //从网络加载数据
    public void editCom() {

        if(mComListPresenter == null){
            mComListPresenter = new ComEditPresenter(this);
            mComListPresenter.setmOnComLisener(new ComEditPresenter.OnComLisener() {
                @Override
                public void onComEditSuccess() {
                    Toast.makeText(FateChatApplication.getInstance(), "编辑帖子提交成功，请等待审核", Toast.LENGTH_SHORT).show();
                    finish();
                }
            });
        }
        String dyContent = editContent.getText().toString().trim();
        if(StringUtil.isNullorEmpty(dyContent)){
            Toast.makeText(FateChatApplication.getInstance(), "请输入文字内容！", Toast.LENGTH_SHORT).show();
            return;
        }
        int type = JHTuiJianBean.TYPE_TEXT;
        if(upImageUrl!=null){
            type = JHTuiJianBean.TYPE_IMAGE;
        }
        long userId = UserInfoManager.getManager(this).getUserId();
        String token = UserInfoManager.getManager(this).getToken();
        mComListPresenter.addCom(userId+"",token,dyContent,upImageUrl,type);
    }
}
