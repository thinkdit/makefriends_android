package com.xindian.fatechat.manager;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.FileProvider;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.hyphenate.easeui.manager.DownloadManager;
import com.thinkdit.lib.util.L;
import com.xindian.fatechat.R;

import java.io.File;
import java.io.IOException;

import static com.igexin.sdk.GTServiceManager.context;


/**
 * 更新服务
 */
public class AppUpdateManager implements DownloadManager.IDownloadListener {
    private static final String TAG = "apkupdate";

    private static final int NOTIFY_ID = 101030;

    private static final int MSG_DOWNLOAD_START = 1;
    private static final int MSG_DOWNLOAD_COMPLETE = 2;
    private static final int MSG_DOWNLOAD_PROGRESS = 3;
    private static final int MSG_DOWNLOAD_FAILURE = 4;

    private String mDownloadPath = "";
    private String APK_NAME = "microdisk.apk";
    private Context mContext;
    private DownloadManager mDownloadManager;
    private NotificationManager mNotificationManager;
    private Notification mNotification;
    private int mCurProgress = 0;
    private OnUpdateCallback mCallback;

    public AppUpdateManager(Context context, OnUpdateCallback callback) {
        mContext = context.getApplicationContext();
        mCallback = callback;
        mDownloadManager = new DownloadManager(this);
        mNotificationManager = (NotificationManager) context.getSystemService(Context
                .NOTIFICATION_SERVICE);
    }

    public void startDownload(String url) {
        if (init()) {
            //"http://www.apk3.com/uploads/soft/20160511/IN.apk"
            mDownloadManager.startDownload(url, mDownloadPath);
        }
    }

    private boolean init() {
        //判断sd卡是否存在
        boolean sdCardExist = Environment.getExternalStorageState().equals(Environment
                .MEDIA_MOUNTED);
        if (sdCardExist) {
            mDownloadPath = Environment.getExternalStorageDirectory().getPath() + "/" +
                    mContext.getPackageName() + "/apk/";//获取跟目录
        } else {
            mDownloadPath = mContext.getCacheDir() + "/apk/";
        }
        L.d(TAG, "apk path =" + mDownloadPath);
        File pathFile = new File(mDownloadPath);
        if (!pathFile.exists()) { //下载文件路径不存在则新建
            pathFile.mkdirs();
        }
        File appFile = new File(mDownloadPath + APK_NAME); //下載文件存在則刪除
        if (appFile.exists()) {
            appFile.delete();
        }
        try {
            appFile.createNewFile();
        } catch (IOException e) {
            Toast.makeText(mContext,"下载失败，请检查应用是否拥有下载文件权限！",Toast.LENGTH_SHORT).show();
            L.e(TAG, "init file error", e);
            return false;
        }
        mDownloadPath = mDownloadPath + APK_NAME;
        return true;
    }

    private void installApk() {
//        Intent intent = new Intent(Intent.ACTION_VIEW);
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        intent.setDataAndType(Uri.fromFile(new File(mDownloadPath)), "application/vnd.android" +
//                ".package-archive");
//        mContext.startActivity(intent);

        Intent intent = new Intent(Intent.ACTION_VIEW);
        //判断是否是AndroidN以及更高的版本
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            String appMV = "com.xindian.fatechat." + "freeChannel." + "provider";
            try {
                ApplicationInfo appInfo = mContext.getPackageManager().getApplicationInfo(mContext.getPackageName(), PackageManager.GET_META_DATA);
                appMV = appInfo.metaData.getString("AUTH");
            }catch (Exception e){
                e.printStackTrace();
            }
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            Uri contentUri = FileProvider.getUriForFile(context,appMV, new File(mDownloadPath));
            intent.setDataAndType(contentUri, "application/vnd.android.package-archive");
        } else {
            intent.setDataAndType(Uri.fromFile(new File(mDownloadPath)), "application/vnd.android.package-archive");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        mContext.startActivity(intent);
    }

    @Override
    public void onDownloadStart() {
        mHandler.sendEmptyMessage(MSG_DOWNLOAD_START);
    }

    @Override
    public void onDownloadProgress(long count, long current) {
        int progress = (int) (100 * current / count);
        mHandler.sendMessage(mHandler.obtainMessage(MSG_DOWNLOAD_PROGRESS, progress, progress));
    }

    @Override
    public void onDownloadSuccess(String url, String localPath) {
        mHandler.sendEmptyMessage(MSG_DOWNLOAD_COMPLETE);
    }

    @Override
    public void onDownloadFailure() {
        mHandler.sendEmptyMessage(MSG_DOWNLOAD_FAILURE);
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_DOWNLOAD_COMPLETE:
                    mCallback.onUpdateEnd();
                    mNotificationManager.cancel(NOTIFY_ID);
                    installApk();
                    break;
                case MSG_DOWNLOAD_START:
                    mCallback.onUpdateStart();
                    setUpNotification();
                    break;
                case MSG_DOWNLOAD_PROGRESS:
                    int progress = msg.arg1;
                    if (progress != mCurProgress) {
                        mCallback.onUpdateProgress(progress);
                        mCurProgress = progress;
                        RemoteViews contentview = mNotification.contentView;
                        contentview.setTextViewText(R.id.status, progress < 100 ? (progress +
                                "%") : mContext.getString(R.string.update_complete));
                        contentview.setProgressBar(R.id.progressbar, 100, progress, false);
                        mNotificationManager.notify(NOTIFY_ID, mNotification);
                    }
                    break;
                case MSG_DOWNLOAD_FAILURE:
                    mCallback.onUpdateEnd();
                    mNotificationManager.cancel(NOTIFY_ID);
                    Toast.makeText(mContext, R.string.update_fail, Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    private void setUpNotification() {
        int icon = R.drawable.app_icon_1;
        CharSequence tickerText = mContext.getString(R.string.app_name) + mContext.getString(R
                .string.update_start);
        long when = System.currentTimeMillis();
        mNotification = new Notification(icon, tickerText, when);
        // 放置在"正在运行"栏目中
        mNotification.flags = Notification.FLAG_ONGOING_EVENT;
        RemoteViews contentView = new RemoteViews(mContext.getPackageName(), R.layout
                .layout_update_download_notification);
        contentView.setTextViewText(R.id.status, tickerText);
        contentView.setProgressBar(R.id.progressbar, 100, mCurProgress, false);
        // 指定个性化视图
        mNotification.contentView = contentView;
        PendingIntent contentIntent = PendingIntent.getActivity(mContext, 0, new Intent(),
                PendingIntent.FLAG_UPDATE_CURRENT);
        // 指定内容意图
        mNotification.contentIntent = contentIntent;
        mNotificationManager.notify(NOTIFY_ID, mNotification);
    }

    public interface OnUpdateCallback {
        public void onUpdateStart();

        public void onUpdateProgress(int progress);

        public void onUpdateEnd();
    }
}
