package com.xindian.fatechat.ui.user;

import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.hyphenate.easeui.model.EventBusModel;
import com.hyphenate.easeui.utils.SnackbarUtil;
import com.jyd.paydemo.Bean;
import com.jyd.paydemo.XianlaiWxH5Pay;
import com.thinkdit.lib.util.DeviceInfoUtil;
import com.xindian.fatechat.R;
import com.xindian.fatechat.databinding.ActivityDiamodnsCashBinding;
import com.xindian.fatechat.manager.PayTypeManager;
import com.xindian.fatechat.manager.UmengEventManager;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.pay.SelectPayWindow;
import com.xindian.fatechat.pay.alipay.AilPayPresenter;
import com.xindian.fatechat.pay.model.PrePayInfoBean;
import com.xindian.fatechat.pay.model.ProductBean;
import com.xindian.fatechat.pay.model.XianLaiWxQueryBean;
import com.xindian.fatechat.pay.presenter.PayPresenter;
import com.xindian.fatechat.pay.wxpay.WxReportPrePayInfoReceiver;
import com.xindian.fatechat.pay.wxpay.WxpayPresenter;
import com.xindian.fatechat.ui.base.BaseActivity;
import com.xindian.fatechat.ui.home.fragment.HomeFragment;
import com.xindian.fatechat.ui.user.model.PayTypeBean;
import com.xindian.fatechat.ui.user.model.User;
import com.xindian.fatechat.ui.user.presenter.CashPresenter;
import com.xindian.fatechat.wxapi.WXPayEntryActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.switfpass.pay.MainApplication.getContext;

public class DiamodnsCashActivity extends BaseActivity implements 
       PayPresenter.onPrePayInfoListener,View.OnClickListener,
        AilPayPresenter.OnAilPayListener,UserInfoManager.IUserInfoUpdateListener,
        CashPresenter.onPayTypeInfoListener,CashPresenter.onGetProductListListener,PayPresenter.onPayInfoByXianLaiWxListener,PayPresenter.onPrePayInfoByXianLaiListener{
    public static final String DIAMODNSBALANCE="balance";
    private ActivityDiamodnsCashBinding mBinding;
    private int  diamodnsBalance;//钻石余额
    private CashPresenter cashPresenter;
    private PayPresenter    prePayPresenter;
    private AilPayPresenter payPresenter;
    private WxpayPresenter wxpayPresenter;
    private int productId;//当前预支付的产品ID
    private int productMoneny;//当前预支付的moneny
    private List<ProductBean> productBeanList;
    private Bean mBean;//闲来微信支付数据bean
    private int payType;//当前支付方式 1->微信 2->支付宝
    private PrePayInfoBean tempPrePayInfoBean;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding= DataBindingUtil.setContentView(this, R.layout.activity_diamodns_cash);
        initToolBar();
        diamodnsBalance = getIntent().getIntExtra(DIAMODNSBALANCE, 0);
        EventBus.getDefault().register(this);
        intiViews();
    }

    private void intiViews() {
        mBinding.txtDiaBalance.setText(diamodnsBalance+"");
        //请求支付方式渠道
        cashPresenter=new CashPresenter(this,this);
        prePayPresenter=new PayPresenter(this,this);
        wxpayPresenter=new WxpayPresenter(this);
        cashPresenter.setOnPayTypeInfoListener(this);
        prePayPresenter.setOnPrePayInfoByXianLaiListener(this);
        prePayPresenter.setOnPayInfoByXianLaiWxListener(this);
//        cashPresenter.getPayType(PayTypeManager.PAY_WX);
//        cashPresenter.getPayType(PayTypeManager.PAY_ZFB);
        cashPresenter.requestGetProductListWithDiamonds(UserInfoManager.getManager(this).getUserInfo());
        UserInfoManager.getManager(this).addUserInfoUpdateListener(this);
        //初始化充值金额按钮点击事件
        mBinding.btnDiaCash100.setOnClickListener(this);
        mBinding.btnDiaCash500.setOnClickListener(this);
        mBinding.btnDiaCash1000.setOnClickListener(this);
        mBinding.btnDiaCash3000.setOnClickListener(this);
        mBinding.btnDiaCash6000.setOnClickListener(this);
        
        //初始化支付选择的tab
        mBinding.tab.addTab(mBinding.tab.newTab().setText("微信支付"),true);
        mBinding.tab.addTab(mBinding.tab.newTab().setText("支付宝"));
        mBinding.tab.post(new Runnable() {
            @Override
            public void run() {
                HomeFragment.setIndicator(mBinding.tab,(int) getResources().getDimension(R.dimen.tab_padding),(int) getResources().getDimension(R.dimen.tab_padding));
            }
        });
       
    }

    /****
     * 点击充值数量唤起支付
     * @param v
     */
    @Override
    public void onClick(View v) {
        //产品少于五个时按钮将隐藏
        switch (v.getId())
        {
            case R.id.btn_dia_cash_100:
                productId=productBeanList.get(0).getPid();
                productMoneny=productBeanList.get(0).getMoney();
                break;
            case R.id.btn_dia_cash_500:
                productId=productBeanList.get(1).getPid();
                productMoneny=productBeanList.get(1).getMoney();
                break;
            case R.id.btn_dia_cash_1000:
                productId=productBeanList.get(2).getPid();
                productMoneny=productBeanList.get(2).getMoney();
                break;
            case R.id.btn_dia_cash_3000:
                productId=productBeanList.get(3).getPid();
                productMoneny=productBeanList.get(3).getMoney();
                break;
            case R.id.btn_dia_cash_6000:
                productId=productBeanList.get(4).getPid();
                productMoneny=productBeanList.get(4).getMoney();
                break;
        }
        User user= UserInfoManager.getManager(this).getUserInfo();
        //微信支付预支付获取在wxPresenter进行，获取成功后会直接请求支付，支付宝需在onGetPrePayInfoSuccess调payPresenter.startPayByAilPay
        if(mBinding.tab.getSelectedTabPosition()==0){
            if(PayTypeManager.getInstance().getWxPayChannel()==null||PayTypeManager.getInstance().getWxPayChannel().equals(""))
            {
                Toast.makeText(this,"抱歉,微信支付正在维护中，请选择其他支付方式。",Toast.LENGTH_SHORT).show();
                return;
            }
            ProductBean bean=new ProductBean();
            bean.setMoney(productMoneny);
            bean.setPid(productId);
            wxpayPresenter.WxPayByXianLai(bean,UserInfoManager.getManager(getContext()).getUserId());
          
        }else if(mBinding.tab.getSelectedTabPosition()==1){
            if(PayTypeManager.getInstance().getZfbPayChannel()==null||PayTypeManager.getInstance().getZfbPayChannel().equals(""))
            {
                Toast.makeText(this,"抱歉,支付宝支付正在维护中，请选择其他支付方式。",Toast.LENGTH_SHORT).show();
                return;
            }
            prePayPresenter.reportPayInfoXianLai(UserInfoManager.getManager(getContext()).getUserInfo().getUserId(), DeviceInfoUtil.getDeviceId(this),
                    "xianlaijiaoyou","xianlaijiaoyou",DeviceInfoUtil.getDevice(),productMoneny,2);
    }
        SnackbarUtil.makeSnackBar(mBinding.getRoot(),"开始请求支付,请稍等!", Snackbar.LENGTH_LONG).setMeessTextColor(getResources().getColor(R.color.white)).show();
      
    }


    /***
     * 获取支付渠道
     * @param 
     */
    @Override
    public void onPayTypeInfoSuccess(PayTypeBean bean) {
        if(bean.getType()==PayTypeManager.PAY_WX) {
            PayTypeManager.getInstance().setWxPayChannel(PayTypeManager.WXPayChannel.valueOf(bean.getCode()));
        }else if(bean.getType()==PayTypeManager.PAY_ZFB)
        {
            PayTypeManager.getInstance().setZfbPayChannel(PayTypeManager.ZFBPayChannel.valueOf(bean.getCode()));
        }
        mBinding.setProductBeanList(productBeanList);
        mBinding.invalidateAll();

    }

    @Override
    public void onPayTypeInfoError(String msg) {
        SnackbarUtil.makeSnackBar(mBinding.getRoot(), msg,
                Toast.LENGTH_SHORT).setMeessTextColor(Color.WHITE).show();
    }

    @Override
    public void onGetPrePayInfoSuccess(PrePayInfoBean bean) {
        PayTypeManager manager=PayTypeManager.getInstance();
        if(manager.getZfbPayChannel().equals(PayTypeManager.ZFBPayChannel.ZFB)) {

            //闲来预支付信息获取成功后，上报给后台
            tempPrePayInfoBean=bean;
            payType=2;
            prePayPresenter.reportPrePayInfoByXianlai(payType,productId,bean.getOrderNum(),UserInfoManager.getManager(getContext()).getUserId(),"diamonds");

        }
    }

    @Override
    public void onGetPrePayInfoError(String msg) {

    }
    

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getWxPayReslut(WXPayEntryActivity.ReslutMsg msg)
    {
        //支付成功
        if(msg.getTag().equals(WXPayEntryActivity.TAG) &&msg.getMsg().equals(WXPayEntryActivity.PAY_SUCCESS))
        {

            Toast.makeText(this,"支付成功!",Toast.LENGTH_SHORT).show();
            refreshData();
        }
    }
    
    @Override
    public void onAilPaySucceed(String orderNum, String code) {
        refreshData();
        prePayPresenter.reportPayInfo(productId,"支付成功,等待服务回调",orderNum,UserInfoManager.getManager(this).getUserId());
        UmengEventManager.getInstance(getContext()).reportPayReslut("客户端回调-支付宝支付成功");
    }

    /***
     * 延迟刷新用户信息
     */
    public void  refreshData()
    {
        Timer timer=new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                UserInfoManager.getManager(getContext()).refreshUserInfo();  
            }
        },1500);
    }

    @Override
    protected void onResume() {
        super.onResume();
        UserInfoManager.getManager(getContext()).refreshUserInfo();
    }

    @Override
    public void onAilPayFaild(String orderNum, String resultStatus) {
        if(orderNum==null) return;
        //支付失败分情况进行失败上报
        String status="其它支付错误";
        if(TextUtils.equals(resultStatus,"8000"))
        {
            status="正在处理中，支付结果未知";
        }else if(TextUtils.equals(resultStatus,"4000")){

            status="订单支付失败";
        }else if(TextUtils.equals(resultStatus,"5000")){

            status="重复请求";
        }else if(TextUtils.equals(resultStatus,"6001")){

            status="用户中途取消";
        }else if(TextUtils.equals(resultStatus,"6002")){

            status="网络连接出错";
        }else if(TextUtils.equals(resultStatus,"6004")){

            status="支付结果未知";
        }
        prePayPresenter.reportPayInfo(productId,status,orderNum,UserInfoManager.getManager(this).getUserId());
        UmengEventManager.getInstance(getContext()).reportPayReslut("客户端回调-支付宝 "+status);
    }

    @Override
    public void onUserInfoUpdated() {
        User userInfo = UserInfoManager.getManager(this).getUserInfo();
        if(userInfo!=null)
        {
            diamodnsBalance= userInfo.getDiamonds().getDiamonds();
            mBinding.txtDiaBalance.setText(diamodnsBalance+"");
        }
    }

    @Override
    protected void onDestroy() {
        UserInfoManager.getManager(this).removeUserInfoUpdateListener(this);
        EventBus.getDefault().unregister(this);
        XianlaiWxH5Pay xianlaiWxH5Pay = wxpayPresenter.getXianlaiWxH5Pay();
        if(xianlaiWxH5Pay!=null && xianlaiWxH5Pay.getPayUtil()!=null)
        {
            xianlaiWxH5Pay.getPayUtil().destory();
        }
        super.onDestroy();
    }

    @Override
    public void onGetProductListSuccess(List<ProductBean> productBeanList) {
        //暂时写死五个产品选择，小于隐藏，多与只显示5个
        this.productBeanList=productBeanList;
        cashPresenter.getPayType(PayTypeManager.PAY_WX);
        cashPresenter.getPayType(PayTypeManager.PAY_ZFB);
     
    }

    @Override
    public void onGetProductListError(String msg) {
        SnackbarUtil.makeSnackBar(mBinding.getRoot(), msg,
                Toast.LENGTH_SHORT).setMeessTextColor(Color.WHITE).show();
    }


    /***
     * 闲来支付回调
     * @param bean
     */
    @Override
    public void onPayInfoByXianLaiWxSuccess(XianLaiWxQueryBean bean) {
        if(bean==null) return;
        if(bean.getPay_result().equals("1"))
        {
            Toast.makeText(this,"支付成功",Toast.LENGTH_LONG).show();
        }else if(bean.getPay_result().equals("2")){
            Toast.makeText(this,"支付取消或失败",Toast.LENGTH_LONG).show();
        }else if(bean.getPay_result().equals("0")) {
            Toast.makeText(this, "支付已取消或者您已支付成功需要等待确认支付信息。", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onPayInfoByXianLaiWxError(String msg) {

    }

    @Override
    public void onPrePayInfoByXianLaiSuccess(PrePayInfoBean bean) {
        if(tempPrePayInfoBean!=null && payType==2) {
            payPresenter = new AilPayPresenter(this, tempPrePayInfoBean.getData(), tempPrePayInfoBean.getOrderNum(), this);
            payPresenter.startPayByAilPay();
            tempPrePayInfoBean=null;
        }else if(payType==1)//这里是真正开始闲来渠道微信支付
        {
            wxpayPresenter.StartXianLaiWithWx(mBean);
        }
    }


    /***
     * 闲来交友微信预支付上报，事件发送来自WxReportPrePayInfoReceiver
     * @param model
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void eventBusWxPreInfoReceiverCallback(EventBusModel model)
    {

        if(model.getAction().equals(WxReportPrePayInfoReceiver.REPORTWXPREPAYINFO))
        {
            Bundle data= (Bundle) model.getData();
            String tradeNo = data.getString("tradeNo");
            mBean = (Bean) data.getSerializable("bean");
            payType=1;
            prePayPresenter.reportPrePayInfoByXianlai(payType,productId,tradeNo,UserInfoManager.getManager(getContext()).getUserId(),"diamonds");
        }
    }

    @Override
    public void onPrePayInfoByXianLaiError(String msg) {

    }

    /***
     * 闲来交友微信预支付查询是否支付成功，事件发送来自WxpayPresenter
     * @param model
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void eventBusWxPayReslutSelect(EventBusModel model)
    {

        if(model.getAction().equals(SelectPayWindow.XIANLAI_WX_PAY_CALLBACK_SELELCT))
        {  
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    prePayPresenter.payInfoByXianlaiWxQueryReslut(XianlaiWxH5Pay.PRODUCTCODE, (String)model.getData(),UserInfoManager.getManager(getContext()).getUserId());
                }
            },2000);


        }
    }
    
}
