package com.xindian.fatechat.ui.user.model;

import com.xindian.fatechat.common.BaseModel;
import com.xindian.fatechat.ui.login.model.RegisterBean;
import com.xindian.fatechat.ui.login.model.UserAuth;

import java.io.Serializable;

/**
 * Created by hx_Alex on 2017/3/23.
 *
 */

public class User extends BaseModel implements Serializable, Cloneable {
    public  static  final String  REALUSER="1";
    public  static  final String  SIMULATIONUSER="2";
    /**
     * 用户id
     */
    private String id;
    /**
     * 性别
     */
    private String sex;
    private int userId;
    private String nickName;
    private int age;
    private int phontCount;
    private String headimg;
    private String height;
    private String marriage;
    private String profession;
    private String qualifications;
    private String revenue;
    private String password;
    private String userLevel;
    private String address;
    private String birthday;
    private String weight;
    private String constellation;
    private String token;
    private UserAuth userAuth;
    private DiamodnsBean diamonds;
    private String wechat;
    private String mobile;
    private String firstMeetHope;
    private String likeMeetAddress;
    private String attentionCount;
    private long lastLoginTime;
    private String loveConcept;
    private String makingFriends;
    private boolean isVip;
    private String customerId;
    private String  userType;
    private String onLineFlag;


    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }
    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public boolean isVip() {
        if(userLevel==null) return false;
        if(userLevel.equals("1")){
            return false;
        }else if(userLevel.equals("2")){
            return  true;
        }
        return false;
    }
    
    public boolean isFollow() {
        return follow;
    }


    public boolean getFollow() {
        return follow;
    }

    public void setFollow(boolean follow) {
        this.follow = follow;
    }

    private boolean follow;


    public String getFirstMeetHope() {
        return firstMeetHope;
    }

    public void setFirstMeetHope(String firstMeetHope) {
        this.firstMeetHope = firstMeetHope;
    }

    public String getLikeMeetAddress() {
        return likeMeetAddress;
    }

    public void setLikeMeetAddress(String likeMeetAddress) {
        this.likeMeetAddress = likeMeetAddress;
    }

    public String getAttentionCount() {
        return attentionCount;
    }

    public void setAttentionCount(String attentionCount) {
        this.attentionCount = attentionCount;
    }

    public long getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(long lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getLoveConcept() {
        return loveConcept;
    }

    public void setLoveConcept(String loveConcept) {
        this.loveConcept = loveConcept;
    }

    public String getImPassword() {
        return imPassword;
    }

    public void setImPassword(String imPassword) {
        this.imPassword = imPassword;
    }

    public String getWechat() {
        return wechat;
    }

    public void setWechat(String wechat) {
        this.wechat = wechat;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    private String imPassword;


    public String getHeadimg() {
        return headimg;
    }

    public String getStringUserId() {
        return String.valueOf(userId);
    }

    public void setHeadimg(String headimg) {
        this.headimg = headimg;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getPhontCount() {
        return phontCount;
    }

    public void setPhontCount(int photoNum) {
        this.phontCount = photoNum;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;

    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getMarriage() {
        return marriage;
    }

    public void setMarriage(String marriage) {
        this.marriage = marriage;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getQualifications() {
        return qualifications;
    }

    public void setQualifications(String qualifications) {
        this.qualifications = qualifications;
    }

    public String getRevenue() {
        return revenue;
    }

    public void setRevenue(String revenue) {
        this.revenue = revenue;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserLevel() {
        return userLevel;
    }

    public void setUserLevel(String userLevel) {
        this.userLevel = userLevel;
        if (userLevel.equals("1")) {
            isVip = false;
        } else if (userLevel.equals("2")) {
            isVip = true;
        }
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getConstellation() {
        return constellation;
    }

    public void setConstellation(String constellation) {
        this.constellation = constellation;
    }

    public UserAuth getUserAuth() {
        return userAuth;
    }

    public void setUserAuth(UserAuth userAuth) {
        this.userAuth = userAuth;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public User getUser() {
        return this;
    }

    public String getMakingFriends() {
        return makingFriends;
    }

    public void setMakingFriends(String makingFriends) {
        this.makingFriends = makingFriends;
    }


    public String getOnLineFlag() {
        return onLineFlag;
    }

    public void setOnLineFlag(String onLineFlag) {
        this.onLineFlag = onLineFlag;
    }

    public User insertData(RegisterBean bean) {
        int userId = bean.getUserId();
        User user = new User();
        user.setUserId(userId);
        return user;
    }

    public DiamodnsBean getDiamonds() {
        return diamonds;
    }

    public void setDiamonds(DiamodnsBean diamodnsBean) {
        this.diamonds = diamodnsBean;
    }

    /***
     * 不影响userinfomanger,使用clone操作先对clone对象进行更新，后台同步后在同步至userinfomanger
     *
     * @return
     */
    public Object clone() {
        User cloneuser = new User();
        try {
            cloneuser = (User) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return cloneuser;
    }
}
