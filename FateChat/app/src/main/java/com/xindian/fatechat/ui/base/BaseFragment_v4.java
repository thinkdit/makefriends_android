package com.xindian.fatechat.ui.base;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.umeng.analytics.MobclickAgent;
import com.xindian.fatechat.R;
import com.xindian.fatechat.ui.user.CashActivity;

/**
 * Fragment基类
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * create an instance of this fragment.
 */
public class BaseFragment_v4 extends Fragment {
    public final String TAG = this.getClass().getSimpleName();

    public BaseFragment_v4() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(getClass().getSimpleName());
    }

    @Override
    public void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(getClass().getSimpleName());
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Glide.with(this).onLowMemory();
    }

    public boolean isScreenLandscape() {
        return getActivity().getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
    }


    public void onShowChange(boolean hide) {

    }
    
    public  void gotoCashActivity(View view,String source)
    {
        ((TextView)view.findViewById(R.id.tv_toolbar_title)).setText(R.string.activity_title);
        ((ImageView)view.findViewById(R.id.iv_title_bar_back)).setVisibility(View.VISIBLE);
        ((ImageView)view.findViewById(R.id.iv_title_bar_back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getContext(),CashActivity.class);
                i.putExtra(CashActivity.ENTRYSOURCE,source);
                startActivity(i);
            }
        });
        ((ImageView)view.findViewById(R.id.iv_title_bar_back)).setImageResource(R.drawable.cash_left_titlebar);
    }
}
