package com.xindian.fatechat.common;

import com.thinkdit.lib.base.ParamCommomBase;
import com.thinkdit.lib.util.StringUtil;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.util.ChannelUtil;

public class XTParamCommom extends ParamCommomBase {

    public XTParamCommom() {
        super();
        String token = UserInfoManager.getManager(FateChatApplication.getInstance()).getToken();
        if (!StringUtil.isNullorEmpty(token)) {
            super.add("token", token);
        }

        int uid = UserInfoManager.getManager(FateChatApplication.getInstance()).getUserId();
        if (!StringUtil.isNullorEmpty(String.valueOf(uid))) {
            super.add("uid", uid);
        }

        /**
         * Android系统 ANDROID, iOS系统 IOS, 浏览器WEB
         */
        super.add("osType", "ANDROID");
        /**
         * 手机 MOBILE, 平板 TABLET, PC设备 PC,TV设置TV;
         */
        super.add("deviceType", "MOBILE");
        //渠道
        super.add("releaseChannel", ChannelUtil.getChannel(FateChatApplication.getInstance()));

    }


}