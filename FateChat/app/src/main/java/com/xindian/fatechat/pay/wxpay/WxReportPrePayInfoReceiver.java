package com.xindian.fatechat.pay.wxpay;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.hyphenate.easeui.model.EventBusModel;
import com.jyd.paydemo.Bean;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by hx_Alex on 2017/6/17.
 * 闲来交友h5微信支付预支付订单获取上报给自己服务器广播
 */

public class WxReportPrePayInfoReceiver extends BroadcastReceiver {
    public static final String REPORTWXPREPAYINFO="WxPrePayInfoReport";
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        /***
         * 发送事件至SelectPayWindow
         */
        if(action.equals(REPORTWXPREPAYINFO))
        {
            EventBusModel model=new EventBusModel();
            model.setAction(REPORTWXPREPAYINFO);
            String tradeNo= intent.getStringExtra("tradeNo");
            Bean mBean= (Bean) intent.getSerializableExtra("bean");
            Bundle data=new Bundle();
            if(tradeNo!=null) {
                data.putString("tradeNo", tradeNo);
                data.putSerializable("bean", mBean);
                model.setData(data);
                EventBus.getDefault().post(model);
            }
          
        }
    }
}
