package com.xindian.fatechat.ui;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.thinkdit.lib.util.StringUtil;
import com.xindian.fatechat.R;
import com.xindian.fatechat.databinding.ActivityWebBinding;
import com.xindian.fatechat.manager.SchemeManager;
import com.xindian.fatechat.util.WebViewUtil;


public class WebActivity extends com.xindian.fatechat.ui.base.BaseActivity implements WebViewUtil.IWebViewCallBack, View
        .OnClickListener {
    public static final String JUMP_WEB_VIEW_URL = "JUMP_WEB_VIEW_URL";
    public static final String JUMP_WEB_VIEW_TITLE = "JUMP_WEB_VIEW_TITLE";
    public static final String JUMP_WEB_VIEW_STR_DATA = "JUMP_WEB_VIEW_STR_DATA";
    private String mUrl;
    private String mTitle;
    private String mId;
    private String mType;
    private String mHtmlStr;
    private ActivityWebBinding mActivityWebBinding;

    //    private ShareWindow mShareWindo
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityWebBinding = DataBindingUtil.setContentView(this, R.layout.activity_web);
        mActivityWebBinding.setIsNative(false);
        Intent intent = getIntent();
        mUrl = intent.getStringExtra(JUMP_WEB_VIEW_URL);
        mTitle = intent.getStringExtra(JUMP_WEB_VIEW_TITLE);
        mHtmlStr = intent.getStringExtra(JUMP_WEB_VIEW_STR_DATA);
        if (StringUtil.isNullorEmpty(mUrl) && StringUtil.isNullorEmpty(mHtmlStr)) {
            finish();
            return;
        }
//        if (mUrl != null && mUrl.startsWith(SchemeManager.BASE_PROTOCOL)) {
//            SchemeManager.getInstance().jumpToActivity(this, mUrl);
//            finish();
//            return;
//        }
        initToolBar();
        setTitle(mTitle);
        initWebView();
    }


    private void setTitle(String title) {
        if (!StringUtil.isNullorEmpty(title)) {
            mActivityWebBinding.setTitle(title);
        }
    }

    private void initWebView() {
        WebViewUtil.initWebView(mActivityWebBinding.webview, this);
        if (!StringUtil.isNullorEmpty(mUrl)) {

            mActivityWebBinding.webview.loadUrl(mUrl);
        } else if (!StringUtil.isNullorEmpty(mHtmlStr)) {
            // WebView加载web资源
            mActivityWebBinding.webview.loadData(mHtmlStr, "text/html; charset=UTF-8", null);
        }
        WebViewUtil.registerFun(this, mActivityWebBinding.webview, this);
        mActivityWebBinding.webview.setWebViewClient(new MyWebViewCient());
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (mActivityWebBinding.webview != null) {
            mActivityWebBinding.webview.onResume();

        }

    }

    @Override
    public void onPause() {
        super.onPause();
        if (mActivityWebBinding.webview != null) {
            mActivityWebBinding.webview.onPause();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        if (mActivityWebBinding.webview != null) {
            mActivityWebBinding.webview.stopLoading();
            mActivityWebBinding.webview.removeAllViews();
            mActivityWebBinding.webview.destroy();
        }
        super.onDestroy();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (mActivityWebBinding.webview.canGoBack()) {
                mActivityWebBinding.webview.goBack();
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public synchronized void onReceivedTitle(WebView view, String title) {
        //没有设置标题时,使用页面标题
        if (StringUtil.isNullorEmpty(mTitle)) {
            mTitle = title;
            setTitle(mTitle);
        }

    }

    @Override
    public void onDownloadStart(String url, String userAgent, String contentDisposition, String
            mimetype, long contentLength) {
        Uri uri = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {

    }

    @Override
    public void onPageFinished(WebView view, String url) {

    }

    @Override
    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {

    }

    @Override
    public void onShare(String id, String type) {
//        if (mShareWindow == null) {
//            mShareWindow = new ShareWindow(this);
//        }
//        mShareWindow.setData(type, id);
//        mShareWindow.show();
//        ShareStat.reportShareClick(this, R.string.share_label_share_click_h5_1);
    }

    @Override
    public void setShare(String id, String type) {
//        if (StringUtil.isNullorEmpty(type) || StringUtil.isNullorEmpty(id)) {
//            return;
//        }
//        mId = id;
//        mType = type;
//        initToolBarWithRightMenu(getResources().getDrawable(R.drawable.h5_btn_share), this);
    }

    @Override
    public void showLiveWeb2Button(String buttonUrl) {

    }

    @Override
    public void onClose() {
        finish();
    }

    @Override
    public String getLiveUserRole() {
        return null;
    }

    @Override
    public void onLiveSubscribe(String starTime, String title, String remark) {

    }

    @Override
    public void showUserInfo(String uid) {

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }



    @Override
    public void onClick(View v) {

    }
    
    class MyWebViewCient extends WebViewClient
    {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if(url.startsWith("fatechat"))
            {
                return   SchemeManager.getInstance().jumpToActivity(WebActivity.this,url);
            }
            return    false;
            
        }
    }
}
