package com.xindian.fatechat.ui.user;

import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.xindian.fatechat.R;
import com.xindian.fatechat.databinding.ActivityPartnerconditionsBinding;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.ui.base.BaseActivity;
import com.xindian.fatechat.ui.user.data.UserData;
import com.xindian.fatechat.ui.user.model.CaseFriendBean;
import com.xindian.fatechat.ui.user.model.User;
import com.xindian.fatechat.ui.user.presenter.FriendscasePresenter;
import com.hyphenate.easeui.utils.SnackbarUtil;
import com.xindian.fatechat.widget.MessageSelectionDialog;

import java.util.List;

import static com.xindian.fatechat.widget.MessageSelectionDialog.AGE_SCOPE_SELECTION;

public class PartnerConditionsActivity extends BaseActivity implements  MessageSelectionDialog.onSaveListener,FriendscasePresenter.onGetFriendsCaseListener {
    private  static  final  int  AGE=1;
    private  static  final  int  CITY=2;
    private  static  final  int  HEGHIT=3;
    private  static  final  int  EDUCATION=4;
    private  static  final  int  INCOME=5;
    private  static final String UNLIMITED="不限";
    private  int openDialog;
    private  FriendscasePresenter friendscasePresenter;
    private User user;
    private ActivityPartnerconditionsBinding mBinding;
    private  CaseFriendBean mBean;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       mBinding= DataBindingUtil.setContentView(this,R.layout.activity_partnerconditions);
        friendscasePresenter=new FriendscasePresenter(this,this);
        user= UserInfoManager.getManager(this).getUserInfo();
        initToolBar();
        friendscasePresenter.requestGetFriendsCase(user);
        mBean=new CaseFriendBean();
        mBinding.setBean(mBean);
    }

    public  void onClickSelectAge(View v)
    {
        showMessageSelectionDialog(AGE,null,"选择年龄",AGE_SCOPE_SELECTION);
    }

    public void onClickCity(View v)
    {
        showMessageSelectionDialog(CITY,null,"设置居住地",MessageSelectionDialog.LIVE_SELECTION);
    }

    public void onClickSelectHeight(View v)
    {
        showMessageSelectionDialog(HEGHIT, null,"身高",MessageSelectionDialog.HEGHIT_SCOPE_SELECTION);
    }

    public void onClickSelectEducation(View v)
    {
        List<String> educationList = UserData.getEducationList();
        educationList.add(0,UNLIMITED);
        showMessageSelectionDialog(EDUCATION, educationList,"学历",null);
    }

    public void onClickSelectIncome(View v)
    {
        List<String> incomeList = UserData.getIncomeList();
        incomeList.add(0,UNLIMITED);
        showMessageSelectionDialog(INCOME,incomeList,"月收入",null);
    }


    public void showMessageSelectionDialog(int dialogTag, List<String> dataList, String titleString, String useKind)
    {
        openDialog=dialogTag;
        MessageSelectionDialog dialog=null;
        if(useKind==null) {
            dialog= new MessageSelectionDialog(this, dataList, titleString, this);
        }else {
            dialog=new MessageSelectionDialog(this,titleString,useKind,this);
            dialog.setView(new EditText(this));
        }
        dialog.show();
    }

    public  void onClickUpdateUserCase(View v){
        friendscasePresenter.requestUpdateFriendsCase(mBinding.getBean());
    }
    
    @Override
    public void onSave(String content, String content2) {
        if(content.equals(UNLIMITED))
        {
            content="";
        }
            if(openDialog==AGE)
            {
                mBinding.txtAge.setText(content);
            }else if(openDialog==CITY){
                mBinding.txtCity.setText(content);
            }else if(openDialog==HEGHIT){
                mBinding.txtHeight.setText(content);
            }else if(openDialog==EDUCATION){
                mBinding.txtEducation.setText(content);
            }else if(openDialog==INCOME){
                mBinding.txtIncome.setText(content);
            }
        
    }

    @Override
    public void onGetFriendsCaseSuccess(CaseFriendBean bean) {
        mBean=bean;
        //如果请求成功但无数据，不做注入
        if (mBean == null) return;
        mBinding.setBean(mBean);
    }

    @Override
    public void onGetFriendsCaseError(String msg) {
        SnackbarUtil.makeSnackBar(mBinding.getRoot(),msg,Toast.LENGTH_SHORT).setMeessTextColor(Color.WHITE).show();
    }

    @Override
    public void onUpdateFriendsCaseSuccess() {
        SnackbarUtil.makeSnackBar(mBinding.getRoot(),"征友条件更新成功!",Toast.LENGTH_SHORT).setMeessTextColor(Color.WHITE).show();
    }

    @Override
    public void onUpdateFriendsCaseError(String msg) {
        SnackbarUtil.makeSnackBar(mBinding.getRoot(),msg,Toast.LENGTH_SHORT).setMeessTextColor(Color.WHITE).show();
    }
}
