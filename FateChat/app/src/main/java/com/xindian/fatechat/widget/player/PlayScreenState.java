package com.xindian.fatechat.widget.player;

/**
 * @author: laohu on 2016/8/11
 * @site: http://ittiger.cn
 */
public enum PlayScreenState {
    FULL_SCREEN,
    NORMAL,
    SMALL
}
