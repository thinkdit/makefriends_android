package com.xindian.fatechat.ui.user;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Toast;

import com.hyphenate.easeui.domain.ChatUserEntity;
import com.hyphenate.easeui.utils.SnackbarUtil;
import com.xindian.fatechat.DemoHelper;
import com.xindian.fatechat.R;
import com.xindian.fatechat.databinding.ActivityUserDetailsBinding;
import com.xindian.fatechat.manager.JumpManager;
import com.xindian.fatechat.manager.SharePreferenceManager;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.ui.base.BaseActivity;
import com.xindian.fatechat.ui.login.LoginDialog;
import com.xindian.fatechat.ui.user.adapter.UserDetailsPhotoAdapter;
import com.xindian.fatechat.ui.user.model.PhotoInfoBean;
import com.xindian.fatechat.ui.user.model.User;
import com.xindian.fatechat.ui.user.presenter.MyAttentionPresenter;
import com.xindian.fatechat.ui.user.presenter.RequestUserInfoPresenter;
import com.xindian.fatechat.util.IMUtil;
import com.xindian.fatechat.widget.StarTDialog;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.List;

public class UserDetailsActivity extends BaseActivity implements RequestUserInfoPresenter
        .UserInfodListener, MyAttentionPresenter.onAddAttentionListener, LoginDialog
        .onLoginListener,AppBarLayout.OnOffsetChangedListener,UserDetailsPhotoAdapter.ClickImgPhotoListener,
        RequestUserInfoPresenter.OtherUserPhotoListener{
    public static final String FRMMEUSERID = "femmeUserId";
    public static final String DISTANCE = "distance";//距离
    private ActivityUserDetailsBinding mBinding;
    private RequestUserInfoPresenter requestUserInfoPresenter;
    private MyAttentionPresenter myAttentionPresenter;
    private int femmeUserId;
    private User frmmeUser;
    private UserDetailsPhotoAdapter userDetailsPhotoAdapter;
    public static void gotoUserDetails(Context context, int userID, String distance) {
        Intent intent = new Intent(context, UserDetailsActivity.class);
        intent.putExtra(FRMMEUSERID, userID);
        if (DISTANCE != null && !DISTANCE.equalsIgnoreCase("")) {
            intent.putExtra(DISTANCE, distance);
        }
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setHasInitSystemBarTint(false);
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_user_details);
        requestUserInfoPresenter = new RequestUserInfoPresenter(this, this);
        requestUserInfoPresenter.setOtherUserPhotoListener(this);
        myAttentionPresenter = new MyAttentionPresenter(this);
        myAttentionPresenter.setOnAddMyAttentionListener(this);
        intiHeadLayout();
        femmeUserId = getIntent().getIntExtra(FRMMEUSERID, 0);
        setUserDistance(getIntent().getStringExtra(DISTANCE));
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        mBinding.photoList.setNestedScrollingEnabled(false);
    }


    @Override
    protected void onResume() {
        super.onResume();

        if(requestUserInfoPresenter!=null){
            requestUserInfoPresenter.requestUserDetailsInfo(femmeUserId);
        }
    }


    private void  intiHeadLayout()
    {
        int statusBarHeight = getStatusBarHeight();
        int toolbarHeghit= (int) getResources().getDimension(R.dimen.actionBarSize);
        if(statusBarHeight!=0)
        {
            mBinding.toolbar.setPadding(0,statusBarHeight,0,0);
            toolbarHeghit=toolbarHeghit+statusBarHeight;
        }else
        {
            int defaultStatusBarHeight = (int) getResources().getDimension(R.dimen.default_statusBarHeight);
            mBinding.toolbar.setPadding(0, (int) defaultStatusBarHeight,0,0);
            toolbarHeghit=toolbarHeghit+statusBarHeight;
        }
        int finalToolbarHeghit = toolbarHeghit;
        mBinding.toolbar.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                ViewGroup.LayoutParams layoutParams = mBinding.toolbar.getLayoutParams();
                layoutParams.height= finalToolbarHeghit;
                mBinding.toolbar.setLayoutParams(layoutParams);

            }
        });
        mBinding.ivTitleBarBack.setOnClickListener(v->finish());
        mBinding.appBarLayout.addOnOffsetChangedListener(this);

    }

    /***
     * toolbar颜色渐变效果
     * @param appBarLayout
     * @param verticalOffset
     */
    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if(verticalOffset==0)
        {
            mBinding.toolbar.setBackgroundResource(R.color.transparent);
            mBinding.ivTitleBarBack.setImageResource(R.drawable.user_detail_back_white);
            mBinding.tvToolbarTitle.setTextColor(Color.WHITE);
        }else
        {
            Float offsetRate= (float) Math.abs(verticalOffset)/appBarLayout.getTotalScrollRange()*255;
            BigDecimal offset=new BigDecimal(offsetRate).setScale(2,BigDecimal.ROUND_HALF_UP);
            mBinding.toolbar.getBackground().setAlpha((int) offset.floatValue());
            mBinding.toolbar.setBackgroundResource(R.color.user_detail_toolbar_background);
            if(offset.floatValue()>=229)
            {
                mBinding.ivTitleBarBack.setImageResource(R.drawable.backicon);
                mBinding.tvToolbarTitle.setTextColor(getResources().getColor(R.color.title_bar_color));
            }
        }
    }



    public void setUserDistance(String distance) {

    }





    public void onClickAttention(View v) {
        if (IMUtil.checkIsLogin(UserDetailsActivity.this, UserDetailsActivity.this)) {
            if (frmmeUser == null) return;
            if (frmmeUser.getFollow()) {
                SnackbarUtil.makeSnackBar(mBinding.getRoot(), "已关注，无法再次关注！",
                        Toast.LENGTH_SHORT).setMeessTextColor(Color.WHITE).show();
            } else {
                myAttentionPresenter.requestAddAttention(UserInfoManager.getManager(this)
                        .getUserId(), femmeUserId);
            }
        }
    }

    public void onClickZhaohu(View v) {
        if (!SharePreferenceManager.getManager(this).isChated(frmmeUser.getUserId())) {
            if (IMUtil.checkIsLogin(UserDetailsActivity.this, UserDetailsActivity.this)) {
                IMUtil.sendHellMessage(this, frmmeUser);
                SnackbarUtil.makeSnackBar(mBinding.getRoot(), "打招呼成功", Toast
                        .LENGTH_SHORT).setMeessTextColor(Color.WHITE).show();
            }
        }else
        {
            SnackbarUtil.makeSnackBar(mBinding.getRoot(), "已打过招呼", Toast
                    .LENGTH_SHORT).setMeessTextColor(Color.WHITE).show();
        }
    }

    public void onClickSixing(View v) {
            if (IMUtil.checkIsLogin(UserDetailsActivity.this, UserDetailsActivity.this)) {
                //当前用户为模拟用户
                boolean isRealUser=false;
                if(frmmeUser.getUserType().equals(User.REALUSER))
                {
                    isRealUser=true;
                }
                ChatUserEntity entity = DemoHelper.getInstance().getChatUserEntity(isRealUser,frmmeUser.getUserId());
                entity.setToAvator(frmmeUser.getHeadimg());
                entity.setToUserID(frmmeUser.getUserId());
                entity.setToNickName(frmmeUser.getNickName());
                //判断是真实用户还是客服
                entity.setRealUser(isRealUser);
                JumpManager.gotoChat(this, entity);
            }
    }

    @Override
    public void onUserInfodSucceed(User user) {
        mBinding.setUser(user);
        frmmeUser = user;
        User mUser = UserInfoManager.getManager(this).getUserInfo();
        mBinding.setIsVip(mUser.isVip());
        requestUserInfoPresenter.requestMyImage(mUser.getUserId(),1,10,user.getUserId());
        //请求数据成功后在显示操作列表
        if(mBinding.footerLayout.getVisibility()==View.INVISIBLE) {
            mBinding.footerLayout.setVisibility(View.VISIBLE);
        }
    }

    /***
     * 通过反射改变FloatingActionButton中Image的大小
     */
    public void reflectFloatingActionButtonImageSize(Class<? extends FloatingActionButton>
                                                             mClass, FloatingActionButton
                                                             mFloatingActionButton) {
        final String ImageSize = "mMaxImageSize";
        final String ImagePadding = "mImagePadding";
        try {
            Field mMaxImageSize = mClass.getDeclaredField(ImageSize);
            Field mImagePadding = mClass.getDeclaredField(ImagePadding);
            mMaxImageSize.setAccessible(true);
            mImagePadding.setAccessible(true);
            mMaxImageSize.setInt(mFloatingActionButton, (int) getResources().getDimension(R.dimen
                    .design_fab_image_size));
            mImagePadding.setInt(mFloatingActionButton, 0);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }


    public void onClickAlbum(View v) {
        if (IMUtil.checkIsLogin(UserDetailsActivity.this, UserDetailsActivity.this)) {
            User mUser = UserInfoManager.getManager(this).getUserInfo();
            if (mUser.isVip()) {
                Intent i = new Intent(UserDetailsActivity.this, MyPhotoActivity.class);
                i.putExtra(MyPhotoActivity.BYOTHERUSERPHOTO, true);
                i.putExtra(MyPhotoActivity.BYOTHERUSEID, femmeUserId);
                startActivity(i);
            } else {
                StarTDialog dialog = new StarTDialog(this, (ViewGroup) mBinding.getRoot());
                dialog.setBtnOkCallback(new StarTDialog.iDialogCallback() {
                    @Override
                    public boolean onBtnClicked() {
                        Intent i = new Intent(UserDetailsActivity.this, CashActivity.class);
                        i.putExtra(CashActivity.ENTRYSOURCE,"相册入口");
                        startActivity(i);
                        return true;
                    }
                });
                dialog.show("提示", "您还不是钻石Vip哦,无法查看他人私密相册，是否前往充值?", "立即充值", "取消");
            }
        }
    }

    public void onClickContactWxOrPhone(View v) {
        if (IMUtil.checkIsLogin(UserDetailsActivity.this, UserDetailsActivity.this)) {
            User mUser = UserInfoManager.getManager(this).getUserInfo();
            if (mUser.isVip()) {
                mBinding.scrollView.scrollTo(0,mBinding.scrollView.getMaxScrollAmount()/2);
            } else {
                StarTDialog dialog = new StarTDialog(this, (ViewGroup) mBinding.getRoot());
                dialog.setBtnOkCallback(new StarTDialog.iDialogCallback() {
                    @Override
                    public boolean onBtnClicked() {
                        Intent i = new Intent(UserDetailsActivity.this, CashActivity.class);
                        i.putExtra(CashActivity.ENTRYSOURCE,"查看微信入口");
                        startActivity(i);
                        return true;
                    }
                });
                dialog.show("提示", "您还不是钻石Vip哦,无法查看他人信息是否前往充值?", "立即充值", "取消");
            }
        }
    }


    public void onClickContactInformation(View view) {
        if (IMUtil.checkIsLogin(UserDetailsActivity.this, UserDetailsActivity.this)) {
            User mUser = UserInfoManager.getManager(this).getUserInfo();
            String contact = null;
            StarTDialog dialog = new StarTDialog(this, (ViewGroup) mBinding.getRoot());
            dialog.setBtnOkCallback(new StarTDialog.iDialogCallback() {
                @Override
                public boolean onBtnClicked() {
                    Intent i = new Intent(UserDetailsActivity.this, CashActivity.class);
                    i.putExtra(CashActivity.ENTRYSOURCE,"查看手机号入口");
                    startActivity(i);
                    return true;
                }
            });

            if (!mUser.isVip()) {
                dialog.show("提示", "您还不是钻石Vip哦,无法查看他人信息是否前往充值?", "立即充值", "取消");
            }

        }
    }

    @Override
    public void onAddAttentionSuccess() {
        requestUserInfoPresenter.requestUserDetailsInfo(femmeUserId);
        SnackbarUtil.makeSnackBar(mBinding.getRoot(), "已关注成功",
                Toast.LENGTH_SHORT).setMeessTextColor(Color.WHITE).show();
    }

    @Override
    public void onAddAttentionError(String msg) {
        SnackbarUtil.makeSnackBar(mBinding.getRoot(), msg, Toast.LENGTH_SHORT)
                .setMeessTextColor(Color.WHITE).show();
    }

    @Override
    public void LoginSucceed(User user) {
        requestUserInfoPresenter.requestUserDetailsInfo(femmeUserId);
    }

    @Override
    public void LoginError(String msg) {

    }


    @Override
    public void OnClickListener() {
        User user=UserInfoManager.getManager(this).getUserInfo();
        if (user.isVip()) {
            Intent i = new Intent(UserDetailsActivity.this, MyPhotoActivity.class);
            i.putExtra(MyPhotoActivity.BYOTHERUSERPHOTO, true);
            i.putExtra(MyPhotoActivity.BYOTHERUSEID, femmeUserId);
            startActivity(i);
        } else {
            StarTDialog dialog = new StarTDialog(this, (ViewGroup) mBinding.getRoot());
            dialog.setBtnOkCallback(new StarTDialog.iDialogCallback() {
                @Override
                public boolean onBtnClicked() {
                    Intent i = new Intent(UserDetailsActivity.this, CashActivity.class);
                    i.putExtra(CashActivity.ENTRYSOURCE,"相册入口");
                    startActivity(i);
                    return true;
                }
            });
            dialog.show("提示", "您还不是钻石Vip哦,无法查看他人私密相册，是否前往充值?", "立即充值", "取消");
        }
    }

    @Override
    public void onOtherUserPhotoSucceed(List<PhotoInfoBean> beanList) {
        //照片为null时，隐藏布局
        if(beanList==null || (beanList!=null && beanList.size()==0))
        {
            mBinding.photoLayout.setVisibility(View.GONE);
            return;
        }
        userDetailsPhotoAdapter=new UserDetailsPhotoAdapter(this,beanList,UserDetailsActivity.this);
        mBinding.photoList.setAdapter(userDetailsPhotoAdapter);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(UserDetailsActivity.this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mBinding.photoList.setLayoutManager(linearLayoutManager);
       
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
