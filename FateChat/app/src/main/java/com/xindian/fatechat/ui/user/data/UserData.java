package com.xindian.fatechat.ui.user.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

/**
 * Created by hx_Alex on 2017/3/30.
 */

public class UserData {
    public  static final String NAME="name";
    public  static final String TIME="time";
    public  static List<String> getOccupationList()
    {
        ArrayList<String> occupationList=new ArrayList<>();
        occupationList.add("在校学生");   occupationList.add("军人");   occupationList.add("私营业主");   occupationList.add("企业职工");
        occupationList.add("农业劳动者"); occupationList.add("政府机关/事业单位工作者");  occupationList.add("其他");
        return  occupationList;
    }

    public  static List<String> getEducationList()
    {
        ArrayList<String> occupationList=new ArrayList<>();
        occupationList.add("初中及以下");   occupationList.add("高中及中专");   occupationList.add("大专");   occupationList.add("本科");
        occupationList.add("硕士及以上");
        return  occupationList;
    }

    public  static List<String> getIncomeList()
    {
        ArrayList<String> occupationList=new ArrayList<>();
        occupationList.add("<2000");   occupationList.add("2000-5000");   occupationList.add("5000-10000");   occupationList.add("10000-20000");
        occupationList.add(">20000");
        return  occupationList;
    }

    public  static List<String> getMarriageList()
    {
        ArrayList<String> occupationList=new ArrayList<>();
        occupationList.add("未婚");   occupationList.add("离异");   occupationList.add("丧偶");
        return  occupationList;
    }
    
    public  static List<String> getHeightList()
    {
        ArrayList<String> occupationList=new ArrayList<>();
        for(int i=150;i<=190;i++)
        {
            occupationList.add(i+"cm");
        }
        return  occupationList;
    }


    public  static List<String> getAge()
    {
        ArrayList<String> ageList=new ArrayList<>();
        for(int i=18;i<=100;i++)
        {
            ageList.add(i+"岁");
        }
        return  ageList;
    }

    
    public static List<String> getWeight()
    {
        ArrayList<String> occupationList=new ArrayList<>();
        for(int i=40;i<=100;i=i+5)
        {
            int j=i+5;
            if(i==100)
            {
                occupationList.add(i+"(kg)以上");
                break;
            }
            occupationList.add(i+"-"+j+"(kg)");
        }
        return occupationList;
    }

    public static List<String> getYear()
    {
        ArrayList<String> occupationList=new ArrayList<>();
        for(int i=1954;i<=1997;i++)
        {
                occupationList.add(i+"");
        }
        return occupationList;
    }

    public static List<String> getMonth()
    {
        ArrayList<String> occupationList=new ArrayList<>();
        for(int i=1;i<=12;i++)
        {
            occupationList.add(i+"");
        }
        return occupationList;
    }

    public static List<String> getDay()
    {
        ArrayList<String> occupationList=new ArrayList<>();
        for(int i=1;i<=31;i++)
        {
            occupationList.add(i+"");
        }
        return occupationList;
    }
    
    public static List<String> getConstellation()
    {
        ArrayList<String> occupationList=new ArrayList<>();
        occupationList.add("水瓶座");occupationList.add("双鱼座");occupationList.add("白羊座");occupationList.add("金牛座");
        occupationList.add("双子座");occupationList.add("巨蟹座");occupationList.add("狮子座");occupationList.add("处女座");
        occupationList.add("天枰座");occupationList.add("天蝎座");occupationList.add("射手座");occupationList.add("摩羯座");
        return occupationList;
    }

    public static List<String> getProvince()
    {
        ArrayList<String> occupationList=new ArrayList<>();
        Set<String> ProvinceSet =getCityMap().keySet();
        Iterator<String> iterable= ProvinceSet.iterator();
        for(;iterable.hasNext();)
        {
            String Province =iterable.next();
            occupationList.add(Province);
        }
        return occupationList;
    }
    
    public static Map<String,String[]> getCityMap()
    {
        Map<String,String[]> model=new LinkedHashMap<String, String[]>();
        model.put("不限", new String[]{"不限"});
        model.put("北京市", new String[]{"不限","东城区","崇文区","宣武区","朝阳区","丰台区","石景山区"});
        model.put("上海市", new String[]{"不限","黄浦区","卢湾区","徐汇区","长宁区","静安区","普陀区","虹口区","杨浦区","闵行区","宝山区","嘉定区","浦东区","金山区","松江区","青浦区","奉贤区","崇明区"});
        model.put("天津市", new String[]{"不限","和平区","河东区","河西区","南开区","河北区","红桥区","虹口区","塘沽区","汉沽区","大港区","东丽区","西青区","津南区","北辰区","武清区","宝坻区","宁河区","静海区","蓟州区"});
        model.put("重庆市",  new String[]{"不限","渝中区","大渡口区","江北区","沙坪坝区","九龙坡区","南岸区","北碚区","渝北区","巴南区","万州区","涪陵区","永川区","璧山区","大足区","綦江区","江津区","合川区","黔江区","长寿区","南川区","铜梁区","潼南区","荣昌区","开州区","梁平区","武隆区","城口县","丰都县","垫江县","忠县","云阳县","奉节县","巫山县","巫溪县","石柱土家族自治县","秀山土家族苗族自治县","酉阳土家族苗族自治县","彭水苗族土家族自治县"});
        model.put("黑龙江省", new String[]{"哈尔滨","齐齐哈尔","牡丹江","大庆","伊春","双鸭山","鹤岗","鸡西","佳木斯","七台河","黑河","绥化","大兴安岭"});
        model.put("吉林省", new String[]{"长春","延边","吉林","白山","白城","四平","松原","辽源","大安","通化"});
        model.put("辽宁省", new String[]{"沈阳","大连","葫芦岛","旅顺","本溪","抚顺","铁岭","辽阳","营口","阜新","朝阳","锦州","丹东","鞍山"});
        model.put("内蒙古省", new String[]{"呼和浩特","呼伦贝尔","锡林浩特","包头","赤峰","海拉尔","乌海","鄂尔多斯","通辽"});
        model.put("河北省", new String[]{"石家庄","唐山","张家口","廊坊","邢台","邯郸","沧州","衡水","承德","保定","秦皇岛"});
        model.put("河南省", new String[]{"郑州","开封","洛阳","平顶山","焦作","鹤壁","新乡","安阳","濮阳","许昌","漯河","三门峡","南阳","商丘","信阳","周口","驻马店"});
        model.put("山东省", new String[]{"济南","青岛","淄博","威海","曲阜","临沂","烟台","枣庄","聊城","济宁","菏泽","泰安","日照","东营","德州","滨州","莱芜","潍坊"});
        model.put("山西省", new String[]{"太原","阳泉","晋城","晋中","临汾","运城","长治","朔州","忻州","大同","吕梁"});
        model.put("江苏省", new String[]{"南京","苏州","昆山","南通","太仓","吴县","徐州","宜兴","镇江","淮安","常熟","盐城","泰州","无锡","连云港","扬州","常州","宿迁"});
        model.put("安徽省", new String[]{"合肥","巢湖","蚌埠","安庆","六安","滁州","马鞍山","阜阳","宣城","铜陵","淮北","芜湖","毫州","宿州","淮南","池州"});
        model.put("陕西省", new String[]{"西安","韩城","安康","汉中","宝鸡","咸阳","榆林","渭南","商洛","铜川","延安"});
        model.put("宁夏省", new String[]{"银川","固原","中卫","石嘴山","吴忠"});
        model.put("甘肃省", new String[]{"兰州","白银","庆阳","酒泉","天水","武威","张掖","甘南","临夏","平凉","定西","金昌"});
        model.put("青海省", new String[]{"西宁","海北","海西","黄南","果洛","玉树","海东","海南"});
        model.put("湖北省", new String[]{"武汉","宜昌","黄冈","恩施","荆州","神农架","十堰","咸宁","襄樊","孝感","随州","黄石","荆门","鄂州"});
        model.put("湖南省", new String[]{"长沙","邵阳","常德","郴州","吉首","株洲","娄底","湘潭","益阳","永州","岳阳","衡阳","怀化","韶山","张家界"});
        model.put("浙江省", new String[]{"杭州","湖州","金华","宁波","丽水","绍兴","雁荡山","衢州","嘉兴","台州","舟山","温州"});
        model.put("江西省", new String[]{"南昌","萍乡","九江","上饶","抚州","吉安","鹰潭","宜春","新余","景德镇","赣州"});
        model.put("福建省", new String[]{"福州","厦门","龙岩","南平","宁德","莆田","泉州","三明","漳州"});
        model.put("贵州省", new String[]{"贵阳","安顺","赤水","遵义","铜仁","六盘水","毕节","凯里","都匀"});
        model.put("四川省", new String[]{"成都","泸州","内江","凉山","阿坝","巴中","广元","乐山","绵阳","德阳","攀枝花","雅安","宜宾","自贡","甘孜州","达州","资阳","广安","遂宁","眉山","南充"});
        model.put("广东省", new String[]{"广州","深圳","潮州","韶关","湛江","惠州","清远","东莞","江门","茂名","肇庆","汕尾","河源","揭阳","梅州","中山","德庆","阳江","云浮","珠海","汕头","佛山"});
        model.put("广西省", new String[]{"南宁","桂林","阳朔","柳州","梧州","玉林","桂平","贺州","钦州","贵港","防城港","百色","北海","河池","来宾","崇左"});
        model.put("云南省", new String[]{"昆明","保山","楚雄","德宏","红河","临沧","怒江","曲靖","思茅","文山","玉溪","昭通","丽江","大理"});
        model.put("海南省", new String[]{"海口","三亚","儋州","琼山","通什","文昌"});
        model.put("新疆省", new String[]{"乌鲁木齐","阿勒泰","阿克苏","昌吉","哈密","和田","喀什","克拉玛依","石河子","塔城","库尔勒","吐鲁番","伊宁"});
        return  model;
    }

    public static ArrayList<Map<String,Object>> nameData()
    {

        List<String> data = Arrays.asList("﹏颜汐ぐ", "谁没爱过一只狗", "ㄔㄨㄣˇ=蠢", "童话.", "嗯 未来~", "久不愈i", "ヤ今生的唯爱", "中二1个软", "Crazy. 疯狂.", "DJ女郞", " 姐很高、也很傲╰つ", "后来,就没有了后来︾"
                , "╄→沵湜莪嘸琺企及的光", "撩妹大班长", "加勒比海带", "独波大侠°", "益若翼", "攬一亱孤風", "舌尖上的诱惑つ", "我知道你是梦不是命#", "我叼大我先上", "谢却荼蘼", "久不愈i", "咖啡杯的、瑕疵"
                , "咱来扇SM一巴掌！", "齐肩马尾拽天下@", "劳娘缺锌缺钙缺人爱", "不服来战lol", "蹲厕唱忐忑", "我累了你滚吧", "故人怎追忆", "暂引樱桃破", "背叛←", "小情天。", "半生风雪", "余阳", "Noslēpum", "Exile つ (流放fan)*"
                , "不要挡在皇帝面前", "〃男人拽,照样甩 ≈", "社会妞现实姐@", "你已被我out", "不爱就滚别浪费我青春", "先生、克制点", "看我纯净天然无公害z", "顽皮可以别赛脸", "你的世界不缺我", "孤星赶月", "你是我要不起的风景", "念卿天涯。", "乄壞Oo鮭鮭魚", "卧笑醉伊人"
                , "<我是流氓我怕谁>", "滚开i", "你就是嫉妒我的美", "小姐ヽ你惹不起的草°", "我姓张很嚣张！", "魅罗红颜乱さ", "世俗污了我的眼i", "村姑也狠潮丶", "孙子，你配我爱么", "看谁谁怀孕", "在你的葬礼放Dj", "傲藐者", "[ 禁。区、", "逼毛不烫自然卷"
                , "霸占你妈当你爸", "傲似你野爹", "和宇宙叫嚣ˇ", "极度嚣张", "夜死神降临", "鄙视你、是瞧得起你", "傲似你祖宗", "狼の嚣张", "跪下、叫帅哥", "贱人゛无处不在", "↘傻瓜，你给我滚回来！", "↘碾压ー切", "甩你是给你面子", "魔尊弑神"
                , "枯", "1.场美丽的烟花雨～", "- 梦旅鹿人", "素时景年一世疏离", "成熟带来的是孤独", "╭ァ1个没安全感旳孩子°", "夲宫丶没時閒", "嘴巴子过来", "噬血啸月", "我拽我的『管你屁事』", "浅夏が忆昔", "︶ㄣ洳影隨形しovё", "聆聽＊幸福", "最佳姿势"
                , "爷来取你狗命", "给我马不停蹄的滚", "虐菜是享受", "瘦成闪电劈死你", "独步杀戮", "柠檬草の味道、", "静抚长琴等君归", "Sunshine Bloom 阳光盛开", "换歌体味即系干", "蔚蓝深海", "想念汇集成河", "指尖盛开的繁花", "往事随风心似烟", "淡淡笑。");
        Random dom=new Random();
        ArrayList<Integer> index=new ArrayList<>();
        for(;index.size()<=10;){
            int  temp= dom.nextInt(data.size());
            if(index.contains(temp))
            {
                continue;
            }
            index.add(temp);
        }
        ArrayList<String> contentList=new ArrayList<>();
        for(Integer i:index){
            contentList.add(data.get(i));
        }
        ArrayList<Map<String,Object>> content=new ArrayList<>();
        for(int i=0;i<contentList.size();i++)
        {
            HashMap<String, Object> temp = new HashMap<>();
            if (i <= 3) {
                temp.put(NAME, contentList.get(i));
                temp.put(TIME, 1);
            } else if (i <= 5) {
                temp.put(NAME, contentList.get(i));
                temp.put(TIME, 2);
            } else if (i <= 7) {
                temp.put(NAME, contentList.get(i));
                temp.put(TIME, 3);
            } else if (i <= 10) {
                temp.put(NAME, contentList.get(i));
                temp.put(TIME, 4);
            }
            content.add(temp);
        }
        
        return content;
    }

    public static ArrayList<Integer> cashTimeData()
    {
        ArrayList<Integer> index=new ArrayList<>();
        Random dom=new Random(20);
        int i = dom.nextInt() + 1;
        for(;index.size()==10;){
            //如果数组长度为0，直接插入当前数据
            if(index.size()==0)
            {
                index.add(i);
            }else
            {
                //首先判断当前数是否大于或者等于数组中最后一个数（最后一个数应是最大的数值）
                //是的话直接插入到最后，不是则为小于,那么依次进行数据对比
                if(i>=index.get(index.size()-1))
                {
                    index.add(i);
                }
                else 
                {
                    for(int j=index.size()-2;j<=0;j--)
                    {
                        //如果大于那么插入到当前这个数的后面,然后后面的数依次位移
                        if(i>=index.get(j))
                        {
                            for(int z=index.size()-1;z<=j+1;z--){
                                int temp=index.get(z);
                                index.add(z,index.get(z-1));
                                index.add(temp);
                            }
                            index.add(j,i);
                            break;
                        }
                    }
                    
                }
            }
            
        }
        return index;
    }
    
     
}
