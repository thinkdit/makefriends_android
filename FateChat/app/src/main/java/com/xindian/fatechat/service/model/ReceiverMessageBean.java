package com.xindian.fatechat.service.model;

import com.xindian.fatechat.common.BaseModel;

/**
 * Created by hx_Alex on 2017/7/21.
 */

public class ReceiverMessageBean extends BaseModel {

    /**
     * content : 
     * type : 
     * state : 
     * reasonDuration : 
     */

    private String content;
    private String type;
    private String state;
    private String reasonDuration;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getReasonDuration() {
        return reasonDuration;
    }

    public void setReasonDuration(String reasonDuration) {
        this.reasonDuration = reasonDuration;
    }
}
