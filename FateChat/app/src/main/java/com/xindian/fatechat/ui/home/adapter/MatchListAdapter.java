package com.xindian.fatechat.ui.home.adapter;

import android.content.Context;
import android.graphics.Rect;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.xindian.fatechat.R;
import com.xindian.fatechat.common.MVVPSetters;
import com.xindian.fatechat.ui.user.model.User;
import com.xindian.fatechat.widget.cardSwipeLayout.CardAdapter;
import com.xindian.fatechat.widget.cardSwipeLayout.RoundImageView;

import java.util.List;

/**
 * Created by hx_Alex on 2017/8/8.
 */

public class MatchListAdapter extends CardAdapter{
    private List<User> dataList;
    private LayoutInflater inflater;
    private Context context;

    public MatchListAdapter(List<User> dataList, Context context) {
        this.dataList = dataList;
        this.context = context;
        inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void appendData( List<User> data) 
    {
        for(User u:data) {
            dataList.add(u);
        }
    }

    public List<User> getDataList() {
        return dataList;
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_match_swipe;
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public void bindView(View view, int index) {
        Object tag = view.getTag();
        MatchViewHolder viewHolder;
        if (null != tag) {
            viewHolder = (MatchViewHolder) tag;
        } else {
            //不缓存时直接取index下数据，否则取顶部数据，每次划掉一个已经remove掉一个顶部数据
            viewHolder = new MatchViewHolder(view);
            view.setTag(viewHolder);
        }
        viewHolder.setContent(dataList.get(index));
    }


    @Override
    public Rect obtainDraggableArea(View view) {
        View contentView = view.findViewById(R.id.layout_content);
        View topLayout = view.findViewById(R.id.img_content);
        View bottomLayout = view.findViewById(R.id.layout_footer);
        int left = view.getLeft() + contentView.getPaddingLeft() + topLayout.getPaddingLeft();
        int right = view.getRight() - contentView.getPaddingRight() - topLayout.getPaddingRight();
        int top = view.getTop() + contentView.getPaddingTop() + topLayout.getPaddingTop();
        int bottom = view.getBottom() - contentView.getPaddingBottom() - bottomLayout.getPaddingBottom();
        return new Rect(left, top, right, bottom);
    }

    class  MatchViewHolder 
    {
        private RoundImageView img_content;
        private TextView tv_name;
        private TextView tv_age;
        private TextView tv_constellation;
        private View view;
        public MatchViewHolder(View view)
        {
            this.view=view;
            img_content= (RoundImageView) view.findViewById(R.id.img_content);
            tv_name= (TextView) view.findViewById(R.id.tv_name);
            tv_age= (TextView) view.findViewById(R.id.tv_age);
            tv_constellation= (TextView) view.findViewById(R.id.tv_constellation);
        }

        public void  setContent(User user) {
            MVVPSetters.imageLoader(img_content,user.getHeadimg(),context.getResources().getDrawable(R.drawable.defalut_user));
            tv_name.setText(user.getNickName());
            tv_age.setText(user.getAge()!=0?user.getAge()+"":"未设置");
            MVVPSetters.setSexIcon(tv_age,user.getSex());
            tv_constellation.setText(user.getConstellation()!=null?user.getConstellation():"未设置");
        }

        public View getView() {
            return view;
        }
    }
}
