package com.xindian.fatechat.util;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.xindian.fatechat.R;
import com.yancy.imageselector.ImageConfig;
import com.yancy.imageselector.ImageSelector;

/**
 * Created by hx_Alex on 2017/3/30.
 * 图片选择工具类
 */

public class imageSelectorUtil {
    
    private final static int WRITE_EXTERNAL_STORAGE_REQUEST_CODE = 1;
    private ImageConfig mImageConfig;
    private Activity mContext;
    public imageSelectorUtil(Activity context,boolean isSingleSelect) 
    {
        mContext=context;
        //判断是否是需要单一选择不做二次裁剪
        if(isSingleSelect) {
            initImageConfigSingle();
        }else 
        {
            initImageConfig();
        }
    }
    public imageSelectorUtil(Activity context)
    {
        mContext=context;
        initImageConfig();
    }

    private void initImageConfig() {
        mImageConfig
                = new ImageConfig.Builder(new GlideLoader())
                // 如果在 4.4 以上，则修改状态栏颜色 （默认黑色）
                .steepToolBarColor(mContext.getResources().getColor(R.color.toolbar_background))
                // 标题的背景颜色 （默认黑色）
                .titleBgColor(mContext.getResources().getColor(R.color.toolbar_background))
                // 提交按钮字体的颜色  （默认白色）
                .titleSubmitTextColor(mContext.getResources().getColor(R.color.title_bar_color))
                // 标题颜色 （默认白色）
                .titleTextColor(mContext.getResources().getColor(R.color.title_bar_color))
                .singleSelect()
                .showCamera()
                .crop(1, 1, 400, 400)
                .scale()
                .filePath("/" + mContext.getPackageName() + "/image/")
                .build();
    }

    public void delCrop() {
        mImageConfig.delCrop();
    }

    /***
     * 选择单一图片初始化配置
     */
    private void initImageConfigSingle() {
        mImageConfig
                = new ImageConfig.Builder(new GlideLoader())
                // 如果在 4.4 以上，则修改状态栏颜色 （默认黑色）
                .steepToolBarColor(mContext.getResources().getColor(R.color.toolbar_background))
                // 标题的背景颜色 （默认黑色）
                .titleBgColor(mContext.getResources().getColor(R.color.toolbar_background))
                // 提交按钮字体的颜色  （默认白色）
                .titleSubmitTextColor(mContext.getResources().getColor(R.color.colorAccent))
                // 标题颜色 （默认白色）
                .titleTextColor(mContext.getResources().getColor(R.color.title_bar_color))
                .mutiSelect()
                .showCamera()
                .mutiSelectMaxSize(5)
                .isShowsubmitButton(true)
                .scale()
                .filePath("/" + mContext.getPackageName() + "/image/")
                .build();
    }


    public void changePortraitSingle() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                //申请WRITE_EXTERNAL_STORAGE权限
                ActivityCompat.requestPermissions(mContext, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        WRITE_EXTERNAL_STORAGE_REQUEST_CODE);
            } else {
                ImageSelector.open(mContext, mImageConfig);
            }
        } else {
            ImageSelector.open(mContext, mImageConfig);
        }

    }

    public void changePortrait() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                //申请WRITE_EXTERNAL_STORAGE权限
                ActivityCompat.requestPermissions(mContext, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        WRITE_EXTERNAL_STORAGE_REQUEST_CODE);
            } else {
                ImageSelector.open(mContext, mImageConfig);
            }
        } else {
            ImageSelector.open(mContext, mImageConfig);
        }

    }
    
    
    public class GlideLoader implements com.yancy.imageselector.ImageLoader {

        @Override
        public void displayImage(Context context, String path, ImageView imageView) {
            Glide.with(context)
                    .load(path)
                    .placeholder(com.yancy.imageselector.R.mipmap.imageselector_photo)
                    .centerCrop()
                    .into(imageView);
        }
    }
}
