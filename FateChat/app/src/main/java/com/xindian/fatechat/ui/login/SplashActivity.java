package com.xindian.fatechat.ui.login;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import com.hyphenate.chat.EMClient;
import com.hyphenate.easeui.EaseConstant;
import com.hyphenate.easeui.domain.ChatUserEntity;
import com.xindian.fatechat.DemoHelper;
import com.xindian.fatechat.R;
import com.xindian.fatechat.common.MVVPSetters;
import com.xindian.fatechat.databinding.ActivityGuideBinding;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.pay.wxpay.bbnpay.BbnPay;
import com.xindian.fatechat.ui.home.fragment.MainActivity;
import com.xindian.fatechat.ui.login.presenter.AccountPresenter;
import com.xindian.fatechat.ui.login.presenter.SplashPresenter;
import com.xindian.fatechat.ui.user.SetActivity;
import com.xindian.fatechat.util.ChannelUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Timer;
import java.util.TimerTask;

/***
 * 启动引导页
 */
public class SplashActivity extends Activity implements SplashPresenter.splashListener{
    public static final String LOGINSUCC="loginSuccessBySplashActivity";
    public static  final  String BYLOGINOUT= "LoginOutBySetActivity";//从界面退出回到此界面不做等待直接显示按钮
    Activity activity;
    private ActivityGuideBinding mBinding;
    private boolean isUserLogin;
    private AccountPresenter mPresenter;
    private SplashPresenter splashPresenter;
    private boolean isSplashLoad;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding= DataBindingUtil.setContentView(this,R.layout.activity_guide);
        activity = this;
        mPresenter=new AccountPresenter(this);
        splashPresenter=new SplashPresenter(this,this);
        splashPresenter.getSplash();
        timeOutLoadSplsh();
        mPresenter.reportUserDevice();
       BbnPay.init (this,getResources().getString(R.string.bbnPay_appid));
        EventBus.getDefault().register(this);
        String flag = getIntent().getStringExtra(SetActivity.TAG);
        
        if(flag!=null && flag.equals(BYLOGINOUT))
        {
            showBtnOperation();
        }else {
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    if (UserInfoManager.getManager(SplashActivity.this).isLogin()) {
                        UserInfoManager.getManager(SplashActivity.this).refreshUserInfo();
                        isUserLogin = true;
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (isUserLogin) {
                                gotoMainActivity();
                            } else {
                                showBtnOperation();
                            }
                        }
                    });
                }
            }, 2000);
        }
    }

    /***
     * 如果为已登录用户则直接进入主界面
     */
    public void gotoMainActivity()
    {
        if (DemoHelper.getInstance().isLoggedIn()) {
            EMClient.getInstance().chatManager().loadAllConversations();
        }
        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        ChatUserEntity chatUserEntity = (ChatUserEntity) getIntent().getSerializableExtra(EaseConstant.EXTRA_CHAT_USER);
        if(chatUserEntity!=null)
        {
            intent.putExtra(EaseConstant.EXTRA_CHAT_USER,chatUserEntity);
        }
        startActivity(intent);
        finish(); 
    }

    /****
     * 如果用户为新用户或者是退出登录了的用户则暂时操作按钮
     */
    public void showBtnOperation()
    {
        ObjectAnimator animator=ObjectAnimator.ofFloat(mBinding.btnRegister,"alpha",0,1);
        ObjectAnimator animator1=ObjectAnimator.ofFloat(mBinding.btnLogin,"alpha",0,1);
        animator.setDuration(1000).start();
        animator1.setDuration(1000).start();
        mBinding.btnLogin.setVisibility(View.VISIBLE);
        mBinding.btnRegister.setVisibility(View.VISIBLE);
    }
    
    public void onClikRegister(View view)
    {
        Intent i=new Intent(this,RegisterActivity.class);
        startActivity(i);
    }

    public void onClikLogin(View view)
    {
        Intent i=new Intent(this,LoginActivity.class);
        startActivity(i);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    /***
     * 当注册或是登录成功后需发送事件finish掉本页面
     * @param msg
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void  onEventCallback(String msg)
    {
        if(LOGINSUCC.equals(msg))
        {
            finish();
        }
    }
    
    
    public void timeOutLoadSplsh()
    {
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(!isSplashLoad){
                            if(ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.channel_fujin1))
                                    ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.channel_fujin2))
                                    ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.channel_fujin3))
                                    ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.channel_fujin4))
                                    ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.channel_fujin5))
                                    ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.channel_fujin6))
                                    ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f809))
                                    ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f810))
                                    ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f811))
                                    ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f812))
                                    ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f813))
                                    ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f818))
                                    ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f819))
                                    ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f820))
                                    ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f821))
                                    ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f822))
                                    ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f823))
                                    ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f824))
                                    ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f825))
                                    ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f826))
                                    ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f827))
                                    ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f828))
                                    ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f829)))

                            {
                                mBinding.imgSplash.setImageResource(R.drawable.fujin_splash);
                            }else
                            {
                                mBinding.imgSplash.setImageResource(R.drawable.loading);

                            }
                        }
                    }
                });
            }
        }, 2000);
    }

    @Override
    public void onGetSplashSucceed(String url) {

        if(ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.channel_fujin1))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.channel_fujin2))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.channel_fujin3))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.channel_fujin4))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.channel_fujin5))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.channel_fujin6))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f809))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f810))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f811))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f812))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f813))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f818))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f819))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f820))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f821))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f822))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f823))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f824))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f825))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f826))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f827))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f828))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f829)))

        {
            MVVPSetters.imageLoader(mBinding.imgSplash,url,getResources().getDrawable(R.drawable.fujin_splash));
        }else
        {

            MVVPSetters.imageLoader(mBinding.imgSplash,url,getResources().getDrawable(R.drawable.loading));

        }
        isSplashLoad=true;
    }

    @Override
    public void onGetSplashError() {
      

        if(ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.channel_fujin1))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.channel_fujin2))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.channel_fujin3))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.channel_fujin4))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.channel_fujin5))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.channel_fujin6))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f809))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f810))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f811))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f812))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f813))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f818))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f819))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f820))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f821))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f822))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f823))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f824))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f825))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f826))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f827))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f828))
                ||ChannelUtil.getChannel(SplashActivity.this).equals(getResources().getString(R.string.f829)))

        {
            mBinding.imgSplash.setImageResource(R.drawable.fujin_splash);
        }else 
        {

            mBinding.imgSplash.setImageResource(R.drawable.loading);

        }
        isSplashLoad=true;
    }
}
