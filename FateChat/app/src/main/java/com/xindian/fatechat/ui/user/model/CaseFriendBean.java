package com.xindian.fatechat.ui.user.model;

import com.xindian.fatechat.common.BaseModel;

/**
 * Created by hx_Alex on 2017/4/1.
 */

public class CaseFriendBean extends BaseModel {

    /**
     * id : 1
     * ageStart : 10
     * ageEnd : 19
     * address : 111
     * heightStart : null
     * heightEnd : null
     * qualifications : null
     * revenue : null
     * userId : 10000
     */

    private int id;
    private int ageStart;
    private int ageEnd;
    private String address;
    private String heightStart;
    private String heightEnd;
    private String qualifications;
    private String revenue;
    private int userId;
    private String ageString;
    private String heightString;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAgeStart() {
        return ageStart;
    }

    public void setAgeStart(int ageStart) {
        this.ageStart = ageStart;
    }

    public int getAgeEnd() {
        return ageEnd;
    }

    public void setAgeEnd(int ageEnd) {
        this.ageEnd = ageEnd;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }



    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }


    public String getHeightStart() {
        return heightStart;
    }

    public void setHeightStart(String heightStart) {
     
        this.heightStart = heightStart;
    }

    public String getHeightEnd() {
        return heightEnd;
    }

    public void setHeightEnd(String heightEnd) {
        this.heightEnd = heightEnd;
    }

    public String getQualifications() {
        return qualifications;
    }

    public void setQualifications(String qualifications) {
        this.qualifications = qualifications;
    }

    public String getRevenue() {
        return revenue;
    }

    public void setRevenue(String revenue) {
        this.revenue = revenue;
    }

    public String getHeightString() {
        return heightString;
    }



    /***
     * 设置身高字符串时，设置其start-end height
     * @param heightString
     */
    public void setHeightString(String heightString) {
        this.heightString = heightString;
        if(heightString==null) return;
        if(heightString.equals("不限")||heightString.equals("")) {
            setHeightStart(null);
            setHeightEnd(null);
        }
        if(heightString.contains("以上"))
        {
            String height = heightString.substring(0, heightString.indexOf("cm"));
            setHeightStart(height);
            setHeightEnd(null);
        }
        if(heightString.contains("-"))
        {
            String[] split = heightString.split("-");
            String heightStart = split[0].substring(0,split[0].indexOf("cm"));
            String heightEnd = split[1].substring(0,split[1].indexOf("cm"));
            setHeightStart(heightStart);
            setHeightEnd(heightEnd);
        }
    }

    public String getAgeString() {
        return ageString;
    }

    /***
     * 设置年龄字符串时，设置其start-end age
     * @param ageString
     */
    public void setAgeString(String ageString) {
        this.ageString = ageString;
        if(ageString==null) return;
        if(ageString.equals("不限") ||ageString.equals(""))
        {
            setAgeStart(0);
            setAgeEnd(0);
        }
        if(ageString.contains("以上"))
        {
            String age = ageString.substring(0, ageString.indexOf("岁"));
            setAgeStart(Integer.valueOf(age));
            setAgeEnd(0);
        }
        if(ageString.contains("-"))
        {
            String[] split = ageString.split("-");
            String ageStart = split[0].substring(0, split[0].indexOf("岁"));
            String ageEnd = split[1].substring(0, split[1].indexOf("岁"));
            setAgeStart(Integer.valueOf(ageStart));
            setAgeEnd(Integer.valueOf(ageEnd));
        }
    }

}
