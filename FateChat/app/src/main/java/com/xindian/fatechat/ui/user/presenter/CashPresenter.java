package com.xindian.fatechat.ui.user.presenter;

import android.content.Context;
import android.os.RemoteException;

import com.alibaba.fastjson.JSON;
import com.xindian.fatechat.common.BasePresenter;
import com.xindian.fatechat.common.ResultModel;
import com.xindian.fatechat.ui.user.model.PayTypeBean;
import com.xindian.fatechat.pay.model.ProductBean;
import com.xindian.fatechat.ui.user.model.User;
import com.xindian.fatechat.widget.MyProgressDialog;

import org.json.JSONException;

import java.util.HashMap;
import java.util.List;

/**
 * Created by hx_Alex on 2017/4/20.
 */

public class CashPresenter extends BasePresenter {

    private static final String  URL_PRODUCTLIST="/auth/product_list";
    private static final String  URL_PRODUCTLISTWITHDIAMONDS= "/auth/product_diamonds_list";
    private  static final  String URL_GET_PAY_TYPE="/pay-method/get-by-type";
    private  static final  String URL_GET_PAY_TYPE_CHANNELID="/pay-method/get-by-channelld";
    private onGetProductListListener getProductListListener;
    private onPayTypeInfoListener onPayTypeInfoListener;
    private onPayTypeInfoByChannelListener onPayTypeInfoByChannelListener;

    private MyProgressDialog mDialog;
    private Context context;
    private List<PayTypeBean>  payTypeBeanList;
    public CashPresenter(Context context,onGetProductListListener listener)
    {
        this.context=context;
        this.getProductListListener=listener;
        mDialog=new MyProgressDialog(context);
    }

    public void setOnPayTypeInfoListener(onPayTypeInfoListener onPayTypeInfoListener) {
        this.onPayTypeInfoListener = onPayTypeInfoListener;
    }

    public void setOnPayTypeInfoByChannelListener(CashPresenter.onPayTypeInfoByChannelListener onPayTypeInfoByChannelListener) {
        this.onPayTypeInfoByChannelListener = onPayTypeInfoByChannelListener;
    }



    public void requestGetProductList(User user)
    {
        HashMap<String,Object> data=new HashMap<>();
        data.put("userId",user.getUserId());
        post(getUrl(URL_PRODUCTLIST),data,null);
        mDialog.show();
    }

    public void requestGetProductListWithDiamonds(User user)
    {
        HashMap<String,Object> data=new HashMap<>();
        data.put("userId",user.getUserId());
        post(getUrl(URL_PRODUCTLISTWITHDIAMONDS),data,null);
        mDialog.show();
    }

    public void getPayType(int type) {
        HashMap<String, Object> data = new HashMap<>();
        data.put("type", type);
        get(getUrl(URL_GET_PAY_TYPE), data, null);
        if(!mDialog.isShowing()) {
            mDialog.show();
        }
    }

 

    /***
     * 根据渠道获取支付方式
     */
    public void getPayTypeChanneid() {
        HashMap<String, Object> data = new HashMap<>();
        get(getUrl(URL_GET_PAY_TYPE_CHANNELID), data, null);
        if(!mDialog.isShowing()) {
            mDialog.show();
        }
    }


    @Override
    public Object asyncExecute(String url, ResultModel resultModel) {
        if(url.contains(URL_PRODUCTLIST)) {
            return JSON.parseArray(resultModel.getData(),ProductBean.class);
        }else if(url.contains(URL_GET_PAY_TYPE))
        {
            return JSON.parseObject(resultModel.getData(), PayTypeBean.class);

        }else if(url.contains(URL_GET_PAY_TYPE_CHANNELID))
        {
            return JSON.parseArray(resultModel.getData(), PayTypeBean.class);
        }else if(url.contains(URL_PRODUCTLISTWITHDIAMONDS))
        {
            return JSON.parseArray(resultModel.getData(),ProductBean.class);

        }
        return  null;
    }

    @Override
    public void onError(String url, Exception e) {
        super.onError(url, e);
    }


    @Override
    public void onFailure(String url, ResultModel resultModel) {
        super.onFailure(url,resultModel);
        if(url.contains(URL_PRODUCTLIST)) {
            getProductListListener.onGetProductListError(resultModel.getMessage().equals("") || resultModel.getMessage() == null ? "未知错误" : resultModel.getMessage());
        }else  if (url.contains(URL_GET_PAY_TYPE)) {
            onPayTypeInfoListener.onPayTypeInfoError(resultModel.getMessage().equals("") || resultModel.getMessage() == null ? "未知错误" : resultModel.getMessage());

        }else  if (url.contains(URL_GET_PAY_TYPE_CHANNELID)) {
            onPayTypeInfoByChannelListener.onPayTypeInfoError(resultModel.getMessage().equals("") || resultModel.getMessage() == null ? "未知错误" : resultModel.getMessage());
        }else  if(url.contains(URL_PRODUCTLISTWITHDIAMONDS))
        {
            getProductListListener.onGetProductListError(resultModel.getMessage().equals("") || resultModel.getMessage() == null ? "未知错误" : resultModel.getMessage());

        }
       
        mDialog.dismiss();
    }

    @Override
    public void onSucceed(String url, ResultModel resultModel) throws JSONException, RemoteException {
        if(url.contains(URL_PRODUCTLIST))
        {
            List<ProductBean> dataModel = (List<ProductBean>) resultModel.getDataModel();
            getProductListListener.onGetProductListSuccess(dataModel);
        }else if(url.contains(URL_GET_PAY_TYPE)){
            PayTypeBean dataModel = (PayTypeBean) resultModel.getDataModel();
            if (dataModel != null) {
                onPayTypeInfoListener.onPayTypeInfoSuccess(dataModel);
            }
        }else if(url.contains(URL_GET_PAY_TYPE_CHANNELID)){
            payTypeBeanList = (List<PayTypeBean>) resultModel.getDataModel();
            if (payTypeBeanList != null) {
                onPayTypeInfoByChannelListener.onPayTypeInfoSuccess(payTypeBeanList);
            }
        }else  if(url.contains(URL_PRODUCTLISTWITHDIAMONDS))
        {
            List<ProductBean> dataModel = (List<ProductBean>) resultModel.getDataModel();
            getProductListListener.onGetProductListSuccess(dataModel);

        }
  
        mDialog.dismiss();
    }

    /**
     * 支付产品列表-》vip 钻石
     */
    public  interface  onGetProductListListener
    {
         void onGetProductListSuccess( List<ProductBean> productBeanList);
        void onGetProductListError(String msg);
    }
    
    public interface onPayTypeInfoListener {
        void  onPayTypeInfoSuccess(PayTypeBean bean);

        void onPayTypeInfoError(String msg);
    }


    public interface onPayTypeInfoByChannelListener {
        void  onPayTypeInfoSuccess(List<PayTypeBean> payTypeBeanList);

        void onPayTypeInfoError(String msg);
    }

}
