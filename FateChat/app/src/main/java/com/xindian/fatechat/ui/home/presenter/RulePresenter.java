package com.xindian.fatechat.ui.home.presenter;

import android.os.RemoteException;
import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.parser.Feature;
import com.thinkdit.lib.util.StringUtil;
import com.xindian.fatechat.common.BasePresenter;
import com.xindian.fatechat.common.ResultModel;
import com.xindian.fatechat.ui.home.model.RulesModel;

import org.json.JSONException;

import java.util.HashMap;

/**
 */

public class RulePresenter extends BasePresenter {
    private final String URL_REGISTER_NEWSRULE = "/auth/registerNewsRule";


    IGetRules mGetRules;

    public RulePresenter(IGetRules rules) {
        mGetRules = rules;
    }

    public void getRules() {
        HashMap<String, Object> data = new HashMap<>();
        get(getUrl(URL_REGISTER_NEWSRULE), data, null);
    }


    @Override
    public Object asyncExecute(String url, ResultModel resultModel) {
        if (url.contains(URL_REGISTER_NEWSRULE)) {
            //json格式化
            String data = resultModel.getData();
            String newData="";
            if(!StringUtil.isNullorEmpty(data))
            {
                newData = data.replaceAll("\\\\", "");
                //newdata格式 {"acceptableWord":"2","channelId":"freeChannel","channelName":"自由渠道",
                // "createTime":1492572944000,"id":9,"messageLimit":"2","messageLimitJson":"{"boy":"1","gay":"1"}"
                //替换包含"{,}"
                String temp = newData.replaceAll("\"\\{", "{");
                newData = temp.replaceAll("\\}\"", "}");
                Log.e("URL_REGISTER_NEWSRULE",newData);
                return JSON.parseObject(newData, RulesModel.class, Feature
                        .IgnoreNotMatch);
            }
        }
        return null;
    }

    @Override
    public void onError(String url, Exception e) {
        super.onError(url, e);
    }


    @Override
    public void onFailure(String url, ResultModel resultModel) {
        super.onFailure(url,resultModel);
        if (url.contains(URL_REGISTER_NEWSRULE)) {
        }
    }

    @Override
    public void onSucceed(String url, ResultModel resultModel) throws JSONException,
            RemoteException {
        if (url.contains(URL_REGISTER_NEWSRULE)) {
            RulesModel ruleMode = (RulesModel) resultModel.getDataModel();
            mGetRules.onGetRulesSuccess(ruleMode);
        }
    }

    public interface IGetRules {
        public void onGetRulesSuccess(RulesModel model);
    }

}
