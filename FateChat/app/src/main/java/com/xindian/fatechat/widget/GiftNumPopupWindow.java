package com.xindian.fatechat.widget;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.PopupWindow;

import com.hyphenate.easeui.domain.ChatUserEntity;
import com.xindian.fatechat.R;
import com.xindian.fatechat.ui.home.presenter.ReportPresenter;

/**
 * 礼物数量选择框
 */

public class GiftNumPopupWindow extends PopupWindow  implements View.OnClickListener{
    private Context mContext;

    public GiftNumPopupWindow(Context context){
        super(context);
        this.mContext = context;
        init();
    }

    private void init(){
        setAnimationStyle(R.style.popup_show_Animation);
        // 设置弹出窗体的宽和高
        setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
        // 设置弹出窗体可点击
        setFocusable(true);
        setBackgroundDrawable(new ColorDrawable(0x00000000));
        //解决华为手机window被navigationBar挡住
        setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        View rootView = View.inflate(mContext, R.layout.layout_gift_num, null);
        rootView.findViewById(R.id.ly_num_1).setOnClickListener(this);
        rootView.findViewById(R.id.ly_num_10).setOnClickListener(this);
        rootView.findViewById(R.id.ly_num_30).setOnClickListener(this);
        rootView.findViewById(R.id.ly_num_66).setOnClickListener(this);
        rootView.findViewById(R.id.ly_num_188).setOnClickListener(this);
        rootView.findViewById(R.id.ly_num_520).setOnClickListener(this);
        rootView.findViewById(R.id.ly_num_1314).setOnClickListener(this);
        setContentView(rootView);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.ly_num_1:
                selectedNum(1);
                dismiss();
                break;
            case R.id.ly_num_10:
                selectedNum(10);
                dismiss();
                break;
            case R.id.ly_num_30:
                selectedNum(30);
                dismiss();
                break;
            case R.id.ly_num_66:
                selectedNum(66);
                dismiss();
                break;
            case R.id.ly_num_188:
                selectedNum(188);
                dismiss();
                break;
            case R.id.ly_num_520:
                selectedNum(520);
                dismiss();
                break;
            case R.id.ly_num_1314:
                selectedNum(1314);
                dismiss();
                break;

        }
    }

    private void selectedNum(int num){
        if(onGiftNumSelecedListener!=null){
            onGiftNumSelecedListener.onNumSeleced(num);
        }
    }

    OnGiftNumSelecedListener onGiftNumSelecedListener;
    public void setOnGiftNumSelecedListener(OnGiftNumSelecedListener onGiftNumSelecedListener) {
        this.onGiftNumSelecedListener = onGiftNumSelecedListener;
    }

    interface OnGiftNumSelecedListener{
        public void onNumSeleced(int num);
    }
}
