package com.xindian.fatechat.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;

/**
 * Created by hx_Alex on 2017/4/1.
 * 菜单item式布局,可将布局事件分发至所有子child view
 */

public class MenuLinearLayout extends LinearLayout {
    private  OnClickListener l;
    private boolean isDispatchComplete=false;//分发是否完成
    public MenuLinearLayout(Context context) {
        super(context);
    }

    public MenuLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MenuLinearLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    
    @Override
    public void setOnClickListener(OnClickListener l) {
        super.setOnClickListener(l);
        this.l=l;
        this.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
//                Log.e("onGlobalLayout","onGlobalLayout");
                dispatchAllChidViewEvent(MenuLinearLayout.this,l);
            }
        });
    }
    
    private void  dispatchAllChidViewEvent(ViewGroup v,OnClickListener l)
    {
//        Log.e("Event","dispatchAllChidViewEvent");
        for (int i=0;i<v.getChildCount();i++)
        {
            View view = v.getChildAt(i);
            //如果是viewGroup，继续分发事件
            if(view instanceof ViewGroup)
            {
                dispatchAllChidViewEvent((ViewGroup)view,l);
            }
            else
            {
                view.setOnClickListener(l);
            }
        }
    }
   
}
