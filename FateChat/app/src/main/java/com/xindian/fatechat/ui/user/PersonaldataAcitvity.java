package com.xindian.fatechat.ui.user;


import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.xindian.fatechat.R;
import com.xindian.fatechat.databinding.ActivityPersonaldataAcitvityBinding;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.ui.base.BaseActivity;
import com.xindian.fatechat.ui.login.presenter.AccountPresenter;
import com.xindian.fatechat.ui.user.data.UserData;
import com.xindian.fatechat.ui.user.model.User;
import com.xindian.fatechat.ui.user.presenter.PersonaldataPresenter;
import com.hyphenate.easeui.utils.SnackbarUtil;
import com.xindian.fatechat.util.imageSelectorUtil;
import com.xindian.fatechat.widget.MessageSelectionDialog;
import com.yancy.imageselector.ImageSelector;
import com.yancy.imageselector.ImageSelectorActivity;

import java.util.List;

/**
 * Created by hx_Alex on 2017/3/27.
 */

public class PersonaldataAcitvity extends BaseActivity implements MessageSelectionDialog.onSaveListener,
        PersonaldataPresenter.onPersonaldataListener,UserInfoManager.IUserInfoUpdateListener,AccountPresenter.IAvatarUploadedListener,
        PersonaldataPresenter.onPersonalExamineHeadImgListener{
    public static  final  String NOW_UP_LOAD_AVATAR="uploadAvatar";
    private  static  final  int  NICKNAME=1;
    private  static  final  int  CITY=2;
    private  static  final  int  QQ=3;
    private  static  final  int  WX=4;
    private  static  final  int  PHONE=5;
    private  static  final  int  EDUCATION=6;
    private  static  final  int  OCCUPATION=7;
    private  static  final  int  WEIGHT=8;
    private  static  final  int  COSTELLATION=9;
    private  static  final  int  DATE=10;
    private  static  final  int  AGE=11;
    private  static  final  int  HEIGHT=12;
    private  static  final  int  INCOME=13;
    private  static  final  int  MARRIAGE=14;
    
    private ActivityPersonaldataAcitvityBinding mBinding;
    private  int openDialog;
    private imageSelectorUtil imageSelectorUtil;
    private User user;
    private PersonaldataPresenter mPresenter;
    private AccountPresenter accountPresenter;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding= DataBindingUtil.setContentView(this, R.layout.activity_personaldata_acitvity);
        initToolBar();
        user= (User) UserInfoManager.getManager(this).getUserInfo().clone();
        UserInfoManager.getManager(this).addUserInfoUpdateListener(this);
        imageSelectorUtil=new imageSelectorUtil(this);
        mBinding.setUser(user);
        mPresenter=new PersonaldataPresenter(this,this);
        mPresenter.setmPersonalExamineHeadImgListener(this);
        accountPresenter=new AccountPresenter(this);
        accountPresenter.setAvatarUploadedListener(this);
        boolean nowUpLoadAvatar = getIntent().getBooleanExtra(NOW_UP_LOAD_AVATAR, false);
        if(nowUpLoadAvatar)
        {
            imageSelectorUtil.changePortrait();
        }
    }
    
 
    
    public void setNickName(View v)
    {
        MessageSelectionDialog dialog = showMessageSelectionDialog(NICKNAME,null,"设置昵称",MessageSelectionDialog.EDIT_SELECTION);
        dialog.setMaxEditLen(5);

    }


    public void setCity(View v)
    {
        showMessageSelectionDialog(CITY,null,"设置居住地",MessageSelectionDialog.LIVE_SELECTION);
    }

    public void setQQ(View v)
    {
        showMessageSelectionDialog(QQ,null,"设置QQ号",MessageSelectionDialog.EDIT_VIP_SELECTION);
    }

    public void setWx(View v)
    {
        showMessageSelectionDialog(WX,null,"设置微信号",MessageSelectionDialog.EDIT_VIP_SELECTION);
    }
    public void setPhone(View v)
    {
        showMessageSelectionDialog(PHONE,null,"设置手机号",MessageSelectionDialog.EDIT_VIP_SELECTION);
    }
    public void onUpdate (View v)
    {
        SnackbarUtil.makeSnackBar(mBinding.getRoot(),"当前信息无法修改", Snackbar.LENGTH_SHORT).setMeessTextColor(Color.WHITE).show();
    }

    public void onClickSelectEducation(View v)
    {
        showMessageSelectionDialog(EDUCATION, UserData.getEducationList(),"学历",null);
    }

    public  void onClickSelectAge(View v)
    {
        openDialog=AGE;
        MessageSelectionDialog dialog=new MessageSelectionDialog(this, UserData.getAge(),"选择年龄",this);
        dialog.show();
    }

    public void onClickSelectOccupation(View v)
    {
        showMessageSelectionDialog(OCCUPATION,UserData.getOccupationList(),"职业",null);
    }
    public void onClickSelectWeight(View v)
    {
        showMessageSelectionDialog(WEIGHT,UserData.getWeight(),"体重",null);
    }
    public void onClickSelectConstellation(View v)
    {
        showMessageSelectionDialog(COSTELLATION,UserData.getConstellation(),"星座",null);
    }

    public void onClickSelectDate(View v)
    {
        showMessageSelectionDialog(DATE,null,"生日",MessageSelectionDialog.DATE_SELECTION);
    }

    public void onClickSelectheight(View v)
    {
        showMessageSelectionDialog(HEIGHT, UserData.getHeightList(),"身高",null);
    }
    public void onClickSelectIncome(View v) {

        showMessageSelectionDialog(INCOME, UserData.getIncomeList(),"月收入",null);
    }

    public void onClickSelectMarriage(View v) {
        showMessageSelectionDialog(MARRIAGE, UserData.getMarriageList(),"婚姻状态",null);
    }
    
    public MessageSelectionDialog showMessageSelectionDialog(int dialogTag, List<String> dataList, String titleString,String useKind)
    {
        openDialog=dialogTag;
        MessageSelectionDialog dialog=null;
        if(useKind==null) {
            dialog= new MessageSelectionDialog(this, dataList, titleString, this);
        }else {
             dialog=new MessageSelectionDialog(this,titleString,useKind,this);
             dialog.setView(new EditText(this));
        }
        dialog.show();
        return  dialog;
    }
 

    public void onClickSelectPhoto(View v)
    {
        imageSelectorUtil.changePortrait();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            if (requestCode == ImageSelector.IMAGE_REQUEST_CODE) {
                // Get Image Path List
                List<String> pathList = data.getStringArrayListExtra(ImageSelectorActivity.EXTRA_RESULT);
                String path = pathList.get(pathList.size() - 1);
                accountPresenter.uploadAvatar(path);
          
            }
        }
    }

    @Override
    public void onSave(String content,String cotent2) {
        if(openDialog==NICKNAME){
            user.setNickName(content);
        }else if(openDialog==CITY) {
            user.setAddress(content);
        }else if(openDialog==WX) {
            user.setWechat(content);
        }else if(openDialog==PHONE) {
            if (isMobileNO(content)){
                user.setMobile(content);
            }else {
                SnackbarUtil.makeSnackBar(mBinding.getRoot(),"手机格式不正确,请重新设置!", Toast.LENGTH_SHORT).setMeessTextColor(Color.WHITE).show();
                return;
            }
        }else if(openDialog==EDUCATION) {
            user.setQualifications(content);
        }else if(openDialog==OCCUPATION) {
            user.setProfession(content);
        } else if(openDialog==WEIGHT) {
            user.setWeight(content);
        }else if(openDialog==COSTELLATION) {
            user.setConstellation(content);
        }else if(openDialog==DATE){
            user.setBirthday(content);
        }else if(openDialog==AGE){
            user.setAge(Integer.parseInt(content.split("岁")[0]));
        }else if(openDialog==HEIGHT){
            user.setHeight(content);
        }else if(openDialog==INCOME){
            user.setRevenue(content);
        }else if(openDialog==MARRIAGE){
            user.setMarriage(content);
        }
        mPresenter.requestUpdateFriendsCase(user);
        
    }

    @Override
    public void onPersonaldataSuccess() {
        SnackbarUtil.makeSnackBar(mBinding.getRoot(),"信息更新成功!", Toast.LENGTH_SHORT).setMeessTextColor(Color.WHITE).show();
        UserInfoManager.getManager(this).refreshUserInfo();
    }

    @Override
    public void onPersonaldataError(String msg) {

    }

    @Override
    public void onUserInfoUpdated() {
        user= (User) UserInfoManager.getManager(this).getUserInfo().clone();
        mBinding.setUser(user);
    }

    @Override
    public void onAvatarUploadedSucceed(String url) {
        user.setHeadimg(url);
        mPresenter.requestExamineHeadImg(user);
    }

    @Override
    public void onAvatarUploadedError() {
        
    }

    @Override
    public void onExamineHeadImgSuccess() {
        SnackbarUtil.makeSnackBar(mBinding.getRoot(),"您的头像信息已上传成功，等待审核中，审核通过后将予以显示!", Snackbar.LENGTH_LONG).setMeessTextColor(Color.WHITE).show();
    }

    @Override
    public void onExamineHeadImgError(String msg) {
        SnackbarUtil.makeSnackBar(mBinding.getRoot(),"头像信息上传失败，原因: "+msg, Snackbar.LENGTH_LONG).setMeessTextColor(Color.WHITE).show();
    }

    public static boolean isMobileNO(String mobiles) {
        //"[1]"代表第1位为数字1，"[358]"代表第二位可以为3、5、8中的一个，"\\d{9}"代表后面是可以是0～9的数字，有9位。  
        String telRegex = "[1][34578]\\d{9}" ;
        if (TextUtils.isEmpty(mobiles)) return false ;
        else return mobiles.matches( telRegex ) ;
    }


}
