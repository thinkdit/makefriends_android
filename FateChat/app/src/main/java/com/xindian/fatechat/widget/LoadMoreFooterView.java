package com.xindian.fatechat.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.xindian.fatechat.R;


/**
 * Created by zjzhu on 16-7-4.
 */
public class LoadMoreFooterView extends LinearLayout implements View.OnClickListener {

    private TextView mDesTextView;
    private ProgressBar mProgressBar;
    private OnRetryLoadListener mOnRetryLoadListener;

    public LoadMoreFooterView(Context context) {
        this(context, null);
    }

    public LoadMoreFooterView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LoadMoreFooterView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mDesTextView = (TextView) findViewById(R.id.des);
        mProgressBar = (ProgressBar) findViewById(R.id.view_progressbar);
    }

    public void showLoading() {
        mDesTextView.setVisibility(GONE);
        mProgressBar.setVisibility(VISIBLE);
        mDesTextView.setOnClickListener(null);
    }

    public void showError() {
        mDesTextView.setVisibility(VISIBLE);
        mProgressBar.setVisibility(GONE);
        mDesTextView.setText(R.string.loadmore_retry);
        mDesTextView.setOnClickListener(this);
    }

    public void showEnd() {
        mDesTextView.setVisibility(VISIBLE);
        mProgressBar.setVisibility(GONE);
        mDesTextView.setText(R.string.loadmore_end);
        mDesTextView.setOnClickListener(null);
    }

    public static LoadMoreFooterView getLoadMoreFooterView(Context context) {
        return (LoadMoreFooterView) View.inflate(context, R.layout.layout_pullup_loadmore, null);
    }

    public void setOnRetryLoadListener(OnRetryLoadListener loadListener) {
        mOnRetryLoadListener = loadListener;
    }

    @Override
    public void onClick(View v) {
        if (mOnRetryLoadListener != null) {
            mOnRetryLoadListener.onRetryLoad();
        }
    }

    public interface OnRetryLoadListener {
        public void onRetryLoad();
    }
}
