package com.xindian.fatechat.ui.login;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.hyphenate.easeui.utils.DBHelper;
import com.hyphenate.easeui.utils.SnackbarUtil;
import com.thinkdit.lib.util.StringUtil;
import com.xindian.fatechat.IMApplication;
import com.xindian.fatechat.R;
import com.xindian.fatechat.databinding.ActivityLoginBinding;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.ui.base.BaseActivity;
import com.xindian.fatechat.ui.home.fragment.MainActivity;
import com.xindian.fatechat.ui.login.presenter.AccountPresenter;
import com.xindian.fatechat.ui.user.model.User;

import org.greenrobot.eventbus.EventBus;

import java.util.Map;

import static android.os.Build.ID;


public class LoginActivity extends BaseActivity implements AccountPresenter.OnLoginListener{
private ActivityLoginBinding mBinding;
    private AccountPresenter mPresenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       mBinding= DataBindingUtil.setContentView(this, R.layout.activity_login);
        initToolBar();
        mPresenter=new AccountPresenter(this);
        mPresenter.setOnLoginListener(this);
        getLastUserInfo();
    }

    /**
     * 获取成功登陆的账号密码信息并显示
     */
    public void getLastUserInfo() {
       
            Map<String, String> loginData = UserInfoManager.getManager(this).GetLoginData();
            if (loginData.get(UserInfoManager.KEY_USERID) != null && loginData.get(UserInfoManager.KEY_PASSWORD) != null) {
                mBinding.editAccount.setText(loginData.get(UserInfoManager.KEY_USERID).toString());
                mBinding.editPassword.setText(loginData.get(UserInfoManager.KEY_PASSWORD).toString());
            }
      
    }

    public boolean checkData(String loginId, String password) {
        if (StringUtil.isNullorEmpty(loginId)) {
            SnackbarUtil.makeSnackBar(mBinding.getRoot(),"账号不能为空,请重新输入!", Toast.LENGTH_SHORT).setMeessTextColor(Color.WHITE).show();
            return false;
        }
        if (StringUtil.isNullorEmpty(password)) {
            SnackbarUtil.makeSnackBar(mBinding.getRoot(),"密码不能为空,请重新输入!", Toast.LENGTH_SHORT).setMeessTextColor(Color.WHITE).show();
            return false;
        }
        return true;
    }
    
    public void onClickLogin(View view)
    {
        String loginId = mBinding.editAccount.getText().toString();
        String password = mBinding.editPassword.getText().toString();
        if (checkData(loginId, password)) {
            mPresenter.requestLogin(loginId, password);
        }
    }

    @Override
    public void LoginSucceed(User user) {
        Intent i=new Intent(this,MainActivity.class);
        startActivity(i);
        /***
         * 登录成功时初始化db
         */
        if (UserInfoManager.getManager(this).getUserInfo() != null) {
            DBHelper.intance().init(IMApplication.getInstance(), UserInfoManager.getManager
                    (this).getUserInfo().getUserId());
        }
        EventBus.getDefault().post(SplashActivity.LOGINSUCC);//发送消息通知引导页登陆成功，finsh掉自己

        finish();
    }

    @Override
    public void LoginError(String msg) {
        SnackbarUtil.makeSnackBar(mBinding.getRoot(),msg, Toast.LENGTH_SHORT).setMeessTextColor(Color.WHITE).show();
    }
}
