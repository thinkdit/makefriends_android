package com.xindian.fatechat.pay.model;

import com.xindian.fatechat.common.BaseModel;

/**
 * Created by hx_Alex on 2017/4/20.
 */

public class ProductBean extends BaseModel {

    /**
     * pid : 111
     * day : 30
     * money : 1
     * remarks : 1.6元/天
     * sort : 1
     * createdTime : 1492055662000
     */

    private int pid;
    private int day;
    private int money;
    private String remarks;
    private int sort;
    private long createdTime;

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public long getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(long createdTime) {
        this.createdTime = createdTime;
    }
}
