package com.xindian.fatechat.ui.user.presenter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.RemoteException;
import android.view.ViewGroup;

import com.alibaba.fastjson.JSON;
import com.xindian.fatechat.common.BasePresenter;
import com.xindian.fatechat.common.ResultModel;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.ui.user.CashActivity;
import com.xindian.fatechat.ui.user.UserDetailsActivity;
import com.xindian.fatechat.ui.user.adapter.AttentionMyListAdapter;
import com.xindian.fatechat.ui.user.adapter.MyAttentionListAdapter;
import com.xindian.fatechat.ui.user.model.UserBean;
import com.xindian.fatechat.widget.StarTDialog;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

/**
 * Created by hx_Alex on 2017/4/4.
 */

public class MyAttentionPresenter extends BasePresenter
{
    private final String  URL_ATTENTIONLIST="/auth/attention_list";
    private final String  URL_CANCELATTENTION="/auth/cancel_attention";
    private final String  URL_ADDATTENTION="/auth/add_attention";
    private final String  URL_ATTENTIONMYLIST="/auth/attention_My_list";
    
    private MyAttentionListAdapter myAttentionAdapter;//我的照片显示适配器
    private AttentionMyListAdapter attentionMyListAdapter;
    private ArrayList<UserBean> mList;
    private Context context;
    private onGetMyAttentionListener mGetListener;
    private onCancelMyAttentionListener mCancelMyAttentionListener;
    private onAddAttentionListener mAddAttentionListener;
    private onGetAttentionMyListener onGetAttentionMyListener;
    private UserBean nowCancelUserBean;
   

    public MyAttentionPresenter(Context context) {
        
        this.context = context;
    }
    
    
    
    private void requestGetMyAttentionList(int userId,int page,int pageSize)
    {
        HashMap<String,Object> data=new HashMap<>();
        data.put("userId",userId);
        data.put("page",page);
        data.put("pageSize",pageSize);
        post(getUrl(URL_ATTENTIONMYLIST),data,context);
    }


    private void requestGetMyAttention(int userId,int page,int pageSize)
    {
        HashMap<String,Object> data=new HashMap<>();
        data.put("userId",userId);
        data.put("page",page);
        data.put("pageSize",pageSize);
        post(getUrl(URL_ATTENTIONLIST),data,context);
    }

    private void requestCancelAttention(int userId,int femmeUserId)
    {
        HashMap<String,Object> data=new HashMap<>();
        data.put("userId",userId);
        data.put("femmeUserId",femmeUserId);
        post(getUrl(URL_CANCELATTENTION),data,context);
    }

    public void requestAddAttention(int userId,int femmeUserId)
    {
        HashMap<String,Object> data=new HashMap<>();
        data.put("userId",userId);
        data.put("femmeUserId",femmeUserId);
        post(getUrl(URL_ADDATTENTION),data,context);
    }

    /***
     * 请求我关注的
     * @param userId
     * @param page
     * @param pageSize
     */
    public void getMyAttentionListAdapter(int userId,int page,int pageSize)
    {
        requestGetMyAttention(userId,page,pageSize);
    }

    /***
     * 请求关注的我
     * @param userId
     * @param page
     * @param pageSize
     */
    public void getAttentionMyListAdapter(int userId,int page,int pageSize)
    {
        requestGetMyAttentionList(userId,page,pageSize);
    }

    /***
     * 切换tab时清空数据
     */
    public void  clearData()
    {
        mList=null;
        myAttentionAdapter=null;
        attentionMyListAdapter=null;
    }

    @Override
    public Object asyncExecute(String url, ResultModel resultModel) {
        if(url.contains(URL_ATTENTIONLIST)) {
            return JSON.parseArray(resultModel.getData(),UserBean.class);
        }  if(url.contains(URL_ATTENTIONMYLIST)) {
            return JSON.parseArray(resultModel.getData(),UserBean.class);
        }
        return  null;
    }


    @Override
    public void onSucceed(String url, ResultModel resultModel) throws JSONException, RemoteException {
        if(url.contains(URL_ATTENTIONLIST)) {
            //如果mlist，mAdapter为null,则是第一次加载数据
            if(mList==null && myAttentionAdapter==null)
            {
                mList= (ArrayList<UserBean> ) resultModel.getDataModel();
                myAttentionAdapter=new MyAttentionListAdapter(mList,context);
                myAttentionAdapter.setOnClickCancelListener(new MyAttentionListAdapter.onClickCancelListener() {
                    @Override
                    public void onClickBtnCancelAttention(int femmeUserId) {
                        nowCancelUserBean=new UserBean();
                        nowCancelUserBean.setUserId(femmeUserId);
                        int userId = UserInfoManager.getManager(context).getUserId();
                        if(userId==0) return;
                        requestCancelAttention(userId,femmeUserId);
                    }

                    @Override
                    public void onClickAttention(int femmeUserId) {
                        Intent i=new Intent(context, UserDetailsActivity.class);
                        i.putExtra(UserDetailsActivity.FRMMEUSERID,femmeUserId);
                        context.startActivity(i);
                    }
                });
            }
            else 
            {
                Collection<UserBean> c=(ArrayList<UserBean> ) resultModel.getDataModel();
                mList.addAll(c);
            }
            mGetListener.onGetMyAttentionSuccess(myAttentionAdapter);
        }else if(url.contains(URL_CANCELATTENTION)){
            for (UserBean bean:mList)
            {
                if(bean.getUserId()==nowCancelUserBean.getUserId()){
                    mList.remove(bean);
                    break;
                }
            }
            mCancelMyAttentionListener.onCancelMyAttentionSuccess(myAttentionAdapter);
            nowCancelUserBean=null;
        }else if(url.contains(URL_ADDATTENTION)){
            mAddAttentionListener.onAddAttentionSuccess();
        }else if(url.contains(URL_ATTENTIONMYLIST))
        {
            //如果mlist，mAdapter为null,则是第一次加载数据
            if(mList==null&& attentionMyListAdapter==null)
            {
                boolean isVip = UserInfoManager.getManager(context).getUserInfo().isVip();
                mList= (ArrayList<UserBean> ) resultModel.getDataModel();
                attentionMyListAdapter=new AttentionMyListAdapter(mList,context,isVip);
                attentionMyListAdapter.setOnClickCancelListener(new AttentionMyListAdapter.onClickCancelListener() {
                    @Override
                    public void onClickAttention(int femmeUserId) {
                        if(!isVip)
                        {
                            showDialog();
                        }else 
                        {
                            Intent i=new Intent(context, UserDetailsActivity.class);
                            i.putExtra(UserDetailsActivity.FRMMEUSERID,femmeUserId);
                            context.startActivity(i);
                        }
                    }
                });
            }
            else
            {
                Collection<UserBean> c=(ArrayList<UserBean> ) resultModel.getDataModel();
                mList.addAll(c);
            }
            onGetAttentionMyListener.onGetAttentionMySuccess(attentionMyListAdapter);
        }
    }
    

    @Override
    public void onFailure(String url, ResultModel resultModel) {
        super.onFailure(url,resultModel);
        if(url.contains(URL_ATTENTIONLIST)) {

            mGetListener.onGetMyAttentionError(resultModel.getMessage());
        }  else if(url.contains(URL_CANCELATTENTION)){
          
            mCancelMyAttentionListener.onCancelMyAttentionError(resultModel.getMessage());
        }else if(url.contains(URL_ADDATTENTION)){

            mAddAttentionListener.onAddAttentionError(resultModel.getMessage());
        }else if(url.contains(URL_ATTENTIONMYLIST)){

            onGetAttentionMyListener.onGetAttentionMyError(resultModel.getMessage());
        }
    }


    
    public void setOnGetMyAttentionListener(onGetMyAttentionListener mGetListener) {
        this.mGetListener = mGetListener;
    }
    public void setonCancelMyAttentionListener(onCancelMyAttentionListener mCancelMyAttentionListener) {
        this.mCancelMyAttentionListener = mCancelMyAttentionListener;
    }
    public void setOnAddMyAttentionListener(onAddAttentionListener mAddAttentionListener) {
        this.mAddAttentionListener = mAddAttentionListener;
    }
    public void setOnGetAttentionMyListener(MyAttentionPresenter.onGetAttentionMyListener onGetAttentionMyListener) {
        this.onGetAttentionMyListener = onGetAttentionMyListener;
    }

   
    public void showDialog()
    {
        if(context instanceof Activity) {
            StarTDialog dialog = new StarTDialog(context, (ViewGroup) ((Activity) context).getWindow().getDecorView());
            dialog.setBtnOkCallback(new StarTDialog.iDialogCallback() {
                @Override
                public boolean onBtnClicked() {
                    Intent i = new Intent(context, CashActivity.class);
                    i.putExtra(CashActivity.ENTRYSOURCE, "相册入口");
                    context.startActivity(i);
                    return true;
                }
            });
            dialog.show("提示", "您还不是钻石Vip哦,无法查看关注你的用户详细资料，是否前往充值?", "立即充值", "取消");
        }
    }

    public  interface  onGetMyAttentionListener
    {
        void onGetMyAttentionSuccess(MyAttentionListAdapter mAdapter);
        void onGetMyAttentionError(String msg);
    }

    public  interface  onGetAttentionMyListener
    {
        void onGetAttentionMySuccess(AttentionMyListAdapter mAdapter);
        void onGetAttentionMyError(String msg);
    }
    
    public  interface  onCancelMyAttentionListener
    {
        void onCancelMyAttentionSuccess(MyAttentionListAdapter mAdapter);
        void onCancelMyAttentionError(String msg);
    }

    public  interface  onAddAttentionListener
    {
        void onAddAttentionSuccess();
        void onAddAttentionError(String msg);
    }
}
