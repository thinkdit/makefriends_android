package com.xindian.fatechat.widget.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.xindian.fatechat.R;
import com.xindian.fatechat.databinding.LayoutGiftItemBinding;
import com.xindian.fatechat.ui.home.model.GiftListModel;

import java.util.List;

/**
 * 礼物列表适配器
 * Created by qiuda on 16/7/6.
 */
public class GiftPortListAdapter extends BaseAdapter {
    private Context mContext;
    private GiftListModel mSelectGift;
    private List<GiftListModel> mDatas;
    private LayoutInflater mLayoutInflater;
    private int mSelectedPosition = -1;

    public int getmSelectedPosition() {
        return mSelectedPosition;
    }
    public void setmSelectedPosition(int mSelectedPosition) {
        this.mSelectedPosition = mSelectedPosition;
        notifyDataSetChanged();
    }

    public GiftPortListAdapter(Context context) {
        mContext = context;
        mLayoutInflater = LayoutInflater.from(mContext);
    }

    public void setData(List<GiftListModel> list) {
        this.mDatas = list;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
//        return 8;//mGiftItemHelper.getItemCount();
        if(mDatas!=null){
            return mDatas.size();
        }
        return 0;
    }

    @Override
    public GiftListModel getItem(int position) {
        if(mDatas!=null && mDatas.size()>position){
            return mDatas.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutGiftItemBinding layoutGiftItemBinding;
        if (convertView == null) {
            layoutGiftItemBinding = DataBindingUtil.inflate(mLayoutInflater, R.layout.layout_gift_item, parent, false);
            convertView = layoutGiftItemBinding.getRoot();
            convertView.setTag(layoutGiftItemBinding);
        } else {
            layoutGiftItemBinding = (LayoutGiftItemBinding) convertView.getTag();
        }
        if(getItem(position)!=null){
            layoutGiftItemBinding.setGift(getItem(position));
            if(mSelectedPosition == position){
                layoutGiftItemBinding.bgGift.setBackgroundResource(R.drawable.shape_item_select);
            }else{
                layoutGiftItemBinding.bgGift.setBackgroundResource(R.color.transparent);
            }
        }
        return convertView;
    }

}
