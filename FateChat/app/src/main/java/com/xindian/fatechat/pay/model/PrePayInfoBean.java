package com.xindian.fatechat.pay.model;

import com.xindian.fatechat.common.BaseModel;

/**
 * Created by hx_Alex on 2017/4/20.
 */

public class PrePayInfoBean extends BaseModel {
        private String data;
        private boolean isWhite;
        private String orderNum;
        public String getData() {
            return data;
        }

        public void setData(String data) {
            this.data = data;
        }

        public boolean isIsWhite() {
            return isWhite;
        }

        public void setIsWhite(boolean isWhite) {
            this.isWhite = isWhite;
        }

        public String getOrderNum() {
        return orderNum;
        }

        public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
        }
}

