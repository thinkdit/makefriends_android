package com.xindian.fatechat.ui.home.presenter;

import android.os.RemoteException;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.parser.Feature;
import com.xindian.fatechat.R;
import com.xindian.fatechat.common.BaseModel;
import com.xindian.fatechat.common.BasePresenter;
import com.xindian.fatechat.common.FateChatApplication;
import com.xindian.fatechat.common.ResultModel;
import com.xindian.fatechat.ui.home.model.BangBean;
import com.xindian.fatechat.ui.home.model.GiftListModel;

import org.json.JSONException;

import java.util.HashMap;
import java.util.List;

/**
 * 榜单
 */
public class BangListPresenter extends BasePresenter {
    private final String URL_GET_LSIT = "/gift/get-rank";

    public static final String TYPE_TIME_THIS = "curWeek";
    public static final String TYPE_TIME_LAST = "lastWeek";
    public static final String TYPE_TYPE_MALE_SEND = "maleSend";
    public static final String TYPE_TYPE_MALE_RECIVE = "maleReceive";
    public static final String TYPE_TYPE_FEMALE_SEND = "femaleSend";
    public static final String TYPE_TYPE_FEMALE_RECIVE = "femaleReceive";

    public void getBangList(String rankTimeEnum,String rankTypeEnum) {
        HashMap<String, Object> data = new HashMap<>();
        data.put("rankTimeEnum",rankTimeEnum);
        data.put("rankTypeEnum",rankTypeEnum);
        get(getUrl(URL_GET_LSIT), data, null);
    }


    @Override
    public Object asyncExecute(String url, ResultModel resultModel) {
        if (url.contains(URL_GET_LSIT)) {
            return JSON.parseArray(resultModel.getData(), BangBean.class);
        }
        return null;
    }

    @Override
    public void onError(String url, Exception e) {
        super.onError(url, e);
    }


    @Override
    public void onFailure(String url, ResultModel resultModel) {
        super.onFailure(url,resultModel);
        if (url.contains(URL_GET_LSIT)) {
        }
    }

    @Override
    public void onSucceed(String url, ResultModel resultModel) throws JSONException,
            RemoteException {
        if (url.contains(URL_GET_LSIT)) {
            if(mOnBangLisener!=null){
                mOnBangLisener.onGetBangListSuccess((List<BangBean>)resultModel.getDataModel());
            }

        }
    }

    private OnBangLisener mOnBangLisener;

    public OnBangLisener getmOnBangLisener() {
        return mOnBangLisener;
    }

    public void setmOnBangLisener(OnBangLisener mOnBangLisener) {
        this.mOnBangLisener = mOnBangLisener;
    }

    public interface OnBangLisener {
        void onGetBangListSuccess(List<BangBean> list);
    }

}
