package com.xindian.fatechat.ui.user.presenter;

import android.content.Context;
import android.os.RemoteException;

import com.alibaba.fastjson.JSON;
import com.xindian.fatechat.common.BasePresenter;
import com.xindian.fatechat.common.ResultModel;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.ui.user.model.CaseFriendBean;
import com.xindian.fatechat.ui.user.model.User;
import com.xindian.fatechat.widget.MyProgressDialog;

import org.json.JSONException;

import java.util.HashMap;

/**
 * Created by hx_Alex on 2017/4/1.
 */

public class FriendscasePresenter extends BasePresenter {
    private final String  URL_FRIENDSCASE="/auth/friendscase";
    private final String  URL_QUERYUSERCASEBYUSERID="/auth/queryUserCaseByUserId";
    private final String  RESLUT_OK="ok";
    private onGetFriendsCaseListener getFriendsCaseListener;
    
    private MyProgressDialog mDialog;
    private  Context context;
    public FriendscasePresenter(Context context,onGetFriendsCaseListener listener)
    {
        this.context=context;
        this.getFriendsCaseListener=listener;
        mDialog=new MyProgressDialog(context);
    }
    
    public void requestGetFriendsCase(User user)
    {
        HashMap<String,Object> data=new HashMap<>();
        data.put("userId",user.getUserId());
        get(getUrl(URL_QUERYUSERCASEBYUSERID),data,null);
        mDialog.show();
    }

    public void requestUpdateFriendsCase(CaseFriendBean caseFriendBean)
    {
        //如果是第一次填写征友条件,userId为null,需要添加userID至Bean
        if(caseFriendBean.getUserId()==0){
            caseFriendBean.setUserId(UserInfoManager.getManager(context).getUserId());
        }
        HashMap<String,Object> data=new HashMap<>();
        data.put("address",caseFriendBean.getAddress());
        data.put("ageEnd",caseFriendBean.getAgeEnd());
        data.put("ageStart",caseFriendBean.getAgeStart());
        data.put("heightEnd",caseFriendBean.getHeightEnd());
        data.put("heightStart",caseFriendBean.getHeightStart());
        data.put("id",caseFriendBean.getId());
        data.put("qualifications",caseFriendBean.getQualifications());
        data.put("revenue",caseFriendBean.getRevenue());
        data.put("userId",caseFriendBean.getUserId());
        post(getUrl(URL_FRIENDSCASE),data,null);
        mDialog.show();
    }

  


    @Override
    public Object asyncExecute(String url, ResultModel resultModel) {
        if(url.contains(URL_QUERYUSERCASEBYUSERID)) {
            return JSON.parseObject(resultModel.getData(),CaseFriendBean.class);
        }else if(url.contains(URL_FRIENDSCASE))
        {
            
        }
        else
        {

        }
        return  null;
    }

    @Override
    public void onError(String url, Exception e) {
        super.onError(url, e);
    }


    @Override
    public void onFailure(String url, ResultModel resultModel) {
        super.onFailure(url,resultModel);
        if(url.contains(URL_QUERYUSERCASEBYUSERID)) {
            getFriendsCaseListener.onGetFriendsCaseError(resultModel.getMessage().equals("") || resultModel.getMessage() == null ? "未知错误" : resultModel.getMessage());
        }
        if(url.contains(URL_FRIENDSCASE))
        {
            getFriendsCaseListener.onUpdateFriendsCaseError(resultModel.getMessage().equals("") || resultModel.getMessage() == null ? "未知错误" : resultModel.getMessage());
        }
//            mDialog.dismiss();
    }

    @Override
    public void onSucceed(String url, ResultModel resultModel) throws JSONException, RemoteException {
        if(url.contains(URL_QUERYUSERCASEBYUSERID))
        {
            CaseFriendBean dataModel = (CaseFriendBean) resultModel.getDataModel();
            getFriendsCaseListener.onGetFriendsCaseSuccess(dataModel);
        }
        if(url.contains(URL_FRIENDSCASE))
        {
            if(resultModel.getCode().equals(RESLUT_OK))
            {
                getFriendsCaseListener.onUpdateFriendsCaseSuccess();
            }
        }
        mDialog.dismiss();
    }


    public  interface  onGetFriendsCaseListener
    {
      void  onGetFriendsCaseSuccess(CaseFriendBean bean);
        void  onGetFriendsCaseError(String msg);
        void  onUpdateFriendsCaseSuccess();
        void  onUpdateFriendsCaseError(String msg);
    }





}
