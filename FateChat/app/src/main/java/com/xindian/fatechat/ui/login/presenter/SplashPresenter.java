package com.xindian.fatechat.ui.login.presenter;

import android.content.Context;
import android.os.AsyncTask;
import android.os.RemoteException;
import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.Target;
import com.thinkdit.lib.util.StringUtil;
import com.xindian.fatechat.common.BasePresenter;
import com.xindian.fatechat.common.ResultModel;
import com.hyphenate.easeui.model.SensitiveBean;

import com.xindian.fatechat.ui.login.model.SplashBean;
import com.xindian.fatechat.util.ChannelUtil;

import org.json.JSONException;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by hx_Alex on 2017/6/7.
 */

public class SplashPresenter extends BasePresenter {
    private final String URL_GETSPLASH = "/auth/getSplashScree";
    private final String URL_GET_SENSITIVE_PKG="/sensitive-word/get-pkg";//获取敏感词库zip包
    private Context context;
    private splashListener mListener;
    private SensitiveWordsListener sensitiveWordsListener;

    public SplashPresenter(Context context,splashListener listener) {
        this.context = context;
        this.mListener=listener;
    }


    public SplashPresenter(Context context,SensitiveWordsListener listener) {
        this.context = context;
        this.sensitiveWordsListener=listener;
    }
    

    public void getSplash()
    {
        String channel= ChannelUtil.getChannel(context);
        Map<String, Object> map = new HashMap<>();
        map.put("releaseChannel", channel);
        get(getUrl(URL_GETSPLASH),map,context);
    }


    public void getSensitivePkg()
    {
        String channel= ChannelUtil.getChannel(context);
        Map<String, Object> map = new HashMap<>();
        get(getUrl(URL_GET_SENSITIVE_PKG),map,context);
    }


    @Override
    public Object asyncExecute(String url, ResultModel resultModel) {
        if(url.contains(URL_GETSPLASH))
        {
            return JSON.parseObject(resultModel.getData(), SplashBean.class);

        } else if(url.contains(URL_GET_SENSITIVE_PKG))
        {
            return JSON.parseObject(resultModel.getData(), SensitiveBean.class);

        }
        return super.asyncExecute(url, resultModel);
    }

    @Override
    public void onSucceed(String url, ResultModel resultModel) throws JSONException, RemoteException {
     if(url.contains(URL_GETSPLASH))
     {
         SplashBean bean= (SplashBean) resultModel.getDataModel();
         if(bean!=null && !StringUtil.isNullorEmpty(bean.getUrl())) {
             getImageCacheAsyncTask imageCacheAsyncTask = new getImageCacheAsyncTask(context, bean);
             imageCacheAsyncTask.execute(bean.getUrl());
         }else 
         {
             mListener.onGetSplashError();
         }

     } else if(url.contains(URL_GET_SENSITIVE_PKG))
     {
         SensitiveBean sensitiveBean = (SensitiveBean) resultModel.getDataModel();
         sensitiveWordsListener.onGetSensitiveWordsSucceed(sensitiveBean);

     }
        
    }

    @Override
    public void onFailure(String url, ResultModel resultModel) {
        if(url.contains(URL_GETSPLASH))
        {
            mListener.onGetSplashError();

        }else if(url.contains(URL_GET_SENSITIVE_PKG))
        {
            sensitiveWordsListener.onGetSensitiveWordsError(resultModel.getMessage().equals("") ||
                    resultModel.getMessage() == null ? "未知错误" : resultModel.getMessage());

        }
    }

    public  interface splashListener
    {
      void  onGetSplashSucceed(String url);
        void  onGetSplashError();
    }



    public  interface SensitiveWordsListener
    {
        void  onGetSensitiveWordsSucceed(SensitiveBean bean);
        void  onGetSensitiveWordsError(String msg);
    }
    

    class getImageCacheAsyncTask extends AsyncTask<String, Void, File> {
        private final Context context;
        private SplashBean bean;
        public getImageCacheAsyncTask(Context context,SplashBean bean) {
            this.context = context;
            this.bean=bean;
        }

        @Override
        protected File doInBackground(String... params) {
            String imgUrl =  params[0];
            try {
                return Glide.with(context)
                        .load(imgUrl)
                        .downloadOnly(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                        .get();
            } catch (Exception ex) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(File result) {
            if (result == null) {
                mListener.onGetSplashSucceed(bean.getUrl());
                return;
            }
            //此path就是对应文件的缓存路径  
            String path = result.getPath();
            Log.e("path", path);
            mListener.onGetSplashSucceed(path);

        }
    }
}
