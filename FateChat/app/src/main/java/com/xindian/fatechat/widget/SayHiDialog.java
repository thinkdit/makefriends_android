package com.xindian.fatechat.widget;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.thinkdit.lib.util.StringUtil;
import com.xindian.fatechat.R;
import com.xindian.fatechat.databinding.LayoutNormalDialogBinding;
import com.xindian.fatechat.databinding.LayoutSayHiDialogBinding;
import com.xindian.fatechat.util.DisplayUtil;


/**
 * 一键打招呼dailog
 * Created by qiuda on 16/7/9.
 */
public class SayHiDialog implements View.OnClickListener, DialogInterface.OnCancelListener {
    private Dialog mDialog;
    private Context mContext;
    private ViewGroup mViewGroup;
    private iDialogCallback mBtnOkCallback;
    private LayoutSayHiDialogBinding mLayoutNormalDialogBinding;

    public SayHiDialog(Context context, ViewGroup viewGroup) {
        mContext = context;
        mViewGroup = viewGroup;
    }

    /**
     * 创建对话框
     *
     */
    private void creat(String imageMain, String imageBg, String image1, String image2, String image3) {
        mDialog = new Dialog(mContext, R.style.Dialog_UserInfo);
        mLayoutNormalDialogBinding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.layout_say_hi_dialog, mViewGroup, false);
        mDialog.setContentView(mLayoutNormalDialogBinding.getRoot());

        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.setOnCancelListener(this);
        mLayoutNormalDialogBinding.buSendHi.setOnClickListener(this);
        mLayoutNormalDialogBinding.setImageHead(imageMain);
        mLayoutNormalDialogBinding.setImageBgs(imageBg);

        mLayoutNormalDialogBinding.setImageGri1(image1);
        mLayoutNormalDialogBinding.setImageGri2(image2);
        mLayoutNormalDialogBinding.setImageGri3(image3);
    }

    public void setCanceledOnTouchOutside(boolean isOutCancel) {
        if (mDialog != null) {
            mDialog.setCanceledOnTouchOutside(isOutCancel);
        }
    }

    public void setCancelable(boolean isCancelable) {
        if (mDialog != null) {
            mDialog.setCancelable(isCancelable);
        }
    }

    public SayHiDialog setBtnOkCallback(iDialogCallback btnOkCallback) {
        mBtnOkCallback = btnOkCallback;
        return this;
    }

    @Override
    public void onClick(View v) {
        if (mDialog != null) {
            if (v == mLayoutNormalDialogBinding.buSendHi) {
                if (mBtnOkCallback != null) {
                    boolean dismiss = mBtnOkCallback.onBtnClicked();
                    if (dismiss) {
                        dismiss();
                    }
                } else {
                    dismiss();
                }
            }
        }
    }

    public void dismiss() {
        if (mDialog != null) {
            if (mContext instanceof Activity) {
                if (!((Activity) mContext).isFinishing()) {
                    mDialog.dismiss();
                }
            } else {
                mDialog.dismiss();
            }
        }
    }

    public Dialog getmDialog() {
        return mDialog;
    }

    /**
     * 显示对话框
     *
     */
    public void show(String imageMain, String imageBg, String image1, String image2, String image3) {
        if (mDialog == null) {
            creat(imageMain, imageBg, image1, image2, image3);
        } else {
            mLayoutNormalDialogBinding.setImageHead(imageMain);
            mLayoutNormalDialogBinding.setImageBgs(imageBg);
            mLayoutNormalDialogBinding.setImageGri1(image1);
            mLayoutNormalDialogBinding.setImageGri2(image2);
            mLayoutNormalDialogBinding.setImageGri3(image3);
        }
        if (mDialog.isShowing()) {
            return;
        }
        try {
            mDialog.show();
        } catch (Exception e) {

        }

        WindowManager.LayoutParams p = mDialog.getWindow().getAttributes(); // 获取对话框当前的参数值
        p.height = WindowManager.LayoutParams.WRAP_CONTENT;
        p.width = (int) (DisplayUtil.dip2px(mContext,260));
        mDialog.getWindow().setAttributes(p);
    }

    @Override
    public void onCancel(DialogInterface dialog) {

    }

    public interface iDialogCancelCallback {
        void onCanceled();
    }

    public interface iDialogCallback {

        /**
         * 添加监听
         *
         * @return 是否关闭对话框
         */
        boolean onBtnClicked();

    }
}
