package com.xindian.fatechat.pay.model;

import com.xindian.fatechat.common.BaseModel;

/**
 * Created by hx_Alex on 2017/4/24.
 */

public class WxPreInfoBean extends BaseModel {

    /**
     * sign : 2C0F71DF81D9180A45F642B80ABD1EF2
     * result_code : 0
     * mch_id : 755437000006
     * status : 0
     * sign_type : MD5
     * charset : UTF-8
     * appid : wx6ae0f4feabfcb549
     * pay_info : {"sign":"2B49B92836805027014DF7F809CF4FAE","timestamp":"1493025538","noncestr":"1493025538036","partnerid":"12723495","prepayid":"wx20170424171857d5877585010088761275","package":"Sign=WXPay","appid":"wx4e0510c18ceb9a93"}
     * token_id : bee3c3d258bb3084df5a9c5f90bcd9a1
     * out_trade_no : 666
     * nonce_str : 6cfe0e6127fa25df2a0ef2ae1067d915
     * transaction_id : 755437000006201704246194880069
     * version : 2.0
     */

    private String sign;
    private String result_code;
    private String mch_id;
    private String status;
    private String sign_type;
    private String charset;
    private String appid;
    private String pay_info;
    private String token_id;
    private String out_trade_no;
    private String nonce_str;
    private String transaction_id;
    private String version;

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getResult_code() {
        return result_code;
    }

    public void setResult_code(String result_code) {
        this.result_code = result_code;
    }

    public String getMch_id() {
        return mch_id;
    }

    public void setMch_id(String mch_id) {
        this.mch_id = mch_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSign_type() {
        return sign_type;
    }

    public void setSign_type(String sign_type) {
        this.sign_type = sign_type;
    }

    public String getCharset() {
        return charset;
    }

    public void setCharset(String charset) {
        this.charset = charset;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getPay_info() {
        return pay_info;
    }

    public void setPay_info(String pay_info) {
        this.pay_info = pay_info;
    }

    public String getToken_id() {
        return token_id;
    }

    public void setToken_id(String token_id) {
        this.token_id = token_id;
    }

    public String getOut_trade_no() {
        return out_trade_no;
    }

    public void setOut_trade_no(String out_trade_no) {
        this.out_trade_no = out_trade_no;
    }

    public String getNonce_str() {
        return nonce_str;
    }

    public void setNonce_str(String nonce_str) {
        this.nonce_str = nonce_str;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
