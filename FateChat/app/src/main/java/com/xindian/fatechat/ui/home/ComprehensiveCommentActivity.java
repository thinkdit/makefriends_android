package com.xindian.fatechat.ui.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.thinkdit.lib.util.StringUtil;
import com.xindian.fatechat.R;
import com.xindian.fatechat.common.FateChatApplication;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.ui.base.BaseActivity;
import com.xindian.fatechat.ui.home.adapter.CommentAdapter;
import com.xindian.fatechat.ui.home.model.CommentBean;
import com.xindian.fatechat.ui.home.model.JHTuiJianBean;
import com.xindian.fatechat.ui.home.presenter.ComDetailPresenter;
import com.xindian.fatechat.ui.home.presenter.ComPLEditPresenter;
import com.xindian.fatechat.ui.home.presenter.ComZanPresenter;
import com.xindian.fatechat.ui.user.UserDetailsActivity;
import com.xindian.fatechat.util.GlideRoundTransform;
import com.xindian.fatechat.widget.player.VideoPlayerHelper;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.switfpass.pay.MainApplication.getContext;

public class ComprehensiveCommentActivity extends BaseActivity {

    public static final String TAG = "testv";
    @BindView(R.id.tvBack)
    TextView tvBack;
    @BindView(R.id.tvBackIcon)
    View tvBackIcon;
    @BindView(R.id.llActionbar)
    View llActionbar;
    @BindView(R.id.sdvUserHead)
    ImageView sdvUserHead;
    @BindView(R.id.isV)
    TextView isV;
    @BindView(R.id.tvUserName)
    TextView tvUserName;
    @BindView(R.id.levelA)
    RelativeLayout levelA;
    @BindView(R.id.tvDetil)
    TextView tvDetil;
    @BindView(R.id.ivThumb)
    ImageView ivThumb;
    @BindView(R.id.ivPlay)
    ImageView ivPlay;
    @BindView(R.id.videoLevelC)
    RelativeLayout videoLevelC;
    @BindView(R.id.ivImageShow)
    ImageView ivImageShow;
//    @BindView(R.dyId.ivImageShowAll)
//    TextView ivImageShowAll;
    @BindView(R.id.imageLevelC)
    RelativeLayout imageLevelC;
    @BindView(R.id.ivBackground)
    ImageView ivBackground;
    @BindView(R.id.tvPlayCount)
    TextView tvPlayCount;
    @BindView(R.id.tvVoiceSumtime)
    TextView tvVoiceSumtime;
    @BindView(R.id.audioBody)
    RelativeLayout audioBody;
    @BindView(R.id.levelE)
    View levelE;
    @BindView(R.id.tvPlayOrPause)
    TextView tvPlayOrPause;
    @BindView(R.id.voiceSeekbar)
    SeekBar voiceSeekbar;
    @BindView(R.id.tvCurrentTime)
    TextView tvCurrentTime;
    @BindView(R.id.tvTotalTime)
    TextView tvTotalTime;
    @BindView(R.id.rlVoiceProgress)
    RelativeLayout rlVoiceProgress;
    @BindView(R.id.tvPlayVoice)
    TextView tvPlayVoice;
    @BindView(R.id.audioLevelC)
    RelativeLayout audioLevelC;
    @BindView(R.id.levelB)
    LinearLayout levelB;
    @BindView(R.id.cbZan)
    CheckBox cbZan;
    @BindView(R.id.tvUnLike)
    TextView tvUnLike;
    @BindView(R.id.tvUnLikeIcon)
    TextView tvUnLikeIcon;
    @BindView(R.id.imShare)
    ImageView tvShare;
    @BindView(R.id.tvPinglun)
    TextView tvPinglun;
    @BindView(R.id.levelD)
    LinearLayout levelD;
    @BindView(R.id.lvComments)
    ListView lvComments;
    @BindView(R.id.tvEditCom)
    EditText tvEditCom;
    @BindView(R.id.im_like_1)
    ImageView im_like_1;
    @BindView(R.id.im_like_2)
    ImageView im_like_2;
    @BindView(R.id.im_like_3)
    ImageView im_like_3;
    @BindView(R.id.im_like_4)
    ImageView im_like_4;
    @BindView(R.id.im_like_5)
    ImageView im_like_5;
    @BindView(R.id.im_like_6)
    ImageView im_like_6;
    @BindView(R.id.im_like_7)
    ImageView im_like_7;
    @BindView(R.id.im_like_8)
    ImageView im_like_8;
    @BindView(R.id.im_like_9)
    ImageView im_like_9;
    @BindView(R.id.im_like_lay)
    View im_like_lay;
    @BindView(R.id.im_like_line)
    View im_like_line;
    @BindView(R.id.line_wenzi)
    View tvLineWenzi;
    private int type;
    private JHTuiJianBean bean;
    private CommentAdapter adapter;

    private int page = 1;

    public static CommentBean paresCommentBean(String json){
        CommentBean commentBean=null;
        try {
            commentBean= JSON.parseObject(json, CommentBean.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  commentBean;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comprehensive_comment);
        ButterKnife.bind(this);
        initBaseData();
        setingShowType(type);
        setingItemThumb(bean,type);//加载内容布局的界面
        setUserMessage();  //设置用户信息和详细内容
        setClickToButtons(bean);//设置按钮监听
        setVideoPlayClickListener();//点击播放按钮事件
        adapter = new CommentAdapter(null, this);
        lvComments.setAdapter(adapter);
        getDataFromNet(page,ComDetailPresenter.PAGE_SIZE);


    }

    @OnClick(R.id.cbZan)
    public void onCbZanClick(View v) {
        bean.setDlike(!bean.isDlike());
        boolean isLike = bean.isDlike();
        int countLike = bean.getDynamicLike();
        cbZan.setChecked(isLike);
        if(isLike){
            countLike++;
        }else{
            countLike--;
        }
        bean.setDynamicLike(countLike);
        cbZan.setText(countLike+"");

        editComZan(ComZanPresenter.TYPE_ZAN);
    }

    @OnClick(R.id.tvUnlikeLay)
    public void onUnLikeClick(View v) {
        bean.setNoLike(!bean.isNoLike());
        boolean isLike = bean.isNoLike();
        int countLike = bean.getDynamicDislike();
        tvUnLikeIcon.setSelected(isLike);
        if(isLike){
            countLike++;
        }else{
            countLike--;
        }
        bean.setDynamicDislike(countLike);
        tvUnLike.setText(countLike+"");

        editComZan(ComZanPresenter.TYPE_CAI);
    }

    @OnClick(R.id.tvSendCom)
    public void onSendComClick(View v) {
        editComPL();
    }

    ComDetailPresenter mComListPresenter;
    //从网络加载数据
    public void getDataFromNet(final int page, final int pageSize) {

        if(mComListPresenter == null){
            mComListPresenter = new ComDetailPresenter();
            mComListPresenter.setmOnComLisener(new ComDetailPresenter.OnComLisener() {

                @Override
                public void onGetComDetailSuccess(JHTuiJianBean list) {
                    if(page == 1){
                        adapter.setData(list.getDynamicCommentList());
                        bean.setLikeUserList(list.getLikeUserList());
                        setLikeUserListUI();
                    }else{
                        adapter.addData(list.getDynamicCommentList());
                    }
                }
            });
        }
        long userId = UserInfoManager.getManager(this).getUserId();
        String token = UserInfoManager.getManager(this).getToken();
        mComListPresenter.getComDetail(bean.getId()+"",userId+"",token,page,pageSize);
    }


    ComPLEditPresenter mComPLPresenter;
    //提交评论
    public void editComPL() {

        if(mComPLPresenter == null){
            mComPLPresenter = new ComPLEditPresenter();
            mComPLPresenter.setmOnComLisener(new ComPLEditPresenter.OnComLisener() {
                @Override
                public void onComEditSuccess() {
                    Toast.makeText(FateChatApplication.getInstance(), "提交成功", Toast.LENGTH_SHORT).show();
                    tvEditCom.setText("");
                    getDataFromNet(page,ComDetailPresenter.PAGE_SIZE);
                }
            });
        }
        String dyContent = tvEditCom.getText().toString().trim();
        if(StringUtil.isNullorEmpty(dyContent)){
            Toast.makeText(FateChatApplication.getInstance(), "请输入评论内容！", Toast.LENGTH_SHORT).show();
            return;
        }
        long userId = UserInfoManager.getManager(this).getUserId();
        String token = UserInfoManager.getManager(this).getToken();
        mComPLPresenter.editCom(bean.getId()+"",userId+"",token,dyContent);
    }

    ComZanPresenter mComZanPresenter;
    //赞或踩
    public void editComZan(int type) {
        if(mComZanPresenter == null){
            mComZanPresenter = new ComZanPresenter();
            mComZanPresenter.setmOnComLisener(new ComZanPresenter.OnZanLisener() {
                @Override
                public void onComEditSuccess() {
                    Toast.makeText(FateChatApplication.getInstance(), "提交成功", Toast.LENGTH_SHORT).show();
                }
            });
        }
        long userId = UserInfoManager.getManager(this).getUserId();
        String token = UserInfoManager.getManager(this).getToken();
        mComZanPresenter.editCom(bean.getId()+"",userId+"",token,type);
    }

    //点击播放按钮事件
    private void setVideoPlayClickListener() {
        ivPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivPlay.setVisibility(View.GONE);
                ivThumb.setVisibility(View.GONE);
                VideoPlayerHelper.getInstance().play(videoLevelC, bean.getSourceUrl(), -1);
            }
        });
    }
    //获得基础数据
    private void initBaseData() {
        Intent intent = getIntent();
        type = intent.getIntExtra("type",JHTuiJianBean.TYPE_TEXT);
        bean = (JHTuiJianBean) intent.getSerializableExtra("bean");
    }
    //根据类型显示对应的布局
    private void setingShowType(int type) {
        if(type == JHTuiJianBean.TYPE_TEXT){
            levelB.setVisibility(View.GONE);
            tvLineWenzi.setVisibility(View.VISIBLE);
        }else if(type == JHTuiJianBean.TYPE_VEDIO){
            levelB.setVisibility(View.VISIBLE);
            videoLevelC.setVisibility(View.VISIBLE);
            audioLevelC.setVisibility(View.GONE);
            imageLevelC.setVisibility(View.GONE);
            //保持视频高宽
        }else if(type == JHTuiJianBean.TYPE_IMAGE){
            levelB.setVisibility(View.VISIBLE);
            audioLevelC.setVisibility(View.GONE);
            videoLevelC.setVisibility(View.GONE);
            imageLevelC.setVisibility(View.VISIBLE);
        }
//        else if("audio".equals(type)){
//            levelB.setVisibility(View.VISIBLE);
//            rlVoiceProgress.setVisibility(View.GONE);
//            audioLevelC.setVisibility(View.VISIBLE);
//            videoLevelC.setVisibility(View.GONE);
////            imageLevelC.setVisibility(View.GONE);
//        }
    }
    //根据类型加载内容布局的界面和对应设置
    private void setingItemThumb( JHTuiJianBean bean, int type) {
        String thumbUrl;
        if(type == JHTuiJianBean.TYPE_IMAGE){
            thumbUrl=bean.getSourceUrl();
            Glide.with(ivImageShow.getContext()).load(thumbUrl)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .fitCenter()
                    .into(ivImageShow);
        }
        else if(type == JHTuiJianBean.TYPE_VEDIO){
            thumbUrl=bean.getSourceUrl();
            Glide.with(ivThumb.getContext()).load(thumbUrl)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .into(ivThumb);

        }
    }

    //设置分享按钮监听
    private void setClickToButtons( final JHTuiJianBean bean) {
        tvShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        View.OnClickListener onBackClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        };
        tvBack.setOnClickListener(onBackClickListener);
        tvBackIcon.setOnClickListener(onBackClickListener);

    }

    //设置用户信息和详细内容
    private void setUserMessage(){
        String headUri = bean.getHeadUrl();
        Glide.with(sdvUserHead.getContext()).load(headUri)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .transform(new GlideRoundTransform(sdvUserHead.getContext(), (int)getResources().getDimension(R.dimen.image_aspect_40dp)))
                .into(sdvUserHead);
        tvUserName.setText(bean.getNickName());
        isV.setText("Lv"+bean.getUserGrade());
        tvDetil.setText(Html.fromHtml(bean.getDynamicContent()));//设置详细内容
        cbZan.setText(bean.getDynamicLike()+"");//赞
        tvUnLike.setText(bean.getDynamicDislike()+"");
        tvPinglun.setText(bean.getCommentNum()+"");//评论
        cbZan.setChecked(bean.isDlike());
        tvUnLikeIcon.setSelected(bean.isNoLike());
        tvEditCom.clearFocus();
        tvShare.setVisibility(View.INVISIBLE);
        //头像
        sdvUserHead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sex = UserInfoManager.getManager(getContext()).getSex();
                String sexOther = bean.getSex();

                if(sex!=null && sexOther!=null && !sex.equals(sexOther)){//异性
                    goToUserDetail(ComprehensiveCommentActivity.this,(int)bean.getUserId());
                }else{
                    Toast.makeText(ComprehensiveCommentActivity.this,R.string.only_sex_look,Toast.LENGTH_SHORT).show();
                }

            }
        });
    }


    private void goToUserDetail(Context context, int userID) {
        UserDetailsActivity.gotoUserDetails(context, userID, null);
    }

    private void setLikeUserListUI(){
        String head1 = null;
        String head2 = null;
        String head3 = null;
        String head4 = null;
        String head5 = null;
        String head6 = null;
        String head7 = null;
        String head8 = null;
        String head9 = null;
        ArrayList<JHTuiJianBean.DynamicLike> headers = bean.getLikeUserList();
        if(headers != null && headers.size()>0){
            im_like_lay.setVisibility(View.VISIBLE);
            im_like_line.setVisibility(View.VISIBLE);
            head1 = headers.get(0).getHeadUrl();
        }else{
            im_like_lay.setVisibility(View.GONE);
            im_like_line.setVisibility(View.GONE);
        }
        if(headers != null && headers.size()>1){
            head2 = headers.get(1).getHeadUrl();
        }
        if(headers != null && headers.size()>2){
            head3 = headers.get(2).getHeadUrl();
        }
        if(headers != null && headers.size()>3){
            head4 = headers.get(3).getHeadUrl();
        }
        if(headers != null && headers.size()>4){
            head5 = headers.get(4).getHeadUrl();
        }
        if(headers != null && headers.size()>5){
            head6 = headers.get(5).getHeadUrl();
        }
        if(headers != null && headers.size()>6){
            head7 = headers.get(6).getHeadUrl();
        }
        if(headers != null && headers.size()>7){
            head8 = headers.get(7).getHeadUrl();
        }
        if(headers != null && headers.size()>8){
            head9 = headers.get(8).getHeadUrl();
        }
        if(head1 != null){
            im_like_1.setVisibility(View.VISIBLE);
            Glide.with(im_like_1.getContext()).load(head1)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .transform(new GlideRoundTransform(im_like_1.getContext(), (int)getResources().getDimension(R.dimen.image_aspect_40dp)))
                    .into(im_like_1);
        }else{
            im_like_1.setVisibility(View.INVISIBLE);
        }
        if(head2 != null){
            im_like_2.setVisibility(View.VISIBLE);
            Glide.with(im_like_2.getContext()).load(head2)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .transform(new GlideRoundTransform(im_like_2.getContext(), (int)getResources().getDimension(R.dimen.image_aspect_40dp)))
                    .into(im_like_2);
        }else{
            im_like_2.setVisibility(View.INVISIBLE);
        }
        if(head3 != null){
            im_like_3.setVisibility(View.VISIBLE);
            Glide.with(im_like_3.getContext()).load(head3)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .transform(new GlideRoundTransform(im_like_3.getContext(), (int)getResources().getDimension(R.dimen.image_aspect_40dp)))
                    .into(im_like_3);
        }else{
            im_like_3.setVisibility(View.INVISIBLE);
        }
        if(head9 != null){
            im_like_9.setVisibility(View.VISIBLE);
            Glide.with(im_like_9.getContext()).load(head9)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .transform(new GlideRoundTransform(im_like_9.getContext(), (int)getResources().getDimension(R.dimen.image_aspect_40dp)))
                    .into(im_like_9);
        }else{
            im_like_9.setVisibility(View.INVISIBLE);
        }
        if(head4 != null){
            im_like_4.setVisibility(View.VISIBLE);
            Glide.with(im_like_4.getContext()).load(head4)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .transform(new GlideRoundTransform(im_like_4.getContext(), (int)getResources().getDimension(R.dimen.image_aspect_40dp)))
                    .into(im_like_4);
        }else{
            im_like_4.setVisibility(View.INVISIBLE);
        }
        if(head5 != null){
            im_like_5.setVisibility(View.VISIBLE);
            Glide.with(im_like_5.getContext()).load(head5)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .transform(new GlideRoundTransform(im_like_5.getContext(), (int)getResources().getDimension(R.dimen.image_aspect_40dp)))
                    .into(im_like_5);
        }else{
            im_like_5.setVisibility(View.INVISIBLE);
        }
        if(head6 != null){
            im_like_6.setVisibility(View.VISIBLE);
            Glide.with(im_like_6.getContext()).load(head6)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .transform(new GlideRoundTransform(im_like_6.getContext(), (int)getResources().getDimension(R.dimen.image_aspect_40dp)))
                    .into(im_like_6);
        }else{
            im_like_6.setVisibility(View.INVISIBLE);
        }
        if(head7 != null){
            im_like_7.setVisibility(View.VISIBLE);
            Glide.with(im_like_7.getContext()).load(head7)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .transform(new GlideRoundTransform(im_like_7.getContext(), (int)getResources().getDimension(R.dimen.image_aspect_40dp)))
                    .into(im_like_7);
        }else{
            im_like_7.setVisibility(View.INVISIBLE);
        }
        if(head8 != null){
            im_like_8.setVisibility(View.VISIBLE);
            Glide.with(im_like_8.getContext()).load(head8)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .transform(new GlideRoundTransform(im_like_8.getContext(), (int)getResources().getDimension(R.dimen.image_aspect_40dp)))
                    .into(im_like_8);
        }else{
            im_like_8.setVisibility(View.INVISIBLE);
        }
    }
    
    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        VideoPlayerHelper.getInstance().stop();
    }

    @Override
    public void onPause() {
        super.onPause();
        VideoPlayerHelper.getInstance().pause();
    }
}
