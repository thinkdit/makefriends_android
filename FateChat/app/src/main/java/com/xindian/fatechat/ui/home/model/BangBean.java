package com.xindian.fatechat.ui.home.model;

import com.xindian.fatechat.common.BaseModel;

import java.util.ArrayList;

/**
 * 榜单
 */

public class BangBean extends BaseModel {
    private int rank;
    private long diamonds;
    private BangUsersBean userInfo;

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public long getDiamonds() {
        return diamonds;
    }

    public void setDiamonds(long diamonds) {
        this.diamonds = diamonds;
    }

    public BangUsersBean getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(BangUsersBean userInfo) {
        this.userInfo = userInfo;
    }
}
