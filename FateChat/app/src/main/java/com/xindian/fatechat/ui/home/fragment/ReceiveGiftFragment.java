package com.xindian.fatechat.ui.home.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.xindian.fatechat.R;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.ui.base.BaseFragment_v4;
import com.xindian.fatechat.ui.user.adapter.GifiRecordAdapter;
import com.xindian.fatechat.ui.user.model.User;
import com.xindian.fatechat.ui.user.presenter.GiftRecordPresenter;
import com.xindian.fatechat.widget.ListviewLoadMoreHelper;

/**
 * Created by hx_Alex on 2017/7/13.
 */

public class ReceiveGiftFragment extends BaseFragment_v4 implements ListviewLoadMoreHelper.ILoadMoreListListener,GiftRecordPresenter.GetGiftRecordWithReceiverListener{
    private final static int deafultPage=10;
    private ListView listView;
    private ListviewLoadMoreHelper loadMoreHelper;
    private GiftRecordPresenter mPresenter;
    private int nowPage;
    private User user;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflate = inflater.inflate(R.layout.activity_gift_fragment, container, false);
        listView= (ListView) inflate.findViewById(R.id.listView);
        mPresenter=new GiftRecordPresenter(getContext());
        loadMoreHelper=new ListviewLoadMoreHelper(listView,this);
        user= UserInfoManager.getManager(getContext()).getUserInfo();
        mPresenter.setReceiverListener(this);
        mPresenter.requestGetGiftRecordWithReceiver(user.getUserId(),deafultPage,nextPage());
        return inflate;
    }
    private int nextPage()
    {
        nowPage=++nowPage;   
        return nowPage;
    }
    @Override
    public void onLoadMore() {
        mPresenter.requestGetGiftRecordWithReceiver(user.getUserId(),deafultPage,nextPage());
    }

    @Override
    public void onGetGiftRecordWithReceiverSuccess(GifiRecordAdapter receiverAdapter,boolean hasMore) {
        if(nowPage==1)
        {
            listView.setAdapter(receiverAdapter);
        }else 
        {
            receiverAdapter.notifyDataSetChanged();
        }
        
       loadMoreHelper.onLoadCompleted(!hasMore);
    }

    @Override
    public void onGetGiftRecordWithReceivernError(String msg) {
        Toast.makeText(getContext(),msg,Toast.LENGTH_LONG).show();  
    }
}
