package com.xindian.fatechat.ui.base;

import android.annotation.TargetApi;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

import com.bumptech.glide.Glide;
import com.readystatesoftware.systembartint.SystemBarTintManager;
import com.umeng.analytics.MobclickAgent;
import com.xindian.fatechat.R;
import com.xindian.fatechat.ui.base.presenter.TitlePresenter;

import java.lang.reflect.Field;


/**
 * Activity 基类
 */
public class BaseActivity extends AppCompatActivity {
    private TitlePresenter mTitlePresenter;
    private  SystemBarTintManager tintManager;
    private  boolean hasInitSystemBarTint=true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (hasInitWindow()) {
            initWindow();
        }
        
    }

    public boolean hasInitWindow() {
        return true;
    }
    public void   setHasInitSystemBarTint(boolean isInit)
    {
        this.hasInitSystemBarTint=isInit;
    }

    public boolean  hasInitSystemBarTint( )
    {
        return hasInitSystemBarTint;
    }

    public SystemBarTintManager getTintManager() {
        return tintManager;
    }
    
    @TargetApi(19)
    private void initWindow() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }
        if(!hasInitSystemBarTint) return;
        // 创建状态栏的管理实例
        tintManager  = new SystemBarTintManager(this);
        // 激活状态栏设置
        tintManager.setStatusBarTintEnabled(true);
        // 激活导航栏设置
        tintManager.setNavigationBarTintEnabled(true);
        // 设置一个颜色给系统栏
        tintManager.setTintColor(Color.parseColor("#8d8d8d"));
        // 设置一个样式背景给导航栏
        tintManager.setNavigationBarTintColor(Color.parseColor("#8d8d8d"));
        tintManager.setTintAlpha(0.4f);

    }

    protected boolean canSwipeDismiss() {
        return true;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (canSwipeDismiss()) {
            //TODO
//            SwipeBackHelper.onCreate(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        MobclickAgent.onPageStart(getClass().getSimpleName());

    }

    @Override
    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        MobclickAgent.onPageEnd(getClass().getSimpleName());

    }

    public View initToolBar() {
        return initToolBar(true);
    }

    public View initToolBar(boolean disPlayHomeAsUp) {
        View view = findViewById(R.id.rl_title);
        mTitlePresenter = new TitlePresenter(view, disPlayHomeAsUp, v -> OnBackpresseed());
        return view;
    }

    public void OnBackpresseed() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Glide.with(this).onLowMemory();
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        Glide.with(this).onTrimMemory(level);
    }

    /**
     * 初始化toolbar及右menu的内容和点击事件监听
     *
     * @param rightText         toolbar 右菜单文字
     * @param righClickListener 右菜单点击事件
     * @return
     */
    public void initToolBarWithRightMenu(String rightText, View.OnClickListener righClickListener) {
        initToolBar();
        mTitlePresenter.initToolBarRightMenu(rightText, righClickListener);
    }

    /**
     * 初始化toolbar及右menu的内容和点击事件监听
     *
     * @param rightDrawable     toolbar 右菜单图片
     * @param righClickListener 右菜单点击事件
     * @return
     */
    public void initToolBarWithRightMenu(Drawable rightDrawable, View.OnClickListener righClickListener) {
        initToolBar();
        mTitlePresenter.initToolBarRightMenu(rightDrawable, righClickListener);
    }

    public void setToolBarRightMenuVisibility(boolean visible) {
        if (mTitlePresenter != null) {
            mTitlePresenter.setToolBarRightMenuVisibility(visible);
        }
    }

    @Override
    protected void onDestroy() {
     
        super.onDestroy();
    }

    /***
     * 获取状态栏高度
     * @return
     */
    public int getStatusBarHeight() {
        Class<?> c = null;

        Object obj = null;

        Field field = null;

        int x = 0, sbar = 0;

        try {

            c = Class.forName("com.android.internal.R$dimen");

            obj = c.newInstance();

            field = c.getField("status_bar_height");

            x = Integer.parseInt(field.get(obj).toString());

            sbar =getResources().getDimensionPixelSize(x);

        } catch (Exception e1) {

            e1.printStackTrace();

        }

        return sbar;
    }
}