package com.xindian.fatechat.ui.user;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.view.View;
import android.widget.Toast;

import com.xindian.fatechat.R;
import com.xindian.fatechat.databinding.ActivityMyAttentionBinding;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.ui.base.BaseActivity;
import com.xindian.fatechat.ui.user.adapter.AttentionMyListAdapter;
import com.xindian.fatechat.ui.user.adapter.MyAttentionListAdapter;
import com.xindian.fatechat.ui.user.model.User;
import com.xindian.fatechat.ui.user.presenter.MyAttentionPresenter;
import com.hyphenate.easeui.utils.SnackbarUtil;
import com.xindian.fatechat.widget.ListviewLoadMoreHelper;

public class MyAttentionActivity extends BaseActivity implements  ListviewLoadMoreHelper.ILoadMoreListListener,
        MyAttentionPresenter.onGetMyAttentionListener,MyAttentionPresenter.onCancelMyAttentionListener,TabLayout.OnTabSelectedListener
        ,MyAttentionPresenter.onGetAttentionMyListener {
    private static final int DEFAULT_PAGE_SIZE=20;
   private ActivityMyAttentionBinding mBinding;
    private ListviewLoadMoreHelper loadMoreHelper;
    private MyAttentionPresenter mPresenter;
    private int nowPage;
    private boolean isLoadmore;
    private User user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       mBinding= DataBindingUtil.setContentView(this, R.layout.activity_my_attention);
        user= UserInfoManager.getManager(this).getUserInfo();
        initToolBar();
        loadMoreHelper=new ListviewLoadMoreHelper(mBinding.listContent,this);
        mPresenter=new MyAttentionPresenter(this);
        mPresenter.setOnGetMyAttentionListener(this);
        mPresenter.setonCancelMyAttentionListener(this);
        mPresenter.setOnGetAttentionMyListener(this);
        mBinding.setIsVip(user!=null?user.isVip():false);
        intiTab();
    }

 
    private void intiTab()
    {
        mBinding.tabAttention.addOnTabSelectedListener(this);
        mBinding.tabAttention.addTab( mBinding.tabAttention.newTab().setText("关注我的"),true);
        mBinding.tabAttention.addTab( mBinding.tabAttention.newTab().setText("我关注的"));
    }

    private int nextPage()
    {
        nowPage=++nowPage;
        return nowPage;
    }

    private  void  clearPage()
    {
        nowPage=0;
        mPresenter.clearData();
    }
    @Override
    public void onLoadMore() {
        if(mBinding.tabAttention.getSelectedTabPosition()==0)
        {
            mPresenter.getAttentionMyListAdapter(user.getUserId(),nextPage(),DEFAULT_PAGE_SIZE);
        }else if(mBinding.tabAttention.getSelectedTabPosition()==1) 
        {
            mPresenter.getMyAttentionListAdapter(user.getUserId(),nextPage(),DEFAULT_PAGE_SIZE);
        }
   
    }

    @Override
    public void onGetMyAttentionSuccess(MyAttentionListAdapter mAdapter) {
       
        if(nowPage==1) {
            mBinding.listContent.setAdapter(mAdapter);
        }else {
            mAdapter.notifyDataSetChanged();
        }
        //判断一下是否还有数据,当前列表数据如果小于当前页数X页显示大小 那么则为下一页无数据
        if(mAdapter.getCount()<nowPage*DEFAULT_PAGE_SIZE){
            isLoadmore=false;
            loadMoreHelper.onLoadCompleted(isLoadmore);
        }
        else {
            isLoadmore=true;
            loadMoreHelper.onLoadCompleted(isLoadmore);
        }
    }

    @Override
    public void onGetMyAttentionError(String msg) {
        SnackbarUtil.makeSnackBar(mBinding.getRoot(),msg, Toast.LENGTH_SHORT).setMeessTextColor(Color.WHITE).show();
    }

    @Override
    public void onCancelMyAttentionSuccess(MyAttentionListAdapter mAdapter) {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onCancelMyAttentionError(String msg) {
        SnackbarUtil.makeSnackBar(mBinding.getRoot(),msg, Toast.LENGTH_SHORT).setMeessTextColor(Color.WHITE).show();
    }

    @Override
    public void onGetAttentionMySuccess(AttentionMyListAdapter mAdapter) {
        if(nowPage==1) {
            mBinding.listContent.setAdapter(mAdapter);
        }else {
            mAdapter.notifyDataSetChanged();
        }
        //判断一下是否还有数据,当前列表数据如果小于当前页数X页显示大小 那么则为下一页无数据
        if(mAdapter.getCount()<nowPage*DEFAULT_PAGE_SIZE){
            isLoadmore=false;
            loadMoreHelper.onLoadCompleted(isLoadmore);
        }
        else {
            isLoadmore=true;
            loadMoreHelper.onLoadCompleted(isLoadmore);
        }
    }

    @Override
    public void onGetAttentionMyError(String msg) {
        SnackbarUtil.makeSnackBar(mBinding.getRoot(),msg, Toast.LENGTH_SHORT).setMeessTextColor(Color.WHITE).show();
    }

    //tab回调事件
    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        clearPage();
        if(tab.getText().equals("关注我的"))
        {
            mPresenter.getAttentionMyListAdapter(user.getUserId(),nextPage(),DEFAULT_PAGE_SIZE);
        }else if(tab.getText().equals("我关注的")){
            mPresenter.getMyAttentionListAdapter(user.getUserId(),nextPage(),DEFAULT_PAGE_SIZE);
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    public void  gotoCashActivity(View view){
        Intent i=new Intent(this,CashActivity.class);
        i.putExtra(CashActivity.ENTRYSOURCE,"谁关注我界面");
        startActivity(i);
    }

  
}
