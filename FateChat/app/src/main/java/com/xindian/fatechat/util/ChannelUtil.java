package com.xindian.fatechat.util;

import android.content.Context;
import android.content.pm.PackageManager;

/**
 * 渠道工具类
 * Created by admin on 2016/4/20.
 */
public class ChannelUtil {
    public static final String CHANNEL_FIRST_TAG = "_first";//首发标记
    public static final String DEFAULT_CHANNEL = "freeChannel";
    public static boolean isFirstChannel = false;

    private static String channel = null;

    public static String getChannel(Context context) {
//        if (channel != null) {
//            return channel;
//        }
//        final String start_flag = "META-INF/channel_";
//        ApplicationInfo appinfo = context.getApplicationInfo();
//        String sourceDir = appinfo.sourceDir;
//        ZipFile zipfile = null;
//        try {
//            zipfile = new ZipFile(sourceDir);
//            Enumeration<?> entries = zipfile.entries();
//            while (entries.hasMoreElements()) {
//                ZipEntry entry = ((ZipEntry) entries.nextElement());
//                String entryName = entry.getName();
//                if (entryName.contains(start_flag)) {
//                    channel = entryName.replace(start_flag, "");
//                    if (channel.endsWith(CHANNEL_FIRST_TAG)) {
//                        channel = channel.replace(CHANNEL_FIRST_TAG, "");
//                        isFirstChannel = true;
//                    }
//                    break;
//                }
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            if (zipfile != null) {
//                try {
//                    zipfile.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//
//        if (channel == null || channel.length() <= 0) {
//            channel = DEFAULT_CHANNEL;//读不到渠道号就默认是官方渠道
//        }

        try {
            if(context==null)
            {
                channel="";
                return channel;
            }
           Object objCannel= context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA).metaData.get("UMENG_CHANNEL");
            if(objCannel!=null)
            {
                if(objCannel instanceof Integer)
                {
                    channel=String.valueOf(objCannel);
                }else if(objCannel instanceof String){
                    channel=(String)objCannel;
                }
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            channel=DEFAULT_CHANNEL;
        }
        if(channel==null ||channel.equals(""))
        {
            channel=DEFAULT_CHANNEL;
        }
        return channel;
    }
}
