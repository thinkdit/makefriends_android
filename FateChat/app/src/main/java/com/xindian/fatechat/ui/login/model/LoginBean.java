package com.xindian.fatechat.ui.login.model;

import com.xindian.fatechat.common.BaseModel;
import com.xindian.fatechat.ui.user.model.User;

/**
 * Created by hx_Alex on 2017/3/23.
 */

public class LoginBean extends BaseModel {
    private User userInfoVo;
    private String imPassword;

    public void setUserInfoVo(User userInfoVo) {
        this.userInfoVo = userInfoVo;
    }

    public User getUserInfoVo() {
        return userInfoVo;
    }

    public String getImPassword() {
        return imPassword;
    }

    public void setImPassword(String imPassword) {
        this.imPassword = imPassword;
    }

}
