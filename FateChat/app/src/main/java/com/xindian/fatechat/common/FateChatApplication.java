package com.xindian.fatechat.common;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.multidex.MultiDex;
import android.util.Log;
import android.view.Gravity;
import com.hyphenate.easeui.domain.ChatUserEntity;
import com.hyphenate.easeui.ui.EaseChatFragment;
import com.igexin.sdk.PushManager;
import com.thinkdit.lib.base.BaseApplication;
import com.umeng.analytics.MobclickAgent;
import com.xindian.fatechat.IMApplication;
import com.xindian.fatechat.manager.LocationManager;
import com.xindian.fatechat.manager.RulesManager;
import com.xindian.fatechat.service.GeTuiReceiverService;
import com.xindian.fatechat.service.GetuiPushService;
import com.xindian.fatechat.service.LoadForBiddenWordsService;
import com.xindian.fatechat.util.ActivityStack;
import com.xindian.fatechat.widget.GiftPopupWindow;

import com.xindian.fatechat.widget.ReportPopupWindow;

/**
 * Created by hx_Alex on 2017/3/23.
 */

public class FateChatApplication extends IMApplication {
    public static final String WX_APP_TYPE = "pay.weixin.app";
    private static final String TAG = "FateChatApplication";
    public ActivityStack stack;
    @Override
    public void onCreate() {
        super.onCreate();
        LocationManager.instance().init(this);
        LocationManager.instance().start();
        RulesManager.instance(this);
        initUmeng();
        Log.e("tag",getApplication().getPackageName()) ;
        intiGeTui();
        startService(new Intent(this, LoadForBiddenWordsService.class));
        Log.e("tag",getApplication().getPackageName()) ;
        stack =ActivityStack.getInstance();
        this.registerActivityLifecycleCallbacks(stack);


    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
    }

    //初始化友盟
    private void initUmeng(){
        MobclickAgent.setScenarioType(this, MobclickAgent.EScenarioType.E_UM_NORMAL);
        MobclickAgent.openActivityDurationTrack(false);
        MobclickAgent.enableEncrypt(true);
    }

    @Override
    protected FateChatApplication getApplication() {
        return this;
    }

    public static FateChatApplication getInstance() {
        return (FateChatApplication) BaseApplication.getInstance();
    }

    ReportPopupWindow mReportPopupWindow;
    @Override
    public void showReportView(Activity activity, ChatUserEntity chatUserEntity) {
        if(activity == null || chatUserEntity == null){
            return;
        }
        if(mReportPopupWindow == null){
            mReportPopupWindow = new ReportPopupWindow(activity,chatUserEntity);
        }
        mReportPopupWindow.setmChatUserEntity(chatUserEntity);
        mReportPopupWindow.showAtLocation(activity.getWindow().getDecorView(), Gravity.BOTTOM,0,0);
    }


    /****
     * 个推初始化，多次初始化不影响使用
     */
    private void intiGeTui() {
//        PackageManager pkgManager = getPackageManager();
//        // 读写 sd card 权限非常重要, android6.0默认禁止的, 建议初始化之前就弹窗让用户赋予该权限
//        boolean sdCardWritePermission =
//                pkgManager.checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, getPackageName()) == PackageManager.PERMISSION_GRANTED;
//
//        // read phone state用于获取 imei 设备信息
//        boolean phoneSatePermission =
//                pkgManager.checkPermission(Manifest.permission.READ_PHONE_STATE, getPackageName()) == PackageManager.PERMISSION_GRANTED;
//        if (Build.VERSION.SDK_INT >= 23 && !sdCardWritePermission || !phoneSatePermission) {
//            requestPermission();
//        } else {
//
//        }
        PushManager.getInstance().initialize(this.getApplicationContext(), GetuiPushService.class);
        PushManager.getInstance().registerPushIntentService(this.getApplicationContext(), GeTuiReceiverService.class);
  
    }

    GiftPopupWindow mGiftPopupWindow;
    private Activity nowActivity;
    @Override
    public void showGiftView(Activity activity, EaseChatFragment easeChatFragment, ChatUserEntity chatUserEntity) {
        if(activity == null || chatUserEntity == null){
            return;
        }

        if(mGiftPopupWindow == null || nowActivity != activity){
            mGiftPopupWindow = new GiftPopupWindow(activity,chatUserEntity);
        }
        nowActivity = activity;

        mGiftPopupWindow.setmChatUserEntity(chatUserEntity);
        mGiftPopupWindow.setEaseChatFragment(easeChatFragment);
        mGiftPopupWindow.showAtLocation(activity.getWindow().getDecorView(), Gravity.BOTTOM,0,0);
    }


}
