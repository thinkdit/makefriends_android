package com.xindian.fatechat.ui.home;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.xindian.fatechat.R;
import com.xindian.fatechat.databinding.ActivityMyComBinding;
import com.xindian.fatechat.ui.base.BaseActivity;
import com.xindian.fatechat.ui.home.fragment.ComFragment;
import com.xindian.fatechat.widget.player.VideoPlayerHelper;

/**
 * 我的帖子
 */
public class MyComActivity extends BaseActivity{
    private ActivityMyComBinding mBinding;
    private ComFragment homeFragment;
    private FragmentManager fragmentManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_my_com);

        initView();
        VideoPlayerHelper.init(this);
    }

    
    private void initView() {
        //组装fragment
        homeFragment = new ComFragment();
        fragmentManager= getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.add(mBinding.layoutMyCom.getId(),homeFragment);//默认先添加首页
        ft.commit();

        //设置成个人的帖子
        homeFragment.setTitle("我的帖子");
        homeFragment.setType(ComFragment.TYPE_MY);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("msg","onResume");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        VideoPlayerHelper.getInstance().stop();
    }

    @Override
    public void onPause() {
        super.onPause();
        VideoPlayerHelper.getInstance().pause();
    }
}
