package com.xindian.fatechat.ui.user.serivce;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Created by hx_Alex on 2017/5/22.
 */

public class NoticationImSerivce extends Service {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Intent broadcast=new Intent();
        broadcast.setAction("ImNotication");
        sendBroadcast(broadcast);
        return super.onStartCommand(intent, flags, startId);
    }
}
