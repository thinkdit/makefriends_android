package com.xindian.fatechat.ui.login.model;

import com.xindian.fatechat.common.BaseModel;

/**
 * Created by hx_Alex on 2017/6/7.
 */

public class SplashBean extends BaseModel {

    /**
     * id : 3
     * name : 22
     * channelId : freeChannel
     * url : http://makefriends-dev.oss-cn-hangzhou.aliyuncs.com/1496735235976timg-2.jpeg
     * creationTime : 1496735254000
     * spare1 : null
     * spare2 : null
     * spare3 : null
     * state : 1
     */

    private int id;
    private String name;
    private String channelId;
    private String url;
    private long creationTime;
    private String spare1;
    private String spare2;
    private String spare3;
    private String state;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public long getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(long creationTime) {
        this.creationTime = creationTime;
    }

    public String getSpare1() {
        return spare1;
    }

    public void setSpare1(String spare1) {
        this.spare1 = spare1;
    }

    public String getSpare2() {
        return spare2;
    }

    public void setSpare2(String spare2) {
        this.spare2 = spare2;
    }

    public String getSpare3() {
        return spare3;
    }

    public void setSpare3(String spare3) {
        this.spare3 = spare3;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
