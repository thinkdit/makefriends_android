package com.xindian.fatechat.manager;

import android.Manifest;
import android.content.Context;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.hyphenate.easeui.manager.MessageLimitManager;
import com.thinkdit.lib.util.PackageUtil;

/**
 */
public class LocationManager implements AMapLocationListener {
    private Context mContext;
    private AMapLocationClient mLocationClient = null;
    private static LocationManager instance;
    private String address;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public static LocationManager instance() {
        if (instance == null) {
            instance = new LocationManager();
        }
        return instance;
    }

    private LocationManager() {
    }

    public void init(Context context) {
        mContext = context;
        mLocationClient = new AMapLocationClient(mContext);
        AMapLocationClientOption locationOption = new AMapLocationClientOption();
        // 设置定位模式为低功耗模式
        locationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Battery_Saving);
        // 设置定位监听
        mLocationClient.setLocationListener(this);
        // 设置是否需要显示地址信息
        locationOption.setNeedAddress(true);
        //设置是否等待设备wifi刷新，如果设置为true,会自动变为单次定位，持续定位时不要使用
        locationOption.setOnceLocationLatest(true);
        // 设置定位参数
        mLocationClient.setLocationOption(locationOption);
    }

    /**
     * 检查是否有定位权限
     *
     * @return 是否
     */
    private boolean checkPermission() {
        return PackageUtil.checkPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) ||
                PackageUtil.checkPermission(mContext, Manifest.permission.ACCESS_NETWORK_STATE) ||
                PackageUtil.checkPermission(mContext, Manifest.permission.ACCESS_WIFI_STATE) ||
                PackageUtil.checkPermission(mContext, Manifest.permission.READ_PHONE_STATE);
    }

    public void start() {
        if (!checkPermission()) {
            return;
        }
        mLocationClient.startLocation();
    }

    public void stop() {
        mLocationClient.stopLocation();
        mLocationClient.onDestroy();
        mLocationClient = null;
    }

    @Override
    public void onLocationChanged(AMapLocation aMapLocation) {
        if (aMapLocation != null && aMapLocation.getErrorCode() == AMapLocation.LOCATION_SUCCESS) {
            address = aMapLocation.getProvince() + "-" + aMapLocation.getCity();
            MessageLimitManager.instance(mContext).setAddress(address);
            stop();
        } else {
        }
    }
}
