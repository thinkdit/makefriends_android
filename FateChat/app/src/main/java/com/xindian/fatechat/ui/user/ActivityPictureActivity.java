package com.xindian.fatechat.ui.user;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.xindian.fatechat.R;
import com.xindian.fatechat.ui.base.BaseActivity;
import com.xindian.fatechat.ui.home.model.Gallery;
import com.xindian.fatechat.widget.PicturesViewPager;
import com.xindian.fatechat.widget.adapter.PictureViewPagerAdapter;


/**
 * 网络图片 大图查看界面
 */
public class ActivityPictureActivity extends BaseActivity implements PicturesViewPager.OnPageSingleTapListener
        , ViewPager.OnPageChangeListener{
    public PicturesViewPager viewPager;
    public ViewGroup toolbar;
    public TextView titleTv;
    public ImageView backIcon;

    private Gallery mGallery;
    private int mPosition;
    private PictureViewPagerAdapter mAdapter;

    /**
     * toolbar高度,用来显示隐藏toolbar
     */
    private int toolbarHeight = 0;
    /**
     * 用来显示隐藏toolbar的动画
     */
    private ValueAnimator anim;
    /**
     * 控制动画显隐
     */
    private boolean animShowing = true;

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gri_activity_picture);

        initView();
        initData();
    }

    protected void initView() {
        viewPager = (PicturesViewPager)findViewById(R.id.picture_viewpager);
        toolbar = (ViewGroup) findViewById(R.id.toolbar);
        titleTv = (TextView) findViewById(R.id.toolbar_title);
        backIcon = (ImageView) findViewById(R.id.toolbar_back);
        backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    protected void initData() {
        Bundle bundle = getIntent().getExtras();
        mGallery = (Gallery) bundle.getSerializable("GALLERY");
        mPosition = bundle.getInt("POSITION", 0);
        if (mGallery == null || mGallery.getList() == null || mGallery.getList().isEmpty()) {
            Toast.makeText(this, R.string.gallery_error,Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        //初始化adpater
        mAdapter = new PictureViewPagerAdapter(this, mGallery.getList());
        viewPager.setAdapter(mAdapter);
        viewPager.setCurrentItem(mPosition, false);
        //初始化title
        setToolBarTitle(mPosition);
        //初始化toolbar显示隐藏动画
        initAnimation();
        initAction();

    }

    protected void initAction() {
        viewPager.setOnSingleTapUpListener(this);
        viewPager.addOnPageChangeListener(this);
    }

    /**
     * 初始化toolbar显示隐藏动画
     */
    private void initAnimation() {
        anim = ValueAnimator.ofFloat(0, 1);
        anim.setDuration(400);
        anim.setInterpolator(new DecelerateInterpolator());
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float ratio = (float) valueAnimator.getAnimatedValue();
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) toolbar.getLayoutParams();
                if (animShowing) {
                    //隐藏
                    params.setMargins(0, (int) (-ratio * toolbarHeight), 0, 0);
                } else {
                    //显示
                    params.setMargins(0, (int) (-(1 - ratio) * toolbarHeight), 0, 0);
                }
                toolbar.setLayoutParams(params);
            }
        });
        anim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                if (!animShowing) {
                    toolbar.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                if (animShowing) {
                    toolbar.setVisibility(View.GONE);
                    animShowing = false;
                } else {
                    animShowing = true;
                }
            }

            @Override
            public void onAnimationCancel(Animator animator) {
            }

            @Override
            public void onAnimationRepeat(Animator animator) {
            }
        });
    }

    /**
     * 点击viewpager事件.隐藏显示bar
     */
    @Override
    public void onPageClick() {
        toolbarHeight = toolbar.getHeight();
        if (anim == null) {
            return;
        }
        if (anim.isRunning()) {
            return;
        } else {
            anim.start();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (anim != null) {
            anim.cancel();
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        setToolBarTitle(position);
    }

    /**
     * 设置title
     *
     * @param position
     */
    private void setToolBarTitle(int position) {
        mPosition = position;
        titleTv.setText(getString(R.string.picture_index,position + 1 + ""));
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

}
