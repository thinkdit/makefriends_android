package com.xindian.fatechat.util;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.github.lzyzsd.jsbridge.BridgeWebView;
import com.github.lzyzsd.jsbridge.BridgeWebViewClient;
import com.github.lzyzsd.jsbridge.DefaultHandler;
import com.thinkdit.lib.util.DeviceInfoUtil;
import com.thinkdit.lib.util.L;
import com.thinkdit.lib.util.StringUtil;
import com.xindian.fatechat.common.XTParamCommom;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.ui.user.model.User;

import java.net.URLDecoder;
import java.util.Map;

/**
 * webview的工具类
 * Created by qiuda on 16/3/23.
 */
public class WebViewUtil {
    private static final String TAG = WebViewUtil.class.getName();

    public static void initWebView(BridgeWebView webview, final IWebViewCallBack callBack) {
        webview.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webview.getSettings().setDefaultTextEncodingName("utf-8");
        webview.getSettings().setDomStorageEnabled(true);
        webview.getSettings().setDatabaseEnabled(true);
        webview.getSettings().setAppCacheEnabled(true);
        webview.setDefaultHandler(new DefaultHandler());

        webview.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
                if (callBack != null) {
                    callBack.onReceivedTitle(view, title);
                }
            }
        });

        webview.setWebViewClient(new BridgeWebViewClient(webview) {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url == null || view == null) return true;

//                if (url.startsWith(SchemeManager.BASE_PROTOCOL)) {
//                    SchemeManager.getInstance().jumpToActivity(webview.getContext(), url, true);
//                    return true;
//                }
                return super.shouldOverrideUrlLoading(view, url);
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                if (callBack != null) {
                    callBack.onPageStarted(view, url, favicon);
                }
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if (callBack != null) {
                    callBack.onPageFinished(view, url);
                }
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String
                    failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
                if (callBack != null) {
                    callBack.onReceivedError(view, errorCode, description, failingUrl);
                }
            }

        });
        webview.setDownloadListener((url, userAgent, contentDisposition, mimetype, contentLength)
                -> {
            if (callBack != null) {
                callBack.onDownloadStart(url, userAgent, contentDisposition, mimetype,
                        contentLength);
            }
        });
    }

    public static void registerFun(final Context context, final BridgeWebView webview, final
    IWebViewCallBack callBack) {
        //注册getUserInfo
        webview.registerHandler("getUserInfo", (data, function) -> {
            User user = UserInfoManager.getManager(context).getUserInfo();
            String text = JSON.toJSONString(user).replace("'", "&apos;");
            function.onCallBack(text);
        });


//        //调用login
//        webview.registerHandler("login", (data, function) -> {
//
//            UserInfoManager.getManager(context).loginOut();
//            Intent intent = new Intent(context, LoginActivity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            context.startActivity(intent);
//        });
//
//        //调用logout
//        webview.registerHandler("logout", (data, function) -> {
//            UserInfoManager.getManager(context).loginOut();
//            Intent intent = new Intent(context, StartActivity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            context.startActivity(intent);
//        });

        //share 分享
        webview.registerHandler("share", (data, function) -> {
            try {
                Map<String, String> map = JSON.parseObject(data, new TypeReference<Map<String,
                        String>>() {
                });
                String id = map.get("id");
                String type = map.get("type");
                callBack.onShare(id, type);

                if (function != null) {
                    function.onCallBack("true");
                }
            } catch (Exception e) {
                L.e(TAG, e);
            }

        });

        //setShareInfo 设置分享内容
        webview.registerHandler("setShareInfo", (data, function) -> {
            try {
                Map<String, String> map = JSON.parseObject(data, new TypeReference<Map<String,
                        String>>() {
                });
                String id = map.get("id");
                String type = map.get("type");
                callBack.setShare(id, type);
                if (function != null) {
                    function.onCallBack("true");
                }
            } catch (Exception e) {
                L.e(TAG, e);
            }
        });

        //getDeviceInfo 获取设备信息
        webview.registerHandler("getDeviceInfo", (data, function) -> {
            XTParamCommom xtParamCommom = new XTParamCommom();
            String jsonString = JSON.toJSONString(xtParamCommom.build());
            function.onCallBack(jsonString);
        });
        //getDeviceInfoForMobil 获取手机ID
        webview.registerHandler("getDeviceId", (data, function) -> function.onCallBack
                (DeviceInfoUtil.getDeviceId(context)));

        //closeWebView 关闭web页面
        webview.registerHandler("closeView", (data, function) -> {
            if (function != null) {
                function.onCallBack("true");
            }
            if (callBack != null) {
                callBack.onClose();
            }
        });

        //jumpPage 跳转web
        webview.registerHandler("jumpPage", (data, function) -> {
            try {
                Map<String, String> map = JSON.parseObject(data, new TypeReference<Map<String,
                        String>>() {
                });
                String url = URLDecoder.decode(map.get("url"), "UTF-8");
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                context.startActivity(intent);
                if (function != null) {
                    function.onCallBack("true");
                }
            } catch (Exception e) {
                L.e(TAG, e);
            }

        });
        //getUserAgent 获取UA
        webview.registerHandler("getUserAgent", (data, function) -> {
            L.d("webviewActivity getUserAgent:", webview.getSettings().getUserAgentString());
            function.onCallBack(webview.getSettings().getUserAgentString());
        });


        //getUserAgent 获取UA
        webview.registerHandler("copyToClipboard", (data, function) -> {
            try {
                Map<String, String> map = JSON.parseObject(data, new TypeReference<Map<String,
                        String>>() {
                });
                String content = map.get("content");
                if (!StringUtil.isNullorEmpty(content)) {
                    content = URLDecoder.decode(content, "UTF-8");
                }
                StringUtil.copyToClipboard(context, content);
                Toast.makeText(context, "复制成功", Toast.LENGTH_SHORT).show();
                if (function != null) {
                    function.onCallBack("true");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        });
//
//        //getUserAgent 获取UA
//        webview.registerHandler("changeBaseUrl", (data, function) -> {
//            try {
//                Map<String, String> map = JSON.parseObject(data, new TypeReference<Map<String,
//                        String>>() {
//                });
//                String api = URLDecoder.decode(map.get("api"), "UTF-8");
//                String chat = URLDecoder.decode(map.get("chat"), "UTF-8");
//                String pay = URLDecoder.decode(map.get("pay"), "UTF-8");
//                String h5 = URLDecoder.decode(map.get("h5"), "UTF-8");
//                String fun = URLDecoder.decode(map.get("fun"), "UTF-8");
//
//                SharePreferenceUtils.putString(context, UrlConstant.SAVE_KEY_URL_API, api);
//                SharePreferenceUtils.putString(context, UrlConstant.SAVE_KEY_URL_CHAT, chat);
//                SharePreferenceUtils.putString(context, UrlConstant.SAVE_KEY_URL_PAY, pay);
//                SharePreferenceUtils.putString(context, UrlConstant.SAVE_KEY_URL_H5, h5);
//                SharePreferenceUtils.putString(context, UrlConstant.SAVE_KEY_URL_FUN, fun);
//
//                UrlConstant.setBaseUrl(api, chat, pay, h5, fun);
//
//
//                if (UserInfoManager.getManager(context).isLogin()) {//登录状态下才登出,并且跳到主页
//                    UserInfoManager.getManager(context).loginOut();
//                    UrlConstant.changeSystemSetting();
//                    StatManager.IS_OPEN = false;
//                    Intent intent = new Intent(context, MainActivity.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    intent.putExtra(MainActivity.ARG_LOGINOUT, true);
//                    context.startActivity(intent);
//                } else {
//                    if (webview.getContext() instanceof Activity) {
//                        ((Activity) webview.getContext()).finish();
//                    }
//                }
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//        });

        //注册直播间活动按钮
        webview.registerHandler("registerButton", (data, function) -> {
            Map<String, String> map = JSON.parseObject(data, new TypeReference<Map<String,
                    String>>() {
            });
            String image = map.get("image");
            callBack.showLiveWeb2Button(image);
            function.onCallBack("true");
        });

        //直播预约
        webview.registerHandler("onLiveSubscribe", (data, function) -> {
            Map<String, String> map = JSON.parseObject(data, new TypeReference<Map<String,
                    String>>() {
            });
            String time = map.get("startTime");
            String title = map.get("title");
            String remark = map.get("remark");
            callBack.onLiveSubscribe(time, title, remark);
            function.onCallBack("true");
        });

        //获取用户直播间角色
        webview.registerHandler("getLiveUserRole", (data, function) -> {
            function.onCallBack(callBack.getLiveUserRole());
        });

        //showUserInfo 显示用户信息 应用场景游戏web点击显示用户资料弹窗
        webview.registerHandler("showUserInfo", (data, function) -> {
            if (callBack != null) {
                Map<String, String> map = JSON.parseObject(data, new TypeReference<Map<String,
                        String>>() {
                });
                callBack.showUserInfo(map.get("id"));
            }
            function.onCallBack("true");
        });

//        //getApiSign 签名
//        webview.registerHandler("getApiSign", (data, function) -> {
//            Map<String, String> map = JSON.parseObject(data, new TypeReference<Map<String,
//                    String>>() {
//            });
//            if (map != null && map.size() > 0) {
//                String method = map.get("method");
//                String parameters = map.get("parameters");
//                boolean isGet = "GET".equals(method);
//                Map<String, String> paramMap = JSON.parseObject(parameters, new
//                        TypeReference<Map<String, String>>() {
//                });
//                XTParamCommom commomParam = new XTParamCommom();
//                Map<String, Object> commomParamMap = commomParam.build();
//                JSONObject jsonObject = new JSONObject();
//                jsonObject.put("parameters", commomParamMap);
//                String token = UserInfoManager.getManager(MicrodiskApplication.getInstance())
//                        .getToken();
//                String md5Str;
//                if (isGet) {
//                    if (paramMap != null && paramMap.size() > 0) {
//                        commomParamMap.putAll(paramMap);
//                    }
//                    md5Str = SecretUtil.getSign(token, XTHttpUtil.getValue(commomParamMap), null,
//                            UrlConstant.IS_DEBUG);
//                } else {
//                    String body = JSON.toJSONString(paramMap);
//                    md5Str = SecretUtil.getSign(token, XTHttpUtil.getValue(commomParamMap), body,
//                            UrlConstant.IS_DEBUG);
//                }
//                L.d(TAG, "getApiSign=" + md5Str);
//                jsonObject.put("sign", md5Str);
//                String result = jsonObject.toJSONString();
//                L.d(TAG, "result=" + result);
//                function.onCallBack(result);
//            }
//        });

        //support的能力
        webview.registerHandler("support", (data, function) -> {
            L.i(WebViewUtil.class.getName(), "handler = support, data from web = " + data);
            if (data.isEmpty()) return;

            String strMethod = JSON.parseObject(data).getString("method");
            switch (strMethod) {
                case "getUserInfo":
                case "login":
                case "logout":
                case "share":
                case "getDeviceInfo":
                case "getDeviceId":
                case "closeView":
                case "jumpPage":
                case "getUserAgent":
                case "copyToClipboard":
                case "changeBaseUrl":
                case "setShareInfo":
                case "onActionCallback":
                case "onLiveSubscribe":
                case "getLiveUserRole":
                case "registerButton":
                case "getApiSign":
                    function.onCallBack("true");
                    break;
                default:
                    function.onCallBack("false");
                    break;
            }
        });
    }

    public interface IWebViewCallBack {

        void onReceivedTitle(WebView view, String title);

        void onDownloadStart(String url, String userAgent, String contentDisposition, String
                mimetype, long contentLength);

        void onPageStarted(WebView view, String url, Bitmap favicon);

        void onPageFinished(WebView view, String url);

        void onReceivedError(WebView view, int errorCode, String description, String failingUrl);

        void onShare(String id, String type);

        void setShare(String id, String type);

        /**
         * 显示直播见web2活动的按钮
         *
         * @param buttonUrl
         */
        void showLiveWeb2Button(String buttonUrl);

        void onClose();

        /**
         * 获取用户角色
         *
         * @return
         */
        String getLiveUserRole();

        /**
         * 直播间预订
         */
        void onLiveSubscribe(String starTime, String title, String remark);

        /**
         * 显示用户信息
         *
         * @param uid 用户id
         */
        void showUserInfo(String uid);
    }
}
