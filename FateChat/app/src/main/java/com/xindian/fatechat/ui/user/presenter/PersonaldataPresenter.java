package com.xindian.fatechat.ui.user.presenter;

import android.content.Context;
import android.os.RemoteException;

import com.alibaba.fastjson.JSONException;
import com.xindian.fatechat.common.BasePresenter;
import com.xindian.fatechat.common.ResultModel;
import com.xindian.fatechat.ui.user.model.User;
import com.xindian.fatechat.widget.MyProgressDialog;

import java.util.HashMap;

/**
 * Created by hx_Alex on 2017/4/7.
 */

public class PersonaldataPresenter extends BasePresenter {
    private final String  URL_UPDATEUSERINFO="/auth/update_user_info";
    private final String  URL_APPOVEUSERHEAD="/approve/user_head";//上传头像审核
    private final String  URL_QUERYUSERCASEBYUSERID="/auth/queryUserCaseByUserId";
    private final String  RESLUT_OK="ok";
    private onPersonaldataListener mPersonaldataListener;
    private onPersonalExamineHeadImgListener mPersonalExamineHeadImgListener;
    private MyProgressDialog mDialog;
    private Context context;
    public PersonaldataPresenter(Context context,onPersonaldataListener listener) {
        this.context = context;
        this.mPersonaldataListener = listener;
        mDialog = new MyProgressDialog(context);
    }

    
    public void setmPersonalExamineHeadImgListener(onPersonalExamineHeadImgListener mPersonalExamineHeadImgListener) {
        this.mPersonalExamineHeadImgListener = mPersonalExamineHeadImgListener;
    }
    
    


    public void requestUpdateFriendsCase(User user)
    {
        String height=user.getHeight();
        if(user.getHeight()!=null &&  user.getHeight().contains("cm"))
        {
            height= user.getHeight().substring(0,user.getHeight().indexOf("cm"));
        }
        HashMap<String,Object> data=new HashMap<>();
        data.put("nickName",user.getNickName());
        data.put("wechat",user.getWechat());
        data.put("mobile",user.getMobile());
        data.put("qualifications",user.getQualifications());
        data.put("profession",user.getProfession());
        data.put("birthday",user.getBirthday());
        data.put("weight",user.getWeight());
        data.put("constellation",user.getConstellation());
        data.put("headimg",user.getHeadimg());
        data.put("address",user.getAddress());
        data.put("age",user.getAge());
        data.put("userId",user.getUserId());
        data.put("height",height);
        data.put("marriage",user.getMarriage());
        data.put("revenue",user.getRevenue());
        post(getUrl(URL_UPDATEUSERINFO),data,null);
        mDialog.show();
    }


    public void requestExamineHeadImg(User user)
    {
        HashMap<String,Object> data=new HashMap<>();
        data.put("nickName",user.getNickName());
        data.put("headUrl",user.getHeadimg());
        data.put("userId",user.getUserId());
       
        post(getUrl(URL_APPOVEUSERHEAD),data,null);
        mDialog.show();
    }




    @Override
    public Object asyncExecute(String url, ResultModel resultModel) {
      
        return  null;
    }

    @Override
    public void onError(String url, Exception e) {
        super.onError(url, e);
    }


    @Override
    public void onFailure(String url, ResultModel resultModel) {
        if(url.contains(URL_UPDATEUSERINFO)) {
            mPersonaldataListener.onPersonaldataError(resultModel.getMessage().equals("") || resultModel.getMessage() == null ? "未知错误" : resultModel.getMessage());
        }  if(url.contains(URL_UPDATEUSERINFO)) {
            mPersonalExamineHeadImgListener.onExamineHeadImgError(resultModel.getMessage().equals("") || resultModel.getMessage() == null ? "未知错误" : resultModel.getMessage());
        }
        mDialog.dismiss();
    }

    @Override
    public void onSucceed(String url, ResultModel resultModel) throws JSONException, RemoteException {
        if(url.contains(URL_UPDATEUSERINFO))
        {
            if(resultModel.getCode().equals(RESLUT_OK))
            {
                mPersonaldataListener.onPersonaldataSuccess();
            }
        }else if(url.contains(URL_APPOVEUSERHEAD))
        {
            if(resultModel.getCode().equals(RESLUT_OK))
            {
                mPersonalExamineHeadImgListener.onExamineHeadImgSuccess();
            }
        }
      
        mDialog.dismiss();
    }


    public  interface  onPersonaldataListener
    {
        void  onPersonaldataSuccess();
        void  onPersonaldataError(String msg);
       
    }


    public  interface  onPersonalExamineHeadImgListener
    {
        void  onExamineHeadImgSuccess();
        void  onExamineHeadImgError(String msg);

    }

}
