package com.xindian.fatechat.ui.home.adapter;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.xindian.fatechat.R;
import com.xindian.fatechat.databinding.ItemNearbyFriendsBinding;
import com.xindian.fatechat.manager.SharePreferenceManager;
import com.xindian.fatechat.ui.user.UserDetailsActivity;
import com.xindian.fatechat.ui.user.model.NearByUser;
import com.xindian.fatechat.util.IMUtil;
import com.hyphenate.easeui.utils.SnackbarUtil;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;


/**
 */
public class NearByFriendsAdapter extends RecyclerView.Adapter<NearByFriendsAdapter
        .BindingViewHolder> {
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private List<NearByUser> mDatas;

    public NearByFriendsAdapter(Context context) {
        mContext = context;
        mLayoutInflater = LayoutInflater.from(mContext);
    }

    public void setDatas(List<NearByUser> datas) {
        mDatas = datas;
        notifyDataSetChanged();
    }

    public void addDatas(List<NearByUser> datas) {
        if (mDatas == null) {
            mDatas = new ArrayList<>();
        }
        int oldItem = 0;
        int newItem = 0;
        if(mDatas!=null){
            oldItem = mDatas.size();
        }
        if(datas!=null){
            newItem = datas.size();
        }
        mDatas.addAll(datas);
        notifyItemRangeChanged(oldItem,newItem);
    }

    @Override
    public BindingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemNearbyFriendsBinding binding = DataBindingUtil.inflate(mLayoutInflater, R.layout
                .item_nearby_friends, parent, false);
        return new BindingViewHolder(binding);
    }


    @Override
    public void onBindViewHolder(BindingViewHolder holder, int position) {
        holder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mDatas.get(position) != null) {
                    UserDetailsActivity.gotoUserDetails(mContext, mDatas.get(position).getUserId
                            (), "5公里以内");
                }
            }
        });
        holder.binding.setInfo(mDatas.get(position));
        if (SharePreferenceManager.getManager(mContext).isChated(mDatas.get(position).getUserId()
        )) {
            holder.binding.hi.setImageResource(R.drawable.img_has_hi);
            holder.binding.hi.setClickable(false);

        } else {
            holder.binding.hi.setClickable(true);
            holder.binding.hi.setImageResource(R.drawable.hi);
            holder.binding.hi.setOnClickListener(v -> {
                if (IMUtil.checkIsLogin(mContext)) {
                    if (mDatas.get(position) != null && IMUtil.checkIsLogin(mContext)) {
                        IMUtil.sendHellMessage(mContext, mDatas.get(position));
                        SnackbarUtil.makeSnackBar(holder.binding.getRoot(), "打招呼成功", Toast
                                .LENGTH_SHORT).setMeessTextColor(Color.WHITE).show();
                        holder.binding.hi.setClickable(false);
                        holder.binding.hi.setImageResource(R.drawable.img_has_hi);
                    }
                }
            });
        }


    }

    @BindingAdapter({"distance"})
    public static void setDistance(TextView view, float distance) {
        DecimalFormat decimalFormat = new DecimalFormat("0.00");//构造方法的字符格式这里如果小数不足2位,会以0补足.
        String str = decimalFormat.format(distance);//format 返回的是字符串
        view.setText(str + "km");
    }

    @BindingAdapter({"heigh"})
    public static void setHeigh(TextView view, String heigh) {
        view.setText(heigh + "cm");
    }

    @Override
    public int getItemCount() {
        if (mDatas != null) {
            return mDatas.size();
        }
        return 0;
    }

    public class BindingViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final ItemNearbyFriendsBinding binding;

        public BindingViewHolder(ItemNearbyFriendsBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

        }

        @Override
        public void onClick(View v) {
        }
    }
}
