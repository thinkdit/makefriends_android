package com.xindian.fatechat.ui.user;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.xindian.fatechat.R;
import com.xindian.fatechat.databinding.ActivitySetBinding;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.ui.base.BaseActivity;
import com.xindian.fatechat.ui.home.fragment.MainActivity;
import com.xindian.fatechat.ui.login.SplashActivity;
import com.xindian.fatechat.util.DataCleanManagerUtil;
import com.hyphenate.easeui.utils.SnackbarUtil;
import com.xindian.fatechat.widget.StarTDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class SetActivity extends BaseActivity {
    public static final String TAG="SetActivity";
    private  static  final int CHANGEPASSWORD=1;
private ActivitySetBinding mBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      mBinding= DataBindingUtil.setContentView(this,R.layout.activity_set);
        EventBus.getDefault().register(this);
        initToolBar();
        if (DataCleanManagerUtil.getTotalCacheSize(this) != null) {
            mBinding.setCache(DataCleanManagerUtil.getTotalCacheSize(this));//获取app缓存
        }

    }
    
    public void onClickChangePassword(View v)
    {
        Intent i=new Intent(this,ChangePasswordActivity.class);
        startActivityForResult(i,CHANGEPASSWORD);
    }
    
    public  void onClickExit(View view)
    {
        UserInfoManager.getManager(this).loginOut(null);

    }
    
    public void onClickClearCache(View v)
    {
        StarTDialog dialog=new StarTDialog(this,mBinding.activitySet);
        dialog.show("提示","清除缓存会导致您的相关信息遗失,是否继续进行清除操作?","清除","取消");
        dialog.setBtnOkCallback(new StarTDialog.iDialogCallback() {
            @Override
            public boolean onBtnClicked() {
                DataCleanManagerUtil.clearAllCache(SetActivity.this);
                mBinding.setCache(DataCleanManagerUtil.getTotalCacheSize(SetActivity.this));//获取app缓存
                return true;
            }
        });
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==CHANGEPASSWORD && resultCode==RESULT_OK)
        {
            SnackbarUtil.makeSnackBar(mBinding.getRoot(),"密码修改成功！", Toast.LENGTH_SHORT).setMeessTextColor(Color.WHITE).show();
        }
    }

    /***
     * 退出回调
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void loginOutCallBack(String msg){
        if(msg.equals(UserInfoManager.IMLOGINOUTSUCCESS))
        {
            Intent i=new Intent(this, SplashActivity.class);
            i.putExtra(SetActivity.TAG,SplashActivity.BYLOGINOUT);
            startActivity(i);
            EventBus.getDefault().post(MainActivity.ONLOGINOUTBYSET);//发送消息通知首页用户已经退出登录，finsh掉自己
            finish();
        }else if(msg.equals(UserInfoManager.IMLOGINOUTERROR))
        {
            SnackbarUtil.makeSnackBar(mBinding.getRoot(),"退出失败请重新尝试", Toast.LENGTH_SHORT).setMeessTextColor(Color.WHITE).show();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
