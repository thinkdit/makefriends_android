package com.xindian.fatechat.ui.home.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.common.pullrefreshview.PullToRefreshView;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.easeui.EaseConstant;
import com.hyphenate.easeui.domain.ChatUserEntity;
import com.hyphenate.easeui.manager.MessageLimitManager;
import com.thinkdit.lib.util.SharePreferenceUtils;
import com.xindian.fatechat.DemoHelper;
import com.xindian.fatechat.R;
import com.xindian.fatechat.common.Constant;
import com.xindian.fatechat.databinding.FragmentRRvBinding;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.ui.base.BaseFragment_v4;
import com.xindian.fatechat.ui.home.adapter.RecommendFriendsAdapter;
import com.xindian.fatechat.ui.home.presenter.RecommendPresenter;
import com.xindian.fatechat.ui.user.model.NearByUser;
import com.xindian.fatechat.ui.user.model.User;
import com.xindian.fatechat.util.IMUtil;
import com.xindian.fatechat.widget.SayHiDialog;
import com.xindian.fatechat.widget.SpacesItemDecoration;

import java.util.List;

/**
 * Created by hx_Alex on 2017/3/24.
 * 首页Fragment
 */

public class RecommendFriendsFragment extends BaseFragment_v4 implements RecommendPresenter
        .onRecommendList {
    private FragmentRRvBinding mBinding;
    private RecommendFriendsAdapter adapter;
    private RecommendPresenter presenter;

    public static RecommendFriendsFragment newInstance() {
        RecommendFriendsFragment RecommendFriendsFragment = new RecommendFriendsFragment();
        Bundle bundle = new Bundle();
        RecommendFriendsFragment.setArguments(bundle);
        return RecommendFriendsFragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
            Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_r_rv, container, false);
        initView();
        presenter = new RecommendPresenter(this.getActivity(), this);
        presenter.recommend_user(true);
        presenter.getSayHiUser();
        return mBinding.getRoot();
    }

    private void initView() {
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(3,
                StaggeredGridLayoutManager.VERTICAL);
        mBinding.rvList.addItemDecoration(new SpacesItemDecoration(8));
        //3.为recyclerView设置布局管理器
        mBinding.rvList.setLayoutManager(staggeredGridLayoutManager);
        adapter = new RecommendFriendsAdapter(this.getActivity());
        mBinding.rvList.setAdapter(adapter);

        mBinding.ptr.setListener(new PullToRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.recommend_user(true);
            }

            @Override
            public void onLoadMore() {
                presenter.recommend_user(false);
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        if (adapter.getItemCount() == 0 || mBinding.rvList.getAdapter() == null) {
            adapter = new RecommendFriendsAdapter(this.getActivity());
            mBinding.rvList.setAdapter(adapter);
            presenter.recommend_user(true);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (isVisibleToUser) {
            if (presenter != null) {
                presenter.recommend_user(true);
            }
        }
        super.setUserVisibleHint(isVisibleToUser);
    }

    @Override
    public void onShowChange(boolean hide) {
        if (hide) {
            if (presenter != null) {
                presenter.recommend_user(true);
            }
        }
        super.onShowChange(hide);
    }

    @Override
    public void onGetRecommendListSuccess(List<User> list, boolean isFirst, boolean hasMore) {
        //对用户头像进行地址加参
        for(int i=0;i<list.size();i++){
            User user = list.get(i);
            StringBuffer temp=new StringBuffer(user.getHeadimg());
            temp.append(Constant.IMAGE_200);
            user.setHeadimg(temp.toString());
            list.remove(i);
            list.add(i,user);
        }
        if (isFirst) {
            adapter.setDatas(list);
        } else {
            if (list != null && list.size() > 0) {
                adapter.addDatas(list);
            }
        }
        mBinding.ptr.onFinishLoading();
    }

    @Override
    public void onGetRecommendListFail(String msg) {
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onGetNearByListSuccess(List<NearByUser> list, boolean isFirst, boolean hasMore) {

    }

    @Override
    public void onGetNearByListFail(String msg) {
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onGetHiListSuccess(List<User> list) {
        doSayHi(list);
    }

    SayHiDialog mSayHiDialog;
    User userIm;



    private void doSayHi(List<User> list) {
        if (list == null || !UserInfoManager.getManager(getActivity()).isLogin()) {

            return;
        }
        int userId = UserInfoManager.getManager(getActivity()).getUserId();
        boolean isHasSay = SharePreferenceUtils.getBoolean(getActivity(), SharePreferenceUtils.KEY_HAS_SAY_HI + userId, false);
        if (!isHasSay && list!=null && list.size()>=4) {
            String imagehead, image1, image2, image3;
            int index1, index2, index3, index4;
            index1 = 0;
            index2 = 1;
            index3 = 2;
            index4 = 3;
            imagehead = list.get(index1).getHeadimg();
            image1 = list.get(index2).getHeadimg();
            image2 = list.get(index3).getHeadimg();
            image3 = list.get(index4).getHeadimg();

            userIm = list.get(index1);

            if (mSayHiDialog == null) {
                mSayHiDialog = new SayHiDialog(getActivity(), null);
                mSayHiDialog.setBtnOkCallback(new SayHiDialog.iDialogCallback() {
                    @Override
                    public boolean onBtnClicked() {
                        SharePreferenceUtils.putBoolean(getActivity(), SharePreferenceUtils.KEY_HAS_SAY_HI + userId, true);
                        //打招呼
                        IMUtil.sendHellMessage(getActivity(), userIm);
                        //聊天
                        sendTextMessage(getString(R.string.say_hi_send_msg), userIm);

                        Toast.makeText(getActivity(), R.string.say_hi_send_su, Toast.LENGTH_SHORT).show();

                        return true;
                    }
                });
            }

            mSayHiDialog.show(imagehead, imagehead, image1, image2, image3);
        }

    }

    //send message
    protected void sendTextMessage(String content, User chatUserEntity) {
        boolean isRealUser = false;
        if (chatUserEntity.getUserType().equals(User.REALUSER)) {
            isRealUser = true;
        }
        ChatUserEntity entity = DemoHelper.getInstance().getChatUserEntity(isRealUser, chatUserEntity.getUserId());
        entity.setToAvator(chatUserEntity.getHeadimg());
        entity.setToUserID(chatUserEntity.getUserId());
        entity.setToNickName(chatUserEntity.getNickName());
        EMMessage message = EMMessage.createTxtSendMessage(content, entity
                .getToIdentify());

        message.setAttribute(EaseConstant.MESSAGE_ATTRIBUTE_USERID, chatUserEntity.getUserId());
        message.setAttribute(EaseConstant.EXTRA_USER_AVATOR, entity.getMyAvator());
        message.setAttribute(EaseConstant.EXTRA_USER_NICKNAME, entity.getMyNickName());
        message.setAttribute(EaseConstant.EXTRA_USER_POS, MessageLimitManager.instance(this
                .getActivity()).getAddress());

        sendMessage(message);
    }


    protected void sendMessage(final EMMessage message) {
        if (message == null) {
            return;
        }
        //send message
        EMClient.getInstance().chatManager().sendMessage(message);

    }


}
