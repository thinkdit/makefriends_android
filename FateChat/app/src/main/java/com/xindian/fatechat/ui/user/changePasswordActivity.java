package com.xindian.fatechat.ui.user;

import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.thinkdit.lib.util.StringUtil;
import com.xindian.fatechat.R;
import com.xindian.fatechat.databinding.ActivityChagePasswordBinding;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.ui.base.BaseActivity;
import com.xindian.fatechat.ui.login.presenter.AccountPresenter;
import com.hyphenate.easeui.utils.SnackbarUtil;

public class ChangePasswordActivity extends BaseActivity implements AccountPresenter.ChangePasswordListener {
    private static final int MINPASSWORDlENGTH=6;
private ActivityChagePasswordBinding mBinding;
    private AccountPresenter mPresenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding= DataBindingUtil.setContentView(this,R.layout.activity_chage_password);
        initToolBar();
        mPresenter=new AccountPresenter(this);
        mPresenter.setChangePasswordListener(this);
    }
    
    public void onClickChangePassword(View view)
    {
        if(checkData())
        {
            int userId = UserInfoManager.getManager(this).getUserId();
            mPresenter.requestChangePassword(userId,mBinding.editNowPassword.getText().toString().trim(),mBinding.editNewPassword.getText().toString().trim());
        }    
    }

    private boolean checkData() {
        if (StringUtil.isNullorEmpty(mBinding.editNowPassword.getText().toString().trim())){
            SnackbarUtil.makeSnackBar(mBinding.getRoot(),"当前密码不能为空!", Toast.LENGTH_SHORT).setMeessTextColor(Color.WHITE).show();
            return  false;
        }
        if (StringUtil.isNullorEmpty(mBinding.editNewPassword.getText().toString().trim())){
            SnackbarUtil.makeSnackBar(mBinding.getRoot(),"新密码不能为空!", Toast.LENGTH_SHORT).setMeessTextColor(Color.WHITE).show();
            return  false;
        }
        if (StringUtil.isNullorEmpty(mBinding.editConfirmPassword.getText().toString().trim())){
            SnackbarUtil.makeSnackBar(mBinding.getRoot(),"确认密码不能为空!", Toast.LENGTH_SHORT).setMeessTextColor(Color.WHITE).show();
            return  false;
        }
        if (!mBinding.editNewPassword.getText().toString().trim().equals(mBinding.editConfirmPassword.getText().toString().trim())){
            SnackbarUtil.makeSnackBar(mBinding.getRoot(),"新密码与确认密码不同，请确认!!", Toast.LENGTH_SHORT).setMeessTextColor(Color.WHITE).show();
            return  false;
        }
        if (mBinding.editNewPassword.getText().toString().trim().length()<MINPASSWORDlENGTH){
            SnackbarUtil.makeSnackBar(mBinding.getRoot(),"新密码长度不能小于"+MINPASSWORDlENGTH+"位!", Toast.LENGTH_SHORT).setMeessTextColor(Color.WHITE).show();
            return  false;
        }
        return  true;
    }


    @Override
    public void onChangPasswordSucceed() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void onChangPasswordError(String msg) {
        SnackbarUtil.makeSnackBar(mBinding.getRoot(),msg, Toast.LENGTH_SHORT).setMeessTextColor(Color.WHITE).show();
    }
}
