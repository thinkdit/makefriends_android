package com.xindian.fatechat.ui.user.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.hyphenate.easeui.domain.ChatUserEntity;
import com.thinkdit.lib.util.StringUtil;
import com.xindian.fatechat.DemoHelper;
import com.xindian.fatechat.R;
import com.xindian.fatechat.common.MVVPSetters;
import com.xindian.fatechat.manager.JumpManager;
import com.xindian.fatechat.ui.home.model.LikeUserBean;
import com.xindian.fatechat.ui.user.model.User;

/**
 * Created by hx_Alex on 2017/7/20.
 */

public class LikeDialog extends AlertDialog  {
    private TextView txt_like_hint;
    private ImageView img_headImg;
    private ImageView btn_talk;
    private ImageView btn_un_talk;
    private  LikeUserBean bean;
    private Context context;
    public LikeDialog(Context context, LikeUserBean bean) {
        super(context,R.style.FullScreen_dialog);
        this.context=context;
        this.bean=bean;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_like);
        txt_like_hint= (TextView) findViewById(R.id.txt_like_hint);
        img_headImg= (ImageView) findViewById(R.id.img_headImg);
        btn_talk= (ImageView) findViewById(R.id.btn_talk);
        btn_un_talk= (ImageView) findViewById(R.id.btn_un_talk);
        intiView();
        setCanceledOnTouchOutside(false);
        setCancelable(false);
    }

    private void intiView() {
        String userName= !StringUtil.isNullorEmpty(bean.getNickName())?bean.getNickName():"";
        txt_like_hint.setText(context.getString(R.string.like_hint,userName));
        MVVPSetters.imageLoader(img_headImg,bean.getHeadimg(),context.getResources().getDrawable(R.drawable.defalut_user));
    
     
        btn_un_talk.setOnClickListener(v->dismiss());
        btn_talk.setOnClickListener(v-> {
            boolean isRealUser=false;
            if(bean.getUserType().equals(User.REALUSER))
            {
                isRealUser=true;
            }
            ChatUserEntity entity = DemoHelper.getInstance().getChatUserEntity(isRealUser,bean.getOthersUserId());
            entity.setToAvator(bean.getHeadimg());
            entity.setToUserID(bean.getOthersUserId());
            entity.setToNickName(bean.getNickName());
            //判断是真实用户还是客服
            entity.setRealUser(isRealUser);
            JumpManager.gotoChat(context,entity);
            dismiss();
        });
    }

    @Override
    public void show() {
        super.show();
        this.getWindow().setWindowAnimations(R.style.popup_window_Animation);
    }
}
