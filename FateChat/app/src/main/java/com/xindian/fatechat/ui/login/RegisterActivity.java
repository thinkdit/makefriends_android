package com.xindian.fatechat.ui.login;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.View;

import com.hyphenate.chat.EMClient;
import com.hyphenate.easeui.utils.SnackbarUtil;
import com.thinkdit.lib.util.StringUtil;
import com.xindian.fatechat.DemoHelper;
import com.xindian.fatechat.R;
import com.xindian.fatechat.common.UrlConstant;
import com.xindian.fatechat.databinding.ActivityRegisterBinding;
import com.xindian.fatechat.ui.WebActivity;
import com.xindian.fatechat.ui.base.BaseActivity;
import com.xindian.fatechat.ui.home.fragment.MainActivity;
import com.xindian.fatechat.ui.user.data.UserData;
import com.xindian.fatechat.ui.user.model.User;
import com.xindian.fatechat.widget.MessageSelectionDialog;

import org.greenrobot.eventbus.EventBus;

public class RegisterActivity extends BaseActivity implements MessageSelectionDialog
        .onSaveListener {
    private final String MALE = "1";
    private final String FAMALE = "2";
    public static final String USERINFO = "USERINFO";
    private ActivityRegisterBinding mBinding;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_register);
        initToolBar();

    }

    public void onClickNext(View v) {
        if (checkData()) {
            user = new User();
            user.setNickName(mBinding.editUserName.getText().toString().trim());
            String txtAge = mBinding.txtAge.getText().toString();
            if(txtAge!=null && txtAge.contains("岁")) {
                user.setAge(Integer.parseInt(mBinding.txtAge.getText().toString().split("岁")[0]));
            }else {
                user.setAge(18);
            }
            user.setSex(mBinding.rgSex.getCheckedRadioButtonId() == mBinding.male.getId() ? MALE
                    : FAMALE);
//            if (!RulesManager.instance(this).isAllow(user.getSex())) {
//                SnackbarUtil.makeSnackBar(mBinding.getRoot(), "服务器异常", Snackbar.LENGTH_LONG)
//                        .setMeessTextColor(Color.WHITE).show();
//                return;
//            }
            Intent i = new Intent(this, PerfectInfoAcitivity.class);
            i.putExtra(USERINFO, user);
            startActivityForResult(i, 1000);
        }
    }

    public boolean checkData() {
        if (StringUtil.isNullorEmpty(mBinding.editUserName.getText().toString().trim())) {
            SnackbarUtil.makeSnackBar(mBinding.getRoot(), "昵称不能为空", Snackbar.LENGTH_LONG)
                    .setMeessTextColor(Color.WHITE).show();
            return false;
        }
        return true;
    }

    public void onClickSelectAge(View v) {
        MessageSelectionDialog dialog = new MessageSelectionDialog(this, UserData.getAge(),
                "选择年龄", this);
        dialog.show();
    }

    @Override
    public void onSave(String content, String content2) {
        mBinding.txtAge.setText(content);
    }

    public void onClickLicense(View view) {
        Intent intent = new Intent(this, WebActivity.class);
        intent.putExtra(WebActivity.JUMP_WEB_VIEW_URL, UrlConstant.URL_HELPCENTER);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (DemoHelper.getInstance().isLoggedIn()) {
                EMClient.getInstance().chatManager().loadAllConversations();
            }
            startActivity(new Intent(this, MainActivity.class));
            EventBus.getDefault().post(SplashActivity.LOGINSUCC);//发送消息通知引导页登陆成功，finsh掉自己
            finish();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
