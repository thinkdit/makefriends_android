package com.xindian.fatechat.widget;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.PopupWindow;

import com.hyphenate.easeui.domain.ChatUserEntity;
import com.xindian.fatechat.R;
import com.xindian.fatechat.ui.home.presenter.ReportPresenter;

import static com.switfpass.pay.MainApplication.getContext;

/**
 * 举报弹窗
 */

public class ReportPopupWindow extends PopupWindow  implements View.OnClickListener{
    private Context mContext;

    public void setmChatUserEntity(ChatUserEntity mChatUserEntity) {
        this.mChatUserEntity = mChatUserEntity;
    }

    private ChatUserEntity mChatUserEntity;
    public ReportPopupWindow(Context context, ChatUserEntity chatUserEntity){
        this.mContext = context;
        this.mChatUserEntity = chatUserEntity;
        init();
    }

    private void init(){
        setAnimationStyle(R.style.popup_show_Animation);
        // 设置弹出窗体的宽和高
        setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        // 设置弹出窗体可点击
        setFocusable(true);
        setBackgroundDrawable(new ColorDrawable(0x00000000));
        //解决华为手机window被navigationBar挡住
        setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        View rootView = View.inflate(mContext, R.layout.layout_share, null);
        rootView.findViewById(R.id.re_drink).setOnClickListener(this);
        rootView.findViewById(R.id.re_se).setOnClickListener(this);
        rootView.findViewById(R.id.re_ad).setOnClickListener(this);
        rootView.findViewById(R.id.re_money).setOnClickListener(this);
        rootView.findViewById(R.id.re_say).setOnClickListener(this);
        rootView.findViewById(R.id.re_cancel).setOnClickListener(this);
        setContentView(rootView);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.re_drink:
                report(mContext.getString(R.string.re_drink));
                dismiss();
                break;
            case R.id.re_se:
                report(mContext.getString(R.string.re_se));
                dismiss();
                break;
            case R.id.re_ad:
                report(mContext.getString(R.string.re_ad));
                dismiss();
                break;
            case R.id.re_money:
                report(mContext.getString(R.string.re_money));
                dismiss();
                break;
            case R.id.re_say:
                report(mContext.getString(R.string.re_say));
                dismiss();
                break;
            case R.id.re_cancel:
                dismiss();
                break;

        }
    }

    ReportPresenter mReportPresenter;
    private void report(String reportStr){
        if(mChatUserEntity==null || reportStr == null){
            return;
        }
        if(mReportPresenter == null){
            mReportPresenter = new ReportPresenter();
        }
        mReportPresenter.report(mChatUserEntity.getMyUserID()+"",mChatUserEntity.getMyNickName()
                ,mChatUserEntity.getToUserID()+"",mChatUserEntity.getToNickName(),mChatUserEntity.getToAvator(),reportStr);
    }
}
