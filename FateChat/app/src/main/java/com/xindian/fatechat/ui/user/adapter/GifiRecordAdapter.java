package com.xindian.fatechat.ui.user.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.xindian.fatechat.R;
import com.xindian.fatechat.databinding.ItemGiftRecordBinding;
import com.xindian.fatechat.ui.user.model.GiftRecordBean;

import java.util.List;

/**
 * Created by hx_Alex on 2017/7/14.
 */

public class GifiRecordAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater inflater;
    private List<GiftRecordBean> dataList;
    private ItemGiftRecordBinding mBinding;
    private boolean isReceiver;

    /***
     * 
     * @param context
     * @param dataList
     * @param isReceiver 此适配器是否为显示收到礼物list
     */
    public GifiRecordAdapter(Context context, List<GiftRecordBean> dataList,boolean isReceiver) {
        this.context = context;
        this.dataList = dataList;
        this.isReceiver=isReceiver;
        inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if(convertView==null)
        {
            mBinding= DataBindingUtil.inflate(inflater, R.layout.item_gift_record,parent,false);
            convertView=mBinding.getRoot();
            convertView.setTag(mBinding);
        }else 
        {
            mBinding= (ItemGiftRecordBinding) convertView.getTag();
        }
        mBinding.setGiftRecord(dataList.get(position));
        mBinding.setIsReceiver(isReceiver);
        return convertView;
    }


    public List<GiftRecordBean> getDataList() {
        return dataList;
    }

}
