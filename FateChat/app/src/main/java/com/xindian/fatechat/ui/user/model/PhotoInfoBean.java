package com.xindian.fatechat.ui.user.model;

import com.xindian.fatechat.common.BaseModel;

import java.util.Date;

/**
 * Created by hx_Alex on 2017/4/10.
 */

public class PhotoInfoBean extends BaseModel {

    /**
     * id : 0
     * userId : 99003
     * imageUrl : http://dongdong-prod.oss-cn-hangzhou.aliyuncs.com/1491650100087timg9.jpg
     * createdTime : null
     */

    private int id;
    private int userId;
    private String imageUrl;
    private Date createdTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }
}
