package com.xindian.fatechat.manager;

import android.content.Context;

import com.hyphenate.easeui.EaseConstant;
import com.thinkdit.lib.util.SharePreferenceUtils;
import com.xindian.fatechat.ui.home.model.RulesModel;
import com.xindian.fatechat.ui.home.presenter.RulePresenter;

/**
 */
public class RulesManager implements RulePresenter.IGetRules {
    private Context mContext;
    private static RulesManager instance;
    RulesModel mRuleMode;

    public static RulesManager instance(Context context) {
        if (instance == null) {
            instance = new RulesManager(context);

        }
        return instance;
    }

    private RulesManager(Context context) {
        mContext = context;
        new RulePresenter(this).getRules();
    }


    @Override
    public void onGetRulesSuccess(RulesModel model) {
        mRuleMode = model;
        int boyvalue = 1;
        int grilvalue = 1;

        if (mRuleMode == null || mRuleMode.getMessageLimitJson() == null) 
        {
          
        }else if(mRuleMode.getMessageLimitJson().getBoy()!=null && !mRuleMode.getMessageLimitJson().getBoy().equalsIgnoreCase("max"))
        {
            try {
                boyvalue = Integer.parseInt(mRuleMode.getMessageLimitJson().getBoy());
                
            } catch (Exception e) {

            }
        }
        
        if(mRuleMode == null || mRuleMode.getMessageLimitJson() == null){
            
        }else if(mRuleMode.getMessageLimitJson().getGirl()!=null && !mRuleMode.getMessageLimitJson().getGirl().equalsIgnoreCase("max"))
        {
            try {
                grilvalue = Integer.parseInt(mRuleMode.getMessageLimitJson().getGirl());
            } catch (Exception e) {

            }
        }
        
        SharePreferenceUtils.putInt(mContext, EaseConstant.MESSAGE_SEND_LIMIT_BOY, boyvalue);
        SharePreferenceUtils.putInt(mContext, EaseConstant.MESSAGE_SEND_LIMIT_GRIL, grilvalue);
        SharePreferenceUtils.putString(mContext, EaseConstant.LIMIT_COM_STATUS, model.getDynamicStatus());
    }

    public boolean isAllow(String sex) {
        if (mRuleMode == null) {
            return true;
        }
        if (mRuleMode.getRegister() == 4) {
            return false;
        }
        if (mRuleMode.getRegister() == 1 && sex.equalsIgnoreCase("1")) {
            return true;
        }
        if (mRuleMode.getRegister() == 2 && sex.equalsIgnoreCase("2")) {
            return true;
        }
        if (mRuleMode.getRegister() == 3) {
            return true;
        }
        return false;
    }
}
