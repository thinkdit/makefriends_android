package com.xindian.fatechat.ui.user.model;

import com.xindian.fatechat.common.BaseModel;

import java.util.ArrayList;

/**
 * Created by hx_Alex on 2017/4/10.
 */

public class MyPhotoBean extends BaseModel {

    /**
     * code : string
     * data : {}
     * message : string
     * requestId : string
     */

     private ArrayList<PhotoInfoBean> data;


   

    public ArrayList<PhotoInfoBean> getData() {
        return data;
    }

    public void setData(ArrayList<PhotoInfoBean> data) {
        this.data = data;
    }

 
    
}
