package com.xindian.fatechat.ui.home.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.hyphenate.easeui.domain.ChatUserEntity;
import com.hyphenate.easeui.domain.LocalMessage;
import com.hyphenate.easeui.domain.LocalSession;
import com.xindian.fatechat.DemoHelper;
import com.xindian.fatechat.R;
import com.xindian.fatechat.common.UrlConstant;
import com.xindian.fatechat.databinding.ItemSessionBinding;
import com.xindian.fatechat.manager.JumpManager;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.ui.WebActivity;
import com.xindian.fatechat.util.DateUtil;
import com.xindian.fatechat.util.GlideRoundTransform;
import com.yanzhenjie.recyclerview.swipe.SwipeMenuAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 */
public class SessionListAdapter extends SwipeMenuAdapter<SessionListAdapter.BindingViewHolder> {
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private boolean isShowCheckBox;
    private List<LocalSession> mDatas;
    private List<Integer> checkItemList;

    public SessionListAdapter(Context context) {
        mContext = context;
        mLayoutInflater = LayoutInflater.from(mContext);
        checkItemList = new ArrayList<>();
    }

    public void setShowCheckBox(boolean showCheckBox) {
        isShowCheckBox = showCheckBox;
        notifyDataSetChanged();
    }

    public List<Integer> getCheckItemList() {
        return checkItemList;
    }

    public void setDatas(List<LocalSession> datas) {
        mDatas = datas;
        notifyDataSetChanged();
    }

    public List<LocalSession> getmDatas() {
        return mDatas;
    }


    @Override
    public View onCreateContentView(ViewGroup parent, int viewType) {

        return mLayoutInflater.inflate(R.layout.item_session, parent, false);
    }

    @Override
    public BindingViewHolder onCompatCreateViewHolder(View realContentView, int viewType) {
        ViewGroup viewGroup = (ViewGroup) realContentView;
        ItemSessionBinding binding = DataBindingUtil.inflate(mLayoutInflater, R.layout
                .item_session, viewGroup, false);
        return new BindingViewHolder(realContentView);
    }


    @Override
    public void onBindViewHolder(BindingViewHolder holder, int position) {
        if (mDatas == null || mDatas.size() == 0) return;
        holder.setData(mDatas, position);
    }


    @Override
    public int getItemCount() {
        if (mDatas != null) {
            return mDatas.size();
        }
        return 0;
    }

    public class BindingViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        //        private final ItemSessionBinding binding=null;
        private RelativeLayout rl, ll;
        private ImageView iv_avatar;
        private TextView unread_msg_number, tv_nick, tv_content, tv_time;
        private CheckBox selectBox;

        public BindingViewHolder(ItemSessionBinding binding) {
            super(binding.getRoot());
//            this.binding = binding;
        }

        public BindingViewHolder(View bindingview) {
            super(bindingview);
            rl = (RelativeLayout) bindingview.findViewById(R.id.rl);
            ll = (RelativeLayout) bindingview.findViewById(R.id.ll);
            iv_avatar = (ImageView) bindingview.findViewById(R.id.iv_avatar);
            unread_msg_number = (TextView) bindingview.findViewById(R.id.unread_msg_number);
            tv_nick = (TextView) bindingview.findViewById(R.id.tv_nick);
            tv_content = (TextView) bindingview.findViewById(R.id.tv_content);
            tv_time = (TextView) bindingview.findViewById(R.id.tv_time);
            selectBox = (CheckBox) bindingview.findViewById(R.id.checkBox);
        }

        public void setData(List<LocalSession> data, int position) {
            if (mDatas.get(position).getUnReadCount() != 0) {
                unread_msg_number.setVisibility(View.VISIBLE);
                unread_msg_number.setText(String.valueOf(mDatas.get(position)
                        .getUnReadCount()));
            } else {
                unread_msg_number.setVisibility(View.GONE);
            }

            if (isShowCheckBox) {
                selectBox.setVisibility(View.VISIBLE);
            } else {
                selectBox.setVisibility(View.GONE);
            }

            selectBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        if (!checkItemList.contains(position)) {
                            checkItemList.add(position);
                        }
                    } else {
                        if (checkItemList.contains(position)) {
                            checkItemList.remove((Object)position);
                        }
                    }
                }
            });
            //解决list缓存机制
            if (selectBox.getVisibility() == View.VISIBLE) {
                if (checkItemList.contains(position)) {
                    selectBox.setChecked(true);
                } else {
                    selectBox.setChecked(false);
                }
            }

            rl.setOnClickListener(view -> {
                if (selectBox.getVisibility() == View.VISIBLE) return;//如果选择框显示的情况，点击item不做如何处理
                if (mDatas.get(position).getLastMsgType() != 4) {

                    ChatUserEntity entity = DemoHelper.getInstance().getChatUserEntity();
                    entity.setToAvator(mDatas.get(position).getAvatar());
                    entity.setToUserID(mDatas.get(position).getUserIdInt());
                    entity.setToNickName(mDatas.get(position).getNickName());
                    entity.setToIdentify(mDatas.get(position).getKeFu());//此会话是已经发生过聊天的列表，kefu字段代表来自于那个用户id，故直接设置当前kefu

                    //判断是真实用户还是客服
                    String kefu = mDatas.get(position).getKeFu();
                    if(kefu!=null && kefu.startsWith("customer")){
                        entity.setRealUser(false);
                    }else{
                        entity.setRealUser(true);
                    }

                    if (entity != null) {
                        JumpManager.gotoChat(mContext, entity);
                    }
                } else {
                    Intent intent = new Intent(mContext, WebActivity.class);
                    intent.putExtra(WebActivity.JUMP_WEB_VIEW_URL, UrlConstant.URL_CONSLUTIM + "?id=" +
                            "" + mDatas.get(position).getUserId() + "&time=" + mDatas.get(position).getLasttime() + "&sex=" +
                            UserInfoManager.getManager(mContext).getUserInfo().getSex());
                    mContext.startActivity(intent);
                }
            });

            SimpleDateFormat sd = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            tv_time.setText(DateUtil.getInterval(sd.format(new Date(mDatas.get(position).getLasttime()))));

            Glide.with(mContext).load(mDatas.get(position).getAvatar()).centerCrop().diskCacheStrategy
                    (DiskCacheStrategy.ALL).transform(new GlideRoundTransform(mContext, (int) mContext.getResources().getDimension(R.dimen.image_aspect_40dp)))
                    .into(iv_avatar);
            tv_nick.setText(mDatas.get(position).getNickName());

            if (mDatas.get(position).getLastMsgType() == LocalMessage.LOCAL_MESSAGE_IMAGE) {
                tv_content.setText("图片消息");
            } else if (mDatas.get(position).getLastMsgType() == LocalMessage.LOCAL_MESSAGE_VOICE) {
                tv_content.setText("语音消息");
            } else {
                String content = mDatas.get(position).getLastMsgContent();
                if(content != null){
                    if(content.startsWith("*$#")){
                        tv_content.setText("礼物消息");
                        tv_content.setVisibility(View.VISIBLE);
                    }else{
                        tv_content.setText(mDatas.get(position).getLastMsgContent());
                    }
                }
            }

        }

        @Override
        public void onClick(View v) {
        }
    }
}
