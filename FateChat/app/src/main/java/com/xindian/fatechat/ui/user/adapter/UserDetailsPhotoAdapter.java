package com.xindian.fatechat.ui.user.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.xindian.fatechat.R;
import com.xindian.fatechat.databinding.ItemUserDetailsPhotoBinding;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.ui.user.model.PhotoInfoBean;
import com.xindian.fatechat.ui.user.model.User;

import java.util.List;

/**
 * Created by hx_Alex on 2017/7/17.
 */

public class UserDetailsPhotoAdapter extends RecyclerView.Adapter<UserDetailsPhotoAdapter.UserDetailsPhotoHodler> {
    private List<PhotoInfoBean> imageUrlList;
    private Context context;
    private LayoutInflater inflater;
    private ClickImgPhotoListener clickImgPhotoListener;
    private int femmeUserId;
    public UserDetailsPhotoAdapter(Context context, List<PhotoInfoBean> imageUrlList,ClickImgPhotoListener clickImgPhotoListener) {
        this.context = context;
        this.imageUrlList = imageUrlList;
        this.clickImgPhotoListener=clickImgPhotoListener;
        inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public UserDetailsPhotoHodler onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemUserDetailsPhotoBinding mBinding= DataBindingUtil.inflate(inflater, R.layout.item_user_details_photo,parent,false);
        return new UserDetailsPhotoHodler(mBinding,clickImgPhotoListener);
    }

    @Override
    public void onBindViewHolder(UserDetailsPhotoHodler holder, int position) {
        User user= UserInfoManager.getManager(context).getUserInfo();
        ItemUserDetailsPhotoBinding mBinding = holder.getBinding();
        if (user != null) {
            mBinding.setIsVip(user.isVip());
            mBinding.setUrl(imageUrlList.get(position).getImageUrl());
        }

    }

    @Override
    public int getItemCount() {
        return imageUrlList.size();
    }






    class UserDetailsPhotoHodler extends RecyclerView.ViewHolder implements View.OnClickListener
    {

        private ItemUserDetailsPhotoBinding mBinding;
        private ClickImgPhotoListener clickImgPhotoListener;
        public UserDetailsPhotoHodler(ItemUserDetailsPhotoBinding mBinding,ClickImgPhotoListener clickImgPhotoListener) {
            super(mBinding.getRoot());
            this.mBinding=mBinding;
            this.clickImgPhotoListener=clickImgPhotoListener;
            mBinding.imgPhoto.setOnClickListener(this);
        }
        public ItemUserDetailsPhotoBinding getBinding() {
            return mBinding;
        }

        @Override
        public void onClick(View v) {
            clickImgPhotoListener.OnClickListener();
        }
    }

    public interface  ClickImgPhotoListener
    {
        void OnClickListener();
    }
}
