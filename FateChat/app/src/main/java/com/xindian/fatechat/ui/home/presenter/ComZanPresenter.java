package com.xindian.fatechat.ui.home.presenter;

import android.os.RemoteException;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.xindian.fatechat.common.BaseModel;
import com.xindian.fatechat.common.BasePresenter;
import com.xindian.fatechat.common.FateChatApplication;
import com.xindian.fatechat.common.ResultModel;

import org.json.JSONException;

import java.util.HashMap;

/**
 * 帖子赞，踩
 */
public class ComZanPresenter extends BasePresenter {
    public static final int TYPE_ZAN = 1;
    public static final int TYPE_CAI = 2;
    private final String URL_EDIT_DY = "/dynamic/dynamic_likes";

    public void editCom(String dynamicId,String userId,String token,int type) {
        HashMap<String, Object> data = new HashMap<>();
        data.put("dynamicId",dynamicId);
        data.put("type",type);
        data.put("userId",userId);
        data.put("token",token);
        post(getUrl(URL_EDIT_DY), data, null);
    }


    @Override
    public Object asyncExecute(String url, ResultModel resultModel) {
        if (url.contains(URL_EDIT_DY)) {
            return JSON.parseObject(resultModel.getData(), BaseModel.class);
        }
        return null;
    }

    @Override
    public void onError(String url, Exception e) {
        super.onError(url, e);
    }


    @Override
    public void onFailure(String url, ResultModel resultModel) {
        super.onFailure(url,resultModel);
        if (url.contains(URL_EDIT_DY)) {
            if(resultModel.getMessage()!=null){
                Toast.makeText(FateChatApplication.getInstance(), resultModel.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onSucceed(String url, ResultModel resultModel) throws JSONException,
            RemoteException {
        if (url.contains(URL_EDIT_DY)) {
            if(mOnComLisener !=null){
                mOnComLisener.onComEditSuccess();
            }

        }
    }

    private OnZanLisener mOnComLisener;

    public OnZanLisener getmOnComLisener() {
        return mOnComLisener;
    }

    public void setmOnComLisener(OnZanLisener mOnComLisener) {
        this.mOnComLisener = mOnComLisener;
    }

    public interface OnZanLisener {
        void onComEditSuccess();
    }

}
