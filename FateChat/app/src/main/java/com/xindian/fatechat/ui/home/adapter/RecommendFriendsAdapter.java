package com.xindian.fatechat.ui.home.adapter;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.thinkdit.lib.util.StringUtil;
import com.xindian.fatechat.R;
import com.xindian.fatechat.databinding.ItemRecommendFriendsBinding;
import com.xindian.fatechat.ui.user.UserDetailsActivity;
import com.xindian.fatechat.ui.user.model.User;

import java.util.ArrayList;
import java.util.List;


/**
 */
public class RecommendFriendsAdapter extends RecyclerView.Adapter<RecommendFriendsAdapter
        .BindingViewHolder> {
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private List<User> mDatas;

    public RecommendFriendsAdapter(Context context) {
        mContext = context;
        mLayoutInflater = LayoutInflater.from(mContext);
    }

    public void setDatas(List<User> datas) {
        mDatas = datas;
        notifyDataSetChanged();
    }

    public void addDatas(List<User> datas) {
        if (mDatas == null) {
            mDatas = new ArrayList<>();
        }
        int oldItem = 0;
        int newItem = 0;
        if(mDatas!=null){
            oldItem = mDatas.size();
        }
        if(datas!=null){
            newItem = datas.size();
        }
        mDatas.addAll(datas);
        notifyItemRangeChanged(oldItem,newItem);
    }

    @Override
    public BindingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemRecommendFriendsBinding binding = DataBindingUtil.inflate(mLayoutInflater, R.layout
                .item_recommend_friends, parent, false);
        return new BindingViewHolder(binding);
    }


    @Override
    public void onBindViewHolder(BindingViewHolder holder, int position) {
        if (mDatas == null) {
            return;
        }
        holder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mDatas.get(position) != null) {
                    UserDetailsActivity.gotoUserDetails(mContext, mDatas.get(position).getUserId
                            (), mDatas.get(position).getAddress());
                }
            }
        });

        holder.binding.setInfo(mDatas.get(position));
        User user = mDatas.get(position);
        if(user!=null && user.getUserType().equals("1") && user.getOnLineFlag()!=null){
            holder.binding.setIsOnline(user.getOnLineFlag().equals("1")?true:false);
        }else 
        {
            holder.binding.setIsOnline(true);
        }
    }

    @BindingAdapter({"mediaType"})
    public static void setTagImage(ImageView view, String mediaType) {
        if (StringUtil.isNullorEmpty(mediaType)) {
            return;
        }
    }

    @Override
    public int getItemCount() {
        if (mDatas != null) {
            return mDatas.size();
        }
        return 0;
    }

    public class BindingViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final ItemRecommendFriendsBinding binding;

        public BindingViewHolder(ItemRecommendFriendsBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        @Override
        public void onClick(View v) {
        }
    }
}
