package com.xindian.fatechat.widget;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.xindian.fatechat.R;

import org.greenrobot.eventbus.EventBus;


/**
 * 普通dialog的进度加载 事件不可以透传
 */
public class MyProgressDialog extends ProgressDialog {

    private ProgressBar mProgressBar;
    private TextView txt_content;
    private String content;
    public MyProgressDialog(Context context) {
        super(context, R.style.FullScreen_dialog);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_progress_dialog);
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.dimAmount = 0.1f;
        getWindow().setAttributes(lp);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        txt_content= (TextView) findViewById(R.id.txt_cotent);
        this.setCanceledOnTouchOutside(false);
    }

    
    public void setTextCotent(String content)
    {
        this.content=content;
    }
    @Override
    public void show() {
        if (!isShowing()) {
            super.show();
            if(content!=null){
                txt_content.setText(content);
            }
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
        EventBus.getDefault().unregister(this);
    }

}
