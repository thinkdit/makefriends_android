package com.xindian.fatechat.ui.user.presenter;

import android.content.Context;
import android.os.RemoteException;

import com.alibaba.fastjson.JSON;
import com.xindian.fatechat.common.BasePresenter;
import com.xindian.fatechat.common.ResultModel;
import com.xindian.fatechat.ui.user.adapter.GifiRecordAdapter;
import com.xindian.fatechat.ui.user.model.GiftRecordBean;
import com.xindian.fatechat.ui.user.model.GiftRecordPageBean;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by hx_Alex on 2017/4/20.
 */

public class GiftRecordPresenter extends BasePresenter {
    private final  static String TAGRECEIVER="receiverUserId";
    private final  static String TAGGIVER="giverUserId";
    private final String  URL_GIFTRECORD="/gift/record";
    private Context context;
    private GifiRecordAdapter receiverAdapter;
    private List<GiftRecordBean>  receiverList;
    private  GetGiftRecordWithReceiverListener receiverListener;
    private  GetGiftRecordWithGiverListener giverListener;

   

    public GiftRecordPresenter(Context context)
    {
        this.context=context;
        receiverList=new ArrayList<>();
    }


    /***
     * 请求收到礼物
     * @param receiverUserId
     * @param PageSize
     * @param Page
     */
    public void requestGetGiftRecordWithReceiver(int receiverUserId ,int PageSize,int Page )
    {
        HashMap<String,Object> data=new HashMap<>();
        data.put("receiverUserId",receiverUserId);
        data.put("PageSize",PageSize);
        data.put("Page",Page);
        get(getUrl(URL_GIFTRECORD),data,null);
     
    }

    /***
     * 请求接收礼物
     * @param PageSize
     * @param Page
     */
    public void requestGetGiftRecordWithGiver(int giverUserId ,int PageSize,int Page )
    {
        HashMap<String,Object> data=new HashMap<>();
        data.put("giverUserId",giverUserId);
        data.put("PageSize",PageSize);
        data.put("Page",Page);
        get(getUrl(URL_GIFTRECORD),data,null);

    }





    public void setReceiverListener(GetGiftRecordWithReceiverListener receiverListener) {
        this.receiverListener = receiverListener;
    }

    public void setGiverListener(GetGiftRecordWithGiverListener giverListener) {
        this.giverListener = giverListener;
    }


    @Override
    public Object asyncExecute(String url, ResultModel resultModel) {
        if(url.contains(URL_GIFTRECORD)) {
            return JSON.parseObject(resultModel.getData(),GiftRecordPageBean.class);
        }
        return  null;
    }

    @Override
    public void onError(String url, Exception e) {
        super.onError(url, e);
    }


    @Override
    public void onFailure(String url, ResultModel resultModel) {
        super.onFailure(url,resultModel);
        if(url.contains(URL_GIFTRECORD) && url.contains(TAGRECEIVER))
        {
         
            receiverListener.onGetGiftRecordWithReceivernError(resultModel.getMessage().equals("") || resultModel.getMessage() == null ? "未知错误" : resultModel.getMessage());
        }else if(url.contains(URL_GIFTRECORD) && url.contains(TAGGIVER))
        {

            giverListener.onGetGiftRecordWithGiverError(resultModel.getMessage().equals("") || resultModel.getMessage() == null ? "未知错误" : resultModel.getMessage());
        }
       
     
    }

    @Override
    public void onSucceed(String url, ResultModel resultModel) throws JSONException, RemoteException {
       //请求收到礼物
        if(url.contains(URL_GIFTRECORD) && url.contains(TAGRECEIVER))
        {
            GiftRecordPageBean giftRecordPageBean = (GiftRecordPageBean) resultModel.getDataModel();
            handleList(giftRecordPageBean,true);
            receiverListener.onGetGiftRecordWithReceiverSuccess(receiverAdapter,giftRecordPageBean.getNumber()>=giftRecordPageBean.getTotalPage());
        }else if(url.contains(URL_GIFTRECORD) && url.contains(TAGGIVER))
        {
            GiftRecordPageBean giftRecordPageBean = (GiftRecordPageBean) resultModel.getDataModel();
            handleList(giftRecordPageBean,false);
            giverListener.onGetGiftRecordWithGiverSuccess(receiverAdapter,giftRecordPageBean.getNumber()>=giftRecordPageBean.getTotalPage());
        }
  
     
    }
    
    
    private void handleList(GiftRecordPageBean bean,boolean isReceiver)
    {
        //首次加载数据
        if( receiverAdapter==null && bean.getNumber()==1)
        {
            
            receiverList=bean.getContent()==null?new ArrayList<>():bean.getContent();
            receiverAdapter=new GifiRecordAdapter(context,receiverList,isReceiver);
        }else 
        {
            receiverAdapter.getDataList().addAll(bean.getContent());
        }
    }
    
    
    public  interface GetGiftRecordWithReceiverListener
    {
        void onGetGiftRecordWithReceiverSuccess(GifiRecordAdapter receiverAdapter,boolean hasMore);
        void onGetGiftRecordWithReceivernError(String msg);
    }

    public  interface GetGiftRecordWithGiverListener
    {
        void onGetGiftRecordWithGiverSuccess(GifiRecordAdapter receiverAdapter,boolean hasMore);
        void onGetGiftRecordWithGiverError(String msg);
    }

}
