package com.xindian.fatechat.util;

import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.thinkdit.lib.base.IRequestCallback;
import com.thinkdit.lib.util.HttpUtils;
import com.thinkdit.lib.util.L;
import com.thinkdit.lib.util.StringUtil;
import com.xindian.fatechat.common.FateChatApplication;
import com.xindian.fatechat.common.ResultModel;
import com.xindian.fatechat.common.UrlConstant;
import com.xindian.fatechat.common.XTParamCommom;
import com.xindian.fatechat.manager.UserInfoManager;

import java.util.Map;
import java.util.Random;

import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * Created by qiuda on 16/6/7.
 */
public class XTHttpUtil extends HttpUtils<ResultModel> {
    private static final String TAG = "HttpUtils";

    private static XTHttpUtil XT_HTTP;
    private Random mRandom;
    public static final MediaType MediaType_JSON = MediaType.parse("application/json; charset=utf-8");
    public static final MediaType MediaType_urlencoded = MediaType.parse("application/x-www-form-urlencoded");
    private XTHttpUtil() {
        mRandom = new Random(System.currentTimeMillis());
    }

    public synchronized static XTHttpUtil getInstance() {
        if (XT_HTTP == null) {
            XT_HTTP = new XTHttpUtil();
        }
        return XT_HTTP;
    }


    /**
     * post 请求
     *
     * @param callBack      回调
     * @param url           url地址
     * @param singleRequest 是否单一请求
     * @param param         参数
     * @param tag           tag
     * @return 请求是否发送成功
     */
    public boolean doPost(IRequestCallback<ResultModel> callBack, String url, boolean singleRequest, Map<String, Object> param, Object tag) {
        XTParamCommom commomParam = new XTParamCommom();
        long ctm = System.currentTimeMillis();
        Map<String, Object> commomParamMap = commomParam.build();
        if (!singleRequest) {
            commomParamMap.put("random", mRandom.nextInt());
        }
        commomParamMap.put("t",ctm);
       String token = UserInfoManager.getManager(FateChatApplication.getInstance()).getToken();
        //将token带入param中
        if (!StringUtil.isNullorEmpty(token)) {
            param.put("token",token);
        }

        String body = JSON.toJSONString(param);
        L.d(TAG, "body=" + body);

        //注册接口添加
        if(url.contains("/auth/register")){
            //appVersion1.2.3&deviceId02%3A00%3A00%3A00%3A00%3A00&deviceModelPIC-AL00&deviceTypeMOBILE&osTypeANDROID&osVersion7.0&releaseChannelfreeChannel&uid-1&age18&height175&password54e014a2becd3cd65118f569dd93fadd&releaseChannelfreeChannel&sex1&t1233333333333MIICXsIBAaKbgQDGQuNkO1erkQEggW5i
            String toSignStr = "appVersion"+commomParamMap.get("appVersion")+"&deviceId"+commomParamMap.get("deviceId")
                    +"&deviceModel"+commomParamMap.get("deviceModel")+"&deviceType"+commomParamMap.get("deviceType")
                    +"&osType"+commomParamMap.get("osType")+"&osVersion"+commomParamMap.get("osVersion")
                    +"&releaseChannel"+commomParamMap.get("releaseChannel")+"&uid"+commomParamMap.get("uid")
                    +"&age"+param.get("age")+"&height"+param.get("height")
                    +"&password"+param.get("password")+"&releaseChannel"+commomParamMap.get("releaseChannel")
                    +"&sex"+param.get("sex")+"&t"+ctm+ UrlConstant.SECREKEY;
            Log.e("toSignStr",toSignStr);
            String sign = Digests.md5(toSignStr);
            commomParamMap.put("sign",sign);
            Log.e("md5",sign);
        }
//       String md5Str = SecretUtil.getSign(token, getValue(commomParamMap), body, UrlConstant.IS_DEBUG);
//        commomParamMap.put("sign", md5Str);
        String requestUrl = StringUtil.urlJoint(url, commomParamMap);
        RequestBody requestBody = RequestBody.create(MediaType_JSON, body);
        return post(callBack, requestUrl, requestBody, null, tag);
    }

    public boolean doPost(IRequestCallback<ResultModel> callBack, String url, Map<String, Object> param, boolean isNotJsonParam,boolean isNormalRespose, Object tag) {
        XTParamCommom commomParam = new XTParamCommom();
        Map<String, Object> commomParamMap = commomParam.build();
        if (isNotJsonParam) {
            if (param != null && param.size() > 0) {
                commomParamMap.putAll(param);
            }
        }

        String token = UserInfoManager.getManager(FateChatApplication.getInstance()).getToken();
        //将token带入param中
        if (!StringUtil.isNullorEmpty(token)) {
            param.put("token",token);
        }
        String requestUrl = StringUtil.urlJoint(url, commomParamMap);
        RequestBody requestBody = RequestBody.create(MediaType_urlencoded, "");
        return post(callBack, requestUrl, requestBody, null, tag,isNormalRespose);
    }

    /**
     * post 请求
     *
     * @param callBack 回调
     * @param url      url地址
     * @param param    参数
     * @param tag      tag
     * @return 请求是否发送成功
     */
    public boolean doPost(IRequestCallback<ResultModel> callBack, String url, Map<String, Object> param, Object tag) {
        return doPost(callBack, url, true, param, tag);
    }

    /**
     * get请求
     *
     * @param callBack      回调
     * @param url           url地址
     * @param singleRequest 是否单一请求
     * @param param         参数
     * @param tag           tag
     * @return 请求是否发送成功
     */
    public boolean doGet(IRequestCallback<ResultModel> callBack, String url, boolean singleRequest, Map<String, Object> param, Object tag) {
        XTParamCommom commomParam = new XTParamCommom();
        Map<String, Object> commomParamMap = commomParam.build();
        if (param != null && param.size() > 0) {
            commomParamMap.putAll(param);
        }
        if (!singleRequest) {
            commomParamMap.put("random", mRandom.nextInt());
        }
        String token = UserInfoManager.getManager(FateChatApplication.getInstance()).getToken();
        //将token带入param中
        if (!StringUtil.isNullorEmpty(token)) {
            param.put("token",token);
        }
//        String md5Str = SecretUtil.getSign(token, getValue(commomParamMap), null, UrlConstant.IS_DEBUG);
//        commomParamMap.put("sign", md5Str);
        String requestUrl = StringUtil.urlJoint(url, commomParamMap);
        L.d(TAG, "requestUrl=" + requestUrl);
        return get(callBack, requestUrl, null, tag);
    }

    /**
     * get 请求
     *
     * @param callBack 回调
     * @param url      url地址
     * @param param    参数
     * @param tag      tag
     * @return 请求是否发送成功
     */
    public boolean doGet(IRequestCallback<ResultModel> callBack, String url, Map<String, Object> param, Object tag) {
        return doGet(callBack, url, true, param, tag);
    }


    public static String getValue(Map<String, Object> param) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, Object> m : param.entrySet()) {
            if (!"token".equals(m.getKey())) {
                sb.append(m.getKey());
                sb.append(m.getValue());
            }
        }
        return sb.toString();
    }
}
