package com.xindian.fatechat.ui.user;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.xindian.fatechat.R;
import com.xindian.fatechat.databinding.ActivityPhotoDetailsBinding;
import com.xindian.fatechat.ui.base.BaseActivity;

public class PhotoDetailsActivity extends BaseActivity implements View.OnClickListener{
    public  static  final  String URL_IMG="url_img";
    public  static  final  String URL_ID="url_Id";
    public  static  final  String ISOTHERUSER="isOtherUser";
    public  static  final  int DELETE_IMAGE=0x100;
    private ActivityPhotoDetailsBinding mBinding;
    private int window_width,window_height,state_height;
    private  String imgUrl;
    private  int  imgId;
   
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding= DataBindingUtil.setContentView(this, R.layout.activity_photo_details);

        View view = initToolBar();
        if(!getIntent().getBooleanExtra(ISOTHERUSER,false)) {
            initToolBarWithRightMenu("删除", this);
            ((Button) view.findViewById(R.id.btn_toolbar_right)).setTextColor(getResources().getColor(R.color.colorAccent));
        }
        intiview();
    }

    private void intiview() {
         imgUrl = getIntent().getStringExtra(URL_IMG);
         imgId = getIntent().getIntExtra(URL_ID,-1);
        mBinding.setPhoto(imgUrl);

    }

    @Override
    public void onClick(View v) {
        setResult(RESULT_OK);
        finish();
    }
  


 
}
