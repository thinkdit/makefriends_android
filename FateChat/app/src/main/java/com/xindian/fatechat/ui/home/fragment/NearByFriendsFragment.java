package com.xindian.fatechat.ui.home.fragment;

import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.common.pullrefreshview.PullToRefreshView;
import com.hyphenate.easeui.utils.SnackbarUtil;
import com.xindian.fatechat.R;
import com.xindian.fatechat.common.Constant;
import com.xindian.fatechat.databinding.FragmentNearByBinding;
import com.xindian.fatechat.manager.SharePreferenceManager;
import com.xindian.fatechat.ui.base.BaseFragment_v4;
import com.xindian.fatechat.ui.home.adapter.NearByFriendsAdapter;
import com.xindian.fatechat.ui.home.presenter.RecommendPresenter;
import com.xindian.fatechat.ui.login.LoginDialog;
import com.xindian.fatechat.ui.user.model.NearByUser;
import com.xindian.fatechat.ui.user.model.User;
import com.xindian.fatechat.util.IMUtil;
import com.xindian.fatechat.widget.RecycleViewDivider;

import java.util.Arrays;
import java.util.List;


/**
 * Created by hx_Alex on 2017/3/24.
 * 首页Fragment
 */

public class NearByFriendsFragment extends BaseFragment_v4 implements RecommendPresenter
        .onRecommendList, LoginDialog.onLoginListener {
    private FragmentNearByBinding mBinding;
    private NearByFriendsAdapter adapter;
    private RecommendPresenter presenter;

    public static NearByFriendsFragment newInstance() {
        NearByFriendsFragment NearByFriendsFragment = new NearByFriendsFragment();
        Bundle bundle = new Bundle();
        NearByFriendsFragment.setArguments(bundle);
        return NearByFriendsFragment;
    }
    
    
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
            Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_near_by, container, false);
        initView();
        presenter = new RecommendPresenter(this.getActivity(), this);
        presenter.nearby_user_list(true);
        return mBinding.getRoot();
    }

    private void initView() {
        LinearLayoutManager staggeredGridLayoutManager = new LinearLayoutManager(this.getActivity
                ());
        mBinding.rvList.addItemDecoration(new RecycleViewDivider(this.getActivity(),
                LinearLayoutManager.HORIZONTAL, 1, getResources().getColor(R.color.divide_line)));
        //3.为recyclerView设置布局管理器
        mBinding.rvList.setLayoutManager(staggeredGridLayoutManager);
        adapter = new NearByFriendsAdapter(this.getActivity());
        mBinding.rvList.setAdapter(adapter);
        mBinding.ptr.setPullUpEnable(false);
        mBinding.ptr.setListener(new PullToRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.nearby_user_list(true);
            }

            @Override
            public void onLoadMore() {
                presenter.nearby_user_list(false);
            }
        });
        mBinding.tvQunliao.setOnClickListener(v -> {
            if (IMUtil.checkIsLogin(NearByFriendsFragment.this.getActivity())) {
                boolean chat = false;
                if (mList != null) {
                    for (int i = 0; i < mList.size(); i++) {
                        if (mList != null && mList.get(i) != null && !SharePreferenceManager
                                .getManager(NearByFriendsFragment.this.getActivity()).isChated
                                        (mList.get(i).getUserId())) {
                            IMUtil.sendHellMessage(NearByFriendsFragment.this.getActivity(),
                                    mList.get(i));
                            chat = true;
                        }
                    }
                    if (chat) {
                        SnackbarUtil.makeSnackBar(mBinding.getRoot(), "群聊打招呼成功", Toast
                                .LENGTH_SHORT).setMeessTextColor(Color.WHITE).show();
                        mBinding.ptr.onAutoRefresh();
                    } else {
                        SnackbarUtil.makeSnackBar(mBinding.getRoot(), "都已经群聊过了", Toast
                                .LENGTH_SHORT).setMeessTextColor(Color.WHITE).show();
                        mBinding.ptr.onAutoRefresh();
                    }
                }
            }
        });

    }

    @Override
    public void onGetRecommendListSuccess(List<User> list, boolean isFirst, boolean hasMore) {

    }

    @Override
    public void onGetRecommendListFail(String msg) {
        Toast.makeText(getContext(),msg,Toast.LENGTH_SHORT).show();
    }

    List<NearByUser> mList;
    int MAX_PEOPLE = 10;
    float[] numbers = new float[MAX_PEOPLE];

    private void generateDistance() {
        for (int i = 0; i < MAX_PEOPLE; i++) {
            numbers[i] = (float) Math.random() * 5;
        }
        Arrays.sort(numbers);
    }

    @Override
    public void onShowChange(boolean hide) {
        super.onShowChange(hide);
        if (presenter != null) presenter.nearby_user_list(true);
    }

    @Override
    public void onGetNearByListSuccess(List<NearByUser> list, boolean isFirst, boolean hasMore) {
        generateDistance();

        //对用户头像进行地址加参
        for(int i=0;i<list.size();i++){
            NearByUser user = list.get(i);
            StringBuffer temp=new StringBuffer(user.getHeadimg());
            temp.append(Constant.IMAGE_200);
            user.setHeadimg(temp.toString());
        }

        if (list != null) {
            for (int i = 0; i < MAX_PEOPLE && i < list.size(); i++) {
                list.get(i).setDistance(numbers[i]);
            }
        }
        if (isFirst) {
            adapter.setDatas(list);
        } else {
            if(list!=null && list.size()>0 && hasMore){
                adapter.addDatas(list);
            }
        }
        mList = list;
        mBinding.ptr.onFinishLoading();
    }

    @Override
    public void onGetNearByListFail(String msg) {
        Toast.makeText(getContext(),msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onGetHiListSuccess(List<User> list) {

    }

    @Override
    public void LoginSucceed(User user) {

    }

    @Override
    public void LoginError(String msg) {

    }
}
