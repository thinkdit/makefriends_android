package com.xindian.fatechat.ui.user;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;

import com.xindian.fatechat.R;
import com.xindian.fatechat.databinding.ActivityReceiveGiftBinding;
import com.xindian.fatechat.ui.base.BaseActivity;
import com.xindian.fatechat.ui.home.fragment.ReceiveGiftFragment;
import com.xindian.fatechat.ui.home.fragment.SendGiftFragment;

import java.util.Arrays;
import java.util.List;

import static com.xindian.fatechat.ui.home.fragment.HomeFragment.setIndicator;

public class ReceiveGiftActivity extends BaseActivity {
    private ActivityReceiveGiftBinding mBinding;
    private ReceiveGiftFragment receiveGiftFragment;
    private SendGiftFragment sendGiftFragment;
    private List<Fragment> fragmentList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding= DataBindingUtil.setContentView(this, R.layout.activity_receive_gift);
        initToolBar();
        intiViews();
    }

    private void intiViews() {
        intiViewPage();
        mBinding.tab.setupWithViewPager(mBinding.viewPager);
        mBinding.tab.getTabAt(0).setText("收到礼物");
        mBinding.tab.getTabAt(1).setText("送出礼物");
        /**
         * 设置指示器长度
         */
        mBinding.tab.post(new Runnable() {
            @Override
            public void run() {
                setIndicator(mBinding.tab, (int) getResources().getDimension(R.dimen.tab_padding),(int) getResources().getDimension(R.dimen.tab_padding));
            }
        });
    }

    private void intiViewPage() {
        receiveGiftFragment=new ReceiveGiftFragment();
        sendGiftFragment=new SendGiftFragment();
        fragmentList = Arrays.asList(receiveGiftFragment, sendGiftFragment);
        mBinding.viewPager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return fragmentList.get(position);
            }

            @Override
            public int getCount() {
                return fragmentList.size();
            }
        });
    }
}
