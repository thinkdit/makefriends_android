package com.xindian.fatechat.manager;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.hyphenate.EMCallBack;
import com.hyphenate.chat.EMClient;
import com.hyphenate.easeui.domain.ChatUserEntity;
import com.hyphenate.easeui.manager.MessageLimitManager;
import com.hyphenate.easeui.utils.DBHelper;
import com.thinkdit.lib.util.SharePreferenceUtils;
import com.thinkdit.lib.util.StringUtil;
import com.xindian.fatechat.DemoHelper;
import com.xindian.fatechat.ui.home.fragment.MainActivity;
import com.xindian.fatechat.ui.login.PerfectInfoAcitivity;
import com.xindian.fatechat.ui.login.SplashActivity;
import com.xindian.fatechat.ui.login.model.LoginBean;
import com.xindian.fatechat.ui.user.SetActivity;
import com.xindian.fatechat.ui.user.dialog.UpLoadHeadImgDialog;
import com.xindian.fatechat.ui.user.model.User;
import com.xindian.fatechat.ui.user.presenter.RequestUserInfoPresenter;
import com.xindian.fatechat.util.ActivityStack;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.NOTIFICATION_SERVICE;
import static com.hyphenate.easeui.EaseConstant.USER_SEX;

/**
 * Created by zjzhu on 28/6/16.
 * 用户信息管理类 所有用户信息更新保存通过此类操作
 */
public class UserInfoManager {
    public static final String KEY_USERID = "key_userId";
    public static final String KEY_PASSWORD = "key_password";
    public static final String KEY_ISSHOWEDUPLOADHEADIMG = "key_isShowedUpLoadHeadImg";
    private final String KEY_MOBILE = "key_mobile";
    private final String KEY_TOKEN = "key_token";
    private final String KEY_SEX = "key_sex";
    private final String KEY_IMID = "key_imId";
    private final String KEY_IMTOKEN = "key_imToken";
    private final String KEY_USER = "key_user";
    private final String KEY_AUTOLOGIN = "key_autologin";
    public static final String IMLOGINOUTSUCCESS="LoginOutSuccess";
    public static final String IMLOGINOUTERROR="Error";
    private static final String DEFALUT_USER_ICON="http://makefriends-dev.oss-cn-hangzhou.aliyuncs.com/1500001904419defalut_user.png";
    private final int LOWEST_HEAD_IMAGE_HEIGHT = 300;
    private final int LOWEST_HEAD_IMAGE_WIDTH = 300;
    private static UserInfoManager sUserInfoManager;
    private boolean islogin = false;
    private Context mContext;
    private static Activity mActivity;
    private User mUserInfo;
    private List<IUserInfoUpdateListener> mUpdateListenerList = new ArrayList<>();
    private RequestUserInfoPresenter mUserInfoPresenter;
    private UpLoadHeadImgDialog dialog;
    public static UserInfoManager getManager(Context context) {
        if(context instanceof  Activity)
        {
            mActivity=(Activity) context;
        }
        
        if (sUserInfoManager == null) {
            sUserInfoManager = new UserInfoManager(context.getApplicationContext());
        }
        return sUserInfoManager;
    }

    private UserInfoManager(Context context) {
        mContext = context;
        Object obj = SharePreferenceUtils.readObject(context, KEY_USER);
        if (obj != null) {
            setUserInfo((User) obj);
        }
    }

    /**
     * 设置登录结果并保存token等数据
     *
     * @param bean
     */
    public void setLoginResult(LoginBean bean) {
        if (bean != null && bean.getUserInfoVo() != null) {
            SharePreferenceUtils.putString(mContext, KEY_TOKEN, bean.getUserInfoVo().getToken());
            SharePreferenceUtils.saveObject(mContext, KEY_USER, bean.getUserInfoVo());
            SharePreferenceUtils.putInt(mContext, KEY_USERID, bean.getUserInfoVo().getUserId());
            SharePreferenceUtils.putString(mContext, KEY_PASSWORD, bean.getUserInfoVo()
                    .getPassword());
            SharePreferenceUtils.putString(mContext, KEY_SEX, bean.getUserInfoVo()
                    .getSex());
            SharePreferenceUtils.putBoolean(mContext, KEY_AUTOLOGIN, true);
            SharePreferenceUtils.putString(mContext, USER_SEX, bean.getUserInfoVo().getSex().equals("2")?"gril":"boy");
            islogin = true;
            addHeadInfoToUser(bean.getUserInfoVo(),false);
        }
    }

    public void loginOut(Context context) {
        EMClient.getInstance().logout(true, new EMCallBack() {

            @Override
            public void onSuccess() {
                //当im退出成功后在做app退出逻辑
                NotificationManager notificationManager = (NotificationManager) mContext.getSystemService
                        (NOTIFICATION_SERVICE);
                notificationManager.cancelAll();
                DBHelper.intance().reset();
                SharePreferenceUtils.removeBoolean(mContext, KEY_AUTOLOGIN);
                SharePreferenceUtils.removeString(mContext, KEY_TOKEN);
                LoginManager.instance(mContext).removeLoginListenerAll();
                islogin = false;
                if(context==null) {
                    EventBus.getDefault().post(IMLOGINOUTSUCCESS);
                } else 
                {
                    Intent i=new Intent(context, SplashActivity.class);
                    i.putExtra(SetActivity.TAG,SplashActivity.BYLOGINOUT);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(i);
                    EventBus.getDefault().post(MainActivity.ONLOGINOUTBYSET);//发送消息通知首页用户已经退出登录，finsh掉自己
                }
            }

            @Override
           public void onProgress(int progress, String status) {
                Log.e("imLoginOut",status+"-----"+progress);

            }

            @Override
            public void onError(int code, String message) {
                EventBus.getDefault().post(IMLOGINOUTERROR);
            }
        });
        mUserInfo = null;
    }

    /**
     * 任何地方请求刷新数据
     */
    public void refreshUserInfo() {
        getUserInfoPresenter().requestUserInfo();
    }

    private RequestUserInfoPresenter getUserInfoPresenter() {
        if (mUserInfoPresenter == null) {
            mUserInfoPresenter = new RequestUserInfoPresenter(mContext, new
                    RequestUserInfoPresenter.UserInfoUpdatedListener() {
                @Override
                public void onUserInfoUpdatedSucceed(User user) {
                    if (user == null) {
                        return;
                    }
                    addHeadInfoToUser(user,true);
                }
            });
        }
        return mUserInfoPresenter;
    }


    public void addHeadInfoToUser(User user,boolean ischeckUpLoad) {

        ChatUserEntity entity = new ChatUserEntity();
        entity.setMyAvator(user.getHeadimg());
        entity.setMyNickName(user.getNickName());
        entity.setMyUserID(user.getUserId());
        entity.setToIdentify(user.getCustomerId());
        entity.setVip(user.isVip());
        DemoHelper.getInstance().setChatUserEntity(entity);
        MessageLimitManager.instance(mContext).reset();
        if (mUserInfo != null && user.getImPassword() == null && mUserInfo.getImPassword() !=
                null) {
            user.setImPassword(mUserInfo.getImPassword());
        }
        cacheUserInfo(user,ischeckUpLoad);
    }

    private void cacheUserInfo(User user,boolean ischeckUpLoad) {
        setUserInfo(user);
        SharePreferenceUtils.saveObject(mContext, KEY_USER, user);
        if(ischeckUpLoad) {
            checkIsNeedUpLoadHeadImg(user);
        }
    }

    /**
     * 检查是否需要强制上传头像
     * @param user
     */
    private void checkIsNeedUpLoadHeadImg(User user)
    {
        boolean isShowed = SharePreferenceUtils.getBoolean(mContext, KEY_ISSHOWEDUPLOADHEADIMG,false);
       
        
        if(user.isVip()&& !isShowed &&  user.getHeadimg()!=null &&  user.getHeadimg().equals(DEFALUT_USER_ICON))
        {
            //弹出上传头像dialog
        if(ActivityStack.getInstance().getTopActivity()!=null) {
            
            if(dialog==null)
            {
                dialog= new UpLoadHeadImgDialog(ActivityStack.getInstance().getTopActivity());
            }
            dialog.setContext(ActivityStack.getInstance().getTopActivity());
            dialog.setUser(user);
            try {
                if(!dialog.isShowing())
                {
                    dialog.show();
                }
            }catch (Exception e)
            {
                e.printStackTrace();
                SharePreferenceUtils.putBoolean(mContext, KEY_ISSHOWEDUPLOADHEADIMG,false);
            }
        }
        }
     
    }

    //更新用户数据 并分发给所有listener
    private void setUserInfo(User userInfo) {
        mUserInfo = userInfo;
        ChatUserEntity entity = new ChatUserEntity();
        entity.setMyAvator(userInfo.getHeadimg());
        entity.setMyNickName(userInfo.getNickName());
        entity.setMyUserID(userInfo.getUserId());
        entity.setToIdentify(userInfo.getCustomerId());
        entity.setVip(userInfo.isVip());
        DemoHelper.getInstance().setChatUserEntity(entity);
        MessageLimitManager.instance(mContext).reset();
        dispatchAll();
    }

    public User getUserInfo() {
        //项目需求默认创建一个user
        if (mUserInfo == null) {
            mUserInfo = new User();
        }
        return mUserInfo;
    }

    /***
     * 判断是否登录了
     *
     * @return
     */
    public boolean isLogin() {
        boolean isAutoLogin = SharePreferenceUtils.getBoolean(mContext, KEY_AUTOLOGIN, false);
        int userId = SharePreferenceUtils.getInt(mContext, KEY_USERID, 0);
        if ((islogin || isAutoLogin) && userId != 0) {
            return true;
        }
        return false;
    }

    public String getToken() {
        return SharePreferenceUtils.getString(mContext, KEY_TOKEN);
    }

    public int getUserId() {
        return SharePreferenceUtils.getInt(mContext, KEY_USERID);
    }

    public String getPassword() {
        return SharePreferenceUtils.getString(mContext, KEY_PASSWORD);
    }

    public String getSex() {
        return SharePreferenceUtils.getString(mContext, KEY_SEX,"1");
    }

    public String getDeviceIdByRemto() {
        return SharePreferenceUtils.getString(mContext, PerfectInfoAcitivity.KEY_DEVICEID,null);
    }
    
    public String getImId() {
        return SharePreferenceUtils.getString(mContext, KEY_IMID);
    }

    public String getImToken() {
        return SharePreferenceUtils.getString(mContext, KEY_IMTOKEN);
    }

    public boolean isOneself(String uid) {
        if (!StringUtil.isNullorEmpty(uid) && uid.equals(getUserId())) {
            return true;
        }
        return false;
    }

    public void addUserInfoUpdateListener(IUserInfoUpdateListener listener) {
        if (!mUpdateListenerList.contains(listener)) {
            mUpdateListenerList.add(listener);
            listener.onUserInfoUpdated();
        }
    }

    public void removeUserInfoUpdateListener(IUserInfoUpdateListener listener) {
        if (mUpdateListenerList.contains(listener)) {
            mUpdateListenerList.remove(listener);
        }
    }

    private void dispatchAll() {
        for (IUserInfoUpdateListener listener : mUpdateListenerList) {
            listener.onUserInfoUpdated();
        }
    }


    public void setNewPassWordSharePreference(String newPassWord) {
        SharePreferenceUtils.putString(mContext, KEY_PASSWORD, newPassWord);
    }


    public interface IUserInfoUpdateListener {
        void onUserInfoUpdated();
    }

    /***
     * 获取上次登录成功的账号
     */
    public Map<String, String> GetLoginData() {
        HashMap<String, String> data = new HashMap<>();
        String loginId = String.valueOf(SharePreferenceUtils.getInt(mContext, KEY_USERID, -1));
        String password = SharePreferenceUtils.getString(mContext, KEY_PASSWORD, null);
        data.put(KEY_USERID, loginId);
        data.put(KEY_PASSWORD, password);
        return data;
    }
    

    

}
