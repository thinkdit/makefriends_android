package com.xindian.fatechat.ui.update;

import android.app.Activity;
import android.os.RemoteException;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.thinkdit.lib.util.SharePreferenceUtils;
import com.xindian.fatechat.R;
import com.xindian.fatechat.common.BasePresenter;
import com.xindian.fatechat.common.ResultModel;

import org.json.JSONException;

import java.util.HashMap;

/**
 * 系统参数接口
 */
public class SystemParamsPresenter extends BasePresenter {
    public static final String KEY_INGNORED_VERSION = "update_ingnored_version";
    private final String URL_APP_VERSION = "/version/latest-version";
    private Activity mContext;
    private boolean mCheckIngnored;

    /**
     * @param context
     * @param checkIngnored 检查是不是忽略此版本 在设置中不需要检查
     */
    public SystemParamsPresenter(Activity context, boolean checkIngnored) {
        mContext = context;
        mCheckIngnored = checkIngnored;
    }

    public void requestAppVersion() {
        get(getUrl(URL_APP_VERSION), new HashMap<>(), mContext);
    }

    @Override
    public Object asyncExecute(String url, ResultModel resultModel) {
        if (url.contains(URL_APP_VERSION)) {
            return JSON.parseObject(resultModel.getData(), AppVersionInfo.class);
        }
        return super.asyncExecute(url, resultModel);
    }

    @Override
    public void onSucceed(String url, ResultModel resultModel) throws JSONException, RemoteException {
        super.onSucceed(url, resultModel);
        if (url.contains(URL_APP_VERSION)) {
            AppVersionInfo info = (AppVersionInfo) resultModel.getDataModel();
            //有新版本 而且没忽略则弹出升级提示
            if (info.isHaveUpgrade() &&
                    (info.isForceUpgrade() || !mCheckIngnored || !info.getUpgradeVersionName().equals(
                            SharePreferenceUtils.getString(mContext, KEY_INGNORED_VERSION)))) {
                new UpdateDialog(mContext, info).show();
            } else {
                if (!mCheckIngnored) {
                    Toast.makeText(mContext, R.string.update_latest_version, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }


    @Override
    public void onFailure(String url, ResultModel resultModel) {
        super.onFailure(url, resultModel);
        if (!mCheckIngnored) {
            Toast.makeText(mContext, resultModel.getMessage(), Toast.LENGTH_LONG).show();
        }
    }
}
