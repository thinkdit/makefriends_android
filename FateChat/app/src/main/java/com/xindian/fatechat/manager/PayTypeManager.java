package com.xindian.fatechat.manager;

/**
 * Created by hx_Alex on 2017/6/9.
 */

public class PayTypeManager {
    public static final Integer PAY_WX=1;
    public static final Integer PAY_ZFB=2;
    private static  PayTypeManager payTypeManager;
    private WXPayChannel wxPayChannel;
    private ZFBPayChannel zfbPayChannel;
    private PayTypeManager(){}
    
    public  static synchronized PayTypeManager getInstance(){
        if(payTypeManager==null){
            payTypeManager=new PayTypeManager();
        }
        return payTypeManager;
    }

    public WXPayChannel getWxPayChannel() {
        return wxPayChannel;
    }

    public void setWxPayChannel(WXPayChannel wxPayChannel) {
        this.wxPayChannel = wxPayChannel;
    }

    public ZFBPayChannel getZfbPayChannel() {
        return zfbPayChannel;
    }

    public void setZfbPayChannel(ZFBPayChannel zfbPayChannel) {
        this.zfbPayChannel = zfbPayChannel;
    }

    public enum WXPayChannel
    {
        WFT,DX
    }
    public enum ZFBPayChannel
    {
        ZFB
    }
}
