package com.xindian.fatechat.ui.home.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.util.ArrayMap;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.xindian.fatechat.R;
import com.xindian.fatechat.common.FateChatApplication;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.ui.home.ComprehensiveCommentActivity;
import com.xindian.fatechat.ui.home.model.JHTuiJianBean;
import com.xindian.fatechat.ui.home.presenter.ComZanPresenter;
import com.xindian.fatechat.ui.user.UserDetailsActivity;
import com.xindian.fatechat.util.DisplayUtil;
import com.xindian.fatechat.util.GlideRoundTransform;
import com.xindian.fatechat.util.GlideUtil;
import com.xindian.fatechat.widget.player.VideoPlayerHelper;

import java.util.List;

import static com.switfpass.pay.MainApplication.getContext;

/**
 * Created by qf on 2016/11/3.
 */
public class ComprehensiveAdapter extends BaseAdapter {
    private final LayoutInflater inflater;
    Context context;
    List<JHTuiJianBean> data;
    public static final String TAG = "testv";

    private boolean isShowStadus = false;

    public void setShowStadus(boolean showStadus) {
        isShowStadus = showStadus;
    }

    public void addData(List<JHTuiJianBean> data) {
        if(this.data == null){
            this.data = data;
        }else{
            this.data.addAll(data);
        }

        notifyDataSetChanged();
    }

    public void setData(List<JHTuiJianBean> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public ComprehensiveAdapter(Context context, List<JHTuiJianBean> data) {
        this.context = context;
        this.data = data;
        inflater = LayoutInflater.from(context);
    }


    @Override
    public int getCount() {
        if(data!=null){
            return data.size();
        }else{
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.comprehensive_item_layout, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        final JHTuiJianBean bean = data.get(position);
        final int type = bean.getType();
        setingShowType(holder, type);//根据类型显示对应的布局
        setBaseData(holder, bean, type);//获取基本信息（用户名，正文，评论，赞等）
        setingItemThumb(holder, bean, type,position);//加载内容布局的界面
        return convertView;

    }


    ComZanPresenter mComZanPresenter;
    //赞或踩
    public void editComZan(JHTuiJianBean bean, int type) {
        if(mComZanPresenter == null){
            mComZanPresenter = new ComZanPresenter();
            mComZanPresenter.setmOnComLisener(new ComZanPresenter.OnZanLisener() {
                @Override
                public void onComEditSuccess() {
                    Toast.makeText(FateChatApplication.getInstance(), "提交成功", Toast.LENGTH_SHORT).show();
                }
            });
        }
        long userId = UserInfoManager.getManager(context).getUserId();
        String token = UserInfoManager.getManager(context).getToken();
        mComZanPresenter.editCom(bean.getId()+"",userId+"",token,type);
    }

    //跳到评论页面
    private void turnToCommentActivity(JHTuiJianBean bean, int type) {
        //TODO 评论页面
        Intent intent= new Intent(context, ComprehensiveCommentActivity.class);
        intent.putExtra("bean",bean);
        intent.putExtra("type",type);
        context.startActivity(intent);
    }

    //点击surfaceview事件
    private void setSurfaceViewClickListener(int position,final Holder holder) {
        holder.svVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VideoPlayerHelper.getInstance().play((ViewGroup) holder.videoLevelC, data.get(position).getSourceUrl(), position);
            }
        });
    }
    //点击播放按钮事件
    private void setVideoPlayClickListener(final int position, Holder holder) {
        holder.ivPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VideoPlayerHelper.getInstance().play((ViewGroup) holder.videoLevelC, data.get(position).getSourceUrl(), position);
            }
        });
    }
    //根据类型加载内容布局的界面和对应设置
    private void setingItemThumb(Holder holder, JHTuiJianBean bean, int type,int position) {
        String thumbUrl;
        if(type == JHTuiJianBean.TYPE_IMAGE){
            thumbUrl=bean.getSourceUrl();

            if (sizeMap.containsKey(thumbUrl) && !sizeMap.get(thumbUrl).isNull()) {
            /*当图片大小数据已得到,先改变item大小,后加载图片*/
                setItemSize(sizeMap.get(thumbUrl), holder.ivImageShow);
//                GlideUtil.beginAsBitmap(context, thumbUrl, null, null)
//                        .placeholder(R.drawable.icon_photo_empty)
//                        .error(R.drawable.icon_photo_error)
//                        .into(holder.ivImageShow);
                Glide.with(holder.ivImageShow.getContext()).load(thumbUrl).diskCacheStrategy(DiskCacheStrategy.ALL)
//                        .placeholder(R.drawable.icon_photo_empty)
//                        .error(R.drawable.icon_photo_error)
                        .fitCenter()
                        .into(holder.ivImageShow);
            } else {
             /*当图片大小数据没得到,通过target回调,根据图片大小改变item大小*/
                GlideUtil.beginAsBitmap(context, thumbUrl, null, null)
                        .placeholder(R.drawable.icon_photo_empty)
                        .error(R.drawable.icon_photo_error)
                        .into(new ImageViewTarget(holder.ivImageShow, thumbUrl));
            }

        }else if(type == JHTuiJianBean.TYPE_VEDIO){
            //TODO 封面
            thumbUrl="";
            Log.i("BG_Image","thumbUrl-->"+thumbUrl+"---position--->"+position);
            Glide.with(holder.ivThumb.getContext()).load(thumbUrl).diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.ivThumb);
            setVideoPlayClickListener(position, holder);
            setSurfaceViewClickListener(position,holder);
        }

    }

    //获取基本信息（用户名，正文，评论，赞等）
    private void setBaseData(Holder holder, JHTuiJianBean bean,final int type) {
        //获取基本信息（用户名，正文，评论，赞等）
        String headUri = bean.getHeadUrl();
        Glide.with(holder.sdvUserHead.getContext()).load(headUri)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .transform(new GlideRoundTransform(holder.sdvUserHead.getContext(), (int)context.getResources().getDimension(R.dimen.aspect_40dp)))
                .into(holder.sdvUserHead);
        holder.tvUserName.setText(bean.getNickName());
        holder.tvDetil.setText(Html.fromHtml(bean.getDynamicContent()));//设置详细内容
        holder.cbZan.setText(bean.getDynamicLike()+"");//赞
        holder.tvPinglun.setText(bean.getCommentNum()+"");//评论
        holder.isV.setVisibility(View.VISIBLE);
        holder.isV.setText("Lv"+bean.getUserGrade());
        holder.tvUnLike.setText(bean.getDynamicDislike()+"");

        holder.cbZan.setChecked(bean.isDlike());
        holder.tvUnLikeIcon.setSelected(bean.isNoLike());

        if(isShowStadus){
            holder.tvStadus.setVisibility(View.VISIBLE);
            int stadus = bean.getState();
            if(stadus == JHTuiJianBean.STADUS_WAIT){
                holder.tvStadus.setText("待审核");
            }
            if(stadus == JHTuiJianBean.STADUS_SU){
                holder.tvStadus.setText("发布成功");
            }
            if(stadus == JHTuiJianBean.STADUS_NO){
                holder.tvStadus.setText("审核不通过");
            }
            if(stadus == JHTuiJianBean.STADUS_DEL){
                holder.tvStadus.setText("已删除");
            }
        }else{
            holder.tvStadus.setVisibility(View.GONE);
        }

        //分享 TODO
        holder.imShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        //头像
        holder.sdvUserHead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sex = UserInfoManager.getManager(getContext()).getSex();
                String sexOther = bean.getSex();

                if(sex!=null && sexOther!=null && !sex.equals(sexOther)){//异性
                    goToUserDetail(context,(int)bean.getUserId());
                }else{
                    Toast.makeText(context,R.string.only_sex_look,Toast.LENGTH_SHORT).show();
                }

            }
        });

        //踩
        holder.tvUnlikeLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bean.setNoLike(!bean.isNoLike());
                boolean isLike = bean.isNoLike();
                int countLike = bean.getDynamicDislike();
                holder.tvUnLikeIcon.setSelected(isLike);
                if(isLike){
                    countLike++;
                }else{
                    countLike--;
                }
                bean.setDynamicDislike(countLike);
                holder.tvUnLike.setText(countLike+"");

                editComZan(bean,ComZanPresenter.TYPE_CAI);
            }
        });

        //赞
         holder.cbZan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bean.setDlike(!bean.isDlike());
                boolean isLike = bean.isDlike();
                int countLike = bean.getDynamicLike();
                holder.cbZan.setChecked(isLike);
                if(isLike){
                    countLike++;
                }else{
                    countLike--;
                }
                bean.setDynamicLike(countLike);
                holder.cbZan.setText(countLike+"");

                editComZan(bean,ComZanPresenter.TYPE_ZAN);
            }
        });

        holder.llRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                turnToCommentActivity(bean,type);
            }
        });
    }


    private void goToUserDetail(Context context, int userID) {
        UserDetailsActivity.gotoUserDetails(context, userID, null);
    }

    //根据类型显示对应的布局
    private void setingShowType(Holder holder, int type) {
        //根据类型显示对应的布局
        holder.tvLineWenzi.setVisibility(View.INVISIBLE);
        if(type == JHTuiJianBean.TYPE_TEXT){
            holder.levelC.setVisibility(View.GONE);
            holder.tvLineWenzi.setVisibility(View.VISIBLE);
        }else if(type == JHTuiJianBean.TYPE_VEDIO){
            holder.levelC.setVisibility(View.VISIBLE);
            holder.videoLevelC.setVisibility(View.VISIBLE);
            holder.audioLevelC.setVisibility(View.GONE);
            holder.imageLevelC.setVisibility(View.GONE);
        }else if(type == JHTuiJianBean.TYPE_IMAGE){
            holder.levelC.setVisibility(View.VISIBLE);
            holder.imageLevelC.setVisibility(View.VISIBLE);
            holder.audioLevelC.setVisibility(View.GONE);
            holder.videoLevelC.setVisibility(View.GONE);
        }
    }

    /**
     * 存放加载过的图片大小数据
     * <url，size>
     */
    private ArrayMap<String, ItemSize> sizeMap = new ArrayMap<>();

    /**
     * 图片异步加载回调类.在此类中设置item大小
     */
    private class ImageViewTarget extends BitmapImageViewTarget {
        private ImageView imageView;
        private String url;

        public ImageViewTarget(ImageView imageView, String url) {
            super(imageView);
            this.imageView = imageView;
            this.url = url;
        }

        @Override
        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
            //1.获取imageview的应有大小
            int viewWidth = imageView.getWidth();
            float scale = resource.getWidth() / (viewWidth * 1.0f);
            int viewHeight = (int) (resource.getHeight() / scale);

            int maxHeight = 400* DisplayUtil.dip2px(FateChatApplication.getInstance(),400);
            if(viewHeight>maxHeight){
                viewHeight = maxHeight;
            }

            //2.创建大小实体类,并为view设置大小
            ItemSize sm = new ItemSize(viewWidth, viewHeight);
            setItemSize(sm, imageView);

            //3.将获取到的大小数据,放入map中。之后item复用时，就提前设置view大小。
            sizeMap.put(url, sm);

            super.onResourceReady(resource, glideAnimation);

            Glide.with(context).load(url).diskCacheStrategy(DiskCacheStrategy.ALL)
                    .fitCenter()
                    .into(imageView);
        }
    }

    /**
     * 设置item大小具体方法
     *
     * @param size
     */
    private void setItemSize(ItemSize size, View view) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.width = size.getWidth();
        layoutParams.height = size.getHeight();
        view.setLayoutParams(layoutParams);
    }

    /**
     * item大小实体类
     */
    public class ItemSize {

        private int height;
        private int width;

        public ItemSize(int width, int height) {
            this.height = height;
            this.width = width;
        }

        public int getHeight() {
            return height;
        }

        public void setHeight(int height) {
            this.height = height;
        }

        public int getWidth() {
            return width;
        }

        public boolean isNull() {
            if (height <= 0 || width <= 0) {
                return true;
            } else {
                return false;
            }
        }

    }

    class Holder {
        ImageView sdvUserHead;
        TextView isV;
        TextView tvUserName;
        ImageView imShare;
        TextView tvStadus;
        RelativeLayout LevelA;
        TextView tvDetil;
        ImageView ivBg;
        ImageView svVideo;
        ImageView ivThumb;
        ImageView ivPlay;
        RelativeLayout videoLevelC;
        ImageView ivImageShow;
        RelativeLayout imageLevelC;
        ImageView ivBackground;
        TextView tvPlayCount;
        TextView tvVoiceSumtime;
        RelativeLayout audioBody;
        View levelE;
        TextView tvPlayOrPause;
        SeekBar voiceSeekbar;
        TextView tvCurrentTime;
        TextView tvTotalTime;
        RelativeLayout rlVoiceProgress;
        TextView tvPlayVoice;
        RelativeLayout audioLevelC;
        LinearLayout levelC;
        CheckBox cbZan;
        TextView tvPinglun;
        View tvPinglunLay;
        LinearLayout LevelD;
        LinearLayout tvUnlikeLay;
        TextView tvUnLike;
        TextView tvUnLikeIcon;
        View tvLineWenzi;
        RelativeLayout llRoot;

        Holder(View view) {
            sdvUserHead = (ImageView)view.findViewById(R.id.sdvUserHead);
            isV = (TextView) view.findViewById(R.id.isV);
            tvUserName = (TextView) view.findViewById(R.id.tvUserName);
            imShare = (ImageView) view.findViewById(R.id.imShare);
            LevelA = (RelativeLayout) view.findViewById(R.id.LevelA);
            tvDetil = (TextView) view.findViewById(R.id.tvDetil);
            tvStadus = (TextView) view.findViewById(R.id.tvStadus);
            ivBg = (ImageView) view.findViewById(R.id.ivBg);
            svVideo = (ImageView) view.findViewById(R.id.svVideo);
            ivThumb = (ImageView) view.findViewById(R.id.ivThumb);
            ivPlay = (ImageView) view.findViewById(R.id.ivPlay);
            videoLevelC = (RelativeLayout) view.findViewById(R.id.videoLevelC);
            ivImageShow = (ImageView) view.findViewById(R.id.ivImageShow);
            imageLevelC = (RelativeLayout) view.findViewById(R.id.imageLevelC);
            ivBackground = (ImageView) view.findViewById(R.id.ivBackground);
            tvPlayCount = (TextView) view.findViewById(R.id.tvPlayCount);
            tvVoiceSumtime = (TextView) view.findViewById(R.id.tvVoiceSumtime);
            audioBody = (RelativeLayout) view.findViewById(R.id.audioBody);
            levelE =  view.findViewById(R.id.levelE);
            tvPlayOrPause = (TextView) view.findViewById(R.id.tvPlayOrPause);
            voiceSeekbar = (SeekBar) view.findViewById(R.id.voiceSeekbar);
            tvCurrentTime = (TextView) view.findViewById(R.id.tvCurrentTime);
            tvTotalTime = (TextView) view.findViewById(R.id.tvTotalTime);
            rlVoiceProgress = (RelativeLayout) view.findViewById(R.id.rlVoiceProgress);
            tvPlayVoice = (TextView) view.findViewById(R.id.tvPlayVoice);
            audioLevelC = (RelativeLayout) view.findViewById(R.id.audioLevelC);
            levelC = (LinearLayout) view.findViewById(R.id.levelC);
            LevelD =  (LinearLayout) view.findViewById(R.id.LevelD);
            cbZan =  (CheckBox) view.findViewById(R.id.cbZan);
            tvLineWenzi =  view.findViewById(R.id.line_wenzi);
            tvUnLike =  (TextView) view.findViewById(R.id.tvUnLike);
            tvUnLikeIcon =  (TextView) view.findViewById(R.id.tvUnLikeIcon);
            tvPinglun =  (TextView) view.findViewById(R.id.tvPinglun);
            tvUnlikeLay =  (LinearLayout) view.findViewById(R.id.tvUnlikeLay);
            tvPinglunLay =   view.findViewById(R.id.tvPinglunLay);
            llRoot =  (RelativeLayout) view.findViewById(R.id.llRoot);
        }
    }

}
