package com.xindian.fatechat.pay;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.hyphenate.easeui.model.EventBusModel;
import com.hyphenate.easeui.utils.SnackbarUtil;
import com.jyd.paydemo.Bean;
import com.jyd.paydemo.XianlaiWxH5Pay;
import com.thinkdit.lib.util.DeviceInfoUtil;
import com.xindian.fatechat.R;
import com.xindian.fatechat.databinding.LayoutSelectPayBinding;
import com.xindian.fatechat.manager.PayTypeManager;
import com.xindian.fatechat.manager.UmengEventManager;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.pay.alipay.AilPayPresenter;
import com.xindian.fatechat.pay.model.PrePayInfoBean;
import com.xindian.fatechat.pay.model.ProductBean;
import com.xindian.fatechat.pay.model.XianLaiWxQueryBean;
import com.xindian.fatechat.pay.presenter.PayPresenter;
import com.xindian.fatechat.pay.wxpay.WxReportPrePayInfoReceiver;
import com.xindian.fatechat.pay.wxpay.WxpayPresenter;
import com.xindian.fatechat.wxapi.WXPayEntryActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Timer;
import java.util.TimerTask;


/**
 * Created by hx_Alex on 2017/4/19.
 */

public class SelectPayWindow implements  PayPresenter.onPrePayInfoListener,AilPayPresenter.OnAilPayListener,UserInfoManager.IUserInfoUpdateListener,
        PayPresenter.onPrePayInfoByXianLaiListener,PayPresenter.onPayInfoByXianLaiWxListener{
    public static final String TAG = "SelectPayWindow";
    public static final String XIANLAI_WX_PAY_CALLBACK_SELELCT = "XIANLAI_WX_PAY_CALLBACK_SELELCT";//闲来微信支付离开微信页面返回app回调查询
    private static final String XIANLAI="xianlaijiaoyou";
    private Activity mActivity;
    private Fragment mFragment;
    private PopupWindow mPopupWindow;
    private IWindowChangeListener mIWindowChangeListener;
    private IFinishListener mFinishListener;
    private LayoutInflater inflater;
    private LayoutSelectPayBinding mBinding;
    private LinearLayout pay_zhifubao;
    private LinearLayout pay_wx;
    private int[] nowSelectPayKinds = {-1};
    private final int[] tempNk = {-1};
    public IFinishListener getmFinishListener() {
        return mFinishListener;
    }
    private ProductBean bean;
    private PrePayInfoBean tempPrePayInfoBean;
    private PayPresenter mPresenter;
    private  AilPayPresenter payPresenter;
    private WxpayPresenter wxpayPresenter;
    private  PayTypeManager payTypeManager;

    private   Bean mBean;//闲来微信支付数据bean
    private int payType;//当前支付方式 1->微信 2->支付宝

    int color = R.color.red;

    private void init() {
        mPopupWindow = new PopupWindow(getContext());
        payTypeManager=PayTypeManager.getInstance();
        mPopupWindow.setAnimationStyle(R.style.popup_window_Animation);
        // 设置弹出窗体的宽和高
        mPopupWindow.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        mPopupWindow.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
        // 设置弹出窗体可点击
        mPopupWindow.setFocusable(true);
        mPopupWindow.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ff0000")));
        //解决华为手机window被navigationBar挡住
        mPopupWindow.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        mBinding = DataBindingUtil.inflate(inflater, R.layout.layout_select_pay, null, false);
    
        mPopupWindow.setContentView(mBinding.getRoot());
        mPopupWindow.setOnDismissListener(() -> {
            if (mIWindowChangeListener != null) {
                mIWindowChangeListener.onWinwdowChange(false);
            }
        });
        pay_zhifubao = mBinding.rbZhifubao;
        pay_wx = mBinding.rbWx;
        pay_zhifubao.setOnClickListener((view)->{
            selectPayRbButtonHandler(view);
        });
        pay_wx.setOnClickListener((view)->{
            selectPayRbButtonHandler(view);
        });
        mBinding.btnGoPay.setOnClickListener((view)->{
            goPay(view);
        });
        if(mBinding.rbWx.getVisibility()!=View.GONE) {
            selectPayRbButtonHandler(pay_wx);
        }else if (mBinding.rbZhifubao.getVisibility()!=View.GONE) {
            selectPayRbButtonHandler(pay_zhifubao);
        }
        mPresenter=new PayPresenter(getContext(),this);
        mPresenter.setOnPrePayInfoByXianLaiListener(this);
        mPresenter.setOnPayInfoByXianLaiWxListener(this);
        wxpayPresenter=new WxpayPresenter((Activity) getContext());
//        //注册eventBus
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        
        mPopupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                setBackgroundAlpha(1.0f);
                EventBus.getDefault().unregister(this);
            }
        });
    }

    public void setProductBean(ProductBean bean) {
        this.bean = bean;
    }
    /***
     * 支付方式选中样式更新
     */
    public void selectPayRbButtonHandler(View v) {
        switch (nowSelectPayKinds[0]) {
            case R.id.rb_zhifubao:
                pay_zhifubao.setSelected(false);
                break;
            case R.id.rb_wx:
                pay_wx.setSelected(false);
                break;
        }
        tempNk[0] = v.getId();
        v.setSelected(true);
        nowSelectPayKinds[0] = tempNk[0];
    }
    



    /***
     * 支付请求入口
     * 支付由于渠道多，调用链有点乱=_=
     * @param v
     */
    public void goPay(View v) {
        
        
        //支付宝付款
        if(nowSelectPayKinds[0]==R.id.rb_zhifubao)
        {
            if(PayTypeManager.getInstance().getZfbPayChannel()==null||PayTypeManager.getInstance().getZfbPayChannel().equals(""))
            {
                Toast.makeText(getContext(),"抱歉,支付宝支付正在维护中，请选择其他支付方式。",Toast.LENGTH_SHORT).show();
                return;
            }
            
            if (UserInfoManager.getManager(getContext()).getUserInfo() != null && bean!=null) {

                //闲来预支付支付
                mPresenter.reportPayInfoXianLai(UserInfoManager.getManager(getContext()).getUserInfo().getUserId(), DeviceInfoUtil.getDeviceId(mActivity),
                        XIANLAI,XIANLAI,DeviceInfoUtil.getDevice(),bean.getMoney(),2);
              

              UmengEventManager.getInstance(getContext()).reportStartCash("支付宝支付");
            }
        }else if(nowSelectPayKinds[0]==R.id.rb_wx)
        {
            if(PayTypeManager.getInstance().getWxPayChannel()==null||PayTypeManager.getInstance().getWxPayChannel().equals(""))
            {
                Toast.makeText(getContext(),"抱歉,微信支付正在维护中，请选择其他支付方式。",Toast.LENGTH_SHORT).show();
                return;
            }
            
            if (UserInfoManager.getManager(getContext()).getUserInfo() != null && bean!=null) {

//                wxpayPresenter.StartWxPay(UserInfoManager.getManager(getContext()).getUserInfo(),bean.getPid());
                //闲来微信支付支付
                wxpayPresenter.WxPayByXianLai(bean,UserInfoManager.getManager(getContext()).getUserId());

                UmengEventManager.getInstance(getContext()).reportStartCash("微信支付");
            }
          
        }
        showSnackbar("开始请求支付,请稍等..");
    }

    public Context getContext() {
        if (mActivity != null) {
            return mActivity;
        }
        return mFragment.getActivity();
    }


    public void show() {
        if (!mPopupWindow.isShowing()) {
            if (mIWindowChangeListener != null) {
                mIWindowChangeListener.onWinwdowChange(true);
            }
            if (mActivity != null) {
//                mPopupWindow.showAtLocation(mActivity.getWindow().getDecorView(),
//                        Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM, 0, 0);
                mPopupWindow.showAtLocation(mActivity.getWindow().getDecorView(), Gravity.CENTER,0,0);
                setBackgroundAlpha(0.5f);
            } else if (mFragment != null) {
//                mPopupWindow.showAtLocation(mFragment.getActivity().getWindow().getDecorView(),
//                        Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM, 0, 0);
                mPopupWindow.showAtLocation(mFragment.getActivity().getWindow().getDecorView(), Gravity.CENTER,0,0);
                setBackgroundAlpha(0.5f);
            }
        }

        //根据支付渠道确定是否打开支付方式
       
        if(payTypeManager.getZfbPayChannel()==null)
        {
            
        }

    }
    
    
    public void dismiss() {
        if (mPopupWindow.isShowing()) {
            mPopupWindow.dismiss();
        }
    }

    /**
     * 回调需要重写
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //TODO
    }

    @Override
    public void onGetPrePayInfoSuccess(PrePayInfoBean bean) {
        if(payTypeManager.getZfbPayChannel().equals(PayTypeManager.ZFBPayChannel.ZFB)) {

            //闲来预支付信息获取成功后，上报给后台
            tempPrePayInfoBean=bean;
            payType=2;
            mPresenter.reportPrePayInfoByXianlai(payType,this.bean.getPid(),bean.getOrderNum(),UserInfoManager.getManager(getContext()).getUserId(),"vip");

        }
    }

    @Override
    public void onGetPrePayInfoError(String msg) {

    }

    public void setmFinishListener(IFinishListener mFinishListener) {
        this.mFinishListener = mFinishListener;
    }

    public SelectPayWindow(Activity activity, ProductBean bean) {
        mActivity = activity;
        inflater = mActivity.getLayoutInflater();
        init();
        this.bean=bean;
    }

    public SelectPayWindow(Fragment fragment) {
        mFragment = fragment;
        inflater = mFragment.getActivity().getLayoutInflater();
        init();
    }

    public void showSnackbar(String msg)
    {
        if(getContext() instanceof  Activity) {
            SnackbarUtil.makeSnackBar(((Activity) getContext()).getWindow().getDecorView(), msg, Snackbar.LENGTH_SHORT)
                    .setMeessTextColor(getContext().getResources().getColor(R.color.white)).show();
        }
    }

    @Override
    public void onAilPaySucceed(String orderNum,String resultStatus) {
        UserInfoManager.getManager(getContext()).refreshUserInfo();
        UserInfoManager.getManager(getContext()).addUserInfoUpdateListener(this);
        mPresenter.reportPayInfo(bean.getPid(),"支付成功,等待服务回调",orderNum,UserInfoManager.getManager(mActivity).getUserId());
        UmengEventManager.getInstance(getContext()).reportPayReslut("客户端回调-支付宝支付成功");
    }

    @Override
    public void onAilPayFaild(String orderNum,String resultStatus) {
        if(orderNum==null) return;
        //支付失败分情况进行失败上报
        String status="其它支付错误";
        if(TextUtils.equals(resultStatus,"8000"))
        {
            status="正在处理中，支付结果未知";
        }else if(TextUtils.equals(resultStatus,"4000")){
            
            status="订单支付失败";
        }else if(TextUtils.equals(resultStatus,"5000")){

            status="重复请求";
        }else if(TextUtils.equals(resultStatus,"6001")){

            status="用户中途取消";
        }else if(TextUtils.equals(resultStatus,"6002")){

            status="网络连接出错";
        }else if(TextUtils.equals(resultStatus,"6004")){

            status="支付结果未知";
        }
        mPresenter.reportPayInfo(bean.getPid(),status,orderNum,UserInfoManager.getManager(mActivity).getUserId());
        UmengEventManager.getInstance(getContext()).reportPayReslut("客户端回调-支付宝 "+status);
    }

    @Override
    public void onUserInfoUpdated() {
        dismiss();
    }

    @Override
    public void onPrePayInfoByXianLaiSuccess(PrePayInfoBean bean) {
        if(tempPrePayInfoBean!=null && payType==2) {
            payPresenter = new AilPayPresenter(mActivity, tempPrePayInfoBean.getData(), tempPrePayInfoBean.getOrderNum(), this);
            payPresenter.startPayByAilPay();
            tempPrePayInfoBean=null;
        }else if(payType==1)//这里是真正开始闲来渠道微信支付
        {
            wxpayPresenter.StartXianLaiWithWx(mBean);
        }
    }

    @Override
    public void onPrePayInfoByXianLaiError(String msg) {

    }
    
    //闲来微信支付回调逻辑处理
    @Override
    public void onPayInfoByXianLaiWxSuccess(XianLaiWxQueryBean bean) {
        if(bean==null) return;
        if(bean.getPay_result().equals("1"))
        {
            Toast.makeText(getContext(),"支付成功",Toast.LENGTH_LONG).show();
        }else if(bean.getPay_result().equals("2")){
            Toast.makeText(getContext(),"支付取消或失败",Toast.LENGTH_LONG).show();
        }else if(bean.getPay_result().equals("0")){
            Toast.makeText(getContext(),"支付已取消或者您已支付成功需要等待确认支付信息。",Toast.LENGTH_LONG).show();
        }
   
    }

    @Override
    public void onPayInfoByXianLaiWxError(String msg) {
        Toast.makeText(getContext(),msg,Toast.LENGTH_LONG).show();
       
    }


    public interface IWindowChangeListener {
        void onWinwdowChange(boolean show);
    }

    public interface IFinishListener {
        void onFinish();
    }
    
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getWxPayReslut(WXPayEntryActivity.ReslutMsg msg)
    {
        //支付成功
        if(msg.getTag().equals(WXPayEntryActivity.TAG) &&msg.getMsg().equals(WXPayEntryActivity.PAY_SUCCESS))
        {
          
            UserInfoManager.getManager(getContext()).refreshUserInfo();
            UserInfoManager.getManager(getContext()).addUserInfoUpdateListener(this);
            dismiss();
        }
    }

    /***
     * 闲来交友微信预支付上报，事件发送来自WxReportPrePayInfoReceiver
     * @param model
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void eventBusWxPreInfoReceiverCallback(EventBusModel model)
    {
      
        if(model.getAction().equals(WxReportPrePayInfoReceiver.REPORTWXPREPAYINFO))
        {
            Bundle data= (Bundle) model.getData();
            String tradeNo = data.getString("tradeNo");
            mBean = (Bean) data.getSerializable("bean");
            payType=1;
            mPresenter.reportPrePayInfoByXianlai(payType,this.bean.getPid(),tradeNo,UserInfoManager.getManager(getContext()).getUserId(),"vip");
        }
    }



    /***
     * 闲来交友微信预支付查询是否支付成功，事件发送来自WxpayPresenter
     * @param model
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void eventBusWxPayReslutSelect(EventBusModel model)
    {

        if(model.getAction().equals(SelectPayWindow.XIANLAI_WX_PAY_CALLBACK_SELELCT))
        {   dismiss();
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    mPresenter.payInfoByXianlaiWxQueryReslut(XianlaiWxH5Pay.PRODUCTCODE, (String)model.getData(),UserInfoManager.getManager(getContext()).getUserId());
                }
            },2000);
          
           
        }
    }
    
    public void unRegisterBroadCastReceiverWithWxPay()
    {
        XianlaiWxH5Pay xianlaiWxH5Pay = wxpayPresenter.getXianlaiWxH5Pay();
        if(xianlaiWxH5Pay!=null && xianlaiWxH5Pay.getPayUtil()!=null)
        {
            xianlaiWxH5Pay.getPayUtil().destory();
        }
        
    }
    
    public boolean isShow()
    {
        return mPopupWindow.isShowing();
    }
    
    

    /**
     * 设置添加屏幕的背景透明度
     *
     * @param bgAlpha
     *            屏幕透明度0.0-1.0 1表示完全不透明
     */
    public void setBackgroundAlpha(float bgAlpha) {
        WindowManager.LayoutParams lp = mActivity.getWindow()
                .getAttributes();
        lp.alpha = bgAlpha;
        mActivity.getWindow().setAttributes(lp);
    }
    
    
}
