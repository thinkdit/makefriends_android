package com.xindian.fatechat.ui.home.presenter;

import android.content.Context;
import android.os.RemoteException;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.xindian.fatechat.R;
import com.xindian.fatechat.common.BasePresenter;
import com.xindian.fatechat.common.ResultModel;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.ui.home.model.ActivityModel;
import com.xindian.fatechat.ui.home.model.PictureModel;
import com.xindian.fatechat.widget.MyProgressDialog;

import org.json.JSONException;

import java.util.HashMap;
import java.util.List;

/**
 * 极品写真请求类
 */

public class MVListPresenter extends BasePresenter {
    private final String URL_CATEGRAY_LIST = "/auth/activity_photo";
    private final String URL_PIC_LIST = "/auth/photo_picture";
    private onRecommendList getFriendsCaseListener;

    private Context context;

    private final int PAGE_SIZE = 20;
    private int mRecommendPage = 1;

    public MVListPresenter(Context context, onRecommendList listener) {
        this.context = context;
        this.getFriendsCaseListener = listener;
    }

    public void getCateList(boolean isRefresh) {
        if (isRefresh) {
            mRecommendPage = 1;
        } else {
            mRecommendPage++;
        }
        HashMap<String, Object> data = new HashMap<>();
        if (UserInfoManager.getManager(context).isLogin()) {
            data.put("token", UserInfoManager.getManager(context).getToken());
            data.put("userId", UserInfoManager.getManager(context).getUserId());
        }
        data.put("page", mRecommendPage);
        data.put("pageSize", PAGE_SIZE);
        post(getUrl(URL_CATEGRAY_LIST), data, null);
    }
    private MyProgressDialog mDialog;
    public void getPicList(long albumId) {
        HashMap<String, Object> data = new HashMap<>();
        if (UserInfoManager.getManager(context).isLogin()) {
            data.put("token", UserInfoManager.getManager(context).getToken());
            data.put("userId", UserInfoManager.getManager(context).getUserId());
        }
        data.put("albumId", albumId);
            post(getUrl(URL_PIC_LIST), data, null);
        if(mDialog == null){
            mDialog = new MyProgressDialog(context);
        }
        mDialog.show();
    }


    @Override
    public Object asyncExecute(String url, ResultModel resultModel) {
        if (url.contains(URL_CATEGRAY_LIST)) {
            return JSON.parseArray(resultModel.getData(), ActivityModel.class);
        }else if (url.contains(URL_PIC_LIST)) {
            return JSON.parseArray(resultModel.getData(), PictureModel.class);
        }
        return null;
    }

    @Override
    public void onError(String url, Exception e) {
        super.onError(url, e);
        if(mDialog!=null){
            mDialog.dismiss();
        }
    }


    @Override
    public void onFailure(String url, ResultModel resultModel) {
        super.onFailure(url,resultModel);

        if(resultModel!=null && resultModel.getMessage()!=null){
            Toast.makeText(context, resultModel.getMessage(),Toast.LENGTH_SHORT).show();
        }

        if(mDialog!=null){
            mDialog.dismiss();
        }
    }

    @Override
    public void onSucceed(String url, ResultModel resultModel) throws JSONException,
            RemoteException {
        if (url.contains(URL_CATEGRAY_LIST)) {
            List<ActivityModel> dataModel = (List<ActivityModel>) resultModel.getDataModel();
            getFriendsCaseListener.onGetCateListSuccess(dataModel, mRecommendPage == 1 ?
                    true : false, dataModel != null ? dataModel.size() == PAGE_SIZE : false);
        }else if (url.contains(URL_PIC_LIST)) {
            List<PictureModel> dataModel = (List<PictureModel>) resultModel.getDataModel();
            getFriendsCaseListener.onGetPicListSuccess(dataModel);
        }

        if(mDialog!=null){
            mDialog.dismiss();
        }

    }


    public interface onRecommendList {
        void onGetCateListSuccess(List<ActivityModel> list, boolean isFirst, boolean hasMore);
        void onGetPicListSuccess(List<PictureModel> list);

    }
}
