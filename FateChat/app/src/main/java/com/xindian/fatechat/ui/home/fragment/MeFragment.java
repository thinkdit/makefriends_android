package com.xindian.fatechat.ui.home.fragment;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.xindian.fatechat.R;
import com.xindian.fatechat.databinding.FragmentMeBinding;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.ui.base.BaseFragment_v4;
import com.xindian.fatechat.ui.home.MyComActivity;
import com.xindian.fatechat.ui.login.LoginDialog;
import com.xindian.fatechat.ui.user.CashActivity;
import com.xindian.fatechat.ui.user.DiamodnsCashActivity;
import com.xindian.fatechat.ui.user.MyAttentionActivity;
import com.xindian.fatechat.ui.user.MyPhotoActivity;
import com.xindian.fatechat.ui.user.PartnerConditionsActivity;
import com.xindian.fatechat.ui.user.PersonaldataAcitvity;
import com.xindian.fatechat.ui.user.ReceiveGiftActivity;
import com.xindian.fatechat.ui.user.SetActivity;
import com.xindian.fatechat.ui.user.contactCustomerActivity;
import com.xindian.fatechat.ui.user.model.User;


/**
 * Created by hx_Alex on 2017/3/24.
 * 我的Fragment
 */

public class MeFragment extends BaseFragment_v4 implements LoginDialog.onLoginListener,
        UserInfoManager.IUserInfoUpdateListener {
    private FragmentMeBinding mBinding;
    private MeFragmentHandeler handeler;
    private User user;
    private LoginDialog dialog;

    public static MeFragment newInstance() {
        MeFragment meFragment = new MeFragment();
        Bundle bundle = new Bundle();
        meFragment.setArguments(bundle);
        return meFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
            Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_me, container, false);
        user = UserInfoManager.getManager(this.getActivity()).getUserInfo();
        UserInfoManager.getManager(this.getActivity()).addUserInfoUpdateListener(this);
        mBinding.setUser(user);

//        gotoCashActivity(mBinding.getRoot(),"我的界面");

        if (this.getContext() != null) {
            handeler = new MeFragmentHandeler(this.getContext());
            mBinding.setHandler(handeler);
        }
        return mBinding.getRoot();
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        Log.e("setUserVisibleHint", "setUserVisibleHint");
        if (isVisibleToUser) {
            Log.e("isVisibleToUser", "isVisibleToUser");
            checkLoginState();
        }
    }


    @Override
    public void onHiddenChanged(boolean hidden) {
        if (!hidden) {
            checkLoginState();
            if(getActivity()!=null && getActivity() instanceof  MainActivity)
            ((MainActivity)getActivity()).setRootViewBackGroud(R.drawable.me_bg);
            ((MainActivity)getActivity()).getTintManager().setTintColor(0);
        }else 
        {
            if(getActivity()!=null && getActivity() instanceof  MainActivity)
            ((MainActivity)getActivity()).setRootViewBackGroud(0);
            ((MainActivity)getActivity()).getTintManager().setTintColor(Color.parseColor("#8d8d8d"));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        checkLoginState();
    }

    public void checkLoginState() {
        if (this.getContext() != null && UserInfoManager.getManager(this.getContext()).isLogin()) {
            UserInfoManager.getManager(this.getActivity()).refreshUserInfo();
            user = UserInfoManager.getManager(this.getActivity()).getUserInfo();
            mBinding.setUser(user);
        } 
    }

    @Override
    public void LoginSucceed(User user) {
        if (this.getActivity() != null && UserInfoManager.getManager(this.getActivity()).isLogin
                ()) {
            user = UserInfoManager.getManager(this.getActivity()).getUserInfo();
            mBinding.setUser(user);
        }
    }

    @Override
    public void LoginError(String msg) {
        Toast.makeText(this.getContext(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUserInfoUpdated() {
        user = UserInfoManager.getManager(this.getActivity()).getUserInfo();
        mBinding.setUser(user);
    }

    /***
     * 个人中心事件处理类
     */
    public class MeFragmentHandeler {
        private Context context;

        public MeFragmentHandeler(Context context) {
            this.context = context;
        }

        public void onClickPresonData(View v) {
            if (!checkIsLogin()) {
                return;
            }
            Intent i = new Intent(context, PersonaldataAcitvity.class);
            startActivity(i);
        }

        public void onClickMyAttention(View v) {
            if (!checkIsLogin()) {
                return;
            }
            Intent i = new Intent(context, MyAttentionActivity.class);
            startActivity(i);
        }

        public void onClickpartnerConditions(View v) {
            if (!checkIsLogin()) {
                return;
            }
            Intent i = new Intent(context, PartnerConditionsActivity.class);
            startActivity(i);
        }

        public void onCustomertelephoneNumClick(View v) {
            if (!checkIsLogin()) {
                return;
            }
            Intent i = new Intent(context, contactCustomerActivity.class);
            startActivity(i);
        }

        public void onClickSet(View v) {
            if (!checkIsLogin()) {
                return;
            }
            Intent i = new Intent(context, SetActivity.class);
            startActivity(i);
        }

        public void onClickReceiveGift(View v) {
            if (!checkIsLogin()) {
                return;
            }
            Intent i = new Intent(context, ReceiveGiftActivity.class);
            startActivity(i);
        }

        public void onClickCash(View v) {
            if (!checkIsLogin()) {
                return;
            }
            Intent i = new Intent(context, CashActivity.class);
            i.putExtra(CashActivity.ENTRYSOURCE,"我的界面会员展示入口");
            startActivity(i);
        }

        public void onClickMyPhoto(View v) {
            if (!checkIsLogin()) {
                return;
            }
            Intent i = new Intent(context, MyPhotoActivity.class);
            startActivity(i);
        }

        public void onClickDiaCash(View v) {
            if (!checkIsLogin()) {
                return;
            }
            int balance=0;
            Intent i = new Intent(context, DiamodnsCashActivity.class);
            if(user!=null && user.getDiamonds()!=null)
            {
                balance=user.getDiamonds().getDiamonds();
            }
            i.putExtra(DiamodnsCashActivity.DIAMODNSBALANCE,balance);
            startActivity(i);
        }

        public void onClickMyCom(View v) {
            if (!checkIsLogin()) {
                return;
            }
            Intent i = new Intent(context, MyComActivity.class);
            startActivity(i);
        }

        public void onClickLogin(View v) {
            dialog = new LoginDialog(context, "登录方可体验更多内容");
            dialog.setOnLoginListener(MeFragment.this);
            dialog.setView(new EditText(this.context));
            dialog.show();
        }

        /***
         * 检查是否登录
         *
         * @return  
         */
        public boolean checkIsLogin() {
            if (context != null && !UserInfoManager.getManager(context).isLogin()) {
                onClickLogin(null);
                return false;
            }
            return true;
        }
    }

}
