package com.xindian.fatechat.manager;

import android.content.Context;

import com.hyphenate.EMCallBack;
import com.hyphenate.chat.EMClient;
import com.hyphenate.easeui.domain.ChatUserEntity;
import com.thinkdit.lib.util.SharePreferenceUtils;
import com.thinkdit.lib.util.StringUtil;
import com.xindian.fatechat.DemoHelper;
import com.xindian.fatechat.ui.login.model.LoginBean;
import com.xindian.fatechat.ui.user.model.User;
import com.xindian.fatechat.ui.user.presenter.RequestUserInfoPresenter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zjzhu on 28/6/16.
 * 用户信息管理类 所有用户信息更新保存通过此类操作
 */
public class SharePreferenceManager {
    public final static String KEY_CHAT_USERIDS = "key_userIds_";
    public static String KEY_CHAT_USERIDS_IMPL;
    private static SharePreferenceManager sUserInfoManager;
    private Context mContext;
    private String mChatUserIds;


    public static SharePreferenceManager getManager(Context context) {
        if (sUserInfoManager == null) {
            sUserInfoManager = new SharePreferenceManager(context.getApplicationContext());
        }
        return sUserInfoManager;
    }

    private SharePreferenceManager(Context context) {
        mContext = context;
        KEY_CHAT_USERIDS_IMPL = KEY_CHAT_USERIDS + UserInfoManager.getManager(context).getUserId();
        mChatUserIds = SharePreferenceUtils.getString(context, KEY_CHAT_USERIDS_IMPL);

    }

    public void addNewUserId(String id) {
        mChatUserIds = mChatUserIds + id + ":";
        SharePreferenceUtils.putString(mContext, KEY_CHAT_USERIDS_IMPL, mChatUserIds);
    }

    public void addNewUserId(int id) {
        mChatUserIds = mChatUserIds + id + ":";
        SharePreferenceUtils.putString(mContext, KEY_CHAT_USERIDS_IMPL, mChatUserIds);
    }

    public String getAddUserIds() {
        return mChatUserIds;
    }

    public boolean isChated(int id) {
        if (mChatUserIds == null) {
            return false;
        }
        if (mChatUserIds.contains(id + ":")) {
            return true;
        }
        return false;
    }

    public boolean isChated(String id) {
        if (mChatUserIds == null) {
            return false;
        }
        if (mChatUserIds.contains(id + ":")) {
            return true;
        }
        return false;
    }

    public void reset(Context context) {
        mContext = context;
        KEY_CHAT_USERIDS_IMPL = KEY_CHAT_USERIDS + UserInfoManager.getManager(context).getUserId();
        mChatUserIds = SharePreferenceUtils.getString(context, KEY_CHAT_USERIDS_IMPL);
    }
}
