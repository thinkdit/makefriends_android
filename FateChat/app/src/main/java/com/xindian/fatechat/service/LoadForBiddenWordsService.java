package com.xindian.fatechat.service;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.hyphenate.easeui.manager.ForbiddenWordsManger;

/**
 * Created by hx_Alex on 2017/7/19.
 */

public class LoadForBiddenWordsService extends IntentService {
    private static final String TAG="LoadForBiddenWordsService";
private ForbiddenWordsManger manger;
    public LoadForBiddenWordsService() {
        super(TAG);
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.e("Service","开始请求下载");
        manger=ForbiddenWordsManger.getInstance(this);
        manger.startCheckForbiddenWords();
    }
}
