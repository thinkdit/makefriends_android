package com.xindian.fatechat.ui.home.fragment;

import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.xindian.fatechat.R;
import com.xindian.fatechat.databinding.FragmentHomeBinding;
import com.xindian.fatechat.ui.base.BaseFragment_v4;
import com.xindian.fatechat.ui.home.adapter.HomeFriendsPagerAdapter;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

/**
 * Created by hx_Alex on 2017/3/24.
 * 首页Fragment
 */

public class HomeFragment extends BaseFragment_v4  {
    private FragmentHomeBinding mBinding;

    public static HomeFragment newInstance() {
        HomeFragment homeFragment = new HomeFragment();
        Bundle bundle = new Bundle();
        homeFragment.setArguments(bundle);
        return homeFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
            Bundle savedInstanceState) {
        Log.w("homeFragment","onCreateView");
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        initView();
        return mBinding.getRoot();
    }

    private void initView() {
        initViewPager();
        mBinding.recTab.setOnClickListener(v->mBinding.viewPager.setCurrentItem(0));
        mBinding.matchTab.setOnClickListener(v->mBinding.viewPager.setCurrentItem(1));
        mBinding.nearTab.setOnClickListener(v->mBinding.viewPager.setCurrentItem(2));
    }

    private void setimg(int checkedId) {

    }

    HomeFriendsPagerAdapter adapter;
    private List<Fragment> fragmentList;
    RecommendFriendsFragment recommendFragment;
    MatchFragment matchFragment;
    NearByFriendsFragment nearByFragment;

    private void initViewPager() {
        //组装fragment
        recommendFragment = RecommendFriendsFragment.newInstance();
        matchFragment = MatchFragment.newInstance();
        nearByFragment = NearByFriendsFragment.newInstance();
        fragmentList = Arrays.asList(recommendFragment,matchFragment,nearByFragment);
        mBinding.viewPager.setAdapter(new FragmentPagerAdapter(getFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                Fragment fragment = fragmentList.get(position);

                return fragment;

            }

            @Override
            public int getCount() {
                return fragmentList.size();
            }
        });
        mBinding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Fragment fragment = fragmentList.get(position);
                if(fragment instanceof MatchFragment){
                    mBinding.viewPager.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ((MatchFragment) fragment).doRemindCom();
                        }
                    },500);

                }
                changeTab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mBinding.viewPager.setCurrentItem(0);
    }

    /***
     * 改变tab指示器
     * @param position
     */
    private void changeTab(int position)
    {
        mBinding.txtRecTab.setTextColor(getResources().getColor(R.color.black));
        mBinding.txtNearTab.setTextColor(getResources().getColor(R.color.black));
        mBinding.txtMatchTab.setTextColor(getResources().getColor(R.color.black));
        mBinding.txtMatchIndicator.setVisibility(View.GONE);
        mBinding.txtRecIndicator.setVisibility(View.GONE);
        mBinding.txtNearIndicator.setVisibility(View.GONE);
        if(position==0)
        {
            mBinding.txtRecTab.setTextColor(getResources().getColor(R.color.colorAccent));
            mBinding.txtRecIndicator.setVisibility(View.VISIBLE);
        }else if(position==1){
            mBinding.txtMatchTab.setTextColor(getResources().getColor(R.color.colorAccent));
            mBinding.txtMatchIndicator.setVisibility(View.VISIBLE);
        }else if(position==2){
            mBinding.txtNearTab.setTextColor(getResources().getColor(R.color.colorAccent));
            mBinding.txtNearIndicator.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        onShowChange(isVisibleToUser);
    }

    @Override
    public void onShowChange(boolean hide) {
        super.onShowChange(hide);
        if (mBinding == null || mBinding.viewPager == null) {
            return;
        }
        int item = mBinding.viewPager.getCurrentItem();
        if (item == 0) {
            recommendFragment.onShowChange(hide);
        } else if (item == 1) {
            matchFragment.onShowChange(hide);
        } else if (item == 2) {
            nearByFragment.onShowChange(hide);
        }
    }

    /***
     * 设置tablayout指示器长度
     * @param tabs
     * @param leftDip
     * @param rightDip
     */
    public static void setIndicator(TabLayout tabs, int leftDip, int rightDip) {
        Class<?> tabLayout = tabs.getClass();
        Field tabStrip = null;
        try {
            tabStrip = tabLayout.getDeclaredField("mTabStrip");
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

        tabStrip.setAccessible(true);
        LinearLayout llTab = null;
        try {
            llTab = (LinearLayout) tabStrip.get(tabs);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        int left = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, leftDip, Resources.getSystem().getDisplayMetrics());
        int right = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, rightDip, Resources.getSystem().getDisplayMetrics());

        for (int i = 0; i < llTab.getChildCount(); i++) {
            View child = llTab.getChildAt(i);
            child.setPadding(0, 0, 0, 0);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1);
            params.leftMargin = left;
            params.rightMargin = right;
            child.setLayoutParams(params);
            child.invalidate();
        }
    }

}
