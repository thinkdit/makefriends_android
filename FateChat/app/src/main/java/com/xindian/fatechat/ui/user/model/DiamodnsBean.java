package com.xindian.fatechat.ui.user.model;

import com.xindian.fatechat.common.BaseModel;

import java.io.Serializable;

/**
 * Created by hx_Alex on 2017/7/11.
 * 用户钻石余额bean
 */

public class DiamodnsBean extends BaseModel implements Serializable{

    /**
     * id : 1
     * uid : 222
     * diamonds : 1000
     * creationTime : 1499680173000
     * modifyTime : 1499680176000
     * spare1 : null
     * spare2 : null
     */

    private int id;
    private int uid;
    private int diamonds;
    private long creationTime;
    private long modifyTime;
    private String spare1;
    private String spare2;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getDiamonds() {
        return diamonds;
    }

    public void setDiamonds(int diamonds) {
        this.diamonds = diamonds;
    }

    public long getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(long creationTime) {
        this.creationTime = creationTime;
    }

    public long getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(long modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getSpare1() {
        return spare1;
    }

    public void setSpare1(String spare1) {
        this.spare1 = spare1;
    }

    public String getSpare2() {
        return spare2;
    }

    public void setSpare2(String spare2) {
        this.spare2 = spare2;
    }
}
