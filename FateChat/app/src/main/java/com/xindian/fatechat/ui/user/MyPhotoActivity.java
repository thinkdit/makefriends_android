package com.xindian.fatechat.ui.user;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.GridLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.common.pullrefreshview.PullToRefreshView;
import com.hyphenate.easeui.utils.SnackbarUtil;
import com.xindian.fatechat.R;
import com.xindian.fatechat.databinding.MyPhotoAcitivityBinding;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.ui.base.BaseActivity;
import com.xindian.fatechat.ui.login.presenter.AccountPresenter;
import com.xindian.fatechat.ui.user.adapter.MyPhotoListAdapter;
import com.xindian.fatechat.ui.user.adapter.MyPhotoListAdapter.OnItemClickListener;
import com.xindian.fatechat.ui.user.decoration.SpacesItemDecoration;
import com.xindian.fatechat.ui.user.model.PhotoInfoBean;
import com.xindian.fatechat.ui.user.model.User;
import com.xindian.fatechat.ui.user.presenter.MyPhotoPresenter;
import com.xindian.fatechat.util.imageSelectorUtil;
import com.xindian.fatechat.widget.MyProgressDialog;
import com.yancy.imageselector.ImageSelector;
import com.yancy.imageselector.ImageSelectorActivity;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

import static com.xindian.fatechat.ui.user.PhotoDetailsActivity.DELETE_IMAGE;

public class MyPhotoActivity extends BaseActivity implements OnItemClickListener, MyPhotoPresenter.onMyPhotoListener,
        AccountPresenter.IAvatarUploadedListener, PullToRefreshView.OnRefreshListener {
    public static final String BYOTHERUSERPHOTO = "byOtehrUserPhoto";
    public static final String BYOTHERUSEID = "byOtehrUserId";
    private static final int DEFAULT_SPANCOUNT = 4;
    private static final int DEFAULT_PAGE_SIZE = 30;
    private static  final long MAXPHOTOSIZE=5242880L;//不超过5MB
    private static  final int MAXPHOTONUM=50;
    private MyPhotoAcitivityBinding mBinding;
    private MyPhotoPresenter mPresenter;
    private imageSelectorUtil imageSelectorUtil;
    private User user;
    private AccountPresenter accountPresenter;
    private String nowSelectPhotoPath;
    private int nowSelectPhotoId;
    private int nowPage;
    private boolean isLoadmore;
    private boolean byOtherUser;
    private int otherUserId;
    private List<String> pathList;
    private MyProgressDialog progressDialog;
 

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.my_photo_acitivity);
        byOtherUser = getIntent().getBooleanExtra(BYOTHERUSERPHOTO, false);
        otherUserId = getIntent().getIntExtra(BYOTHERUSEID, 0);
        mPresenter = new MyPhotoPresenter(this, this, byOtherUser);
        accountPresenter = new AccountPresenter(this);
        accountPresenter.setAvatarUploadedListener(this);
        user = UserInfoManager.getManager(this).getUserInfo();
        intiViews();
        initToolBar();
        progressDialog=new MyProgressDialog(this);
        progressDialog.setTextCotent("正在上传中,请稍等...");
    }

    /**
     * 初始化RecyclerView
     */
    private void intiViews() {
        if (!byOtherUser) {
            mPresenter.getPhotoListAdapter(user.getUserId(), nextPage(), DEFAULT_PAGE_SIZE, user.getUserId(),false);
        } else if (otherUserId != 0) {
            mPresenter.getPhotoListAdapter(user.getUserId(), nextPage(), DEFAULT_PAGE_SIZE, otherUserId,false);
        }
        mBinding.listPhoto.setLayoutManager(new GridLayoutManager(MyPhotoActivity.this, DEFAULT_SPANCOUNT));
        mBinding.listPhoto.addItemDecoration(new SpacesItemDecoration(8));
        imageSelectorUtil = new imageSelectorUtil(this, true);
        mBinding.pullGroup.setPullDownEnable(false);
        mBinding.pullGroup.setListener(this);
    }

    /***
     * 点击照片触发事件
     * 当position为0时，为add图片
     */
    @Override
    public void onItemClickListener(View view, int position, Object value, int id) {

        if (position == 0 && !byOtherUser) {
            int photoSize = mBinding.listPhoto.getAdapter().getItemCount() - 1;
            if(photoSize<MAXPHOTONUM) {
                addPhoto();
            }else {
                SnackbarUtil.makeSnackBar(mBinding.getRoot(), "您的图片总数已经达到最大数量！", Toast.LENGTH_SHORT).setMeessTextColor(Color.WHITE).show();
            }
        } else {
            //进入查看图片详情
            nowSelectPhotoPath = (String) value;
            nowSelectPhotoId = id;
            gotoPhotoDetails(view, nowSelectPhotoPath, nowSelectPhotoId);
        }

    }

    public void addPhoto() {
        imageSelectorUtil.changePortraitSingle();
    }


    public void gotoPhotoDetails(View view, String urlImg, int urlId) {
        Intent i = new Intent(this, PhotoDetailsActivity.class);
        i.putExtra(PhotoDetailsActivity.URL_IMG, urlImg);
        i.putExtra(PhotoDetailsActivity.URL_ID, urlId);
        i.putExtra(PhotoDetailsActivity.ISOTHERUSER, byOtherUser);
        Pair<View, String> imagePair = Pair.create(view, getString(R.string.imageViewTransitionName));
        ActivityOptionsCompat compat = ActivityOptionsCompat.makeSceneTransitionAnimation(this, imagePair);
        ActivityCompat.startActivityForResult(this,i, PhotoDetailsActivity.DELETE_IMAGE, compat.toBundle());

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        /***
         * 图片选择回调
         */
        if (resultCode == RESULT_OK && data != null) {
            if (requestCode == ImageSelector.IMAGE_REQUEST_CODE) {
                // Get Image Path List
                List<String> pathList = data.getStringArrayListExtra(ImageSelectorActivity.EXTRA_RESULT);
                this.pathList=pathList;
                batchUpLoaded(this.pathList);
            }
        }

        if (requestCode == DELETE_IMAGE && resultCode == RESULT_OK) {
            PhotoInfoBean bean = new PhotoInfoBean();
            bean.setImageUrl(nowSelectPhotoPath);
            bean.setId(nowSelectPhotoId);
            bean.setUserId(user.getUserId());
            mPresenter.deletePhoto(bean);
            nowSelectPhotoPath = null;
            nowSelectPhotoId = 0;
        }
    }

    private int nextPage() {
        nowPage = ++nowPage;
        return nowPage;
    }
    
    private  void refeshPhotoList()
    {
        nowPage=0;
        mPresenter.getPhotoListAdapter(user.getUserId(), nextPage(), DEFAULT_PAGE_SIZE, user.getUserId(),true);
    }

    @Override
    public void onAddPhotoSuccess(MyPhotoListAdapter mAdapter) {
        batchUpLoaded(pathList);
        refeshPhotoList();
    }

    @Override
    public void onAddPhotoError(String msg) {
        SnackbarUtil.makeSnackBar(mBinding.getRoot(), msg, Toast.LENGTH_SHORT).setMeessTextColor(Color.WHITE).show();
    }

    @Override
    public void onGetPhotoSuccess(MyPhotoListAdapter mAdapter) {
        mAdapter.setmOnItemClickListener(this);
        if (nowPage == 1) {
            mBinding.listPhoto.setAdapter(mAdapter);
        } else {
            mAdapter.notifyDataSetChanged();
        }
        //判断一下是否还有数据,当前列表数据如果小于当前页数X页显示大小 那么则为下一页无数据
        if ((mAdapter.getItemCount() - 1) < nowPage * DEFAULT_PAGE_SIZE) {
            isLoadmore = false;
            mBinding.pullGroup.setPullUpEnable(false);
        } else {
            isLoadmore = true;
        }
    }

    @Override
    public void onGetPhotoError(String msg) {
        SnackbarUtil.makeSnackBar(mBinding.getRoot(), msg, Toast.LENGTH_SHORT).setMeessTextColor(Color.WHITE).show();
    }

    @Override
    public void onDeletePhotoSuccess(MyPhotoListAdapter mAdapter) {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onDeletePhotoError(String msg) {
        SnackbarUtil.makeSnackBar(mBinding.getRoot(), msg, Toast.LENGTH_SHORT).setMeessTextColor(Color.WHITE).show();
    }

    @Override
    public void onAvatarUploadedSucceed(String url) {
        mPresenter.addPhoto(user.getUserId(), url);
        //从list中移除当前第一位
        this.pathList.remove(0);
    }

    @Override
    public void onAvatarUploadedError() {
        
    }

    /***
     * 批量上传图片
     * @param pathList
     */
    public void batchUpLoaded( List<String> pathList )
    {
        if(pathList.size()==0) 
        {
            progressDialog.dismiss();
            return;
        }
        progressDialog.show();
        String path = pathList.get(0);
        if(checkPhotoSize(path)) {
            accountPresenter.uploadAvatar(path);
        }
    }
    
    @Override
    public void onRefresh() {

    }

    /***
     * 加载更多
     */
    @Override
    public void onLoadMore() {
        if (isLoadmore) {
            mPresenter.getPhotoListAdapter(user.getUserId(), nextPage(), DEFAULT_PAGE_SIZE, user.getUserId(),false);
            mBinding.pullGroup.onFinishLoading();
        } else {
            mBinding.pullGroup.onFinishLoading();
            mBinding.pullGroup.setPullUpEnable(false);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();
    }


    /**
     * 获取指定文件大小
     * 
     * @return
     * @throws Exception
     */
    private  long getFileSize(File file) throws Exception
    {
        long size = 0;
        if (file.exists()){
            FileInputStream fis = null;
            fis = new FileInputStream(file);
            size = fis.available();
        }
        else{
            size=-1;
            Log.e("获取文件大小","文件不存在!");
        }
        return size;
    }

    /***
     * 检查上传文件是否大于指定大小
     * @param path
     * @return
     */
    public boolean checkPhotoSize(String path)
    {
        File file=new File(path);
        try {
            long fileSize = getFileSize(file);
            if(fileSize>0 && fileSize<MAXPHOTOSIZE){
                Log.e("ok","ok");
                return true;
            }else if(fileSize==-1){
                Toast.makeText(this,"文件不存在，请检查",Toast.LENGTH_SHORT).show();
            }else if(fileSize>MAXPHOTOSIZE){
                Toast.makeText(this,"图片大于5MB暂无法上传，请重新选择!",Toast.LENGTH_SHORT).show();
            }
       
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }
    
}
