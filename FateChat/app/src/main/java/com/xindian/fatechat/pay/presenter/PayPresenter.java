package com.xindian.fatechat.pay.presenter;

import android.content.Context;
import android.os.RemoteException;
import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.xindian.fatechat.common.BasePresenter;
import com.xindian.fatechat.common.ResultModel;
import com.xindian.fatechat.pay.crypt.EncodeJson;
import com.xindian.fatechat.pay.model.PrePayInfoBean;
import com.xindian.fatechat.pay.model.XianLaiWxQueryBean;
import com.xindian.fatechat.ui.user.model.User;
import com.xindian.fatechat.widget.MyProgressDialog;

import org.json.JSONException;

import java.util.HashMap;

/**
 * Created by hx_Alex on 2017/4/20.
 */

public class PayPresenter extends BasePresenter {
    public   static final  String  RECHAGRETYPE_VIP="vip";
    public  static final  String  RECHAGRETYPE_DIAMONDS="diamonds";
    private  static final String URL_PREPAYINFO = "/auth/pre-pay-info";
    private  static final  String URL_WX_BBNPAY_PRE="/wWebChat/w_weChat-pay-info";//点芯微信支付预支付
    private  static final  String URL_WX_WFT_PRE="/swiftWeChat/wft-weChat-pay-info";//威富通微信支付预支付
    private  static final  String URL_PRY_REPORT_INFO="/report/pay_report_info";//支付失败上报

    private  static final  String URL_PRY_REPORT_INFO_XIANLAI="http://api.5taogame.com/pay/AppPayMoney";//闲来交友预支付
    private  static final  String URL_PRY_REPORT_INFO_XIANLAI_QFINGER_PAY="/qFingerPay/qFinger-weChat-pay-info";//闲来交友预支付
    private  static final  String URL_INFO_XIANLAI_PAY_QUERY_RESLUT="http://api.5taogame.com/pay/orderQueryResult";//闲来微信支付支付结果回调查询
    private onPrePayInfoListener onPrePayInfoListener;
    private onWxPrePayInfoListener onWxPrePayInfoListener;
    private onPrePayInfoByXianLaiListener onPrePayInfoByXianLaiListener;
    private onPayInfoByXianLaiWxListener onPayInfoByXianLaiWxListener;
    private MyProgressDialog mDialog;
    private Context context;

    public PayPresenter(Context context, onPrePayInfoListener listener) {
        this.context = context;
        this.onPrePayInfoListener = listener;
        mDialog = new MyProgressDialog(context);
    }

    public PayPresenter(Context context, onWxPrePayInfoListener listener) {
        this.context = context;
        this.onWxPrePayInfoListener = listener;
        mDialog = new MyProgressDialog(context);
    }


    public void setOnPrePayInfoByXianLaiListener(PayPresenter.onPrePayInfoByXianLaiListener onPrePayInfoByXianLaiListener) {
        this.onPrePayInfoByXianLaiListener = onPrePayInfoByXianLaiListener;
    }
    //闲来微信支付支付结果回调接口
    public void setOnPayInfoByXianLaiWxListener(PayPresenter.onPayInfoByXianLaiWxListener onPayInfoByXianLaiWxListener) {
        this.onPayInfoByXianLaiWxListener = onPayInfoByXianLaiWxListener;
    }





    public void requestPrePayInfo(User user,int productId,String rechargeType) {

        HashMap<String, Object> data = new HashMap<>();
        data.put("userId", user.getUserId());
        data.put("productId", productId);
        data.put("rechargeType", rechargeType);
        post(getUrl(URL_PREPAYINFO), data, null);
        if(!mDialog.isShowing()) {
            mDialog.show();
        }
    }

    public void requestWxPrePayInfo(User user,int productId,String rechargeType) {
        HashMap<String, Object> data = new HashMap<>();
        data.put("userId", user.getUserId());
        data.put("productId", productId);
        data.put("rechargeType", rechargeType);
        post(getUrl(URL_WX_BBNPAY_PRE), data, null);
        if(!mDialog.isShowing()) {
            mDialog.show();
        }
    }

    public void requestWxPrePayInfoBywft(User user,int productId,String rechargeType) {
        HashMap<String, Object> data = new HashMap<>();
        data.put("userId", user.getUserId());
        data.put("productId", productId);
        data.put("rechargeType", rechargeType);
        post(getUrl(URL_WX_WFT_PRE), data, null);
        if(!mDialog.isShowing()) {
            mDialog.show();
        }
    }

    public void reportPayInfo(int pid,String status ,String tradeNo,int userId ) {
        HashMap<String, Object> data = new HashMap<>();
        data.put("pid", pid);
        data.put("info", status);
        data.put("tradeNo", tradeNo);
        data.put("userId", userId);
        post(getUrl(URL_PRY_REPORT_INFO), data, null);
    }

    public void reportPayInfoXianLai(int userId,String imei ,String product_code,String qudao_code,String phone_shop,float pay_money,int pay_flag_code) {
        HashMap<String, Object> data = new HashMap<>();
        data.put("user_id", userId);
        data.put("imei", imei);
        data.put("product_code", product_code);
        data.put("qudao_code", qudao_code);
        data.put("phone_shop", phone_shop);
        data.put("pay_money", pay_money);
        data.put("pay_flag_code", pay_flag_code);
        data.put("pay_type_code", 4);
        post(URL_PRY_REPORT_INFO_XIANLAI, data,true,true, null);
        
    }

    
    public void reportPrePayInfoByXianlai(int payType,int productId  ,String tradeNo,int userId,String rechargeType) {
        HashMap<String, Object> data = new HashMap<>();
        data.put("payType", payType);
        data.put("productId", productId);
        data.put("tradeNo", tradeNo);
        data.put("userId", userId);
        data.put("rechargeType", rechargeType);
        post(getUrl(URL_PRY_REPORT_INFO_XIANLAI_QFINGER_PAY), data, null);
    }

    public void payInfoByXianlaiWxQueryReslut(String product_code,String order_id,int userId) {
        HashMap<String, Object> data = new HashMap<>();
        data.put("product_code", product_code);
        data.put("order_id", order_id);
        data.put("user_id", userId);
        post(URL_INFO_XIANLAI_PAY_QUERY_RESLUT, data,true,true, null);
    }



    @Override
    public Object asyncExecute(String url, ResultModel resultModel) {
        if (url.contains(URL_PREPAYINFO)) {
            return JSON.parseObject(resultModel.getData(), PrePayInfoBean.class);
        }else if(url.contains(URL_WX_BBNPAY_PRE))
        {
            return JSON.parseObject(resultModel.getData(), PrePayInfoBean.class);
        }else if(url.contains(URL_WX_WFT_PRE))
        {
            return JSON.parseObject(resultModel.getData(), PrePayInfoBean.class);
        }
        return null;
    }

    @Override
    public void onError(String url, Exception e) {
        super.onError(url, e);
    }


    @Override
    public void onFailure(String url, ResultModel resultModel) {
        if (url.contains(URL_PREPAYINFO)) {
            onPrePayInfoListener.onGetPrePayInfoError(resultModel.getMessage().equals("") || resultModel.getMessage() == null ? "未知错误" : resultModel.getMessage());
        } else  if (url.contains(URL_WX_BBNPAY_PRE)) {
            onWxPrePayInfoListener.onGetPrePayInfoError(resultModel.getMessage().equals("") || resultModel.getMessage() == null ? "未知错误" : resultModel.getMessage());

        } else  if (url.contains(URL_PRY_REPORT_INFO_XIANLAI)) {
            onPrePayInfoListener.onGetPrePayInfoError(resultModel.getMessage().equals("") || resultModel.getMessage() == null ? "未知错误" : resultModel.getMessage());
        }else if(url.contains(URL_PRY_REPORT_INFO_XIANLAI_QFINGER_PAY)){
            onPrePayInfoByXianLaiListener.onPrePayInfoByXianLaiError(resultModel.getMessage().equals("") || resultModel.getMessage() == null ? "未知错误" : resultModel.getMessage());
        }else if(url.contains(URL_INFO_XIANLAI_PAY_QUERY_RESLUT))
        {
            onPayInfoByXianLaiWxListener.onPayInfoByXianLaiWxError(resultModel.getMessage().equals("") || resultModel.getMessage() == null ? "未知错误" : resultModel.getMessage());
        }
        
        if (mDialog.isShowing()) {
            mDialog.dismiss();
        }
    }

    @Override
    public void onSucceed(String url, ResultModel resultModel) throws JSONException, RemoteException {
        if (url.contains(URL_PREPAYINFO)) {
            PrePayInfoBean dataModel = (PrePayInfoBean) resultModel.getDataModel();
            onPrePayInfoListener.onGetPrePayInfoSuccess(dataModel);
        }  else if (url.contains(URL_WX_BBNPAY_PRE)) {
            PrePayInfoBean dataModel = (PrePayInfoBean) resultModel.getDataModel();
            if (dataModel.getData() != null) {
                onWxPrePayInfoListener.onGetPrePayInfoSuccess(dataModel);
            }
        }else if(url.contains(URL_WX_WFT_PRE)){
            PrePayInfoBean dataModel = (PrePayInfoBean) resultModel.getDataModel();
            String token_id = JSON.parseObject(dataModel.getData()).getString("token_id");
            dataModel.setData(token_id);
            if (dataModel.getData() != null) {
                onWxPrePayInfoListener.onGetPrePayInfoSuccess(dataModel);
            }
        }else if(url.contains(URL_PRY_REPORT_INFO_XIANLAI))
        {
            String dataModel = (String)resultModel.getDataModel();
            Log.e("onSucceed",dataModel);
            if(dataModel!=null) {
                String s = EncodeJson.EncodeJson(dataModel);
                PrePayInfoBean xianlaiZfbToPreBean = getXianlaiZfbToPreBean(s);
                if(xianlaiZfbToPreBean!=null&& xianlaiZfbToPreBean.getData()!=null&&xianlaiZfbToPreBean.getOrderNum()!=null) {
                    onPrePayInfoListener.onGetPrePayInfoSuccess(xianlaiZfbToPreBean);
                }else {
                    onPrePayInfoListener.onGetPrePayInfoError("预支付订单获取失败!");
                }
            }
        }else if(url.contains(URL_PRY_REPORT_INFO_XIANLAI_QFINGER_PAY))
        {
            onPrePayInfoByXianLaiListener.onPrePayInfoByXianLaiSuccess(null);
        }else if(url.contains(URL_INFO_XIANLAI_PAY_QUERY_RESLUT))
        {
            String encodeJson = EncodeJson.EncodeJson((String) resultModel.getDataModel());
            XianLaiWxQueryBean bean = JSON.parseObject(encodeJson, XianLaiWxQueryBean.class);
            onPayInfoByXianLaiWxListener.onPayInfoByXianLaiWxSuccess(bean);
        }
        mDialog.dismiss();
    }
    
    private  PrePayInfoBean getXianlaiZfbToPreBean(String data){
        PrePayInfoBean bean=new PrePayInfoBean();
        try {
            JSONObject jsonData = JSON.parseObject(data);
            String order_encode_string = jsonData.getString("order_encode_string");
            String orderid = jsonData.getString("orderid");
            bean.setData(order_encode_string);
            bean.setOrderNum(orderid);
        }catch (Exception ex)
        {
            return  null;
        }
 
        return  bean;
    }

    public interface onPrePayInfoListener {
        void onGetPrePayInfoSuccess(PrePayInfoBean bean);

        void onGetPrePayInfoError(String msg);
    }

    public interface onWxPrePayInfoListener {
        void onGetPrePayInfoSuccess(PrePayInfoBean bean);

        void onGetPrePayInfoError(String msg);
    }
  

    public interface onPrePayInfoByXianLaiListener {
        void onPrePayInfoByXianLaiSuccess(PrePayInfoBean bean);

        void onPrePayInfoByXianLaiError(String msg);
    }

    public interface onPayInfoByXianLaiWxListener {
        void onPayInfoByXianLaiWxSuccess(XianLaiWxQueryBean bean);

        void onPayInfoByXianLaiWxError(String msg);
    }
  

}


