package com.xindian.fatechat.ui.user.presenter;

import android.content.Context;
import android.os.RemoteException;

import com.alibaba.fastjson.JSON;
import com.xindian.fatechat.common.BasePresenter;
import com.xindian.fatechat.common.ResultModel;
import com.xindian.fatechat.ui.user.adapter.MyPhotoListAdapter;
import com.xindian.fatechat.ui.user.model.PhotoInfoBean;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

/**
 * Created by hx_Alex on 2017/4/4.
 */

public class MyPhotoPresenter extends BasePresenter 
{
    private final String  URL_MYIMAGE="/auth/myImage";
    private final String  URL_ADDIMAGE="/auth/addImage";
    private final String  URL_DELETEIMAGE="/auth/deleteImage";
    
    private MyPhotoListAdapter mAdapter;//我的照片显示适配器
    private ArrayList<PhotoInfoBean> mList;
    private Context context;
    private  onMyPhotoListener mListener;
    private  PhotoInfoBean nowPhotoInfo;//当前需要添加的照片path
    private  boolean isShowOtherUserPhoto;
    private boolean isRefesh;
    public MyPhotoPresenter(Context context,onMyPhotoListener mListener,boolean isShowOtherUserPhoto) {
        this.context = context;
        this.mListener=mListener;
        this.isShowOtherUserPhoto=isShowOtherUserPhoto;
    }
    
    
    
    private void requestMyImage(int userId,int page,int pageSize,int otherId)
    {
        HashMap<String,Object> data=new HashMap<>();
        data.put("userId",userId);
        data.put("page",page);
        data.put("pageSize",pageSize);
        data.put("othersId",otherId);
        post(getUrl(URL_MYIMAGE),data,context);
    }

    private void requestAddImage(int userId,String imagesUrl)
    {
        HashMap<String,Object> data=new HashMap<>();
        data.put("userId",userId);
        data.put("imagesUrl",imagesUrl);
     
        post(getUrl(URL_ADDIMAGE),data,context);
    }

    private void requestDeleteImage(int userId,int imgId)
    {
        HashMap<String,Object> data=new HashMap<>();
        data.put("userId",userId);
        data.put("imgId",imgId);
        post(getUrl(URL_DELETEIMAGE),data,context);
    }


    public void getPhotoListAdapter(int userId,int page,int pageSize,int otherId,boolean isRefesh)
    {
            this.isRefesh=isRefesh;
            requestMyImage(userId, page, pageSize, otherId);
    }



    @Override
    public Object asyncExecute(String url, ResultModel resultModel) {
        if(url.contains(URL_MYIMAGE)) {
            return JSON.parseArray(resultModel.getData(),PhotoInfoBean.class);
        }
        return  null;
    }


    @Override
    public void onSucceed(String url, ResultModel resultModel) throws JSONException, RemoteException {
        if(url.contains(URL_MYIMAGE)) {
            //如果mlist，mAdapter为null,则是第一次加载数据
            if((mList==null&& mAdapter==null) ||isRefesh)
            {
                mList= (ArrayList<PhotoInfoBean> ) resultModel.getDataModel();
                mAdapter=new MyPhotoListAdapter(context,mList,isShowOtherUserPhoto);
                isRefesh=false;
            }
            else 
            {
                Collection<PhotoInfoBean> c=(ArrayList<PhotoInfoBean> ) resultModel.getDataModel();
                mList.addAll(c);
            }
            mListener.onGetPhotoSuccess(mAdapter);
        }else if(url.contains(URL_ADDIMAGE))
        {
            mList.add(nowPhotoInfo);
            mListener.onAddPhotoSuccess(mAdapter);
            nowPhotoInfo=null;
        }else if(url.contains(URL_DELETEIMAGE))
        {
           for (PhotoInfoBean bean:mList)
           {
               if(bean.getImageUrl().equals(nowPhotoInfo.getImageUrl()))
               {
                   mList.remove(bean);
                   break;
               }
           }
            mListener.onDeletePhotoSuccess(mAdapter);
            nowPhotoInfo=null;
        }
        
    }

    @Override
    public void onFailure(String url, ResultModel resultModel) {
        super.onFailure(url,resultModel);
        if(url.contains(URL_MYIMAGE)) {
        
            mListener.onGetPhotoError(resultModel.getMessage());
        }   else if(url.contains(URL_ADDIMAGE)) {

            mListener.onAddPhotoError(resultModel.getMessage());
        }else if(url.contains(URL_DELETEIMAGE)) {

            mListener.onDeletePhotoError(resultModel.getMessage());
        }
    }

    public void addPhoto(int userId,String path)
    {
        if (path != null) {
            nowPhotoInfo=new PhotoInfoBean();
            nowPhotoInfo.setImageUrl(path);
            requestAddImage(userId,path);
        }
    }

    public void deletePhoto(PhotoInfoBean bean)
    {
        if (bean.getImageUrl() != null && bean.getId()!=0) {
            nowPhotoInfo=bean;
            requestDeleteImage(bean.getUserId(),bean.getId());
        }
    }
    
    public  interface  onMyPhotoListener
    {
        void onAddPhotoSuccess(MyPhotoListAdapter mAdapter);
        void onAddPhotoError(String msg);
        void onGetPhotoSuccess(MyPhotoListAdapter mAdapter);
        void onGetPhotoError(String msg);
        void onDeletePhotoSuccess(MyPhotoListAdapter mAdapter);
        void onDeletePhotoError(String msg);
    }
}
