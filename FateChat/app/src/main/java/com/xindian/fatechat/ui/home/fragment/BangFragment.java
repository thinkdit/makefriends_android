package com.xindian.fatechat.ui.home.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.common.pullrefreshview.PullToRefreshView;
import com.xindian.fatechat.R;
import com.xindian.fatechat.common.Constant;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.ui.base.BaseFragment_v4;
import com.xindian.fatechat.ui.home.adapter.BangListAdapter;
import com.xindian.fatechat.ui.home.model.BangBean;
import com.xindian.fatechat.ui.home.model.BangUsersBean;
import com.xindian.fatechat.ui.home.presenter.BangListPresenter;
import com.xindian.fatechat.ui.user.UserDetailsActivity;

import java.util.List;

/**
 * 榜单
 */
public class BangFragment extends BaseFragment_v4{

    View headView;
    LinearLayout topLay1;
    LinearLayout topLay2;
    LinearLayout topLay3;

    TextView tab_this_week;
    TextView tab_last_week;
    TextView tab_this_week_s;
    TextView tab_last_week_s;
    TextView tab_get;
    TextView tab_send;

    ListView recyclerViewHot;
    PullToRefreshView pullToRefreshView;
    private BangListAdapter mBangListAdapter;
    BangListPresenter presenter;


    View view;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.bang_layout, container, false);
            recyclerViewHot = (ListView) view.findViewById(R.id.bang_recyclerview);
            headView = inflater.inflate(R.layout.layout_bang_head,null);
            topLay1 = (LinearLayout) headView.findViewById(R.id.lay_1);
            topLay2 = (LinearLayout) headView.findViewById(R.id.lay_2);
            topLay3 = (LinearLayout) headView.findViewById(R.id.lay_3);
            tab_this_week = (TextView)view.findViewById(R.id.tab_this_week);
            tab_last_week = (TextView) view.findViewById(R.id.tab_last_week);
            tab_this_week_s = (TextView) view.findViewById(R.id.tab_this_week_s);
            tab_last_week_s = (TextView) view.findViewById(R.id.tab_last_week_s);
            tab_get = (TextView) headView.findViewById(R.id.tab_get);
            tab_send = (TextView) headView.findViewById(R.id.tab_send);

            pullToRefreshView = (PullToRefreshView)view.findViewById(R.id.bang_list);
            pullToRefreshView.setPullDownEnable(false);
            pullToRefreshView.setPullUpEnable(false);
            recyclerViewHot.addHeaderView(headView);
            recyclerViewHot.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if(position>0){
                        position--;
                        BangBean bangBean = mBangListAdapter.getmDatas().get(position);
                        if(bangBean!=null && bangBean.getUserInfo()!=null && type!=BangListPresenter.TYPE_TYPE_FEMALE_SEND && type!=BangListPresenter.TYPE_TYPE_MALE_SEND){
                            goToUserDetail(getActivity(),(int)bangBean.getUserInfo().getUserId());
                        }
                    }
                }
            });
            tab_this_week.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!time.equals(BangListPresenter.TYPE_TIME_THIS)){
                        time = BangListPresenter.TYPE_TIME_THIS;
                        tab_this_week.setTextColor(getResources().getColor(R.color.text_pink_main));
                        tab_this_week_s.setBackgroundResource(R.drawable.shape_bg_pink);
                        tab_last_week.setTextColor(getResources().getColor(R.color.text_black_main));
                        tab_last_week_s.setBackgroundColor(getResources().getColor(R.color.transparent));
                        getDatas();
                    }
                }
            });
            tab_last_week.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!time.equals(BangListPresenter.TYPE_TIME_LAST)){
                        time = BangListPresenter.TYPE_TIME_LAST;
                        tab_last_week.setTextColor(getResources().getColor(R.color.text_pink_main));
                        tab_last_week_s.setBackgroundResource(R.drawable.shape_bg_pink);
                        tab_this_week.setTextColor(getResources().getColor(R.color.text_black_main));
                        tab_this_week_s.setBackgroundColor(getResources().getColor(R.color.transparent));
                        getDatas();
                    }
                }
            });

            tab_get.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    boolean isChanged = false;
                    if(sex!=null && sex.equals("1")){//男性
                        if(!type.equals(BangListPresenter.TYPE_TYPE_FEMALE_RECIVE)){
                            type = BangListPresenter.TYPE_TYPE_FEMALE_RECIVE;
                            isChanged = true;
                        }
                    }else{//女性
                        if(!type.equals(BangListPresenter.TYPE_TYPE_MALE_RECIVE)){
                            type = BangListPresenter.TYPE_TYPE_MALE_RECIVE;
                            isChanged = true;
                        }
                    }

                    if(isChanged){
                        tab_get.setBackgroundResource(R.drawable.shape_white_cycle_bg);
                        tab_send.setBackgroundResource(R.color.transparent);
                        getDatas();
                    }
                }
            });

            tab_send.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    boolean isChanged = false;
                    if(sex!=null && sex.equals("1")){//男性
                        if(!type.equals(BangListPresenter.TYPE_TYPE_MALE_SEND)){
                            type = BangListPresenter.TYPE_TYPE_MALE_SEND;
                            isChanged = true;
                        }
                    }else{//女性
                        if(!type.equals(BangListPresenter.TYPE_TYPE_FEMALE_SEND)){
                            type = BangListPresenter.TYPE_TYPE_FEMALE_SEND;
                            isChanged = true;
                        }
                    }

                    if(isChanged){
                        tab_send.setBackgroundResource(R.drawable.shape_white_cycle_bg);
                        tab_get.setBackgroundResource(R.color.transparent);
                        getDatas();
                    }
                }
            });

            topLay1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(bangBean1!=null && bangBean1.getUserInfo()!=null && type!=BangListPresenter.TYPE_TYPE_FEMALE_SEND && type!=BangListPresenter.TYPE_TYPE_MALE_SEND){
                        goToUserDetail(getActivity(),(int)bangBean1.getUserInfo().getUserId());
                    }
                }
            });
            topLay2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(bangBean2!=null && bangBean2.getUserInfo()!=null && type!=BangListPresenter.TYPE_TYPE_FEMALE_SEND && type!=BangListPresenter.TYPE_TYPE_MALE_SEND){
                        goToUserDetail(getActivity(),(int)bangBean2.getUserInfo().getUserId());
                    }
                }
            });
            topLay3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(bangBean3!=null && bangBean3.getUserInfo()!=null && type!=BangListPresenter.TYPE_TYPE_FEMALE_SEND && type!=BangListPresenter.TYPE_TYPE_MALE_SEND){
                        goToUserDetail(getActivity(),(int)bangBean3.getUserInfo().getUserId());
                    }
                }
            });

            initData();
        }
        return view;
    }

    private void goToUserDetail(Context context, int userID) {
        UserDetailsActivity.gotoUserDetails(context, userID, null);
    }

    private BangListPresenter getPresenter(){
        if(presenter == null){
            presenter = new BangListPresenter();
            presenter.setmOnBangLisener(new BangListPresenter.OnBangLisener() {
                @Override
                public void onGetBangListSuccess(List<BangBean> list) {
                    //对用户头像进行地址加参
                    for(int i=0;i<list.size();i++){
                        BangBean user = list.get(i);
                        BangUsersBean bangUsersBean = user.getUserInfo();
                        StringBuffer temp=new StringBuffer(bangUsersBean.getHeadimg());
                        temp.append(Constant.IMAGE_200);
                        bangUsersBean.setHeadimg(temp.toString());
                        user.setUserInfo(bangUsersBean);
                    }
                    BangBean bangBean1 = null;
                    BangBean bangBean2 = null;
                    BangBean bangBean3 = null;
                    if(list!=null){
                         if(list.size()>=3){
                            bangBean1 = list.remove(0);
                            bangBean2 = list.remove(0);
                            bangBean3 = list.remove(0);
                         }else if(list.size() == 2){
                             bangBean1 = list.remove(0);
                             bangBean2 = list.remove(0);
                         }else if(list.size() == 1){
                             bangBean1 = list.remove(0);
                         }
                    }

                    setHeadViewUI(bangBean1,bangBean2,bangBean3);
                    mBangListAdapter.setType(type);
                    mBangListAdapter.setDatas(list);


                }
            });
        }

        return presenter;
    }

    private String type = BangListPresenter.TYPE_TYPE_FEMALE_RECIVE;
    private String time = BangListPresenter.TYPE_TIME_THIS;
    private String sex = "1";
    protected void initData() {
        mBangListAdapter = new BangListAdapter(getActivity());
        recyclerViewHot.setAdapter(mBangListAdapter);

        sex = UserInfoManager.getManager(getContext()).getSex();
        if(sex!=null && sex.equals("1")){//男性
            type = BangListPresenter.TYPE_TYPE_FEMALE_RECIVE;
        }else{//女性
            type = BangListPresenter.TYPE_TYPE_MALE_RECIVE;
        }
        getDatas();

    }

    private void getDatas(){
        getPresenter();
        presenter.getBangList(time,type);
    }

    BangBean bangBean1;
    BangBean bangBean2;
    BangBean bangBean3;
    private void setHeadViewUI(BangBean bangBean1,BangBean bangBean2,BangBean bangBean3){
        this.bangBean1 = bangBean1;
        this.bangBean2 = bangBean2;
        this.bangBean3 = bangBean3;
        if(bangBean1==null && bangBean2==null && bangBean3==null){
            headView.setVisibility(View.GONE);
        }else{
            headView.setVisibility(View.VISIBLE);
            if(bangBean1!=null && bangBean1.getUserInfo()!=null){
                topLay1.setVisibility(View.VISIBLE);
                //头像
                Glide.with(view.getContext()).load(bangBean1.getUserInfo().getHeadimg()).diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into((ImageView)headView.findViewById(R.id.head_1));
                //名字
                ((TextView)headView.findViewById(R.id.name_1)).setText(bangBean1.getUserInfo().getNickName());
                //钻石
                String zuanshiStr = "";
                if(type.equals(BangListPresenter.TYPE_TYPE_FEMALE_RECIVE) || type.equals(BangListPresenter.TYPE_TYPE_MALE_RECIVE)){
                    zuanshiStr = "收到";
                }else{
                    zuanshiStr = "送出";
                }
                zuanshiStr = zuanshiStr+bangBean1.getDiamonds()+"颗钻石的礼物";
                ((TextView)headView.findViewById(R.id.gift_detail_1)).setText(zuanshiStr);
                //年纪
                ((TextView)headView.findViewById(R.id.age_1)).setText(bangBean1.getUserInfo().getAge()+"");

                String sexStr = bangBean1.getUserInfo().getSex();
                if (sexStr.equalsIgnoreCase("2")) {
                    ((LinearLayout)headView.findViewById(R.id.layout_age1)).setBackgroundResource(R.drawable.shape_txt_half_circle_pink_gril);
                    ((ImageView)headView.findViewById(R.id.image_age_1)).setBackgroundResource(R.drawable.gender);
                } else {
                    ((LinearLayout)headView.findViewById(R.id.layout_age1)).setBackgroundResource(R.drawable.shape_txt_half_circle_pink_boy);
                    ((ImageView)headView.findViewById(R.id.image_age_1)).setBackgroundResource(R.drawable.gender_man);
                }

            }else{
                topLay1.setVisibility(View.INVISIBLE);
            }

            if(bangBean2!=null && bangBean2.getUserInfo()!=null){
                topLay2.setVisibility(View.VISIBLE);
                //头像
                Glide.with(view.getContext()).load(bangBean2.getUserInfo().getHeadimg()).diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into((ImageView)headView.findViewById(R.id.head_2));
                //名字
                ((TextView)headView.findViewById(R.id.name_2)).setText(bangBean2.getUserInfo().getNickName());
                //钻石
                String zuanshiStr = "";
                if(type.equals(BangListPresenter.TYPE_TYPE_FEMALE_RECIVE) || type.equals(BangListPresenter.TYPE_TYPE_MALE_RECIVE)){
                    zuanshiStr = "收到";
                }else{
                    zuanshiStr = "送出";
                }
                zuanshiStr = zuanshiStr+bangBean2.getDiamonds()+"颗钻石的礼物";
                ((TextView)headView.findViewById(R.id.gift_detail_2)).setText(zuanshiStr);
                //年纪
                ((TextView)headView.findViewById(R.id.age_2)).setText(bangBean2.getUserInfo().getAge()+"");

                String sexStr = bangBean2.getUserInfo().getSex();
                if (sexStr.equalsIgnoreCase("2")) {
                    ((LinearLayout)headView.findViewById(R.id.layout_age)).setBackgroundResource(R.drawable.shape_txt_half_circle_pink_gril);
                    ((ImageView)headView.findViewById(R.id.image_age_2)).setBackgroundResource(R.drawable.gender);
                } else {
                    ((LinearLayout)headView.findViewById(R.id.layout_age)).setBackgroundResource(R.drawable.shape_txt_half_circle_pink_boy);
                    ((ImageView)headView.findViewById(R.id.image_age_2)).setBackgroundResource(R.drawable.gender_man);
                }

            }else{
                topLay2.setVisibility(View.INVISIBLE);
            }

            if(bangBean3!=null && bangBean3.getUserInfo()!=null){
                topLay3.setVisibility(View.VISIBLE);
                //头像
                Glide.with(view.getContext()).load(bangBean3.getUserInfo().getHeadimg()).diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into((ImageView)headView.findViewById(R.id.head_3));
                //名字
                ((TextView)headView.findViewById(R.id.name_3)).setText(bangBean3.getUserInfo().getNickName());
                //钻石
                String zuanshiStr = "";
                if(type.equals(BangListPresenter.TYPE_TYPE_FEMALE_RECIVE) || type.equals(BangListPresenter.TYPE_TYPE_MALE_RECIVE)){
                    zuanshiStr = "收到";
                }else{
                    zuanshiStr = "送出";
                }
                zuanshiStr = zuanshiStr+bangBean3.getDiamonds()+"颗钻石的礼物";
                ((TextView)headView.findViewById(R.id.gift_detail_3)).setText(zuanshiStr);
                //年纪
                ((TextView)headView.findViewById(R.id.age_3)).setText(bangBean3.getUserInfo().getAge()+"");
                String sexStr = bangBean3.getUserInfo().getSex();
                if (sexStr.equalsIgnoreCase("2")) {
                    ((LinearLayout)headView.findViewById(R.id.layout_age3)).setBackgroundResource(R.drawable.shape_txt_half_circle_pink_gril);
                    ((ImageView)headView.findViewById(R.id.image_age_3)).setBackgroundResource(R.drawable.gender);
                } else {
                    ((LinearLayout)headView.findViewById(R.id.layout_age3)).setBackgroundResource(R.drawable.shape_txt_half_circle_pink_boy);
                    ((ImageView)headView.findViewById(R.id.image_age_3)).setBackgroundResource(R.drawable.gender_man);
                }
            }else{
                topLay3.setVisibility(View.INVISIBLE);
            }

        }

    }

    @Override
    public void onStart() {
        super.onStart();
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

}
