package com.xindian.fatechat.ui.home.model;

import com.xindian.fatechat.common.BaseModel;

import java.io.Serializable;

/**
 * 相册列表
 */

public class PictureModel extends BaseModel implements Serializable, Cloneable {
    private String albumUrl;
    private String createTime;
    private long activityPhotoId;
    private long id;
    private String spare1;
    private String spare2;

    public String getAlbumUrl() {
        return albumUrl;
    }

    public void setAlbumUrl(String albumUrl) {
        this.albumUrl = albumUrl;
    }

    public long getActivityPhotoId() {
        return activityPhotoId;
    }

    public void setActivityPhotoId(long activityPhotoId) {
        this.activityPhotoId = activityPhotoId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSpare1() {
        return spare1;
    }

    public void setSpare1(String spare1) {
        this.spare1 = spare1;
    }

    public String getSpare2() {
        return spare2;
    }

    public void setSpare2(String spare2) {
        this.spare2 = spare2;
    }
}
