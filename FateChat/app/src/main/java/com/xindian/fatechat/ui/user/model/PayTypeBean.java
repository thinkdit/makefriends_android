package com.xindian.fatechat.ui.user.model;

import com.xindian.fatechat.common.BaseModel;

/**
 * Created by hx_Alex on 2017/6/9.
 */

public class PayTypeBean extends BaseModel {

    /**
     * id : 3
     * name : 支付宝
     * code : ZFB
     * type : 2
     * status : 1
     * channelId : null
     * createTime : 1496807231000
     * lastUpdateTime : 1496829288000
     */

    private int id;
    private String name;
    private String code;
    private int type;
    private int status;
    private String channelId;
    private long createTime;
    private long lastUpdateTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public long getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(long lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }
}
