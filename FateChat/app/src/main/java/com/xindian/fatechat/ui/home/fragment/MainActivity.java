package com.xindian.fatechat.ui.home.fragment;


import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.hyphenate.EMConnectionListener;
import com.hyphenate.EMError;
import com.hyphenate.EMMessageListener;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMConversation;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.easeui.EaseConstant;
import com.hyphenate.easeui.domain.ChatUserEntity;
import com.hyphenate.easeui.domain.LocalMessage;
import com.hyphenate.easeui.domain.LocalSession;
import com.hyphenate.easeui.interfaces.IContact;
import com.hyphenate.easeui.manager.ContactManager;
import com.hyphenate.easeui.model.EventBusModel;
import com.hyphenate.easeui.utils.DBHelper;
import com.hyphenate.easeui.utils.MessageHelper;
import com.hyphenate.util.EMLog;

import com.igexin.sdk.PushManager;
import com.thinkdit.lib.util.SharePreferenceUtils;
import com.umeng.analytics.MobclickAgent;
import com.xindian.fatechat.IMApplication;
import com.xindian.fatechat.R;
import com.xindian.fatechat.databinding.ActivityMainBinding;
import com.xindian.fatechat.manager.LoginManager;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.ui.ChatActivity;
import com.xindian.fatechat.ui.base.BaseActivity;
import com.xindian.fatechat.ui.home.model.RulesModel;
import com.xindian.fatechat.ui.login.LoginDialog;
import com.xindian.fatechat.ui.login.presenter.AccountPresenter;
import com.xindian.fatechat.ui.update.SystemParamsPresenter;
import com.xindian.fatechat.ui.user.CashActivity;
import com.xindian.fatechat.ui.user.PersonaldataAcitvity;
import com.xindian.fatechat.ui.user.UserDetailsActivity;
import com.xindian.fatechat.ui.user.model.User;
import com.xindian.fatechat.util.DisplayUtil;
import com.xindian.fatechat.widget.StarTDialog;
import com.xindian.fatechat.widget.player.VideoPlayerHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.beecloud.BeeCloud;


public class MainActivity extends BaseActivity implements RadioGroup.OnCheckedChangeListener,
         LoginDialog.onLoginListener {
    private static final int REQUEST_PERMISSION = 0;
    private ActivityMainBinding mBinding;
    private Fragment homeFragment, imFragment, meFragment, mBangFragment,mComFragment;
    private List<Fragment> fragmentList;
    private boolean isClearCheck = false;//clearCheck时不响应onCheckedChanged
    private   FragmentManager fragmentManager;
    private  Fragment nowShowFragment;
    public  static final String ONLOGINOUTBYSET="onLoginOutBySet";
    public  static final String SHOWEDITCHATWINDOW="showEditChatWindow";
    public  static final String HIDEEDITCHATWINDOW="hideEditChatWindow";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
//        initFragment();
        MobclickAgent.setDebugMode(true);
        intiBeecloudPay();//初始化Beecloud支付
        intiIM();
        EventBus.getDefault().register(this);
        intiTab();
        intiView();
        requestUpVesion();
        setAlias();

        VideoPlayerHelper.init(this);

        String status = SharePreferenceUtils.getString(this, EaseConstant.LIMIT_COM_STATUS);
        if(status!=null && status.equals(RulesModel.KEY_NOT_DY_STATUS)){
            //影藏社区
            hideCom();
        }
    }

    //影藏社区
    private void hideCom(){
        mBinding.tabCom.setVisibility(View.GONE);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)mBinding.unreadMsgNumber.getLayoutParams();
        params.leftMargin = DisplayUtil.dip2px(this,130);
        mBinding.unreadMsgNumber.setLayoutParams(params);
    }
    
    @Override
    protected void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
      
    }

    EMConnectionListener connectionListener = new EMConnectionListener() {
        @Override
        public void onDisconnected(int error) {
            EMLog.d("global listener", "onDisconnect" + error);
            if (error == EMError.USER_REMOVED) {// 显示帐号已经被移除
            } else if (error == EMError.USER_LOGIN_ANOTHER_DEVICE) {// 显示帐号在其他设备登录
            } else if (error == EMError.SERVER_SERVICE_RESTRICTED) {
            }
        }

        @Override
        public void onConnected() {
            if(!DBHelper.intance().isInit()) {
                DBHelper.intance().init(IMApplication.getInstance(), UserInfoManager.getManager
                        (MainActivity.this).getUserInfo().getUserId());
            }
        }
    };

    EMMessageListener messageListener = new EMMessageListener() {

        @Override
        public void onMessageReceived(List<EMMessage> messages) {
            // notify new message
            refreshUIWithMessage();
        }

        @Override
        public void onCmdMessageReceived(List<EMMessage> messages) {
            //red packet code : 处理红包回执透传消息
            refreshUIWithMessage();
        }

        @Override
        public void onMessageRead(List<EMMessage> messages) {
        }

        @Override
        public void onMessageDelivered(List<EMMessage> message) {
        }

        @Override
        public void onMessageChanged(EMMessage message, Object change) {
        }
    };

    private void refreshUIWithMessage() {
        runOnUiThread(new Runnable() {
            public void run() {
                if (imFragment instanceof ImFragment) {
                    ImFragment fragment = (ImFragment) imFragment;
                    fragment.refresh();
                }

            }
        });
    }

    private void loadUnReadMsg() {
        List<LocalMessage> messages = new ArrayList<>();
        HashMap<String, LocalSession> sessions = new HashMap<>();
        String addSessions = "";
        int total = 0;
        for (EMConversation conversation : EMClient.getInstance().chatManager()
                .getAllConversations().values()) {
            int count = conversation.getUnreadMsgCount();
            total = total + count;
            if (count == 0) {
                continue;
            }
            EMMessage lastMessage = conversation.getLastMessage();
            conversation.getAllMessages();
            List<EMMessage> list = conversation.loadMoreMsgFromDB(lastMessage.getMsgId(), count -
                    1);
            list.add(lastMessage);
            int size = list != null ? list.size() : 0;
            if (size < count) {
                count = size;
            }
            if (size > 0 && count > 0) {
                for (int i = 0; i < count; i++) {
                    EMMessage msg = list.get(size - i - 1);
                    LocalMessage localMessage = MessageHelper.ServerToLocalMsg(msg);
                    if (localMessage != null) {
                        messages.add(localMessage);
                    }
                    if (sessions.get(localMessage.getUserId()) == null) {
                        LocalSession session = MessageHelper.ServerToLocalSession(msg);
                        sessions.put(String.valueOf(localMessage.getUserId()), session);
                    } else {
                        LocalSession session = sessions.get(localMessage.getUserId());
                        session.setUnReadCount(session.getUnReadCount() + 1);
                    }
                }
            }
        }

        if (messages != null && messages.size() > 0) {
            DBHelper.intance().insertLocalMessageDao(messages);
        }
        if (sessions != null && sessions.size() > 0) {
            DBHelper.intance().insertLocalSessionDao(new ArrayList<>(sessions.values()));
        }
        setUnReadCount(total);
    }

    private void intiView() {
        //组装fragment
        homeFragment = HomeFragment.newInstance();
        imFragment = ImFragment.newInstance();
        meFragment = MeFragment.newInstance();
        mBangFragment = new BangFragment();
        mComFragment = new ComFragment();
        fragmentManager= getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.add(mBinding.layoutMainContent.getId(),homeFragment);//默认先添加首页
        ft.add(mBinding.layoutMainContent.getId(), mBangFragment);//默认先添加首页
        ft.add(mBinding.layoutMainContent.getId(),imFragment);//默认先添加首页
        ft.add(mBinding.layoutMainContent.getId(),mComFragment);//默认先添加首页
        ft.add(mBinding.layoutMainContent.getId(),meFragment);//默认先添加首页
        ft.hide(mBangFragment);
        ft.hide(imFragment);
        ft.hide(mComFragment);
        ft.hide(meFragment);
        ft.commit();
        nowShowFragment=homeFragment;
        mBinding.layoutTab.check(mBinding.tabHome.getId());//默认进入首页时选中的为首页
    }

    /**
     *聊天界面列表操作面板按钮点击事件
     * 消息发送至imfragment
     * @param view
     */
    public void onClickEditChatList(View view)
    {
        EventBusModel model=new EventBusModel();
        if(view.getId()==mBinding.btnDelete.getId()){
            model.setAction(ImFragment.DELETE);
        }else if(view.getId()==mBinding.btnRead.getId()){
            model.setAction(ImFragment.STATEREAD);
        }
        EventBus.getDefault().post(model);
      
    }

    private void intiIM()
    {
        AccountPresenter mPresenter = new AccountPresenter(this);
        mPresenter.loginIM();
        EMClient.getInstance().addConnectionListener(connectionListener);
        EMClient.getInstance().chatManager().addMessageListener(messageListener);
//        if (EMClient.getInstance().isConnected()) {
//            loadUnReadMsg();
//        }
        LoginManager.instance(this).registerLoginListerer(this);
        ContactManager.instance().setiContact(new IContact() {
            @Override
            public void goToUserDetail(Context context, int userID) {
                int myUserId = UserInfoManager.getManager(MainActivity.this).getUserId();
                if(myUserId == userID){
                    Intent i = new Intent(context, PersonaldataAcitvity.class);
                    startActivity(i);
                }else{
                    UserDetailsActivity.gotoUserDetails(context, userID, null);
                }
            }

            @Override
            public void gotoCharge(Context context, ViewGroup group) {
                StarTDialog dialog = new StarTDialog(context, group);
                dialog.setBtnOkCallback(new StarTDialog.iDialogCallback() {
                    @Override
                    public boolean onBtnClicked() {
                        Intent i = new Intent(MainActivity.this, CashActivity.class);
                        i.putExtra(CashActivity.ENTRYSOURCE,"聊天界面");
                        startActivity(i);
                        return true;
                    }
                });
                dialog.show("提示", "立刻充值vip，查看并获取用户联系方式，享受更多特权?", "立即充值", "取消");
            }
        });

    }


    private void intiTab() {
        mBinding.layoutTab.setOnCheckedChangeListener(this);
    }


    /***
     * 设置设备别名
     */
    public void setAlias()
    {
        int userId = UserInfoManager.getManager(this).getUserId();
        if(userId!=0) {
            boolean b = PushManager.getInstance().bindAlias(this, String.valueOf(userId));
        }
    }
 




    @Override
    protected void onResume() {
        super.onResume();
        Log.e("msg","onResume");
        if(!DBHelper.intance().isInit()) {
            DBHelper.intance().init(IMApplication.getInstance(), UserInfoManager.getManager
                    (MainActivity.this).getUserInfo().getUserId());
        }
        mBinding.layoutTab.setOnCheckedChangeListener(this);
    }



    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if (isClearCheck) return;
        mBinding.tabMe.setTextColor(this.getResources().getColor(R.color.text_gray_main));
        mBinding.tabIm.setTextColor(this.getResources().getColor(R.color.text_gray_main));
        mBinding.tabHome.setTextColor(this.getResources().getColor(R.color.text_gray_main));
        mBinding.tabCom.setTextColor(this.getResources().getColor(R.color.text_gray_main));
        mBinding.tabActivity.setTextColor(this.getResources().getColor(R.color.text_gray_main));
        if (checkedId == mBinding.tabHome.getId()) {
            showFragment(nowShowFragment,homeFragment);
            mBinding.tabHome.setTextColor(this.getResources().getColor(R.color.colorAccent));
        } else if (checkedId == mBinding.tabActivity.getId()) {
            showFragment(nowShowFragment, mBangFragment);
            mBinding.tabActivity.setTextColor(this.getResources().getColor(R.color.colorAccent));
        } else if (checkedId == mBinding.tabIm.getId()) {

            showFragment(nowShowFragment,imFragment);
            mBinding.tabIm.setTextColor(this.getResources().getColor(R.color.colorAccent));
        } else if (checkedId == mBinding.tabCom.getId()) {

            showFragment(nowShowFragment,mComFragment);
            mBinding.tabCom.setTextColor(this.getResources().getColor(R.color.colorAccent));
        } else if (checkedId == mBinding.tabMe.getId()) {
            showFragment(nowShowFragment,meFragment);
            mBinding.tabMe.setTextColor(this.getResources().getColor(R.color.colorAccent));
        }
    }
    
    private  void showFragment(Fragment formFragment,Fragment toFragment)
    {
        if(fragmentManager!=null)
        {
              FragmentTransaction ft=fragmentManager.beginTransaction();
//            ft.replace(mBinding.layoutMainContent.getId(),toFragment);
//            ft.commit();
            if(nowShowFragment!=toFragment)
            {
                nowShowFragment=toFragment;
                if(toFragment.isAdded())
                {
                    ft.hide(formFragment).show(toFragment).commitAllowingStateLoss();
                }else 
                {
                    ft.hide(formFragment).add(mBinding.layoutMainContent.getId(),toFragment).commit();
                }
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exitByDoubleClick();
        }
        return false;
    }
    
    private boolean isExit;
    private  Toast mToast;
    private void exitByDoubleClick() {
        moveTaskToBack(false);

    }

    //请求新版本
    private void requestUpVesion(){
        new SystemParamsPresenter(this, true).requestAppVersion();
    }


    public void setUnReadCount(int num) {
        Log.d("****", "num = " + num);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (num > 0) {
                    mBinding.unreadMsgNumber.setText(num + "");
                    mBinding.unreadMsgNumber.setVisibility(View.VISIBLE);
                } else {
                    mBinding.unreadMsgNumber.setVisibility(View.GONE);
                }
            }
        });
      
    }

    /***
     * 回调为IM登录成功后的回调
     * @param user
     */
    @Override
    public void LoginSucceed(User user) {
        if (UserInfoManager.getManager(MainActivity.this).getUserInfo() != null) {
            DBHelper.intance().init(IMApplication.getInstance(), UserInfoManager.getManager
                    (MainActivity.this).getUserInfo().getUserId());
        }
        loadUnReadMsg();
        showImByNotication();
    }

    @Override
    public void LoginError(String msg) {

    }

    /***
     * 初始化Beecloud支付
     */
    private  void intiBeecloudPay()
    {
        //测试模式
        BeeCloud.setSandbox(true);
        BeeCloud.setAppIdAndSecret(getResources().getString(R.string.beecloud_appid),
                getResources().getString(R.string.test_beecloud_secret));
    }

    /***
     * 暂只处理当在设置界面退出时，finsh主界面
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void  eventBusCallback(String msg)
    {
        if(ONLOGINOUTBYSET.equals(msg))
        {
            finish();
        }
    }

    /***
     * 事件来源ImFragment,编辑事件
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void  eventBusEditChatWindow(EventBusModel model)
    {
        if(model.getAction().equals(SHOWEDITCHATWINDOW))
        {
            mBinding.editChatLayout.setVisibility(View.VISIBLE);
        }else if(model.getAction().equals(HIDEEDITCHATWINDOW))
        {
            mBinding.editChatLayout.setVisibility(View.GONE);
        }
    }



    public void showImByNotication()
    {

        Intent intent = new Intent(this, ChatActivity.class);
        ChatUserEntity chatUserEntity = (ChatUserEntity) getIntent().getSerializableExtra(EaseConstant.EXTRA_CHAT_USER);
        if(chatUserEntity!=null)
        {
            intent.putExtra(EaseConstant.EXTRA_CHAT_USER,chatUserEntity);
            startActivity(intent);
        }
     
    }
    public void setRootViewBackGroud(int resId)
    {
        mBinding.layoutMainContent.setBackgroundResource(resId);
      
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        VideoPlayerHelper.getInstance().stop();
    }

    @Override
    public void onPause() {
        super.onPause();
        VideoPlayerHelper.getInstance().pause();
    }
}
