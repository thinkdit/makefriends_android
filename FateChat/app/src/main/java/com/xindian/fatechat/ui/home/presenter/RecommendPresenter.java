package com.xindian.fatechat.ui.home.presenter;

import android.content.Context;
import android.os.RemoteException;

import com.alibaba.fastjson.JSON;
import com.xindian.fatechat.common.BasePresenter;
import com.xindian.fatechat.common.ResultModel;
import com.xindian.fatechat.manager.LocationManager;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.ui.user.model.NearByUser;
import com.xindian.fatechat.ui.user.model.User;
import com.xindian.fatechat.widget.MyProgressDialog;

import org.json.JSONException;

import java.util.HashMap;
import java.util.List;

/**
 * Created by hx_Alex on 2017/4/1.
 */

public class RecommendPresenter extends BasePresenter {
    private final String URL_NEARBY_LIST = "/auth/nearby_user_list_v2";
    private final String URL_RECOM_USER_LIST = "/auth/recom_user_list_v2";
    private final String URL_HI_USER_LIST = "/auth/getSimUserByRandom";
    private onRecommendList getFriendsCaseListener;

    private MyProgressDialog mDialog;
    private Context context;

    private final int PAGE_SIZE = 18;

    private int mNearByPage = 1;
    private int mRecommendPage = 1;

    public RecommendPresenter(Context context, onRecommendList listener) {
        this.context = context;
        this.getFriendsCaseListener = listener;
        mDialog = new MyProgressDialog(context);
    }

    public void requestGetFriendsCase(User user) {
        HashMap<String, Object> data = new HashMap<>();
        data.put("userId", user.getUserId());
        get(getUrl(URL_RECOM_USER_LIST), data, null);
        mDialog.show();
    }

    public void nearby_user_list(boolean isRefresh) {
        if (isRefresh) {
            mNearByPage = 1;
        } else {
          mNearByPage=++mNearByPage;
        }
        HashMap<String, Object> data = new HashMap<>();
        if (UserInfoManager.getManager(context).isLogin()) {
            data.put("token", UserInfoManager.getManager(context).getToken());
            data.put("userId", UserInfoManager.getManager(context).getUserId());
        }
        data.put("address", LocationManager.instance().getAddress() == null ? "湖南省-长沙" :
                LocationManager.instance().getAddress());
        data.put("page", mNearByPage);
        data.put("pageSize", 10);
        post(getUrl(URL_NEARBY_LIST), data, null);
    }

    public void recommend_user(boolean isRefresh) {
      
        if (isRefresh) {
            mRecommendPage = 1;
        } else {
            mRecommendPage++;
        }
        HashMap<String, Object> data = new HashMap<>();
        if (UserInfoManager.getManager(context).isLogin()) {
            data.put("token", UserInfoManager.getManager(context).getToken());
            data.put("userId", UserInfoManager.getManager(context).getUserId());
        }
        data.put("page", mRecommendPage);
        data.put("pageSize", PAGE_SIZE);
        post(getUrl(URL_RECOM_USER_LIST), data, null);
    }

    public void getSayHiUser() {
        String sex = UserInfoManager.getManager(context).getSex();
        if(sex == null){
            sex = "1";
        }
        HashMap<String, Object> data = new HashMap<>();
        data.put("sex", sex);
        get(getUrl(URL_HI_USER_LIST), data, null);
    }


    @Override
    public Object asyncExecute(String url, ResultModel resultModel) {
       
        if (url.contains(URL_RECOM_USER_LIST)) {
          
            return JSON.parseArray(resultModel.getData(), User.class);
        } if (url.contains(URL_HI_USER_LIST)) {
            return JSON.parseArray(resultModel.getData(), User.class);
        } else if (url.contains(URL_NEARBY_LIST)) {
            return JSON.parseArray(resultModel.getData(), NearByUser.class);
        } else {

        }
        return null;
    }

    @Override
    public void onError(String url, Exception e) {
        super.onError(url, e);
    }


    @Override
    public void onFailure(String url, ResultModel resultModel) {
        super.onFailure(url,resultModel);
        if (url.contains(URL_RECOM_USER_LIST)) {
            getFriendsCaseListener.onGetRecommendListFail(resultModel.getMessage().equals("") ||
              resultModel.getMessage() == null ? "未知错误" : resultModel.getMessage());
        }
        if (url.contains(URL_HI_USER_LIST)) {
        }
        if (url.contains(URL_NEARBY_LIST)) {
            getFriendsCaseListener.onGetNearByListFail(resultModel.getMessage().equals("") ||
                    resultModel.getMessage() == null ? "未知错误" : resultModel.getMessage());
        }
//        mDialog.dismiss();
    }

    @Override
    public void onSucceed(String url, ResultModel resultModel) throws JSONException,
            RemoteException {
        if (url.contains(URL_RECOM_USER_LIST)) {
            List<User> dataModel = (List<User>) resultModel.getDataModel();
          
            getFriendsCaseListener.onGetRecommendListSuccess(dataModel, mRecommendPage == 1 ?
                    true : false, dataModel != null ? dataModel.size() == PAGE_SIZE : false);
        }
        else if (url.contains(URL_HI_USER_LIST)) {
            List<User> dataModel = (List<User>) resultModel.getDataModel();
            getFriendsCaseListener.onGetHiListSuccess(dataModel);
        }
        else if (url.contains(URL_NEARBY_LIST)) {
            List<NearByUser> dataModel = (List<NearByUser>) resultModel.getDataModel();
            getFriendsCaseListener.onGetNearByListSuccess(dataModel, mNearByPage == 1 ? true :
                    false, dataModel != null ? dataModel.size() == PAGE_SIZE : false);
        }
//        mDialog.dismiss();
    }


    public interface onRecommendList {
        void onGetRecommendListSuccess(List<User> list, boolean isFirst, boolean hasMore);
        void onGetRecommendListFail(String msg);
        void onGetNearByListSuccess(List<NearByUser> list, boolean isFirst, boolean hasMore);
        void onGetNearByListFail(String msg);

        void onGetHiListSuccess(List<User> list);
    }
}
