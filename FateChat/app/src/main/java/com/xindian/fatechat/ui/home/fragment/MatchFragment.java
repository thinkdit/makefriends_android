package com.xindian.fatechat.ui.home.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.thinkdit.lib.util.SharePreferenceUtils;
import com.xindian.fatechat.R;
import com.xindian.fatechat.common.FateChatApplication;
import com.xindian.fatechat.databinding.MatchFragmentBinding;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.ui.base.BaseFragment_v4;
import com.xindian.fatechat.ui.home.adapter.MatchListAdapter;
import com.xindian.fatechat.ui.home.model.LikeUserBean;
import com.xindian.fatechat.ui.home.presenter.MatchPresenter;
import com.xindian.fatechat.ui.user.dialog.LikeDialog;
import com.xindian.fatechat.ui.user.model.User;
import com.xindian.fatechat.widget.MatchRemindDialog;
import com.xindian.fatechat.widget.cardSwipeLayout.CardSlidePanel;

import java.util.List;

/***
 * 撮合页面
 */
public class MatchFragment extends BaseFragment_v4 implements MatchPresenter.GetMatchListListener,CardSlidePanel.CardSwitchListener,MatchPresenter.GetMatchLikeListener{
    public  static final  int  DEFALUT_SIZE=50;
    public  static final  int  DEFALUT_LIKE=100;//默认喜欢数量，达到此数余数时自动喜欢-模拟用户
 private MatchFragmentBinding mBinding;
  private MatchListAdapter mAdapter;
    private MatchPresenter mPresenter;
     private int page=0;
        private int swipeCount;
         private int swipeLikeCount;
          private boolean hasMore;
           private int userId;
            private int nowIndex;
             private boolean isRealUesr;
     
    public static MatchFragment newInstance() {
        MatchFragment matchFragment = new MatchFragment();
        Bundle bundle = new Bundle();
        matchFragment.setArguments(bundle);
        return matchFragment;
    }
    
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding= DataBindingUtil.inflate(inflater, R.layout.match_fragment,container,false);
        userId= UserInfoManager.getManager(getContext()).getUserId();
        mPresenter=new MatchPresenter(getContext(),this);
        mPresenter.setGetMatchLikeListener(this);
        mPresenter.requestGetMatchList(userId,DEFALUT_SIZE,nextPage());

        return mBinding.getRoot();
    }
    
    private  void intiView(List<User> dataList)
    {
        mAdapter=new MatchListAdapter(dataList,getContext());
        mBinding.recyclerViewMatch.setAdapter(mAdapter);
        mBinding.btnLikes.setOnClickListener(v -> {onClickAnim(mBinding.btnLikes);
            mBinding.recyclerViewMatch.vanishOnBtnClick(CardSlidePanel.VANISH_TYPE_RIGHT);});
        mBinding.btnUnLike.setOnClickListener(v -> {onClickAnim(mBinding.btnUnLike);mBinding.recyclerViewMatch.vanishOnBtnClick(CardSlidePanel.VANISH_TYPE_LEFT);});
        mBinding.recyclerViewMatch.setCardSwitchListener(this);
    }


    MatchRemindDialog mReComDialog;
    public void doRemindCom() {
        if (!UserInfoManager.getManager(FateChatApplication.getInstance()).isLogin()) {
            return;
        }
        int userId = UserInfoManager.getManager(FateChatApplication.getInstance()).getUserId();
        boolean isHasSay = SharePreferenceUtils.getBoolean(FateChatApplication.getInstance(), SharePreferenceUtils.KEY_HAS_REM_MATCH + userId, false);
        if (!isHasSay) {
            if (mReComDialog == null) {
                mReComDialog = new MatchRemindDialog(getActivity());
            }
            mReComDialog.show();
        }

    }
    
    private int nextPage()
    {
        page++;
        return page;
    }
    
    
    
    @Override
    public void onGetMatchListSuccess(List<User> dataList, boolean hasMore) {
        this.hasMore=hasMore;
        if(dataList!=null)
        {
            if(page==1 && mAdapter==null)
            {
                intiView(dataList);
            }else if(mAdapter!=null) 
            {
                mAdapter.appendData(dataList);
                mAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onGetMatchListError(String msg) {
        Toast.makeText(getContext(),msg,Toast.LENGTH_LONG);
    }

    @Override
    public void onShow(int index) {
        
    }
    
    private  void checkCardCache()
    {
        int size = mAdapter.getDataList().size()-swipeCount;
        Log.e("checkCardCache","size:"+size);
        if(size<25 && hasMore)
        {
            mPresenter.requestGetMatchList(userId,DEFALUT_SIZE,nextPage());
            Log.e("checkCardCache","下一页数据");
            Toast.makeText(getContext(),"下一页数据",Toast.LENGTH_SHORT);
        }else if(size<25 && !hasMore)
        {
            page=0;
            mPresenter.requestGetMatchList(userId,DEFALUT_SIZE,nextPage());
            Log.e("checkCardCache","重新加载数据");
            Toast.makeText(getContext(),"重新加载数据",Toast.LENGTH_SHORT);
        }
    }

    @Override
    public void onCardVanish(int index, int type) {
        
//         mAdapter.getDataList().remove(0);
//         mAdapter.notifyDataSetChanged();
        //喜欢
        if(type==CardSlidePanel.VANISH_TYPE_RIGHT)
        {
            int otherUserId = mAdapter.getDataList().get(index).getUserId();
            mPresenter.requestGetMatchLike(this.userId,otherUserId);
            swipeLikeCount++;
        }else if(type==CardSlidePanel.VANISH_TYPE_LEFT)
        {
     
        }
        swipeCount++;
        nowIndex=index;
        checkCardCache();
        Log.e("swipeCount","swipeCount:"+swipeCount);
    }
    
    private void onClickAnim(View view)
    {
        Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.scale_set);
        view.startAnimation(animation);
    }

    @Override
    public void onGetMatchLikeSuccess(LikeUserBean bean) {

        LikeDialog dialog;
        //先检查是不是互相喜欢的
        if(bean.isLike() && bean.getUserType()!=null && bean.getHeadimg()!=null && bean.getNickName()!=null && bean.getOthersUserId()!=0)
        {
            dialog=new LikeDialog(getContext(),bean);
            dialog.show();
        }else {//如果不喜欢，检查滑动喜欢总数是否大于100,
            int count = swipeLikeCount >=DEFALUT_LIKE ? swipeLikeCount % DEFALUT_LIKE : 1;
            if (count == 0 || isRealUesr) {
                User user = mAdapter.getDataList().get(nowIndex);
                if (user.getUserType().equals(User.REALUSER)) {
                    isRealUesr = true;
                    return;
                }
                LikeUserBean likeUserBean=new LikeUserBean();
                likeUserBean.setHeadimg(user.getHeadimg());
                likeUserBean.setNickName(user.getNickName());
                likeUserBean.setOthersUserId(user.getUserId());
                likeUserBean.setUserType(user.getUserType());
                dialog=new LikeDialog(getContext(),likeUserBean);
                dialog.show();
                isRealUesr=false;
            }
        }
        
  
    }

    @Override
    public void onGetMatchLikeError(String msg) {

    }
}
