package com.xindian.fatechat.ui.user.model;

import com.xindian.fatechat.common.BaseModel;

import java.util.List;

/**
 * Created by hx_Alex on 2017/7/14.
 */

public class GiftRecordPageBean extends BaseModel {

    /**
     * code : ok
     * message : null
     * requestId : null
     * data : {"content":[{"id":1,"giverUserId":222,"receiverUserId":177,"giftId":1,"giftName":"么么哒","giftUrl":"http://img.dingsns.com/pub/20161015082221987_5763.png","giverUserName":"哈哈哈","receiverUserName":"路","giftPrice":1,"giftNum":2,"totalPrice":2,"createTime":1499842543000}],"number":1,"size":10,"total":1,"totalPage":1}
     */

    private String code;
    private String message;
    private String requestId;
    private List<GiftRecordBean> content;
    private int number;
    private int size;
    private int total;
    private int totalPage;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public List<GiftRecordBean> getContent() {
        return content;
    }

    public void setContent(List<GiftRecordBean> data) {
        this.content = data;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }
}
