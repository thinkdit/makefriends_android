package com.xindian.fatechat.ui.login.presenter;

import android.content.Context;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.hyphenate.EMCallBack;
import com.hyphenate.chat.EMClient;
import com.thinkdit.lib.base.ResultModelBase;
import com.umeng.analytics.MobclickAgent;
import com.xindian.fatechat.DemoHelper;
import com.xindian.fatechat.R;
import com.xindian.fatechat.common.BasePresenter;
import com.xindian.fatechat.common.ResultModel;
import com.xindian.fatechat.manager.LoginManager;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.ui.login.model.LoginBean;
import com.xindian.fatechat.ui.login.model.RegisterBean;
import com.xindian.fatechat.ui.user.model.User;
import com.xindian.fatechat.ui.user.presenter.FileUploadPresenter;
import com.xindian.fatechat.util.ChannelUtil;
import com.xindian.fatechat.util.Md5Util;
import com.xindian.fatechat.widget.MyProgressDialog;

import org.json.JSONException;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by hx_Alex on 2017/3/31.
 * 注册P类
 */

public class AccountPresenter extends BasePresenter {
    public final String URL_REGISTER = "/auth/register";
    private final String URL_lOGIN = "/auth/login";
    private final String URL_CHANGPASSWORD = "/auth/update_password";
    private final String URL_ACTIVATEUSERINFO = "/report/activate_user_info";//激活用户统计口径上报   
    private OnRegisterListener registerListren;
    private OnLoginListener onLoginListener;
    private Context context;
    private IUserInfoModifyListener mModifyListener;
    private FileUploadPresenter mImageUploadPresenter;
    private IAvatarUploadedListener mIAvatarUploadedListener;
    private ChangePasswordListener mChangePasswordListener;
    private MyProgressDialog mDialog;
    private String nowPassword;
    private String newPassword;

    public AccountPresenter(Context context) {
        this.context = context;
        mDialog = new MyProgressDialog(context);
    }

    public void setUserInfoModifyListener(IUserInfoModifyListener listener) {
        mModifyListener = listener;
    }

    public void setAvatarUploadedListener(IAvatarUploadedListener listener) {
        mIAvatarUploadedListener = listener;
    }

    public void setChangePasswordListener(ChangePasswordListener listener) {
        mChangePasswordListener = listener;
    }
    
    


    /**
     * 更新头像
     *
     * @param newAvatarUrl
     */
    public void commitAvatar(String newAvatarUrl) {
        Map<String, Object> map = new HashMap<>();
        map.put("avatarUrl", newAvatarUrl);
        map.put("id", UserInfoManager.getManager(context).getUserId());
//        post(getUrl(COMPLETE_USERINFO), map, mContext);
    }

    public void uploadAvatar(String avatarPath) {
        if (mImageUploadPresenter == null) {
            mImageUploadPresenter = new FileUploadPresenter(context, mIFileUploadedListener);
        }
        mImageUploadPresenter.startUpload(avatarPath, FileUploadPresenter.FILE_TYPE_HEAD);
    }

    public void reportUserDevice() {
        Map<String, Object> map = new HashMap<>();
        post(getUrl(URL_ACTIVATEUSERINFO), map, context);
    }


    public void requestRegister(User user) {
        HashMap<String, Object> data = new HashMap<>();
        //数据匹配后台数据
        String height = user.getHeight();
        if (user.getHeight() != null && user.getHeight().contains("cm")) {
            height = user.getHeight().substring(0, user.getHeight().indexOf("cm"));
        }
        data.put("age", user.getAge());
        data.put("headimg", user.getHeadimg());
        data.put("height", height);
        data.put("marriage", user.getMarriage());
        data.put("nickName", user.getNickName());
        data.put("profession", user.getProfession());
        data.put("qualifications", user.getQualifications());
        data.put("revenue", user.getRevenue());
        data.put("sex", user.getSex());
        data.put("releaseChannel", ChannelUtil.getChannel(context) == null ? "freeChannel" :
                ChannelUtil.getChannel(context));
        data.put("password", Md5Util.getMD5(user.getPassword()));
        post(getUrl(URL_REGISTER), data, null);

        //加载框不可取消
        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(false);

        mDialog.show();
    }

    public void loginIM() {
        if (UserInfoManager.getManager(context).getUserInfo().getImPassword() == null) {
            return;
        }
        DemoHelper.getInstance().setCurrentUserName(UserInfoManager.getManager(context)
                .getUserInfo().getUserId() + "");
        EMClient.getInstance().login(UserInfoManager.getManager(context).getUserInfo().getUserId
                () + "", UserInfoManager.getManager(context).getUserInfo().getImPassword(), new
                EMCallBack() {

            @Override
            public void onSuccess() {
                Log.d(TAG, "login: onSuccess");
                EMClient.getInstance().chatManager().loadAllConversations();
                LoginManager.instance(context).postLoginSuccess(null);
              
            }

            @Override
            public void onProgress(int progress, String status) {
                Log.d(TAG, "login: onProgress");
            }

            @Override
            public void onError(final int code, final String message) {
                Log.d(TAG, "login: onError  "+code+"   msg:"+message);
            }
        });
    }


    public void requestLogin(String loginId, String passWord) {
        HashMap<String, Object> data = new HashMap<>();
        data.put("loginId", loginId);
        data.put("password", Md5Util.getMD5(passWord));
        nowPassword = passWord;
        post(getUrl(URL_lOGIN), data, null);
        mDialog.show();
    }

    public void requestChangePassword(int userId, String oldPassword, String newPassword) {
        HashMap<String, Object> data = new HashMap<>();
        data.put("userId", userId);
        data.put("oldPassword", Md5Util.getMD5(oldPassword));
        data.put("newPassword", Md5Util.getMD5(newPassword));
        this.newPassword = newPassword;
        post(getUrl(URL_CHANGPASSWORD), data, null);
        mDialog.show();
    }

    @Override
    public Object asyncExecute(String url, ResultModel resultModel) {
        if (url.contains(URL_REGISTER)) {
            return JSON.parseObject(resultModel.getData(), RegisterBean.class);
        } else if (url.contains(URL_lOGIN)) {
            LoginBean loginBean = JSON.parseObject(resultModel.getData(), LoginBean.class);
            return loginBean;
        } else {

        }
        return null;
    }

    @Override
    public void onError(String url, Exception e) {
        super.onError(url, e);

        if(mDialog!=null){
            mDialog.dismiss();
        }
    }


    @Override
    public void onFailure(String url, ResultModel resultModel) {
        super.onFailure(url, resultModel);
        if (url.contains(URL_REGISTER)) {
            registerListren.registerError(resultModel.getMessage().equals("") || resultModel
                    .getMessage() == null ? "未知错误" : resultModel.getMessage());
        } else if (url.contains(URL_lOGIN)) {
            onLoginListener.LoginError(resultModel.getMessage().equals("") || resultModel
                    .getMessage() == null ? "未知错误" : resultModel.getMessage());
            nowPassword = null;

        } else if (url.contains(URL_CHANGPASSWORD)) {
            mChangePasswordListener.onChangPasswordError(resultModel.getMessage().equals("") ||
                    resultModel.getMessage() == null ? "未知错误" : resultModel.getMessage());
        }

        if(mDialog!=null){
            mDialog.dismiss();
        }
    }

    @Override
    public void onSucceed(String url, ResultModel resultModel) throws JSONException,
            RemoteException {
        if (url.contains(URL_REGISTER)) {
            User user = new User();
            RegisterBean dataModel = (RegisterBean) resultModel.getDataModel();
            user = user.insertData(dataModel);
            registerListren.registerSucceed(user);
        } else if (url.contains(URL_lOGIN)) {
            LoginBean dataModel = (LoginBean) resultModel.getDataModel();
            dataModel.getUserInfoVo().setImPassword(dataModel.getImPassword());
            dataModel.getUserInfoVo().setPassword(nowPassword);
            UserInfoManager.getManager(context).setLoginResult(dataModel);
            loginIM();
            onLoginListener.LoginSucceed(UserInfoManager.getManager(context).getUserInfo());
            nowPassword = null;

            if(dataModel!=null && dataModel.getUserInfoVo()!=null){
                MobclickAgent.onProfileSignIn(dataModel.getUserInfoVo().getUserId()+"");
            }
        } else if (url.contains(URL_CHANGPASSWORD)) {
            if (resultModel.getCode().equals(ResultModelBase.SUCCESS_CODE)) {
                //将新密码替换老密码保存
                UserInfoManager.getManager(context).setNewPassWordSharePreference(newPassword);
                mChangePasswordListener.onChangPasswordSucceed();
            }
        }

        if(mDialog!=null){
            mDialog.dismiss();
        }
    }

    private FileUploadPresenter.IFileUploadedListener mIFileUploadedListener = new
            FileUploadPresenter.IFileUploadedListener() {

        @Override
        public void onFileUploadProgress(long currentSize, long totalSize) {

        }

        @Override
        public void onFileUploadedSuccess(String url) {
            if (mIAvatarUploadedListener != null) {
                mIAvatarUploadedListener.onAvatarUploadedSucceed(url);
            }
        }

        @Override
        public void onFileUploadedFailure() {
            mIAvatarUploadedListener.onAvatarUploadedError();
            Toast.makeText(context, R.string.user_file_upload_error, Toast.LENGTH_LONG).show();
        }
    };


    public void setOnLoginListener(OnLoginListener onLoginListener) {
        this.onLoginListener = onLoginListener;
    }


    public void setRegisterListren(OnRegisterListener registerListren) {
        this.registerListren = registerListren;
    }

    /****************
     * 接口
     *************/
    public interface OnRegisterListener {
        void registerSucceed(User user);

        void registerError(String msg);
    }

    public interface OnLoginListener {
        void LoginSucceed(User user);

        void LoginError(String msg);
    }

    public interface IAvatarUploadedListener {
        void onAvatarUploadedSucceed(String url);
        void onAvatarUploadedError();
    }

    public interface IUserInfoModifyListener {
        public void onModifySucceed();
    }

    public interface ChangePasswordListener {
        void onChangPasswordSucceed();

        void onChangPasswordError(String msg);
    }
    
  
}
