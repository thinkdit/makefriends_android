package com.xindian.fatechat.ui.user.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hyphenate.easeui.model.EventBusModel;
import com.thinkdit.lib.util.SharePreferenceUtils;
import com.xindian.fatechat.R;
import com.xindian.fatechat.common.MVVPSetters;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.ui.login.SplashActivity;
import com.xindian.fatechat.ui.user.UpLoadHeadImgActivity;
import com.xindian.fatechat.ui.user.model.User;
import com.xindian.fatechat.util.ActivityStack;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Created by hx_Alex on 2017/7/20.
 */

public class UpLoadHeadImgDialog extends AlertDialog implements View.OnClickListener {
    public  static  final String  ACTION="UpLoadHeadImgDialog";
    private ImageView img_headImg;
    private TextView btn_upLoad;
    private TextView txt_message;
    private User user;
    private Context context;
    private String msg;
    private  int timeCount=3;
    private CloseDialogHandler handler;
    public UpLoadHeadImgDialog(Context context) {
        super(context, R.style.FullScreen_dialog);
        setCanceledOnTouchOutside(false);
        setCancelable(false);
        this.context=context;
        EventBus.getDefault().register(this);
    }
    
    
    public void setUser(User user) {
        this.user = user;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_upload_headimg);
        img_headImg= (ImageView) findViewById(R.id.img_headImg);
        btn_upLoad= (TextView) findViewById(R.id.tv_normal_dialog_bu);
        txt_message=(TextView)findViewById(R.id.tv_normal_dialog_text);
        MVVPSetters.imageLoader(img_headImg,user.getHeadimg(),context.getResources().getDrawable(R.drawable.defalut_user),(float)context.getResources().getDimension(R.dimen.aspect_40dp));
        btn_upLoad.setOnClickListener(this);
        if(msg!=null)
        {
            txt_message.setText(msg);
        }
    }

    @Override
    public void onClick(View v) {
        Intent i=new Intent(context, UpLoadHeadImgActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }

    /***
     * 上传回调
     * 事件来源 UpLoadHeadImgActivity
     * @param model
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public  void onEventBusWithUpLoadCallback(EventBusModel model)
    {
       
        //上传成功
       if(model.getAction().equals(ACTION) && ((int)model.getData())==1)
       {
           SharePreferenceUtils.putBoolean(context, UserInfoManager.KEY_ISSHOWEDUPLOADHEADIMG,true);
           txt_message.setText("您的头像信息已上传成功，等待审核中，审核通过后将予以显示!");
           btn_upLoad.setText(context.getResources().getString(R.string.close_dialog,timeCount));
           handler=new CloseDialogHandler();
           handler.sendEmptyMessageDelayed(0,1000);
       }   else if(model.getAction().equals(ACTION))
       {
           txt_message.setText("头像信息上传失败,请重新尝试上传");
       }
    }


    @Override
    public void dismiss() {
        super.dismiss();
        EventBus.getDefault().unregister(this);
    }

    //重写show ，show时acitivity不能为引导页
    @Override
    public void show() {
        Activity topActivity = ActivityStack.getInstance().getTopActivity();
        //
        if(topActivity instanceof SplashActivity)
        {
            Runnable runnable=new Runnable() {
                @Override
                public void run() {
                        show();
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            };
            runnable.run();
            return;
        }
        super.show();
    }


    class CloseDialogHandler extends Handler
    {
        @Override
        public void handleMessage(Message msg) {
            if(timeCount==0){
                dismiss();
            }else {
                timeCount = --timeCount;
                btn_upLoad.setText(context.getResources().getString(R.string.close_dialog,timeCount));
                sendEmptyMessageDelayed(0, 1000);
            }
        }
    }
    
    
}
