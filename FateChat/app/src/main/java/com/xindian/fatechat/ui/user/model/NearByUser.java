package com.xindian.fatechat.ui.user.model;

import java.io.Serializable;

/**
 * Created by hx_Alex on 2017/3/23.
 */

public class NearByUser extends User implements Serializable, Cloneable {
    private float distance;

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }
}
