package com.xindian.fatechat.util.glide;

public interface OnPercentToTarget {
    void onPercent(Float percent);
}