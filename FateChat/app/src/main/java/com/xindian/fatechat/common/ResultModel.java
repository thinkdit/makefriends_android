package com.xindian.fatechat.common;

import com.thinkdit.lib.base.ResultModelBase;

import okhttp3.Request;

/**
 * Created by QiuDa on 15/11/30.
 */
public class ResultModel extends BaseModel implements ResultModelBase {
    public static final String ERROR_CODE_ACCOUNT_BALANCE_NOT_ENOUGH = "AC0103";
    public static final String ERROR_GAME_ACCOUNT_BALANCE_NOT_ENOUGH = "AC0107";
    public static final String ERROR_CODE_KICK_LIVE_ROOM = "LV14";

    private String url;
    private String code;
    private String message;
    private String data;
    private Request request;
    private Object dataModel;
    private String requestId;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    /**
     * 请求的tag（预留）
     */
    private Object tag;

    private Exception e;

    public ResultModel() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public void setException(Exception e) {
        this.e = e;
    }

    @Override
    public Exception getException() {
        return e;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Object getDataModel() {
        return dataModel;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public Request getRequest() {
        return request;
    }

    public void setDataModel(Object dataModel) {
        this.dataModel = dataModel;
    }


    public Object getTag() {
        return tag;
    }

    public void setTag(Object tag) {
        this.tag = tag;
    }

    @Override
    public String toString() {
        return "ResultModel{" +
                "url='" + url + '\'' +
                ", code='" + code + '\'' +
                ", message='" + message + '\'' +
                ", data='" + data + '\'' +
                ", dataModel=" + dataModel +
                '}';
    }


}
