package com.xindian.fatechat.ui.home.presenter;

import android.content.Context;
import android.os.RemoteException;

import com.alibaba.fastjson.JSON;
import com.xindian.fatechat.common.BasePresenter;
import com.xindian.fatechat.common.ResultModel;
import com.xindian.fatechat.ui.home.fragment.MatchFragment;
import com.xindian.fatechat.ui.home.model.LikeUserBean;
import com.xindian.fatechat.ui.user.model.User;

import org.json.JSONException;

import java.util.HashMap;
import java.util.List;

/**
 * Created by hx_Alex on 2017/8/9.
 */

public class MatchPresenter extends BasePresenter {
    private Context context;
    private final String URL_MATCH_USET = "/match/match_user";
    private final String URL_MATCH_LIKE = "/match/match_like";
    private GetMatchLikeListener getMatchLikeListener;
    private GetMatchListListener getMatchListListener;
    public MatchPresenter(Context context,GetMatchListListener getMatchListListener) {
        this.context = context;
        this.getMatchListListener=getMatchListListener;
    }

    public void setGetMatchLikeListener(GetMatchLikeListener getMatchLikeListener) {
        this.getMatchLikeListener = getMatchLikeListener;
    }

    /***
     * 获取撮合列表
     */
    public  void requestGetMatchList(int userId,int pageSize,int page)
    {
        HashMap<String,Object> data=new HashMap<>();
        data.put("userId",userId);
        data.put("pageSize",pageSize);
        data.put("page",page);
        post(getUrl(URL_MATCH_USET),data,null);
    }

    /***
     * 滑动匹配喜欢
     *
     */
    public  void requestGetMatchLike(int userId,int otherId)
    {
        HashMap<String,Object> data=new HashMap<>();
        data.put("userId",userId);
        data.put("othersUserId",otherId);
        post(getUrl(URL_MATCH_LIKE),data,null);
    }

    @Override
    public Object asyncExecute(String url, ResultModel resultModel) {
        if(url.contains(URL_MATCH_USET))
        {
           return JSON.parseArray(resultModel.getData(),User.class);
        }else if(url.contains(URL_MATCH_LIKE)){
            return JSON.parseObject(resultModel.getData(), LikeUserBean.class);
        }
        return super.asyncExecute(url, resultModel);
    }

    @Override
    public void onSucceed(String url, ResultModel resultModel) throws JSONException, RemoteException {
        super.onSucceed(url, resultModel);
        if(url.contains(URL_MATCH_USET))
        {
            boolean isHasMore=false;
            List<User> dataModel = (List<User>) resultModel.getDataModel();
            if(dataModel!=null && dataModel.size()== MatchFragment.DEFALUT_SIZE)
            {
                isHasMore=true;
            }
            getMatchListListener.onGetMatchListSuccess(dataModel,isHasMore);
        }else if(url.contains(URL_MATCH_LIKE)){
            LikeUserBean bean= (LikeUserBean) resultModel.getDataModel();
            getMatchLikeListener.onGetMatchLikeSuccess(bean);
        } 
    }


    @Override
    public void onFailure(String url, ResultModel resultModel) {
        if(url.contains(URL_MATCH_USET))
        {
            getMatchListListener.onGetMatchListError(resultModel.getMessage().equals("") || resultModel.getMessage() == null ? "未知错误" : resultModel.getMessage());
        }else if(url.contains(URL_MATCH_LIKE)){
            getMatchLikeListener.onGetMatchLikeError(resultModel.getMessage().equals("") || resultModel.getMessage() == null ? "未知错误" : resultModel.getMessage());
        }

    }

    public  interface GetMatchListListener
    {
        void onGetMatchListSuccess(List<User> dataList, boolean hasMore);
        void onGetMatchListError(String msg);
    }

    public  interface GetMatchLikeListener
    {
        void onGetMatchLikeSuccess(LikeUserBean bean);
        void onGetMatchLikeError(String msg);
    }
}
