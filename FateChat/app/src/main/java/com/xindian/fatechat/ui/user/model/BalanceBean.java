package com.xindian.fatechat.ui.user.model;

import com.xindian.fatechat.common.BaseModel;

/**
 * 余额model
 */

public class BalanceBean extends BaseModel {

    /**
     "creationTime": "2017-07-14T08:34:12.189Z",
     "diamonds": 0,
     "modifyTime": "2017-07-14T08:34:12.189Z",
     "spare1": "string",
     "spare2": "string",
     "uid": 0
     */

    private int uid;
    private String creationTime;
    private String modifyTime;
    private int diamonds;
    private String spare1;
    private String spare2;

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(String creationTime) {
        this.creationTime = creationTime;
    }

    public String getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }

    public int getDiamonds() {
        return diamonds;
    }

    public void setDiamonds(int diamonds) {
        this.diamonds = diamonds;
    }

    public String getSpare1() {
        return spare1;
    }

    public void setSpare1(String spare1) {
        this.spare1 = spare1;
    }

    public String getSpare2() {
        return spare2;
    }

    public void setSpare2(String spare2) {
        this.spare2 = spare2;
    }
}
