package com.xindian.fatechat.ui.home.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 图片库（一组图片）
 * Created by DP on 2016/9/18.
 */
public class Gallery implements Serializable {
    private static final long serialVersionUID = -7505467711968557340L;
    public static final String TYPE_VIDEO = "video";
    public static final String TYPE_LIVE = "live";
    public static final String TYPE_IMAGE = "image";
    public static final String TYPE_FANHAO = "fanhao";
    private long id;
    private int galleryclass;//          图片分类
    public String title;//          标题
    public String img;//图片
    public int imgLacol;//本地图片
    public String categoryUrl;//分类链接
    public String webUrl;//网页链接
    public String videoUrl;//视屏播放地址
    public String videoPreImageUrl;//视屏封面地址
    public String type;//媒体类型
    public String time;//分类链接
    public String count;//          访问数
    private int rcount;//           回复数
    private int fcount;//          收藏数
    public int size;//      图片多少张
    private List<Picture> list; //图片们s

    public ArrayList<Gallery> getListGallery() {
        return listGallery;
    }

    public void setListGallery(ArrayList<Gallery> listGallery) {
        this.listGallery = listGallery;
    }

    public ArrayList<Gallery> listGallery; //番号列表

    @Override
    public String toString() {
        return "Gallery id:" + id + " galleryclass:" + galleryclass + " title:" + title
                + " img:" + img + " count:" + count + " rcount:" + rcount + " fcount:" + fcount
                + " size:" + size + " list:" + list;
    }

    public int getImgLacol() {
        return imgLacol;
    }

    public void setImgLacol(int imgLacol) {
        this.imgLacol = imgLacol;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getVideoPreImageUrl() {
        return videoPreImageUrl;
    }

    public void setVideoPreImageUrl(String videoPreImageUrl) {
        this.videoPreImageUrl = videoPreImageUrl;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Picture> getList() {
        return list;
    }

    public void setList(List<Picture> list) {
        this.list = list;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getGalleryclass() {
        return galleryclass;
    }

    public void setGalleryclass(int galleryclass) {
        this.galleryclass = galleryclass;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public int getRcount() {
        return rcount;
    }

    public void setRcount(int rcount) {
        this.rcount = rcount;
    }

    public int getFcount() {
        return fcount;
    }

    public void setFcount(int fcount) {
        this.fcount = fcount;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getCategoryUrl() {
        return categoryUrl;
    }

    public void setCategoryUrl(String categoryUrl) {
        this.categoryUrl = categoryUrl;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }
}
