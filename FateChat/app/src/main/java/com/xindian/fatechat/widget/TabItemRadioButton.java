package com.xindian.fatechat.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.widget.RadioButton;

import com.xindian.fatechat.R;

/**
 * Created by hx_Alex on 2017/3/24.
 * 自定义drawable大小radiobutton
 */

public class TabItemRadioButton extends RadioButton {
    private  int mDrawableSize;
    private Drawable drawableTop,drawableRight,drawableBottom,drawableLeft;
    
    public TabItemRadioButton(Context context) {
        super(context);
    }

    public TabItemRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.TabItem);
        setAttts(a);
    }

    public TabItemRadioButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.TabItem);
        setAttts(a);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public TabItemRadioButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.TabItem);
        setAttts(a);
    }

    /***
     * 设置相关属性
     * @param a
     */
    private  void setAttts( TypedArray a)
    {
        int n = a.getIndexCount();
        for (int i = 0; i < n; i++) {
            int attr = a.getIndex(i);
            switch (attr) {
                case R.styleable.TabItem_mDrawableSize:
                    mDrawableSize = a.getDimensionPixelSize(R.styleable.TabItem_mDrawableSize, 50);
                    break;
                case R.styleable.TabItem_drawableTop:
                    drawableTop = a.getDrawable(attr);
                    break;
                case R.styleable.TabItem_drawableBottom:
                    drawableRight = a.getDrawable(attr);
                    break;
                case R.styleable.TabItem_drawableRight:
                    drawableBottom = a.getDrawable(attr);
                    break;
                case R.styleable.TabItem_drawableLeft:
                    drawableLeft = a.getDrawable(attr);
                    break;
                default :
                    break;
            }
        }
        a.recycle();
        setCompoundDrawablesWithIntrinsicBounds(drawableLeft, drawableTop, drawableRight, drawableBottom);
    }

 
 
    /***
     * 重写此方法，设置drawable的size
     * @param left
     * @param top
     * @param right
     * @param bottom
     */
    @Override
    public void setCompoundDrawablesWithIntrinsicBounds(Drawable left, Drawable top, Drawable right, Drawable bottom) {
        if (left != null) {
            left.setBounds(0, 0, mDrawableSize, mDrawableSize);
        }
        if (right != null) {
            right.setBounds(0, 0, mDrawableSize, mDrawableSize);
        }
        if (top != null) {
            top.setBounds(0, 0, mDrawableSize, mDrawableSize);
        }
        if (bottom != null) {
            bottom.setBounds(0, 0, mDrawableSize, mDrawableSize);
        }
        setCompoundDrawables(left, top, right, bottom);
    }
}
