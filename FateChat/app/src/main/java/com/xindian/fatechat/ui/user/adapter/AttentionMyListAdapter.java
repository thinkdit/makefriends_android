package com.xindian.fatechat.ui.user.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.xindian.fatechat.R;
import com.xindian.fatechat.databinding.ItemAttentionMyBinding;
import com.xindian.fatechat.ui.user.model.UserBean;

import java.util.List;


/**
 * Created by hx_Alex on 2017/3/27.
 */

public class AttentionMyListAdapter extends BaseAdapter {
    private List<UserBean>  userList;
    private Context context;
    private LayoutInflater inflater;
    private ItemAttentionMyBinding mBinding;
    private  onClickCancelListener mListener;
    private boolean isVip;
    public AttentionMyListAdapter(List<UserBean> userList, Context context,boolean isVip) {
        this.userList = userList;
        this.context = context;
        this.isVip=isVip;
        inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    public void setOnClickCancelListener(onClickCancelListener mListener)
    {
        this.mListener=mListener;
    }
    
    @Override
    public int getCount() {
        return userList.size();
    }

    @Override
    public Object getItem(int position) {
        return userList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            mBinding= DataBindingUtil.inflate(inflater, R.layout.item_attention_my,parent,false);
            convertView = mBinding.getRoot();
            convertView.setTag(mBinding);
        }
        else
        {
            mBinding= (ItemAttentionMyBinding) convertView.getTag();
        }
        
        if (userList== null || userList.size()==0) {
            return convertView;
        }
        else
        {
            mBinding.setUser(userList.get(position));
            mBinding.setIsVip(isVip);
            mBinding.layoutAttention.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onClickAttention(userList.get(position).getUserId());
                }
            });
        }
        return convertView;
    }
    
    public  interface  onClickCancelListener
    {
        void  onClickAttention(int femmeUserId);
    }
}
