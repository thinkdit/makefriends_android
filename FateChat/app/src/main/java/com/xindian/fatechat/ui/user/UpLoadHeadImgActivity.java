package com.xindian.fatechat.ui.user;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.hyphenate.easeui.model.EventBusModel;
import com.xindian.fatechat.R;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.ui.base.BaseActivity;
import com.xindian.fatechat.ui.login.presenter.AccountPresenter;
import com.xindian.fatechat.ui.user.dialog.UpLoadHeadImgDialog;
import com.xindian.fatechat.ui.user.model.User;
import com.xindian.fatechat.ui.user.presenter.PersonaldataPresenter;
import com.xindian.fatechat.util.imageSelectorUtil;
import com.yancy.imageselector.ImageSelector;
import com.yancy.imageselector.ImageSelectorActivity;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

public class UpLoadHeadImgActivity extends BaseActivity implements AccountPresenter.IAvatarUploadedListener,PersonaldataPresenter.onPersonalExamineHeadImgListener {
    private com.xindian.fatechat.util.imageSelectorUtil imageSelectorUtil;
    private AccountPresenter accountPresenter;
    private PersonaldataPresenter mPresenter;
    private User user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_up_load_head_img);
     
        user= UserInfoManager.getManager(this).getUserInfo();
        imageSelectorUtil=new imageSelectorUtil(this);
        accountPresenter=new AccountPresenter(this);
        accountPresenter.setAvatarUploadedListener(this);
        mPresenter=new PersonaldataPresenter(this,null);
        mPresenter.setmPersonalExamineHeadImgListener(this);
        imageSelectorUtil.changePortrait();
    }
    
    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().getDecorView().setBackgroundColor(Color.TRANSPARENT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            if (requestCode == ImageSelector.IMAGE_REQUEST_CODE) {
                // Get Image Path List
                List<String> pathList = data.getStringArrayListExtra(ImageSelectorActivity.EXTRA_RESULT);
                String path = pathList.get(pathList.size() - 1);
                accountPresenter.uploadAvatar(path);
            }
        }else
        {
            finish();
        }

    }

    @Override
    public void onAvatarUploadedSucceed(String url) {
        user.setHeadimg(url);
        mPresenter.requestExamineHeadImg(user);
    }

    @Override
    public void onAvatarUploadedError() {
        EventBusModel model=new EventBusModel();
        model.setAction(UpLoadHeadImgDialog.ACTION);
        model.setData(0);
        EventBus.getDefault().post(model);
        finish();
    }

    @Override
    public void onExamineHeadImgSuccess() {
        EventBusModel model=new EventBusModel();
        model.setAction(UpLoadHeadImgDialog.ACTION);
        model.setData(1);
        EventBus.getDefault().post(model);
        finish();
      
    }

    @Override
    public void onExamineHeadImgError(String msg) {
        EventBusModel model=new EventBusModel();
        model.setAction(UpLoadHeadImgDialog.ACTION);
        model.setData(0);
        EventBus.getDefault().post(model);
        finish();
     
    }
}
