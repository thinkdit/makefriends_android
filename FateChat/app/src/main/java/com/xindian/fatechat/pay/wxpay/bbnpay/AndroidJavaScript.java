package com.xindian.fatechat.pay.wxpay.bbnpay;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.util.Patterns;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;

/**
 * Created by hx_Alex on 2017/5/12.
 */

public class AndroidJavaScript {
    Activity activity;
    WebView webview;

    public AndroidJavaScript(Activity c, WebView wv) {
        this.activity = c;
        this.webview = wv;
    }

    @JavascriptInterface
    public void backKeydown() {
        
        webview.post(new Runnable() {
            @Override
            public void run() {
                if(webview.canGoBack()) {
                    webview.goBack();
                } else {
                    activity.finish();
                }
            }
        });
     

    }

    @JavascriptInterface
    public void back_mtch() {
        this.activity.finish();
    }

    @JavascriptInterface
    public void gotoapp() {
        String back_str = com.payh5.bbnpay.mobile.main.BbnPay.get_back();
        Log.e("gotoapp Activity", back_str);
        if(back_str.equals("")) {
            this.activity.finish();
        } else if(Patterns.WEB_URL.matcher(back_str).matches()) {
            this.activity.finish();
        } else if(back_str.startsWith("file://")) {
            this.activity.finish();
        } else {
            Intent i = new Intent(back_str);
            this.activity.startActivity(i);
            this.activity.finish();
        }

    }

    public void queryorder() {
    }
}
