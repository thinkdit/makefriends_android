package com.xindian.fatechat.ui.home.presenter;

import android.content.Context;
import android.os.RemoteException;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.xindian.fatechat.common.BaseModel;
import com.xindian.fatechat.common.BasePresenter;
import com.xindian.fatechat.common.FateChatApplication;
import com.xindian.fatechat.common.ResultModel;
import com.xindian.fatechat.widget.MyProgressDialog;

import org.json.JSONException;

import java.util.HashMap;

/**
 * 帖子编辑
 */
public class ComEditPresenter extends BasePresenter {
    private final String URL_EDIT_DY = "/dynamic/release_dynamic";
    MyProgressDialog myProgressDialog;


    public ComEditPresenter(Context context){
        myProgressDialog = new MyProgressDialog(context);
    }

    public void addCom(String userId,String token,String dynamicContent,String sourceUrl,int type) {
        HashMap<String, Object> data = new HashMap<>();
        data.put("dynamicContent",dynamicContent);
        data.put("falg",1);
        data.put("sourceUrl",sourceUrl);
        data.put("type",type);
        data.put("userId",userId);
        data.put("token",token);
        post(getUrl(URL_EDIT_DY), data, null);
        myProgressDialog.show();
    }

    public void editCom(String dynamicId,String userId,String token,String dynamicContent,String sourceUrl,int type) {
        HashMap<String, Object> data = new HashMap<>();
        data.put("dynamicId",dynamicId);
        data.put("dynamicContent",dynamicContent);
        data.put("falg",2);
        data.put("sourceUrl",sourceUrl);
        data.put("type",type);
        data.put("userId",userId);
        data.put("token",token);
        post(getUrl(URL_EDIT_DY), data, null);
    }


    @Override
    public Object asyncExecute(String url, ResultModel resultModel) {
        if (url.contains(URL_EDIT_DY)) {
            return JSON.parseObject(resultModel.getData(), BaseModel.class);
        }
        return null;
    }

    @Override
    public void onError(String url, Exception e) {
        super.onError(url, e);
        myProgressDialog.dismiss();
    }


    @Override
    public void onFailure(String url, ResultModel resultModel) {
        super.onFailure(url,resultModel);
        myProgressDialog.dismiss();

        if (url.contains(URL_EDIT_DY)) {
            if(resultModel.getMessage()!=null){
                Toast.makeText(FateChatApplication.getInstance(), resultModel.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onSucceed(String url, ResultModel resultModel) throws JSONException,
            RemoteException {
        myProgressDialog.dismiss();

        if (url.contains(URL_EDIT_DY)) {
            if(mOnComLisener !=null){
                mOnComLisener.onComEditSuccess();
            }

        }
    }

    private OnComLisener mOnComLisener;

    public OnComLisener getmOnComLisener() {
        return mOnComLisener;
    }

    public void setmOnComLisener(OnComLisener mOnComLisener) {
        this.mOnComLisener = mOnComLisener;
    }

    public interface OnComLisener {
        void onComEditSuccess();
    }

}
