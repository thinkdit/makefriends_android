package com.xindian.fatechat.ui.user.serivce;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.hyphenate.easeui.EaseConstant;
import com.hyphenate.easeui.domain.ChatUserEntity;
import com.xindian.fatechat.ui.ChatActivity;

import java.util.List;

/**
 * Created by hx_Alex on 2017/5/22.
 */

public class NoticationImReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e("NoticationImReceiver","NoticationImReceiver");
        ChatUserEntity chatUserEntity = (ChatUserEntity) intent.getSerializableExtra(EaseConstant.EXTRA_CHAT_USER);
        Intent i=new Intent();
        if(getAppSatus(context,context.getPackageName())) {
             i = new Intent(context, ChatActivity.class);
            i.putExtra(EaseConstant.EXTRA_CHAT_USER, chatUserEntity);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
         
        }else 
        {
             i  = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
             i.putExtra(EaseConstant.EXTRA_CHAT_USER, chatUserEntity);
        }
        context.startActivity(i);
    }

    /**
     * 返回app运行状态 
     * 1:程序在前台运行 
     * 2:程序在后台运行 
     * 3:程序未启动 
     * 注意：需要配置权限<uses-permission android:name="android.permission.GET_TASKS" /> 
     */
    public boolean getAppSatus(Context context, String pageName) {

        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> list = am.getRunningTasks(20);

        //判断程序是否在栈顶  
        if (list.get(0).topActivity.getPackageName().equals(pageName)) {
            return true;
        } else {
            //判断程序是否在栈里  
            for (ActivityManager.RunningTaskInfo info : list) {
                if (info.topActivity.getPackageName().equals(pageName)) {
                    return true;
                }
            }
            return false;//栈里找不到，返回3  
        }
    }
}
