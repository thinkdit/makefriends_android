package com.xindian.fatechat.ui.home.model;


import com.xindian.fatechat.common.BaseModel;

public class RulesModel extends BaseModel {
    public static final String KEY_NOT_DY_STATUS = "0";
    private String messageLimit;
    private limitSex messageLimitJson;
    private long createTime;
    private String channelName;
    private int id;
    private int state;
    private String channelId;
    private int register;
    private PushButton pushButton;
    private String dynamicStatus;

    public String getDynamicStatus() {
        return dynamicStatus;
    }

    public void setDynamicStatus(String dynamicStatus) {
        this.dynamicStatus = dynamicStatus;
    }

    public void setMessageLimit(String messageLimit) {
        this.messageLimit = messageLimit;
    }

    public String getMessageLimit() {
        return messageLimit;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getState() {
        return state;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setRegister(int register) {
        this.register = register;
    }

    public int getRegister() {
        return register;
    }
    
    public limitSex getMessageLimitJson() {
        return messageLimitJson;
    }

    public void setMessageLimitJson(limitSex messageLimitJson) {
        this.messageLimitJson = messageLimitJson;
    }
    public PushButton getPushButton() {
        return pushButton;
    }

    public void setPushButton(PushButton pushButton) {
        this.pushButton = pushButton;
    }
    @Override
    public String toString() {
        return "Response{" +
                "messageLimit = '" + messageLimit + '\'' +
                ",createTime = '" + createTime + '\'' +
                ",channelName = '" + channelName + '\'' +
                ",id = '" + id + '\'' +
                ",state = '" + state + '\'' +
                ",channelId = '" + channelId + '\'' +
                ",register = '" + register + '\'' +
                "}";
    }
    
   public class limitSex
   {

       /**
        * boy : 12
        * gay : max
        */

       private String boy;
       private String girl;

       public String getBoy() {
           return boy;
       }

       public void setBoy(String boy) {
           this.boy = boy;
       }

       public String getGirl() {
           return girl;
       }

       public void setGirl(String girl) {
           this.girl = girl;
       }
   } 
    
    public class  PushButton
    {
        private boolean giftIcon;
        private boolean giftButton;

        public boolean isGiftIcon() {
            return giftIcon;
        }

        public void setGiftIcon(boolean giftIcon) {
            this.giftIcon = giftIcon;
        }

        public boolean isGiftButton() {
            return giftButton;
        }

        public void setGiftButton(boolean giftButton) {
            this.giftButton = giftButton;
        }
    }
}