package com.xindian.fatechat.ui.user.presenter;

import android.content.Context;
import android.os.RemoteException;

import com.alibaba.fastjson.JSON;
import com.xindian.fatechat.common.BasePresenter;
import com.xindian.fatechat.common.ResultModel;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.ui.login.model.LoginBean;
import com.xindian.fatechat.ui.user.model.PhotoInfoBean;
import com.xindian.fatechat.ui.user.model.User;
import com.xindian.fatechat.widget.MyProgressDialog;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by hx_Alex on 2017/3/23.
 */

public class RequestUserInfoPresenter extends BasePresenter {
    private final String REQUEST_USERINFO = "/auth/authentication";
    private final String REQUEST_USERDETAILSINFO = "/auth/getUserById";
    private final String  URL_MYIMAGE="/auth/myImage";
    private Context mContext;
    private UserInfoUpdatedListener mUpdatedListener;
    private UserInfodListener UserInfodListener;
    private MyProgressDialog mDialog;
    private OtherUserPhotoListener otherUserPhotoListener;
    private ArrayList<PhotoInfoBean> mList;
    public RequestUserInfoPresenter(Context context, UserInfoUpdatedListener listener) {
        mContext = context;
        mUpdatedListener = listener;
    }

    public RequestUserInfoPresenter(Context context, UserInfodListener listener) {
        mContext = context;
        UserInfodListener = listener;
    }
    
    public void setOtherUserPhotoListener(OtherUserPhotoListener otherUserPhotoListener) {
        this.otherUserPhotoListener = otherUserPhotoListener;
    }
    
    
    public void requestUserInfo() {
        Map<String, Object> map = new HashMap<>();
        map.put("userId", UserInfoManager.getManager(mContext).getUserId());
        post(getUrl(REQUEST_USERINFO), map, mContext);
        
    }



    public void requestMyImage(int userId,int page,int pageSize,int otherId)
    {
        HashMap<String,Object> data=new HashMap<>();
        data.put("userId",userId);
        data.put("page",page);
        data.put("pageSize",pageSize);
        data.put("othersId",otherId);
        post(getUrl(URL_MYIMAGE),data,mContext);
    }

    /****
     * 获取用户信息，也可以获取自己的信息 暂只作为获取他人信息接口
     *
     * @param othersId
     */
    public void requestUserDetailsInfo(int othersId) {
        Map<String, Object> map = new HashMap<>();
        map.put("userId", UserInfoManager.getManager(mContext).getUserInfo().getUserId());
        map.put("othersId", othersId);
        post(getUrl(REQUEST_USERDETAILSINFO), map, mContext);
        mDialog=new MyProgressDialog(mContext);
        if(mDialog!=null) {
            mDialog.show();
        }
    }

    public void setUserInfodListener(RequestUserInfoPresenter.UserInfodListener userInfodListener) {
        UserInfodListener = userInfodListener;
    }

    @Override
    public void onSucceed(String url, ResultModel resultModel) throws JSONException,
            RemoteException {
        if (url.contains(REQUEST_USERINFO)) {
            if (mUpdatedListener != null) {
                LoginBean loginBean = (LoginBean) resultModel.getDataModel();
                loginBean.getUserInfoVo().setImPassword(loginBean.getImPassword());
                mUpdatedListener.onUserInfoUpdatedSucceed(loginBean.getUserInfoVo());
            }
        } else if (url.contains(REQUEST_USERDETAILSINFO)) {
            UserInfodListener.onUserInfodSucceed((User) resultModel.getDataModel());
            if(mDialog!=null) {
                mDialog.dismiss();
            }
        }else if(url.contains(URL_MYIMAGE)) {
            mList= (ArrayList<PhotoInfoBean>) resultModel.getDataModel();
            otherUserPhotoListener.onOtherUserPhotoSucceed(mList);
        }
    }

    @Override
    public Object asyncExecute(String url, ResultModel resultModel) {
        if (url.contains(REQUEST_USERINFO)) {
            return JSON.parseObject(resultModel.getData(), LoginBean.class);
        } else if (url.contains(REQUEST_USERDETAILSINFO)) {
            return JSON.parseObject(resultModel.getData(), User.class);
        } else if(url.contains(URL_MYIMAGE)) {
            return JSON.parseArray(resultModel.getData(),PhotoInfoBean.class);
        }
        return null;
    }

    @Override
    public void onFailure(String url, ResultModel resultModel) {
        super.onFailure(url, resultModel);
        if(mDialog!=null) {
            mDialog.dismiss();
        }
    }

    public interface UserInfoUpdatedListener {
        void onUserInfoUpdatedSucceed(User user);
    }

    public interface UserInfodListener {
        void onUserInfodSucceed(User user);
    }

    public interface OtherUserPhotoListener {
        void onOtherUserPhotoSucceed(List<PhotoInfoBean> bean);
    }
}
