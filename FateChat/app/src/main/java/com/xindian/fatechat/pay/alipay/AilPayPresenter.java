package com.xindian.fatechat.pay.alipay;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.widget.Toast;

import com.alipay.sdk.app.PayTask;
import com.xindian.fatechat.common.BasePresenter;

import java.util.Map;

/**
 * Created by hx_Alex on 2017/4/19.
 * 支付宝支付p类
 * 适应支付宝V2接口
 */

public class AilPayPresenter extends BasePresenter {
    private   String orderInfo=null;//订单信息 --从服务端获取用于支付宝
    private   String orderNum=null;//订单信息 --从服务端获取自动生成的订单号，用于支付结果上报
    private Activity mContext;
    private  final static  int SDK_PAY_FLAG=0x001;
    private  PayHandler mHandler;
    private  OnAilPayListener mListener;
    public AilPayPresenter(Activity context, String orderInfo,String orderNum, OnAilPayListener mListener) {
        this.mContext = context;
        this.orderInfo=orderInfo;
        this.orderNum=orderNum;
        this.mListener=mListener;
        mHandler=new PayHandler();
    }

    /***
     * 开始请求支付宝支付
     */
    public void startPayByAilPay()
    {
        if(orderInfo==null){
            Toast.makeText(mContext,"订单信息为空请重新尝试",Toast.LENGTH_LONG).show();
        }
        Runnable payRunnable = new Runnable() {
            @Override
            public void run() {
                PayTask alipay = new PayTask(mContext);

                Map<String, String> result = alipay.payV2(orderInfo, true);
                Message msg = new Message();
                msg.what = SDK_PAY_FLAG;
                msg.obj = result;
                mHandler.sendMessage(msg);
            }
        };
        // 必须异步调用
        Thread payThread = new Thread(payRunnable);
        payThread.start();  
    }
    
    
    class  PayHandler extends Handler
    {
        @Override
        public void handleMessage(Message msg) {
            if(msg.what==SDK_PAY_FLAG){
                PayResult payResult = new PayResult((Map<String, String>) msg.obj);
                /**
                 对于支付结果，请商户依赖服务端的异步通知结果。同步通知结果，仅作为支付结束的通知。
                 */
                String resultInfo = payResult.getResult();// 同步返回需要验证的信息
                String resultStatus = payResult.getResultStatus();
                // 判断resultStatus 为9000则代表支付成功
                if (TextUtils.equals(resultStatus, "9000")) {
                    Toast.makeText(mContext, "支付完成", Toast.LENGTH_SHORT).show();
                    mListener.onAilPaySucceed(orderNum,resultStatus);

                } else {
                    // 判断resultStatus 为非“9000”则代表可能支付失败
                    // “8000”代表支付结果因为支付渠道原因或者系统原因还在等待支付结果确认，最终交易是否成功以服务端异步通知为准（小概率状态）
                    if (TextUtils.equals(resultStatus, "8000")) {
                        Toast.makeText(mContext, "支付结果确认中", Toast.LENGTH_SHORT).show();
                        mListener.onAilPaySucceed(orderNum,resultStatus);

                    } else if (TextUtils.equals(resultStatus, "6001")){
                        // 其他值就可以判断为支付失败，包括用户主动取消支付，或者系统返回的错误
                        Toast.makeText(mContext, "支付取消", Toast.LENGTH_SHORT).show();
                        mListener.onAilPayFaild(orderNum,resultStatus);
                    }

                }
            }
        }
    }
    
    
    public interface  OnAilPayListener
    {
       void onAilPaySucceed(String orderNum,String code);
        void onAilPayFaild(String orderNum,String resultStatus);
    }
}
