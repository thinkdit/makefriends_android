package com.xindian.fatechat.ui.user.data;

import android.content.Context;

import com.xindian.fatechat.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * Created by hx_Alex on 2017/4/18.
 */

public class CashListStaticData {
    static {
        nameListData= Arrays.asList("﹏颜汐ぐ", "谁没爱过一只狗", "ㄔㄨㄣˇ=蠢", "童话.", "嗯 未来~", "久不愈i", "ヤ今生的唯爱", "中二1个软", "Crazy. 疯狂.", "DJ女郞", " 姐很高、也很傲╰つ", "后来,就没有了后来︾"
                , "╄→沵湜莪嘸琺企及的光", "撩妹大班长", "加勒比海带", "独波大侠°", "益若翼", "攬一亱孤風", "舌尖上的诱惑つ", "我知道你是梦不是命#", "我叼大我先上", "谢却荼蘼", "久不愈i", "咖啡杯的、瑕疵"
                , "咱来扇SM一巴掌！", "齐肩马尾拽天下@", "劳娘缺锌缺钙缺人爱", "不服来战lol", "蹲厕唱忐忑", "我累了你滚吧", "故人怎追忆", "暂引樱桃破", "背叛←", "小情天。", "半生风雪", "余阳", "Noslēpum", "Exile つ (流放fan)*"
                , "不要挡在皇帝面前", "〃男人拽,照样甩 ≈", "社会妞现实姐@", "你已被我out", "不爱就滚别浪费我青春", "先生、克制点", "看我纯净天然无公害z", "顽皮可以别赛脸", "你的世界不缺我", "孤星赶月", "你是我要不起的风景", "念卿天涯。", "乄壞Oo鮭鮭魚", "卧笑醉伊人"
                , "<我是流氓我怕谁>", "滚开i", "你就是嫉妒我的美", "小姐ヽ你惹不起的草°", "我姓张很嚣张！", "魅罗红颜乱さ", "世俗污了我的眼i", "村姑也狠潮丶", "孙子，你配我爱么", "看谁谁怀孕", "在你的葬礼放Dj", "傲藐者", "[ 禁。区、", "逼毛不烫自然卷"
                , "霸占你妈当你爸", "傲似你野爹", "和宇宙叫嚣ˇ", "极度嚣张", "夜死神降临", "鄙视你、是瞧得起你", "傲似你祖宗", "狼の嚣张", "跪下、叫帅哥", "贱人゛无处不在", "↘傻瓜，你给我滚回来！", "↘碾压ー切", "甩你是给你面子", "魔尊弑神"
                , "枯", "1.场美丽的烟花雨～", "- 梦旅鹿人", "素时景年一世疏离", "成熟带来的是孤独", "╭ァ1个没安全感旳孩子°", "夲宫丶没時閒", "嘴巴子过来", "噬血啸月", "我拽我的『管你屁事』", "浅夏が忆昔", "︶ㄣ洳影隨形しovё", "聆聽＊幸福", "最佳姿势"
                , "爷来取你狗命", "给我马不停蹄的滚", "虐菜是享受", "瘦成闪电劈死你", "独步杀戮", "柠檬草の味道、", "静抚长琴等君归", "Sunshine Bloom 阳光盛开", "换歌体味即系干", "蔚蓝深海", "想念汇集成河", "指尖盛开的繁花", "往事随风心似烟", "淡淡笑。");
        nameConetentListData=new LinkedList<>();
        contentList=new ArrayList<>();
        timeConetentListData=new LinkedList<>();
    }
    public  static final String NAME="name";
    public  static final String TIME="time";
    private static final  int INTILISTSIZE=50;
    private static final  int MINUTE=60;
    public static final  int REFRESH=6;//刷新时间
    private static List<String> nameListData;
    private static LinkedList<String> nameConetentListData;
    private static LinkedList<Integer> timeConetentListData;
    private static ArrayList<HashMap<String, String>> contentList;
    private static  String DAY30="";
    private static  String DAY90="";
    /***
     * 获取充值展示数据列表
     * @return
     */
    public static ArrayList<HashMap<String, String>> getCashContentListData(Context context, int...cashMoney)
    {
        if(cashMoney.length>=2) {
            DAY30 = context.getString(R.string.DAY30, cashMoney[0]);
            DAY90 = context.getString(R.string.DAY90, cashMoney[1]);
            intiData();
        }
        return  contentList;
    }

    private static void intiData()
    {
        if(nameListData==null) return;
        for(;nameConetentListData.size()<INTILISTSIZE;){
            Random random=new Random();
            int index = random.nextInt(nameListData.size());
            /**
             * 如果姓名列表当中包含此条姓名重新循环
             */
            if(nameConetentListData.contains(nameListData.get(index)))
            {
                continue;
            }
            else 
            {
                nameConetentListData.add(nameListData.get(index));
            }
        }
       
        for(int i=0;i<nameConetentListData.size();i++)
        {
            String[] content=new String[]{DAY30,DAY90,DAY30,DAY90,DAY30,DAY90,DAY30,DAY90,DAY30,DAY90};
            Random random=new Random();
            HashMap<String,String> tempMap=new HashMap<>();
            tempMap.put(NAME,nameConetentListData.get(i));
            tempMap.put(TIME,content[random.nextInt(content.length-1)]);
            contentList.add(tempMap);
        }
    }
    
}
