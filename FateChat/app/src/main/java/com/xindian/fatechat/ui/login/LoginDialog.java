package com.xindian.fatechat.ui.login;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.thinkdit.lib.util.StringUtil;
import com.xindian.fatechat.R;
import com.xindian.fatechat.manager.LoginManager;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.ui.login.presenter.AccountPresenter;
import com.xindian.fatechat.ui.user.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by hx_Alex on 2017/3/28.
 * 提示登录或者注册Dialog
 */

public class LoginDialog extends AlertDialog implements AccountPresenter.OnLoginListener {
    public static final int REGISTER_SUCCESS = 1;

    private String title;
    private TextView titleView;
    private EditText edit_account, edit_password;
    private Button btn_ok, btn_cancel;
    private RelativeLayout layoutMain;
    private AccountPresenter mPresenter;
    private onLoginListener mLoginListener;
    Context context;

    public LoginDialog(Context context, String title) {
        super(context, R.style.Dialog_UserInfo);
        this.title = title;
        this.context = context;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_prompt_dialog);
        intiView();
        titleView.setText(title);
        getWindow().getAttributes().windowAnimations = R.style.dialog_anim;
    }


    private void intiView() {
        mPresenter = new AccountPresenter(context);
        mPresenter.setOnLoginListener(this);
        titleView = (TextView) findViewById(R.id.txt_title);
        layoutMain = (RelativeLayout) findViewById(R.id.dialog_main);
        btn_ok = (Button) findViewById(R.id.btn_ok);
        btn_cancel = (Button) findViewById(R.id.btn_cancel);
        edit_account = (EditText) findViewById(R.id.edit_account);
        edit_password = (EditText) findViewById(R.id.edit_password);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String loginId = edit_account.getText().toString();
                String password = edit_password.getText().toString();
                if (checkData(loginId, password)) {
                    mPresenter.requestLogin(loginId, password);
                }
            }
        });
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, RegisterActivity.class);
                if (context instanceof Activity) {
                    ((Activity) context).startActivityForResult(i, REGISTER_SUCCESS);
                }
                dismiss();
            }
        });
        getLastUserInfo();
    }

    @Override
    public void show() {
        super.show();


    }


    public boolean checkData(String loginId, String password) {
        if (StringUtil.isNullorEmpty(loginId)) {
            Toast.makeText(context, "账号不能为空,请重新输入", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (StringUtil.isNullorEmpty(password)) {
            Toast.makeText(context, "密码不能为空,请重新输入", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    /**
     * 获取成功登陆的账号密码信息并显示
     */
    public void getLastUserInfo() {
        if (context != null) {
            Map<String, String> loginData = UserInfoManager.getManager(context).GetLoginData();
            if (loginData.get(UserInfoManager.KEY_USERID) != null && loginData.get
                    (UserInfoManager.KEY_PASSWORD) != null) {
                edit_account.setText(loginData.get(UserInfoManager.KEY_USERID).toString());
                edit_password.setText(loginData.get(UserInfoManager.KEY_PASSWORD).toString());
            }
        }
    }


    public void setOnLoginListener(onLoginListener onLoginListener) {
        mLoginListener = onLoginListener;
    }


    @Override
    public void LoginSucceed(User user) {
        if (isShowing()) {
            dismiss();
        }
        if (mLoginListener != null) {
            mLoginListener.LoginSucceed(user);
        }
    }

    @Override
    public void LoginError(String msg) {
        if (mLoginListener != null) {
            mLoginListener.LoginError(msg);
        }
    }

    public interface onLoginListener {
        void LoginSucceed(User user);

        void LoginError(String msg);
    }


}
