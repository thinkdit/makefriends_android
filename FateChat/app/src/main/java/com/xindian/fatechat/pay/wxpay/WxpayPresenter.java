package com.xindian.fatechat.pay.wxpay;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.hyphenate.easeui.model.EventBusModel;
import com.jyd.paydemo.Bean;
import com.jyd.paydemo.XianlaiWxH5Pay;
import com.switfpass.pay.MainApplication;
import com.switfpass.pay.activity.PayPlugin;
import com.switfpass.pay.bean.RequestMsg;
import com.switfpass.pay.utils.Constants;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.xindian.fatechat.R;
import com.xindian.fatechat.common.BasePresenter;
import com.xindian.fatechat.manager.PayTypeManager;

import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.pay.SelectPayWindow;

import com.xindian.fatechat.pay.model.PrePayInfoBean;
import com.xindian.fatechat.pay.model.ProductBean;
import com.xindian.fatechat.pay.model.WxPreInfoBean;
import com.xindian.fatechat.pay.presenter.PayPresenter;
import com.xindian.fatechat.pay.wxpay.bbnpay.BbnPay;
import com.xindian.fatechat.ui.user.model.User;

import org.greenrobot.eventbus.EventBus;

import cn.beecloud.BCPay;
import cn.beecloud.async.BCCallback;
import cn.beecloud.async.BCResult;
import cn.beecloud.entity.BCPayResult;
import cn.beecloud.entity.BCReqParams;


/**
 * Created by hx_Alex on 2017/4/23.
 * 微信支付p类
 */

public class WxpayPresenter  extends BasePresenter implements PayPresenter.onWxPrePayInfoListener,BCCallback,XianlaiWxH5Pay.OnWxH5PayCallBack{
    public  final  static String TAG="WxpayPresenter";
    public  final  static String PAY_SUCCESSL="支付成功";
    public  final  static String PAY_FAIL="支付取消";
    public  final  static String PAY_CANCEL="支付失败";
    
    private Activity mContext;
    private User user;
    private static int pid;
    private PayPresenter mPersenter;
    private WxPreInfoBean mBean;
    private static String orderNum;
    private XianlaiWxH5Pay xianlaiWxH5Pay;
    
    public WxpayPresenter(Activity mContext)
    {
        this.mContext=mContext;
        mPersenter=new PayPresenter(mContext,this);
        xianlaiWxH5Pay=new XianlaiWxH5Pay(mContext,this);
    }

    public XianlaiWxH5Pay getXianlaiWxH5Pay() {
        return xianlaiWxH5Pay;
    }
    
    public static String getOrderNum() {
        return orderNum;
    }

    public static int getPid() {
        return pid;
    }

    public void StartWxPay(User user, int pid,String rechargeType)
    {
        if(user ==null|| pid==0 ){
            Toast.makeText(mContext, "预支付订单获取失败", Toast.LENGTH_LONG).show();
            return;
        }
        if(!isWXAppInstalledAndSupported())return;
        this.user=user;
        this.pid=pid;
        
        PayTypeManager.WXPayChannel wxPayChannel = PayTypeManager.getInstance().getWxPayChannel();
        if(wxPayChannel.equals(PayTypeManager.WXPayChannel.WFT))
        {
            mPersenter.requestWxPrePayInfoBywft(user,pid,rechargeType);
        }else if(wxPayChannel.equals(PayTypeManager.WXPayChannel.DX)){
            mPersenter.requestWxPrePayInfo(user,pid,rechargeType);
        }
    }


    /***
     * 闲来渠道微信支付--不走后台预支付接口
     */
    public void WxPayByXianLai(ProductBean mBean,int userId)
    {
        if(mBean==null && mBean.getMoney()==0)
        { 
            Toast.makeText(mContext,"请求金额为空,请重新尝试",Toast.LENGTH_SHORT).show();
            return;
        }
        Float money=Float.parseFloat(String.valueOf(mBean.getMoney()));
        xianlaiWxH5Pay.setData(money,userId,mBean.getPid());//必须设置数据
        xianlaiWxH5Pay.startPayWxByXianLai();
    }

    public void StartXianLaiWithWx(Bean bean)
    {
        xianlaiWxH5Pay.pay(bean);
    }


    /***
     * 支付渠道支付
     */
    public void payMetod(PrePayInfoBean bean)
    {
        PayTypeManager.WXPayChannel wxPayChannel = PayTypeManager.getInstance().getWxPayChannel();
        if(wxPayChannel.equals(PayTypeManager.WXPayChannel.WFT))
        {
            WxPayBywft(bean);
        }else if(wxPayChannel.equals(PayTypeManager.WXPayChannel.DX)){
            WxPayBybbnpay(mContext,mContext.getResources().getString(R.string.BbnPay_popurl),bean.getData(),"cashActivity");
        }
    }

    @Override
    public void onGetPrePayInfoSuccess(PrePayInfoBean bean) {
             
        orderNum=bean.getOrderNum();
        payMetod(bean);
       
    }

    @Override
    public void onGetPrePayInfoError(String msg) {
   
    }
    
    /***
     * BeeCloud渠道微信支付
     */
    public void  WxPayByBeeCloud(){
        
        if (BCPay.isWXAppInstalledAndSupported() &&
                BCPay.isWXPaySupported()) {
            // 在发起微信请求之前必须先initWechatPay
            BCPay.PayParams payParam = new BCPay.PayParams();
            payParam.channelType = BCReqParams.BCChannelTypes.BC_WX_APP;
            //商品描述
            payParam.billTitle = "支付测试";
            //支付金额，以分为单位，必须是正整数
            payParam.billTotalFee = 10;
            //商户自定义订单号
            payParam.billNum ="testWxpay";
            // 第二个参数实现BCCallback接口，在done方法中查看支付结果
            BCPay.getInstance(mContext).reqPaymentAsync(payParam,this);
        }else{
            Toast.makeText(mContext,
                    "您尚未安装微信或者安装的微信版本不支持", Toast.LENGTH_LONG).show();
        }
    }


    /***
     * 威富通渠道微信支付
     */
    public void  WxPayBywft(PrePayInfoBean bean){

        RequestMsg msg = new RequestMsg();
        msg.setTokenId(bean.getData());
        msg.setTradeType(MainApplication.WX_APP_TYPE);
        msg.setAppId(mContext.getResources().getString(R.string.wx_app_id));//wxd3a1cdf74d0c41b3
        PayPlugin.unifiedAppPay(mContext, msg);
    }

    /***
     * 点芯渠道微信支付
     */
    public void  WxPayBybbnpay(Activity activity, String popurl, String transid,String action){
        BbnPay.startPay (activity,popurl,transid,action);
    }
    
    
    /***
     * BeeCloud渠道微信支付回调
     */
    @Override
    public void done(BCResult bcResult) {
        final BCPayResult bcPayResult = (BCPayResult) bcResult;
        String payReslut = bcPayResult.getResult();
        payReslutMsg msg=new payReslutMsg();
        if (payReslut.equals(BCPayResult.RESULT_SUCCESS)) {
            msg.setTag(payMsg.PAY_SUCCESS);
            msg.setMsg("支付成功");
            Log.e("WxPayByBeeCloud", "支付成功");
        } else if (payReslut.equals(BCPayResult.RESULT_CANCEL)) {
            msg.setTag(payMsg.PAY_CANCEL);
            msg.setMsg("用户取消支付");
            Log.e("WxPayByBeeCloud", "用户取消支付");
        }
        else if (payReslut.equals(BCPayResult.RESULT_FAIL)) {
            msg.setTag(payMsg.PAY_CANCEL);
            msg.setMsg("支付失败, 原因: " + bcPayResult.getErrCode() +
                    " # " + bcPayResult.getErrMsg() +
                    " # " + bcPayResult.getDetailInfo());
            
            Log.e("WxPayByBeeCloud", "支付失败, 原因: " + bcPayResult.getErrCode() +
                    " # " + bcPayResult.getErrMsg() +
                    " # " + bcPayResult.getDetailInfo());
        }
        
        //发送回调
        EventBus.getDefault().post(msg);
    }



    /***
     * 检查是否安装了微信
     * @return
     */
    private boolean isWXAppInstalledAndSupported() {
        IWXAPI msgApi = WXAPIFactory.createWXAPI(mContext, null);
        msgApi.registerApp(Constants.APP_ID);
        boolean sIsWXAppInstalled= msgApi.isWXAppInstalled()
                && msgApi.isWXAppSupportAPI();
        boolean sIsWXAppSupported=  msgApi.isWXAppSupportAPI();
        boolean sIsWXAppInstalledAndSupported= sIsWXAppInstalled && sIsWXAppSupported;
        if(!sIsWXAppInstalled)
        {
            Toast.makeText(mContext,
                    "您尚未安装微信,请先安装微信或选择其他支付方式", Toast.LENGTH_LONG).show();
        }else if(!sIsWXAppSupported)
        {
            Toast.makeText(mContext,
                    "您的微信版本暂不支持微信支付，请升级您的微信或选择其他支付方式", Toast.LENGTH_LONG).show();
        }
        return sIsWXAppInstalledAndSupported;
    }

    
    
    @Override
    public void onWXH5PayReturnApp(int pid, String msg, String tradeNo) {
        mPersenter.reportPayInfo(pid,msg,tradeNo, UserInfoManager.getManager(mContext).getUserId());
        //返回app后通知检查是否为支付成功
        EventBusModel model=new EventBusModel();
        model.setData(tradeNo);
        model.setAction(SelectPayWindow.XIANLAI_WX_PAY_CALLBACK_SELELCT);
        EventBus.getDefault().post(model);
    }
    


    public class payReslutMsg
    {
        private payMsg tag;
        private String msg;

        public payMsg getTag() {
            return tag;
        }

        public void setTag(payMsg tag) {
            this.tag = tag;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }
    }
    
   public enum payMsg
   {
       PAY_SUCCESS,PAY_FAIL,PAY_CANCEL
   } 
}
