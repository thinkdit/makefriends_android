package com.xindian.fatechat.ui.login.model;

import com.xindian.fatechat.common.BaseModel;

import java.io.Serializable;

/**
 * Created by hx_Alex on 2017/3/31.
 * vip信息
 */

public class UserAuth extends BaseModel implements Serializable{

    /**
     * id : 0
     * userId : 0
     * beginTime : null
     * endTime : null
     */

    private int id;
    private int userId;
    private long beginTime;
    private long endTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public long getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(long beginTime) {
        this.beginTime = beginTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }
}
