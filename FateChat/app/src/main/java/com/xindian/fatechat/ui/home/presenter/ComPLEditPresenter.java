package com.xindian.fatechat.ui.home.presenter;

import android.os.RemoteException;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.xindian.fatechat.common.BaseModel;
import com.xindian.fatechat.common.BasePresenter;
import com.xindian.fatechat.common.FateChatApplication;
import com.xindian.fatechat.common.ResultModel;

import org.json.JSONException;

import java.util.HashMap;

/**
 * 评论编辑
 */
public class ComPLEditPresenter extends BasePresenter {
    private final String URL_EDIT_DY = "/dynamic/release_comment";

    public void editCom(String dynamicId,String userId,String token,String commentContent) {
        HashMap<String, Object> data = new HashMap<>();
        data.put("dynamicId",dynamicId);
        data.put("commentContent",commentContent);
        data.put("userId",userId);
        data.put("token",token);
        post(getUrl(URL_EDIT_DY), data, null);
    }


    @Override
    public Object asyncExecute(String url, ResultModel resultModel) {
        if (url.contains(URL_EDIT_DY)) {
            return JSON.parseObject(resultModel.getData(), BaseModel.class);
        }
        return null;
    }

    @Override
    public void onError(String url, Exception e) {
        super.onError(url, e);
    }


    @Override
    public void onFailure(String url, ResultModel resultModel) {
        super.onFailure(url,resultModel);
        if (url.contains(URL_EDIT_DY)) {
            if(resultModel.getMessage()!=null){
                Toast.makeText(FateChatApplication.getInstance(), resultModel.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onSucceed(String url, ResultModel resultModel) throws JSONException,
            RemoteException {
        if (url.contains(URL_EDIT_DY)) {
            if(mOnComLisener !=null){
                mOnComLisener.onComEditSuccess();
            }

        }
    }

    private OnComLisener mOnComLisener;

    public OnComLisener getmOnComLisener() {
        return mOnComLisener;
    }

    public void setmOnComLisener(OnComLisener mOnComLisener) {
        this.mOnComLisener = mOnComLisener;
    }

    public interface OnComLisener {
        void onComEditSuccess();
    }

}
