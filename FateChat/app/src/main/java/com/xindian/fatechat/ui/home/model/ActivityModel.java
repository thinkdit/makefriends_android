package com.xindian.fatechat.ui.home.model;

import com.xindian.fatechat.common.BaseModel;
import com.xindian.fatechat.ui.login.model.RegisterBean;
import com.xindian.fatechat.ui.login.model.UserAuth;

import java.io.Serializable;

/**
 * 活动列表
 */

public class ActivityModel extends BaseModel implements Serializable, Cloneable {
    private String cover;
    private String createTime;
    private String describe;
    private long id;
    private String spare1;
    private String spare2;

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSpare1() {
        return spare1;
    }

    public void setSpare1(String spare1) {
        this.spare1 = spare1;
    }

    public String getSpare2() {
        return spare2;
    }

    public void setSpare2(String spare2) {
        this.spare2 = spare2;
    }
}
