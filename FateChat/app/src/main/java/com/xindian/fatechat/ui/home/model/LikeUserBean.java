package com.xindian.fatechat.ui.home.model;

/**
 * Created by hx_Alex on 2017/8/10.
 */

public class LikeUserBean {

    /**
     * customerId : string
     * headimg : string
     * like : true
     * nickName : string
     * othersUserId : 0
     * sex : string
     * userType : string
     */

    private String customerId;
    private String headimg;
    private boolean like;
    private String nickName;
    private int othersUserId;
    private String sex;
    private String userType;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getHeadimg() {
        return headimg;
    }

    public void setHeadimg(String headimg) {
        this.headimg = headimg;
    }

    public boolean isLike() {
        return like;
    }

    public void setLike(boolean like) {
        this.like = like;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public int getOthersUserId() {
        return othersUserId;
    }

    public void setOthersUserId(int othersUserId) {
        this.othersUserId = othersUserId;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }
}
