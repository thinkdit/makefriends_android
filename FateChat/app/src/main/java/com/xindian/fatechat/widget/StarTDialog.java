package com.xindian.fatechat.widget;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.thinkdit.lib.util.StringUtil;
import com.xindian.fatechat.R;
import com.xindian.fatechat.databinding.LayoutNormalDialogBinding;


/**
 * 公共dailog
 * Created by qiuda on 16/7/9.
 */
public class StarTDialog implements View.OnClickListener, DialogInterface.OnCancelListener {
    private Dialog mDialog;
    private Context mContext;
    private ViewGroup mViewGroup;
    private boolean mTextGravityCenter;
    private iDialogCallback mBtnCancelCallback;
    private iDialogCallback mBtnOkCallback;
    private iDialogCancelCallback mIDialogCancelCallback;
    private LayoutNormalDialogBinding mLayoutNormalDialogBinding;

    public StarTDialog(Context context, ViewGroup viewGroup) {
        mContext = context;
        mViewGroup = viewGroup;
    }

    public void setTextGravityCenter(boolean isCenter) {
        mTextGravityCenter = isCenter;
        if (mLayoutNormalDialogBinding != null) {
            if (mTextGravityCenter) {
                mLayoutNormalDialogBinding.tvNormalDialogText.setGravity(Gravity.CENTER);
            } else {
                mLayoutNormalDialogBinding.tvNormalDialogText.setGravity(Gravity.NO_GRAVITY);
            }
        }
    }


    /**
     * 创建对话框
     *
     * @param title 标题
     * @param text  内容
     * @param btn1  按钮1  OK按钮
     * @param btn2  按钮2 Cancel按钮
     */
    private void creat(String title, String image, String text, String btn1, String btn2) {
        mDialog = new Dialog(mContext, R.style.Dialog_UserInfo);
        mLayoutNormalDialogBinding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.layout_normal_dialog, mViewGroup, false);
        mDialog.setContentView(mLayoutNormalDialogBinding.getRoot());
        if (mTextGravityCenter) {
            mLayoutNormalDialogBinding.tvNormalDialogText.setGravity(Gravity.CENTER);
        } else {
            mLayoutNormalDialogBinding.tvNormalDialogText.setGravity(Gravity.NO_GRAVITY);
        }

        mDialog.setCancelable(true);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.setOnCancelListener(this);
        mLayoutNormalDialogBinding.btnDialogCancel.setOnClickListener(this);
        mLayoutNormalDialogBinding.btnDailogOk.setOnClickListener(this);
        mLayoutNormalDialogBinding.setTitle(title);
        mLayoutNormalDialogBinding.setImage(image);
        mLayoutNormalDialogBinding.setText(text);
        mLayoutNormalDialogBinding.setBtn1(btn1);
        mLayoutNormalDialogBinding.setBtn2(btn2);
    }

    public void setCanceledOnTouchOutside(boolean isOutCancel) {
        if (mDialog != null) {
            mDialog.setCanceledOnTouchOutside(isOutCancel);
        }
    }

    public void setCancelable(boolean isCancelable) {
        if (mDialog != null) {
            mDialog.setCancelable(isCancelable);
        }
    }

    public StarTDialog setBtnCancelCallback(iDialogCallback btnCancelCallback) {
        mBtnCancelCallback = btnCancelCallback;
        return this;
    }

    public StarTDialog setBtnOkCallback(iDialogCallback btnOkCallback) {
        mBtnOkCallback = btnOkCallback;
        return this;
    }

    public void setIDialogCancelCallback(iDialogCancelCallback IDialogCancelCallback) {
        mIDialogCancelCallback = IDialogCancelCallback;
    }


    @Override
    public void onClick(View v) {
        if (mDialog != null) {
            if (v == mLayoutNormalDialogBinding.btnDailogOk) {
                if (mBtnOkCallback != null) {
                    boolean dismiss = mBtnOkCallback.onBtnClicked();
                    if (dismiss) {
                        dismiss();
                    }
                } else {
                    dismiss();
                }
            } else if (v == mLayoutNormalDialogBinding.btnDialogCancel) {
                if (mBtnCancelCallback != null) {
                    boolean dismiss = mBtnCancelCallback.onBtnClicked();
                    if (dismiss) {
                        dismiss();
                    }
                } else {
                    dismiss();
                }
            }
        }
    }

    public void dismiss() {
        if (mDialog != null) {
            if (mContext instanceof Activity) {
                if (!((Activity) mContext).isFinishing()) {
                    mDialog.dismiss();
                }
            } else {
                mDialog.dismiss();
            }
        }
    }

    public String getText() {
        return (String) mLayoutNormalDialogBinding.tvNormalDialogText.getText();
    }

    public LayoutNormalDialogBinding getmLayoutNormalDialogBinding() {
        return mLayoutNormalDialogBinding;
    }

    public Dialog getmDialog() {
        return mDialog;
    }

    /**
     * 显示对话框
     *
     * @param text 内容
     */
    public void show(String text) {
        show(null, text);
    }

    /**
     * 显示对话框
     *
     * @param title 标题
     * @param text  内容
     */
    public void show(String title, String text) {
        show(title, null, text, mContext.getString(R.string.common_ok), mContext.getString(R.string.common_cancle));
    }

    /**
     * 显示对话框
     *
     * @param text 内容
     * @param btn1 OK按钮文字
     * @param btn2 Cancel 按钮文字
     */
    public void show(String text, String btn1, String btn2) {
        show(null, null, text, btn1, btn2);
    }


    /**
     * 显示对话框
     *
     * @param title 标题
     * @param text  内容
     * @param btn1  OK按钮文字
     * @param btn2  Cancel 按钮文字
     */
    public void show(String title, String text, String btn1, String btn2) {
        show(title, null, text, btn1, btn2);
    }

    /**
     * 显示对话框
     *
     * @param title 标题
     * @param text  内容
     * @param image 图片
     * @param btn1  OK按钮文字
     * @param btn2  Cancel 按钮文字
     */
    public void show(String title, String image, String text, String btn1, String btn2) {
        if (StringUtil.isNullorEmpty(btn2)) {
            btn2 = mContext.getString(R.string.common_cancle);
        }

        if (StringUtil.isNullorEmpty(btn1)) {
            btn1 = mContext.getString(R.string.common_ok);
        }
        if (mDialog == null) {
            creat(title, image, text, btn1, btn2);
        } else {
            mLayoutNormalDialogBinding.setTitle(title);
            mLayoutNormalDialogBinding.setText(text);
            mLayoutNormalDialogBinding.setImage(image);
            mLayoutNormalDialogBinding.btnDailogOk.setText(btn1);
            mLayoutNormalDialogBinding.btnDialogCancel.setText(btn2);
        }
        if (mDialog.isShowing()) {
            return;
        }
        mLayoutNormalDialogBinding.vStartDialogLine.setVisibility(View.VISIBLE);
        mLayoutNormalDialogBinding.btnDialogCancel.setVisibility(View.VISIBLE);
        try {
            mDialog.show();
        } catch (Exception e) {

        }
    }

    /**
     * 显示单个按钮
     *
     * @param text 内容
     */
    public void showSingle(String text) {
        showSingle(null, text);
    }

    /**
     * 显示单个按钮
     *
     * @param title 标题
     * @param text  内容
     */
    public void showSingle(String title, String text) {
        showSingle(title, null, text, mContext.getString(R.string.common_ok));
    }


    /**
     * 显示单个按钮对话框
     *
     * @param title 标题
     * @param text  内容
     * @param btn1  按钮文字
     */
    public void showSingle(String title, String image, String text, String btn1) {
        if (mDialog == null) {
            creat(title, image, text, btn1, "");
        }

        if (mDialog.isShowing()) {
            return;
        }
        mLayoutNormalDialogBinding.vStartDialogLine.setVisibility(View.GONE);
        mLayoutNormalDialogBinding.btnDialogCancel.setVisibility(View.GONE);
        try {
            mDialog.show();
        } catch (Exception e) {
        }
    }

    @Override
    public void onCancel(DialogInterface dialog) {

    }

    public interface iDialogCancelCallback {
        void onCanceled();
    }

    public interface iDialogCallback {

        /**
         * 添加监听
         *
         * @return 是否关闭对话框
         */
        boolean onBtnClicked();

    }
}
