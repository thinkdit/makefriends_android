package com.xindian.fatechat.ui.user.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.xindian.fatechat.R;
import com.xindian.fatechat.databinding.ItemPhotoBinding;
import com.xindian.fatechat.ui.user.model.PhotoInfoBean;

import java.util.List;

/**
 * Created by hx_Alex on 2017/4/4.
 */

public class MyPhotoListAdapter extends RecyclerView.Adapter<MyPhotoListAdapter.ItemPhotoViewHodler> {
    private List<PhotoInfoBean> photoList;
    private Context context;
    private LayoutInflater inflater;
    private  OnItemClickListener mOnItemClickListener;
    private  boolean isShowOtherUserPhoto;
    public MyPhotoListAdapter(Context context, List<PhotoInfoBean> photoList,boolean isShowOtherUserPhoto) {
        this.context = context;
        this.photoList = photoList;
        this.isShowOtherUserPhoto=isShowOtherUserPhoto;
        inflater=LayoutInflater.from(context);
    }

    @Override
    public ItemPhotoViewHodler onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemPhotoBinding mBinding= DataBindingUtil.inflate(inflater, R.layout.item_photo,parent,false);
        return new ItemPhotoViewHodler(mBinding,mOnItemClickListener) ;
    }

    @Override
    public void onBindViewHolder(ItemPhotoViewHodler holder, int position) {
        //第一位默认为从相册选取入口图片
        if(position==0 && !isShowOtherUserPhoto)
        {
            holder.getmBinding().imgAdd.setVisibility(View.VISIBLE);
            holder.getmBinding().imgPhoto.setVisibility(View.GONE);
        }
        else 
        {
            holder.getmBinding().imgAdd.setVisibility(View.GONE);
            holder.getmBinding().imgPhoto.setVisibility(View.VISIBLE);
            if(!isShowOtherUserPhoto) {
                holder.getmBinding().setPhoto(photoList.get(position - 1).getImageUrl());
                holder.getmBinding().setId(photoList.get(position - 1).getId());
            }else {
                holder.getmBinding().setPhoto(photoList.get(position ).getImageUrl());
                holder.getmBinding().setId(photoList.get(position ).getId());
            }
        }
    }

    @Override
    public int getItemCount() {
        if(photoList==null)
        {
            return  !isShowOtherUserPhoto?1:0;
        }
        return !isShowOtherUserPhoto?photoList.size()+1:photoList.size() ;
    }



    public OnItemClickListener getmOnItemClickListener() {
        return mOnItemClickListener;
    }

    public void setmOnItemClickListener(OnItemClickListener mOnItemClickListener) {
        this.mOnItemClickListener = mOnItemClickListener;
    }
    
    /***
     * item点击事件
     */
    public  interface  OnItemClickListener
    {
       void onItemClickListener(View view,int position,Object value,int id);
    }
    /***
     * viewHodler
     */
    public class ItemPhotoViewHodler extends RecyclerView.ViewHolder implements View.OnClickListener {
        private  ItemPhotoBinding mBinding;
        private OnItemClickListener mItemClickListener;
        public ItemPhotoViewHodler(ItemPhotoBinding mBinding,OnItemClickListener mItemClickListener) {
            super(mBinding.getRoot());
            this.mBinding=mBinding;
            this.mItemClickListener=mItemClickListener;
            mBinding.imgPhoto.setOnClickListener(this);
            mBinding.imgAdd.setOnClickListener(this);
        }

        public ItemPhotoBinding getmBinding() {
            return mBinding;
        }

        
        @Override
        public void onClick(View v) {
            if(getAdapterPosition()==0 && !isShowOtherUserPhoto)
            {
                mItemClickListener.onItemClickListener(v,getAdapterPosition(),null,0);
            }else {
                if(!isShowOtherUserPhoto) {
                    mItemClickListener.onItemClickListener(v, getAdapterPosition(), mBinding.getPhoto(), photoList.get(getAdapterPosition() - 1).getId());
                }else 
                {
                    mItemClickListener.onItemClickListener(v, getAdapterPosition(), mBinding.getPhoto(), photoList.get(getAdapterPosition()).getId());
                }
            }
        }
    }
}

