package com.xindian.fatechat.service;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;

import com.alibaba.fastjson.JSON;
import com.igexin.sdk.GTIntentService;
import com.igexin.sdk.PushManager;
import com.igexin.sdk.message.GTCmdMessage;
import com.igexin.sdk.message.GTTransmitMessage;
import com.thinkdit.lib.util.SharePreferenceUtils;
import com.xindian.fatechat.R;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.service.model.ReceiverMessageBean;
import com.xindian.fatechat.ui.login.SplashActivity;
import com.xindian.fatechat.ui.user.MyAttentionActivity;
import com.xindian.fatechat.ui.user.dialog.PunishDialog;
import com.xindian.fatechat.ui.user.dialog.UpLoadHeadImgDialog;
import com.xindian.fatechat.ui.user.model.User;
import com.xindian.fatechat.util.ActivityStack;

import static com.xindian.fatechat.manager.UserInfoManager.KEY_ISSHOWEDUPLOADHEADIMG;


/**
 * Created by hx_Alex on 2017/7/13.
 */

public class GeTuiReceiverService extends GTIntentService {
    private static final int REQUSETCODE = 2525;
    private int notifyId = 1;

    @Override
    public void onReceiveServicePid(Context context, int i) {

    }

    @Override
    public void onReceiveClientId(Context context, String s) {

    }

    @Override
    public void onReceiveMessageData(Context context, GTTransmitMessage gtTransmitMessage) {
        PushManager.getInstance().sendFeedbackMessage(context, gtTransmitMessage.getTaskId(), gtTransmitMessage.getMessageId(), 90001);
        String msg = new String(gtTransmitMessage.getPayload());
        boolean isLogin = UserInfoManager.getManager(context).isLogin();
        Log.e("onReceiveMessageData", msg);
        if (msg != null && isLogin) {
            handlePushMsg(msg, context);

        }

    }

    @Override
    public void onReceiveOnlineState(Context context, boolean b) {
        Log.e(TAG, "onReceiveOnlineState -> " + b);
    }

    @Override
    public void onReceiveCommandResult(Context context, GTCmdMessage gtCmdMessage) {
        Log.e(TAG, "onReceiveCommandResult -> " + gtCmdMessage.getAction());
    }

    /***
     * 根据不同的type处理不同的自定义消息
     */
    private void handlePushMsg(String msg, Context context) {

        ReceiverMessageBean messageBean = JSON.parseObject(msg, ReceiverMessageBean.class);
        Log.e(TAG, "onReceiveCommandResult -> " + messageBean.getContent());
        String type = messageBean.getType();
        if (type != null && !type.equals("")) {

            //关注
            if (type.equals(ReceiverType.follow.toString())) {

                String pushMsg = messageBean.getContent();
                Intent i = new Intent(context, MyAttentionActivity.class);
                showNotication(context, pushMsg, "关注消息", "有新的关注消息", i, false);
            } else if (type.equals(ReceiverType.headAudi.toString()))//头像审批
            {
                String pushMsg = messageBean.getContent();
                //此为IntentService，当任务执行完毕后，将关闭服务，必须将handler指向主线程，否则会有概率出现线程已经dead的情况
                Handler h = new Handler(getMainLooper());
                h.post(new Runnable() {
                    @Override
                    public void run() {
                        showUpLoadHeadImgDialog(context, pushMsg);
                    }
                });
            } else if (type.equals(ReceiverType.punish.toString()))//处分
            {
                String pushMsg = messageBean.getContent();
                String punishType = null;
                String reasonDuration = messageBean.getReasonDuration();
                if (messageBean.getState().equals("1")) {
                    punishType = PunishDialog.NOTICE;
                } else if (messageBean.getState().equals("2")) {
                    punishType = PunishDialog.CLOSE_ACCOUNT;
                }
                Handler h = new Handler(getMainLooper());
                String finalPunishType = punishType;

                h.post(new Runnable() {
                    @Override
                    public void run() {
                        showPunishDialog(context, pushMsg, finalPunishType, reasonDuration);
                    }
                });
            } else if (type.equals(ReceiverType.currency.toString()))//通用
            {
                String pushMsg = messageBean.getContent();
                showCurrencyNotication(context, pushMsg);
            }

        }

    }


    private void showNotication(Context context, String msg, String title, String hint, Intent i, boolean isCurrency) {

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);
        RemoteViews view_custom = new RemoteViews(context.getPackageName(), com.hyphenate.easeui.R.layout
                .layout_view_custom);
        view_custom.setImageViewResource(com.hyphenate.easeui.R.id.custom_icon, R.drawable.my_attention);
        view_custom.setTextViewText(com.hyphenate.easeui.R.id.tv_custom_title, title);
        view_custom.setTextViewText(com.hyphenate.easeui.R.id.tv_custom_content, msg);
        // mBuilder.setNumber(notificationNum);
        if (i != null && !isCurrency) {
            PendingIntent pendingIntent = PendingIntent.getActivity(context, REQUSETCODE, i, PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder.setContentIntent(pendingIntent);
        } else if (i != null && isCurrency) {
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, REQUSETCODE, i, PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder.setContentIntent(pendingIntent);
        }
        mBuilder.setContent(view_custom).setWhen(System.currentTimeMillis())
                // 通知产生的时间，会在通知信息里显示
                .setTicker(hint).setPriority(Notification.PRIORITY_DEFAULT)// 设置该通知优先级
                .setOngoing(false)// 不是正在进行的 true为正在进行
                // 效果和.flag一样
                .setSmallIcon(R.drawable.my_attention);
        Notification notify = mBuilder.build();
        notify.flags |= Notification.FLAG_AUTO_CANCEL;
        NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(notifyId++, notify);
    }


    private void showCurrencyNotication(Context context, String msg) {
        String appName = getAppName(context);
        int drawableIcon = 0;
        Log.e("showCurrencyNotication",appName);
        if (appName != null) {
            if (appName.equals("缘份交友")) {
                drawableIcon = R.drawable.app_icon_1;
            } else if (appName.equals("附近交友")) {
                drawableIcon = R.drawable.fujin_app_icon;
            }else 
            {
                drawableIcon = R.drawable.app_icon_1;
            }
        } else {
            drawableIcon = R.drawable.app_icon_1;
        }
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);
        RemoteViews view_custom = new RemoteViews(context.getPackageName(), com.hyphenate.easeui.R.layout
                .layout_view_custom);
        view_custom.setImageViewResource(com.hyphenate.easeui.R.id.custom_icon,drawableIcon);
        view_custom.setTextViewText(com.hyphenate.easeui.R.id.tv_custom_title, appName);
        view_custom.setTextViewText(com.hyphenate.easeui.R.id.tv_custom_content, msg);
        Intent msgIntent = new Intent(context, SplashActivity.class);
        // mBuilder.setNumber(notificationNum);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, REQUSETCODE, msgIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(pendingIntent);

        mBuilder.setContent(view_custom).setWhen(System.currentTimeMillis())
                // 通知产生的时间，会在通知信息里显示
                .setTicker(appName).setPriority(Notification.PRIORITY_DEFAULT)// 设置该通知优先级
                .setOngoing(false)// 不是正在进行的 true为正在进行
                // 效果和.flag一样
                .setSmallIcon(drawableIcon);
        Notification notify = mBuilder.build();
        notify.flags |= Notification.FLAG_AUTO_CANCEL;
        NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(notifyId++, notify);
    }

    private void showUpLoadHeadImgDialog(Context context, String msg) {
        showNotication(context, "抱歉您的审核未通过，原因为:" + msg, "头像审核结果", "头像审核结果", null, false);
        SharePreferenceUtils.putBoolean(context, KEY_ISSHOWEDUPLOADHEADIMG, false);
        if (context != null) {
            Activity topActivity = ActivityStack.getInstance().getTopActivity();
            UpLoadHeadImgDialog dialog = new UpLoadHeadImgDialog(topActivity);
            dialog.setContext(topActivity);
            User user = UserInfoManager.getManager(this).getUserInfo();
            dialog.setUser(user);
            dialog.setMsg("抱歉您的审核未通过，原因为:" + msg);
            if (!dialog.isShowing()) {
                try {
                    dialog.show();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

        }
    }


    private void showPunishDialog(Context context, String msg, String punishType, String reasonDuration) {

        showNotication(context, "您被系统检测到存在违规行为", "违规行为处理", "违规行为处理", null, false);
        if (context != null) {
            Activity topActivity = ActivityStack.getInstance().getTopActivity();
            PunishDialog dialog = new PunishDialog(topActivity);
            dialog.setContext(context);
            User user = UserInfoManager.getManager(this).getUserInfo();
            dialog.setUser(user);
            dialog.setMsg(msg);
            dialog.setPunishType(punishType);
            dialog.setOpenTime(reasonDuration);
            if (!dialog.isShowing()) {
                dialog.show();
            }

        }
    }


    public enum ReceiverType {
        follow, headAudi, punish, currency
    }

    /*
    * 获取应用程序名称
    */
    public static String getAppName(Context context) {
        try {
            PackageManager packageManager = context.getPackageManager();
            PackageInfo packageInfo = packageManager.getPackageInfo(
                    context.getPackageName(), 0);
            String labelRes = packageInfo.applicationInfo.nonLocalizedLabel.toString();
            return labelRes;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            Log.e("showCurrencyNotication", e.getMessage());
            return null;
        }
     
    }

    /*
    * 获取程序 图标
    */
    public Drawable getAppIcon(String packname, Context context) {
        try {
            PackageManager packageManager = context.getPackageManager();
            ApplicationInfo info = packageManager.getApplicationInfo(packname, 0);
            return info.loadIcon(packageManager);
        } catch (PackageManager.NameNotFoundException e) {
            // TODO Auto-generated catch block    
            e.printStackTrace();
            return  null;
        }
     
    }


}
