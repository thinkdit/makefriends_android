package com.xindian.fatechat.pay.wxpay.bbnpay;

import android.app.Activity;
import android.content.Intent;

import com.payh5.bbnpay.mobile.activity.Payh5Activity;
import com.payh5.bbnpay.mobile.main.BbnpayResultCallback;

import java.net.URLEncoder;

/**
 * Created by hx_Alex on 2017/5/12.
 */

public class BbnPay {
    private static String appid;
    private static int screenType;
    private static Activity act;
    private static String bbnpay_back;

    public BbnPay() {
    }

    public static void init(Activity activity, String app) {
        appid = app;
        screenType = 1;
        act = activity;
        bbnpay_back = "";
    }

    public static void init(Activity activity, int scrtype, String app) {
        appid = app;
        screenType = scrtype;
        act = activity;
    }

    public static void startPay(Activity activity, String popurl, String params) {
        String pkName = activity.getPackageName();
        Intent i = new Intent(activity, Payh5Activity.class);
        popurl = popurl + "?appid=" + appid + "&transid=" + params + "&pkname=" + URLEncoder.encode(pkName);
        i.putExtra("payurl", popurl);
        activity.startActivity(i);
    }

    public static void startPay(Activity activity, String popurl, String params, String backaction) {
        bbnpay_back = backaction;
        String pkName = activity.getPackageName();
        Intent i = new Intent(activity, BbnPayh5Activity.class);
        popurl = popurl + "?appid=" + appid + "&transid=" + params + "&pkname=" + URLEncoder.encode(pkName);
        i.putExtra("payurl", popurl);
        activity.startActivity(i);
    }

    public static void startPay(Activity activity, String popurl, String params, BbnpayResultCallback callback) {
        Intent i = new Intent(activity, Payh5Activity.class);
        popurl = popurl + "?" + params;
        i.putExtra("payurl", popurl);
        activity.startActivity(i);
    }

    public static String get_back() {
        return bbnpay_back;
    }
}
