package com.xindian.fatechat.ui.home.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by qf on 2016/11/3.
 */
public class JHTuiJianBean implements Serializable{

    public static final int STADUS_WAIT = 0;
    public static final int STADUS_SU = 1;
    public static final int STADUS_NO = 2;
    public static final int STADUS_DEL = 3;

    public static final int TYPE_TEXT = 1;
    public static final int TYPE_IMAGE = 2;
    public static final int TYPE_VEDIO = 3;

    private String anonymous;// (string, optional),
    private String commentNum;//  (integer, optional): 评论数 ,
    private String creationTime;//  (string, optional): 发布时间 ,
    private boolean dlike;//  (boolean, optional): 是否喜欢true为喜欢了 ,
    private ArrayList<CommentBean> dynamicCommentList;//  (Array[DynamicComment], optional): 动态的评论集合 ,
    private String dynamicContent;//  (string, optional): 动态内容 ,
    private int dynamicDislike;//  (integer, optional): 动态不喜欢数 ,
    private int dynamicLike;//  (integer, optional): 动态喜欢数 ,
    private String headUrl;//  (string, optional): 发布者用户头像 ,
    private int id;//  (integer, optional),
    private ArrayList<DynamicLike> likeUserList;//  (Array[DynamicLike], optional): 动态点赞过的用户头像 ,
    private String nickName;//  (string, optional): 发布者用户昵称 ,
    private boolean noLike;//  (boolean, optional): 是否不喜欢true为不喜欢了 ,
    private String sourceUrl;//  (string, optional): 视频或者图片地址 ,
    private String spare1;//  (string, optional),
    private String spare2;//  (string, optional): 审核拒绝原因 ,
    private int state;//  (integer, optional): 状态 0待审核1审核通过2审核拒绝3删除 ,
    private int type;//  (string, optional): 1 文字 2图片 3 视频 ,
    private int userGrade;//  (integer, optional): 等级 ,
    private long userId;//  (integer, optional): 发布者用户id
    private String sex;//性别


    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAnonymous() {
        return anonymous;
    }

    public void setAnonymous(String anonymous) {
        this.anonymous = anonymous;
    }

    public String getCommentNum() {
        return commentNum;
    }

    public void setCommentNum(String commentNum) {
        this.commentNum = commentNum;
    }

    public String getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(String creationTime) {
        this.creationTime = creationTime;
    }

    public boolean isDlike() {
        return dlike;
    }

    public void setDlike(boolean dlike) {
        this.dlike = dlike;
    }

    public ArrayList<CommentBean> getDynamicCommentList() {
        return dynamicCommentList;
    }

    public void setDynamicCommentList(ArrayList<CommentBean> dynamicCommentList) {
        this.dynamicCommentList = dynamicCommentList;
    }

    public String getDynamicContent() {
        return dynamicContent;
    }

    public void setDynamicContent(String dynamicContent) {
        this.dynamicContent = dynamicContent;
    }

    public int getDynamicDislike() {
        return dynamicDislike;
    }

    public void setDynamicDislike(int dynamicDislike) {
        this.dynamicDislike = dynamicDislike;
    }

    public int getDynamicLike() {
        return dynamicLike;
    }

    public void setDynamicLike(int dynamicLike) {
        this.dynamicLike = dynamicLike;
    }

    public String getHeadUrl() {
        return headUrl;
    }

    public void setHeadUrl(String headUrl) {
        this.headUrl = headUrl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<DynamicLike> getLikeUserList() {
        return likeUserList;
    }

    public void setLikeUserList(ArrayList<DynamicLike> likeUserList) {
        this.likeUserList = likeUserList;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public boolean isNoLike() {
        return noLike;
    }

    public void setNoLike(boolean noLike) {
        this.noLike = noLike;
    }

    public String getSourceUrl() {
        return sourceUrl;
    }

    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }

    public String getSpare1() {
        return spare1;
    }

    public void setSpare1(String spare1) {
        this.spare1 = spare1;
    }

    public String getSpare2() {
        return spare2;
    }

    public void setSpare2(String spare2) {
        this.spare2 = spare2;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getUserGrade() {
        return userGrade;
    }

    public void setUserGrade(int userGrade) {
        this.userGrade = userGrade;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public class DynamicLike {
        private int dynamicId;
        private String headUrl;
        private int type;
        private long userId;

        public int getDynamicId() {
            return dynamicId;
        }

        public void setDynamicId(int dynamicId) {
            this.dynamicId = dynamicId;
        }

        public String getHeadUrl() {
            return headUrl;
        }

        public void setHeadUrl(String headUrl) {
            this.headUrl = headUrl;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public long getUserId() {
            return userId;
        }

        public void setUserId(long userId) {
            this.userId = userId;
        }
    }
}
