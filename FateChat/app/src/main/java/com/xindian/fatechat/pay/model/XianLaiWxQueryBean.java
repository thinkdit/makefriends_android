package com.xindian.fatechat.pay.model;

import com.xindian.fatechat.common.BaseModel;

/**
 * Created by hx_Alex on 2017/6/28.
 */

public class XianLaiWxQueryBean extends BaseModel 
{

    /**
     * code : 0
     * order_id : 1703241648292449616100077
     * pay_result : 0
     */

    private String code;
    private String order_id;
    private String pay_result;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getPay_result() {
        return pay_result;
    }

    public void setPay_result(String pay_result) {
        this.pay_result = pay_result;
    }
}
