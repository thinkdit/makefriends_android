package com.xindian.fatechat.ui.home.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.xindian.fatechat.ui.home.fragment.NearByFriendsFragment;
import com.xindian.fatechat.ui.home.fragment.RecommendFriendsFragment;


/**
 */
public class HomeFriendsPagerAdapter extends FragmentPagerAdapter {

    public HomeFriendsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public Fragment getItem(int position) {
        return position == 0 ? new RecommendFriendsFragment() : new NearByFriendsFragment();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }
}
