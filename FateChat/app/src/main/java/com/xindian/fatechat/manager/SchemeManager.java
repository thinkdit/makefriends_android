package com.xindian.fatechat.manager;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;

import com.xindian.fatechat.ui.user.PersonaldataAcitvity;

import static com.xindian.fatechat.ui.user.PersonaldataAcitvity.NOW_UP_LOAD_AVATAR;

/**
 * Scheme协议类
 */
public class SchemeManager {
    public static final String BASE_PROTOCOL = "fatechat";
    public static final String BASE_DOMAIN = BASE_PROTOCOL + "/";
    public static final String BASE_HTTP = "http";
    public static final String BASE_HTTPS = "https";
    public static final int SCHEME_REQUEST_CODE = 88;
    private final String TAG = "SchemeManager";
    public static final String SCHEME_START_HEAD_PORTTAIT = BASE_PROTOCOL + "://" + "FateChat/" + "headPortrait";
    public static final String SCHEME_START_HEAD_PRESONLDATA = BASE_PROTOCOL + "://" + "FateChat/" + "personlData";

    private static SchemeManager schemaManager;


    public static SchemeManager getInstance() {
        if (schemaManager == null) {
            schemaManager = new SchemeManager();
        }
        return schemaManager;
    }


    /**
     * Scheme 跳转界面 Context处理
     *
     * @param c   context
     * @param url 协议地址
     * @return 是否跳转
     */
    public boolean jumpToActivity(Context c, String url) {
        return jumpToActivity(c, url, false);
    }


    /**
     * Scheme 跳转界面   Context处理
     *
     * @param c         context
     * @param url       协议地址
     * @param isNewTask 是否重启Task
     * @return 是否跳转
     */
    public boolean jumpToActivity(Context c, String url, boolean isNewTask) {
        Intent intent = getJumpIntent(c, url, isNewTask);
        if (intent != null) {
            c.startActivity(intent);
            return true;
        }
        return false;
    }


    /**
     * Scheme 跳转界面  Activity处理
     *
     * @param f   Fragment
     * @param url 协议地址
     * @return 是否跳转
     */
    public boolean jumpToActivity(Fragment f, String url) {
        return jumpToActivity(f, url, false, SCHEME_REQUEST_CODE, false);
    }

    /**
     * Scheme 跳转界面 Activity处理
     *
     * @param f            Fragment
     * @param url          url
     * @param needCallback 是否需要返回
     * @return 是否跳转
     */
    public boolean jumpToActivity(Fragment f, String url, boolean needCallback) {
        return jumpToActivity(f, url, needCallback, SCHEME_REQUEST_CODE, false);
    }

    /**
     * Scheme 跳转界面  Activity处理
     *
     * @param f   Fragment
     * @param url 协议地址
     * @return 是否跳转
     */
    public boolean jumpToActivity(Fragment f, String url, boolean needCallback, int requestCode) {
        return jumpToActivity(f, url, needCallback, requestCode, false);
    }


    /**
     * Scheme 跳转界面 Activity处理
     *
     * @param f            Fragment
     * @param url          url
     * @param needCallback 是否需要返回
     * @param requestCode  requestCode
     * @param isNewTask    是否重启Task
     * @return 是否跳转
     */
    public boolean jumpToActivity(Fragment f, String url, boolean needCallback, int requestCode, boolean isNewTask) {
        Intent intent = getJumpIntent(f.getActivity(), url, isNewTask);
        if (intent != null) {
            if (needCallback) {
                f.startActivityForResult(intent, requestCode);
            } else {
                f.startActivity(intent);
            }
            return true;
        }
        return false;
    }

    private Intent getJumpIntent(Context context, String url, boolean isNewTask) {
        Intent i=new Intent();
        if(url.startsWith(SCHEME_START_HEAD_PORTTAIT))
        {
            i.setClass(context, PersonaldataAcitvity.class);
            i.putExtra(NOW_UP_LOAD_AVATAR,true);
        }else if(url.startsWith(SCHEME_START_HEAD_PRESONLDATA))
        {
            i.setClass(context, PersonaldataAcitvity.class);
        }
        return  i;
    }

    /**
     * Scheme 跳转界面  Activity处理
     *
     * @param a   Activity
     * @param url 协议地址
     * @return 是否跳转
     */
    public boolean jumpToActivity(Activity a, String url) {
        return jumpToActivity(a, url, false, SCHEME_REQUEST_CODE, false);
    }

    /**
     * Scheme 跳转界面 Activity处理
     *
     * @param a            Activity
     * @param url          url
     * @param needCallback 是否需要返回
     * @return 是否跳转
     */
    public boolean jumpToActivity(Activity a, String url, boolean needCallback) {
        return jumpToActivity(a, url, needCallback, SCHEME_REQUEST_CODE, false);
    }

    /**
     * Scheme 跳转界面  Activity处理
     *
     * @param a   Activity
     * @param url 协议地址
     * @return 是否跳转
     */
    public boolean jumpToActivity(Activity a, String url, boolean needCallback, int requestCode) {
        return jumpToActivity(a, url, needCallback, requestCode, false);
    }


    /**
     * Scheme 跳转界面 Activity处理
     *
     * @param a            Activity
     * @param url          url
     * @param needCallback 是否需要返回
     * @param requestCode  requestCode
     * @param isNewTask    是否重启Task
     * @return 是否跳转
     */
    public boolean jumpToActivity(Activity a, String url, boolean needCallback, int requestCode, boolean isNewTask) {
        Intent intent = getJumpIntent(a, url, isNewTask);
        if (intent != null) {
            if (needCallback) {
                a.startActivityForResult(intent, requestCode);
            } else {
                a.startActivity(intent);
            }
            return true;
        }
        return false;
    }
    /**
     * 打开页面,不带入参数
     *
     * @param c         c
     * @param isNewTask 是否启用新Task
     */
    private Intent navigateCommon(Context context, Class c, boolean isNewTask) {
        Intent i = new Intent(context, c);
        if (isNewTask) {
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        return i;
    }

}
