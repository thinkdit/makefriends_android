package com.xindian.fatechat.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Administrator on 2016/11/1 0001.
 */
public class HttpUntil {
    static OkHttpClient okHttpClient;
    public static String getStringByOkhttp(String url, Context context) {
        String json = "";
        CacheControl cacheControl ;
        if(isNetworkConnected(context)){
            cacheControl = CacheControl.FORCE_NETWORK;
        }else{
            cacheControl = CacheControl.FORCE_CACHE;
        }
        if(okHttpClient == null){
            okHttpClient = new OkHttpClient()
                    .newBuilder()
                    .cache(new Cache(context.getCacheDir(), 1024 * 1024 * 30))
                    .connectTimeout(20, TimeUnit.SECONDS)
                    .readTimeout(40, TimeUnit.SECONDS)
                    .build();//1、创建OkHttpClient
        }


        Request request = new Request.Builder()//2、通过构造器构造Request
                .url(url)//配置URL
                .cacheControl(cacheControl)
                .tag("tag")//为request设置标签，将来可以通过标签找到请求，并取消之
                .build();
        try {
            Response response = okHttpClient.newCall(request).execute();//3、执行Request
            //4、解析Response
            if (response.isSuccessful()) {
                json = response.body().string();//拿到字符流，还可以是bytes(),byteStream()
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return json;
    }


    public static String getVideoJH(String pages, Context context) {
        String url = MVUrlConstant.getVideoJH.replace("%s",pages);
        String json = getStringByOkhttp(url, context);
        return json;
    }

    public static String getjinghuaDrawString(int num, Context context) {
        String url = MVUrlConstant.getjinghuaDrawString.replace("%s",num+"");
        String json = getStringByOkhttp(url, context);
        return json;
    }
    public static String getJHShiPinJson(int count, Context context) {
        String url = MVUrlConstant.getJHShiPinJson.replace("%s",count+"");
        String json = getStringByOkhttp(url, context);
        return json;
    }

    public static String getJHDuanZiJson(int count, Context context) {
        String url = MVUrlConstant.getJHDuanZiJson.replace("%s",count+"");
        String json = getStringByOkhttp(url, context);
        return json;
    }

    public static String getJHLengZhiShiJson(int count, Context context) {
        String url = MVUrlConstant.getJHLengZhiShiJson.replace("%s",count+"");
        String json = getStringByOkhttp(url, context);
        return json;
    }

    public static String getJHMeiNvJson(int count, Context context) {
        String url = MVUrlConstant.getJHMeiNvJson.replace("%s",count+"");
        String json = getStringByOkhttp(url, context);
        return json;
    }

    public static String getJHPaiHangJson(int count, Context context) {
        String url = MVUrlConstant.getJHPaiHangJson.replace("%s",count+"");
        String json = getStringByOkhttp(url, context);
        return json;
    }

    public static String getJHSheHuiJson(int count, Context context) {
        String url = MVUrlConstant.getJHSheHuiJson.replace("%s",count+"");
        String json = getStringByOkhttp(url, context);
        return json;
    }

    public static String getJHTuiJianJson(int count, Context context) {
        String url = MVUrlConstant.getJHTuiJianJson.replace("%s",count+"");
        String json = getStringByOkhttp(url, context);
        return json;
    }

    public static String getJHWangHongJson(int count, Context context) {
        String url = MVUrlConstant.getJHWangHongJson.replace("%s",count+"");
        String json = getStringByOkhttp(url, context);
        return json;
    }

    public static String getJHYouXiJson(int count, Context context) {
        String url = MVUrlConstant.getJHYouXiJson.replace("%s",count+"");
        String json = getStringByOkhttp(url, context);
        return json;
    }

    public static String getXTShengYinJson(int count, Context context) {
        String url = MVUrlConstant.getjinghuaDrawString.replace("%s",count+"");
        String json = getStringByOkhttp(url, context);
        return json;
    }

    public static String getXTQuanBuJson(int count, Context context) {
        String url = MVUrlConstant.getjinghuaDrawString.replace("%s",count+"");
        String json = getStringByOkhttp(url, context);
        return json;
    }

    public static String getXTShiPinJson(int count, Context context) {
        String url = MVUrlConstant.getXTShiPinJson.replace("%s",count+"");
        String json = getStringByOkhttp(url, context);
        return json;
    }

    public static String getXTDuanZiJson(int count, Context context) {
        String url = MVUrlConstant.getXTDuanZiJson.replace("%s",count+"");
        String json = getStringByOkhttp(url, context);
        return json;
    }

    public static String getXTTuPianJson(int count, Context context) {
        String url = MVUrlConstant.getXTTuPianJson.replace("%s",count+"");
        String json = getStringByOkhttp(url, context);
        return json;
    }

    public static String getXTWangHongJson(int count, Context context) {
        String url = MVUrlConstant.getXTWangHongJson.replace("%s",count+"");
        String json = getStringByOkhttp(url, context);
        return json;
    }


    public static String getXTMeiNvJson(int count, Context context) {
        String url = MVUrlConstant.getXTMeiNvJson.replace("%s",count+"");
        String json = getStringByOkhttp(url, context);
        return json;
    }

    public static String getXTLengZhiShiJson(int count, Context context) {
        String url = MVUrlConstant.getXTLengZhiShiJson.replace("%s",count+"");
        String json = getStringByOkhttp(url, context);
        return json;
    }

    public static String getXTYouXiJson(int count, Context context) {
        String url = MVUrlConstant.getXTYouXiJson.replace("%s",count+"");
        String json = getStringByOkhttp(url, context);
        return json;
    }

    public static String getPingLunJson(int itemID, Context context) {
        String url = MVUrlConstant.getPingLunJson.replace("%s",itemID+"");
        String json = getStringByOkhttp(url, context);
        return json;
    }

    /**
     * 获取豆瓣美女网页数据
     * @param url
     * @param page
     * @param context
     * @return
     */
    public static String getDBMN(String url,int page, Context context) {
        String json = getStringByOkhttp(url+"&pager_offset="+page, context);
        return json;
    }

    /**
     * 获取妹子图美女网页数据
     * @param url
     * @param page
     * @param context
     * @return
     */
    public static String getMZMN(String url,int page, Context context) {
        String requestUrl = url;
        if(page>1){
            requestUrl = url+"/page/"+page;
        }
        String json = getStringByOkhttp(requestUrl, context);
        return json;
    }

    /**
     * 获取妹子6美女网页数据
     * @param url
     * @param page
     * @param context
     * @return
     */
    public static String getMZ6MN(String url,int page, Context context) {
        String requestUrl = url;
        if(page>1){
            requestUrl = url+"index_"+page+".html";
        }
        String json = getStringByOkhttp(requestUrl, context);
        return json;
    }

    /**
     * 获取老司机网页数据
     * @param url
     * @param page
     * @param context
     * @return
     */
    public static String getLSJ(String url,int page, Context context) {
        String requestUrl = url;
        if(page>1){
            requestUrl = url+"?page="+page;
        }
        String json = getStringByOkhttp(requestUrl, context);
        return json;
    }

    /**
     * 获取蝌蚪窝网页数据
     * @param url
     * @param page
     * @param context
     * @return
     */
    public static String getKDWVedio(String url,int page, Context context) {
        String requestUrl = url;
        if(page>1){
            requestUrl = url+"latest-updates/"+page+"/";
        }
        String json = getStringByOkhttp(requestUrl, context);
        return json;
    }

    /**
     * 获取蝌蚪窝最热网页数据
     * @param url
     * @param page
     * @param context
     * @return
     */
    public static String getKDWZRVedio(String url,int page, Context context) {
        String requestUrl = url;
        if(page>1){
            requestUrl = url+page+"/";
        }
        String json = getStringByOkhttp(requestUrl, context);
        return json;
    }
    /**
     * 获取大鸡吧网页数据
     * @param url
     * @param page
     * @param context
     * @return
     */
    public static String getLDJBVedio(String url,int page, Context context) {
        String requestUrl = url;
        if(page>1){
            requestUrl = url+"/page/"+page;
        }
        String json = getStringByOkhttp(requestUrl, context);
        return json;
    }

    /**
     * 获取BEEM网页数据
     * @param url
     * @param page
     * @param context
     * @return
     */
    public static String getBEEMVedio(String url,int page, Context context) {
        String requestUrl = url;
        if(page>1){
            requestUrl = url+"/page/"+page;
        }
        String json = getStringByOkhttp(requestUrl, context);
        return json;
    }

    /**
     * 获取BEEM网页数据
     * @param url
     * @param page
     * @param context
     * @return
     */
    public static String getOUMEILive(String url,int page, Context context) {
        String requestUrl = url;
        if(page>1){
            requestUrl = url+"/?page="+page;
        }
        String json = getStringByOkhttp(requestUrl, context);
        return json;
    }


    /**
     * 番号女优网页数据
     * @param url
     * @param page
     * @param context
     * @return
     */
    public static String getFanNY(String url,int page, Context context) {
        String requestUrl = url;
        if(page>1){
            requestUrl = url+"/"+page;
        }
        String json = getStringByOkhttp(requestUrl, context);
        return json;
    }

    /**
     * 番号影片网页数据
     * @param url
     * @param page
     * @param context
     * @return
     */
    public static String getFanYP(String url,int page, Context context) {
        String requestUrl = url;
        if(page>1){
            requestUrl = url+"/page/"+page;
        }
        String json = getStringByOkhttp(requestUrl, context);
        return json;
    }

    /**
     * 番号搜索网页数据
     * @param url
     * @param page
     * @param context
     * @return
     */
    public static String getFanSOU(String url,String keyWorld,int page, Context context) {
        try {
            keyWorld = URLEncoder.encode(keyWorld,"utf-8");
            String requestUrl = url+"/"+keyWorld;
            if(page>1){
                requestUrl = requestUrl+"/"+page;
            }
            String json = getStringByOkhttp(requestUrl, context);
            return json;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    private static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null && ni.isConnectedOrConnecting();
    }
}
