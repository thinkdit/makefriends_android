package com.xindian.fatechat.ui.user.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.xindian.fatechat.R;
import com.xindian.fatechat.databinding.ItemMyAttentionBinding;
import com.xindian.fatechat.ui.user.model.UserBean;

import java.util.List;


/**
 * Created by hx_Alex on 2017/3/27.
 */

public class MyAttentionListAdapter extends BaseAdapter {
    private List<UserBean>  userList;
    private Context context;
    private LayoutInflater inflater;
    private ItemMyAttentionBinding mBinding;
    private  onClickCancelListener mListener;
    public MyAttentionListAdapter(List<UserBean> userList, Context context) {
        this.userList = userList;
        this.context = context;
        inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    public void setOnClickCancelListener(onClickCancelListener mListener)
    {
        this.mListener=mListener;
    }
    
    @Override
    public int getCount() {
        return userList.size();
    }

    @Override
    public Object getItem(int position) {
        return userList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            mBinding= DataBindingUtil.inflate(inflater, R.layout.item_my_attention,parent,false);
            convertView = mBinding.getRoot();
            convertView.setTag(mBinding);
        }
        else
        {
            mBinding= (ItemMyAttentionBinding) convertView.getTag();
        }
        
        if (userList== null || userList.size()==0) {
            return convertView;
        }
        else
        {
            mBinding.setUser(userList.get(position));
            mBinding.btnAttentionCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onClickBtnCancelAttention(userList.get(position).getUserId());
                }
            });
            mBinding.layoutAttention.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onClickAttention(userList.get(position).getUserId());
                }
            });
        }
        return convertView;
    }


    
    public  interface  onClickCancelListener
    {
       void  onClickBtnCancelAttention(int  femmeUserId);
        void  onClickAttention(int  femmeUserId);
    }
}
