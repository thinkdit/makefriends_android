package com.xindian.fatechat.ui.user.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.xindian.fatechat.R;
import com.xindian.fatechat.databinding.ItemCashUserBinding;
import com.xindian.fatechat.ui.user.data.UserData;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by hx_Alex on 2017/4/14.
 */

public class UserCashListAdapter extends BaseAdapter {
    private static final String DAY30="前成功充值68元成为钻石VIP";
    private static final String DAY90="前成功充值97元成为钻石VIP";
    private ArrayList<HashMap<String, String>> nameLsit;
    private Context context;
    private LayoutInflater inflater;
    private ItemCashUserBinding mBinding;
    public UserCashListAdapter(Context context, ArrayList<HashMap<String, String>> nameLsit) {
        this.context = context;
        this.nameLsit = nameLsit;
        inflater= LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return nameLsit.size();
    }

    @Override
    public Object getItem(int position) {
        return nameLsit.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null)
        {
            mBinding= DataBindingUtil.inflate(inflater, R.layout.item_cash_user,parent,false);
            convertView=mBinding.getRoot();
            convertView.setTag(mBinding);
        }
        else
        {
            mBinding= (ItemCashUserBinding) convertView.getTag();
        }
        mBinding.setName((String) nameLsit.get(position).get(UserData.NAME));
        mBinding.setContent(nameLsit.get(position).get(UserData.TIME));
        return convertView;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }
}
