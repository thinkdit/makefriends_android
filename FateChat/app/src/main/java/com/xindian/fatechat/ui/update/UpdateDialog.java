package com.xindian.fatechat.ui.update;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.method.ScrollingMovementMethod;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.thinkdit.lib.util.SharePreferenceUtils;
import com.xindian.fatechat.R;
import com.xindian.fatechat.manager.AppUpdateManager;

/**
 */
public class UpdateDialog extends Dialog implements View.OnClickListener,
        AppUpdateManager.OnUpdateCallback {

    private Activity mActivity;
    private AppUpdateManager mAppUpdateManager;
    private AppVersionInfo mVersionInfo;
    private View mLayoutBtn;
    private View mClosetBtn;
    private ProgressBar mProgressBar;
    private boolean mFinish;

    private final static int WRITE_EXTERNAL_STORAGE_REQUEST_CODE = 1;

    public UpdateDialog(Activity activity, AppVersionInfo info) {
        super(activity, R.style.FullScreen_dialog);
        mActivity = activity;
        mVersionInfo = info;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_up_dialog);
        if (mVersionInfo.isForceUpgrade()) { //强制升级不让取消
            setCancelable(false);
            setCanceledOnTouchOutside(false);
            setOnKeyListener((DialogInterface dialog, int keyCode, KeyEvent event) -> {
                        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
                            return true;
                        } else {
                            return false;
                        }
                    }
            );
        }
        setOnDismissListener((DialogInterface dialog) -> {
            mFinish = true;
        });
        mLayoutBtn = findViewById(R.id.tv_normal_dialog_bu);
        mClosetBtn = findViewById(R.id.tv_normal_close_bu);
        mProgressBar = (ProgressBar) findViewById(R.id.progressbar);
        mLayoutBtn.setOnClickListener(this);
        mClosetBtn.setOnClickListener(this);
        TextView content = (TextView) findViewById(R.id.tv_normal_dialog_text);
        content.setText(mVersionInfo.getUpgradeDescription());
        content.setMovementMethod(new ScrollingMovementMethod());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_normal_close_bu:
                if (mVersionInfo.isForceUpgrade()) {
                    dismiss();
                    mActivity.finish();
                } else {
                    SharePreferenceUtils.putString(mActivity,
                            SystemParamsPresenter.KEY_INGNORED_VERSION, mVersionInfo.getUpgradeVersionName());
                    dismiss();
                }
                break;
            case R.id.tv_normal_dialog_bu:
                //6.0以上系统先申请权限
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(mActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        //申请WRITE_EXTERNAL_STORAGE权限
                        ActivityCompat.requestPermissions(mActivity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                WRITE_EXTERNAL_STORAGE_REQUEST_CODE);
                    } else {
                        mAppUpdateManager = new AppUpdateManager(mActivity, this);
                        mAppUpdateManager.startDownload(mVersionInfo.getDownloadUrl());
                    }
                } else {
                    mAppUpdateManager = new AppUpdateManager(mActivity, this);
                    mAppUpdateManager.startDownload(mVersionInfo.getDownloadUrl());
                }

                break;
        }
    }

    @Override
    public void onUpdateStart() {
        if (!mFinish) {
            mLayoutBtn.setVisibility(View.GONE);
            mClosetBtn.setVisibility(View.GONE);
            mProgressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onUpdateProgress(int progress) {
        if (!mFinish) {
            mProgressBar.setProgress(progress);
        }
    }

    @Override
    public void onUpdateEnd() {
        if (!mFinish) {
            if (mVersionInfo.isForceUpgrade()) {
                mLayoutBtn.setVisibility(View.VISIBLE);
                mClosetBtn.setVisibility(View.VISIBLE);
                mProgressBar.setVisibility(View.GONE);
            } else {
                dismiss();
            }
        }
    }
}
