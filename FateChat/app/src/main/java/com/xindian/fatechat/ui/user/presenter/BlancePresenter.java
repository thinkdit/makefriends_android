package com.xindian.fatechat.ui.user.presenter;

import android.os.RemoteException;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.parser.Feature;
import com.xindian.fatechat.common.BasePresenter;
import com.xindian.fatechat.common.FateChatApplication;
import com.xindian.fatechat.common.ResultModel;
import com.xindian.fatechat.manager.UserInfoManager;
import com.xindian.fatechat.ui.home.model.RulesModel;
import com.xindian.fatechat.ui.user.model.BalanceBean;

import org.json.JSONException;

import java.util.HashMap;

/**
 * 钻石余额接口
 */

public class BlancePresenter extends BasePresenter {
    private final String URL_GET_BALANCE = "/account/get_account_balance";


    IGetBalance mGetRules;

    public BlancePresenter(IGetBalance rules) {
        mGetRules = rules;
    }

    public void getBalance() {
        HashMap<String, Object> data = new HashMap<>();
        data.put("userId", UserInfoManager.getManager(FateChatApplication.getInstance()).getUserId());
        data.put("token", UserInfoManager.getManager(FateChatApplication.getInstance()).getToken());
        post(getUrl(URL_GET_BALANCE), data, null);
    }


    @Override
    public Object asyncExecute(String url, ResultModel resultModel) {
        if (url.contains(URL_GET_BALANCE)) {
            return JSON.parseObject(resultModel.getData(), BalanceBean.class, Feature
                    .IgnoreNotMatch);
        }
        return null;
    }

    @Override
    public void onError(String url, Exception e) {
        super.onError(url, e);
    }


    @Override
    public void onFailure(String url, ResultModel resultModel) {
        super.onFailure(url,resultModel);
        if (url.contains(URL_GET_BALANCE)) {
        }
    }

    @Override
    public void onSucceed(String url, ResultModel resultModel) throws JSONException,
            RemoteException {
        if (url.contains(URL_GET_BALANCE)) {
            BalanceBean ruleMode = (BalanceBean) resultModel.getDataModel();
            mGetRules.onGetBalanceSuccess(ruleMode);
        }
    }

    public interface IGetBalance {
        public void onGetBalanceSuccess(BalanceBean model);
    }

}
