package com.xindian.fatechat.manager;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import com.xindian.fatechat.ui.login.LoginDialog;
import com.xindian.fatechat.ui.user.model.User;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by leaves on 2017/4/26.
 */

public class LoginManager {
    private static LoginManager instance;
    private List<LoginDialog.onLoginListener> listeners = new ArrayList<>();
    private  static  final  int MSG_LGOINSUCCESS=1;
    private  static  final  int MSG_LGOINERORR=-1;
    private  static  final  String USER="user";
    private ImLoginSuccessHandler handler;
    public void registerLoginListerer(LoginDialog.onLoginListener l) {
        listeners.add(l);
    }

    
    
    public  LoginManager(Context context)
    {
        handler=new ImLoginSuccessHandler(context.getMainLooper());
    }
    /***
     * 退出账号时需要remove掉所有监听器
     */
    public  void removeLoginListenerAll()
    {
        if(listeners==null || listeners.size()==0) return;
      Collection<LoginDialog.onLoginListener> collection= listeners;
        listeners.removeAll(collection);
    }

    public void postLoginSuccess(User user) {
        Message msg=new Message();
        Bundle data=new Bundle();
        data.putSerializable(USER,user);
        msg.what=MSG_LGOINSUCCESS;
        msg.setData(data);
        handler.sendMessage(msg);
    }

    public static LoginManager instance(Context context) {
      
        if (instance == null) {
            instance = new LoginManager(context);
        }
        return instance;
    }
    
    class ImLoginSuccessHandler extends Handler
    {
        public ImLoginSuccessHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            Bundle data = msg.getData();
            switch (msg.what)
          {
              case MSG_LGOINSUCCESS:
                  if (listeners != null && listeners.size() > 0 ) {

                      for (LoginDialog.onLoginListener l : listeners) {
                          User user = (User) data.get(USER);
                          l.LoginSucceed(user);
                      }
                  }
                  break;
              case MSG_LGOINERORR:
                  break;
          }
            
        }
    }
}
