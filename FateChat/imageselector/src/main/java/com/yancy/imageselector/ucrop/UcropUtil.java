package com.yancy.imageselector.ucrop;

import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;
import com.yalantis.ucrop.UCrop;

/**
 * Created by hx_Alex on 2017/7/10.
 * ucrop剪切库封装
 */

public class UcropUtil {
    private UCrop mUcrop;
    private UCrop.Options options;
    private static UcropUtil util;
    public synchronized  static UcropUtil getUcropUtil()
    {
        if(util==null)
        {
            util=new UcropUtil();
        }
        return util;
    }
    /***
     * 配置裁剪参数
     * param 
     * source->需裁剪的图片
     * destination->裁剪完成后保存的uri
     */
    public UCrop cofigUcrop( @NonNull Uri source, @NonNull Uri destination)
    {
        
        mUcrop= mUcrop.of(source, destination);
        options =new UCrop.Options();
        options.withAspectRatio(1,1);//设置图片宽高比
        options.setCompressionFormat(Bitmap.CompressFormat.JPEG);//设置图片格式为jpg
        options.setCompressionQuality(80);
        options.setShowCropFrame(true);
        mUcrop.withOptions(options);
        return  mUcrop;
    }
    
    
}
