package com.thinkdit.lib.base;

import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Created by qiuda on 16/6/8.
 */
public class RequestParamBuilder {
    private SortedMap<String, Object> mParam;


    public RequestParamBuilder() {
        mParam = new TreeMap<>();
    }


    public RequestParamBuilder add(String name, Object value) {
        if (name != null && value != null) {
            mParam.put(name, value);
        }
        return this;
    }

    public RequestParamBuilder addAll(Map<String, Object> mapList) {
        if (mapList != null) {
            mParam.putAll(mapList);
        }
        return this;
    }

    public Map<String, Object> build() {
        return mParam;
    }


}
