package com.thinkdit.lib.base;

import android.os.RemoteException;

import org.json.JSONException;

/**
 * Created by qiuda on 16/6/7.
 */
public interface IRequestCallback<T extends ResultModelBase> {

    /**
     * 异步处理（同步get调用才有效）
     *
     * @param url         url
     * @param resultModel 结果对象
     */
    Object asyncExecute(String url, T resultModel);

    /**
     * 请求成功
     *
     * @param url         url
     * @param resultModel 结果对象
     */
    void onSucceed(String url, T resultModel) throws JSONException, RemoteException;

    /**
     * 请求错误
     *
     * @param url url
     * @param e   错误
     */
    void onError(String url, Exception e);


    /**
     * 请求失败
     *
     * @param url         url
     * @param resultModel 结果对象
     */
    void onFailure(String url, T resultModel);

}
