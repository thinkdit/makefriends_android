package com.thinkdit.lib.base;

/**
 * Created by QiuDa on 15/11/30.
 */
public interface ResultModelBase {
    /**
     *http 成功code
     */
    String SUCCESS_CODE = "ok";
    String SUCCESS_CODE_200 = "200";
    /**
     * http response返回为null
     */
    String ERROR_CODE_RESULT = "90000";
    /**
     * http 解析失败
     */
    String ERROR_CODE_PARSE = "90001";
    /**
     * http 请求失败
     */
    String ERROR_CODE_REQUEST = "90002";
    /**
     * 连接异常
     */
    String ERROR_CODE_CONNECT = "90003";


    void setCode(String code);

    String getCode();

    void setMessage(String message);

    String getMessage();

    void setUrl(String url);

    String getUrl();

    void setException(Exception e);

    Exception getException();

    void setDataModel(Object obj);

    Object getDataModel();

}
