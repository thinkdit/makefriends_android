package com.thinkdit.lib.base;

import android.app.Application;
import android.content.Context;

/**
 * Created by qiuda on 16/6/6.
 */
public abstract class BaseApplication<T extends BaseApplication> extends Application {
    private static BaseApplication APP_CONTEXT;

    public static BaseApplication getInstance() {
        return APP_CONTEXT;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        APP_CONTEXT = getApplication();
    }


    protected abstract T getApplication();


}
