package com.thinkdit.lib.util;

import android.content.Context;
import android.content.res.Configuration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * QiuDa
 */
public class DeviceInfoUtil {
    private static final int BUFFER_SIZE = 1024;
    private static final String TAG = "DeviceInfo";
    private static int width;
    private static int height;

    /**
     * 获取Cpu利用信息
     *
     * @return
     */
    public static int getCpuUsedMsg() {
        String Result;
        try {
            Process p = Runtime.getRuntime().exec("top -n 1");

            BufferedReader br = new BufferedReader(new InputStreamReader
                    (p.getInputStream()));
            while ((Result = br.readLine()) != null) {
                if (Result.trim().length() < 1) {
                    continue;
                } else {
                    String[] CPUusr = Result.split("%");
                    String[] CPUusage = CPUusr[0].split("User");
                    String[] SYSusage = CPUusr[1].split("System");

                    try {
                        int userUse = Integer.parseInt(CPUusage[1].trim());
                        int sysUse = Integer.parseInt(SYSusage[1].trim());
                        return userUse + sysUse;
                    } catch (NumberFormatException e) {

                    }
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    /**
     * @return cpu info
     */
    public static String getCupInfo() {
        String str = null;
        FileReader fileReader = null;
        BufferedReader bufferedReader = null;
        try {
            fileReader = new FileReader("/proc/cpuinfo");
            if (fileReader != null) {
                try {
                    bufferedReader = new BufferedReader(fileReader, BUFFER_SIZE);
                    str = bufferedReader.readLine();
                    bufferedReader.close();
                    fileReader.close();
                } catch (IOException ex) {
                    L.d(TAG, "读CPU信息失败");
                }
            }
        } catch (FileNotFoundException fileEx) {
            L.d(TAG, "读CPU信息失败");
        }

        return str.trim();
    }

    /**
     * 获取分辨率
     *
     * @param context
     * @throws JSONException
     */
    public static String getResolution(Context context) {
        String resolution = "";
        try {
            DisplayMetrics metrics = new DisplayMetrics();
            WindowManager window = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            window.getDefaultDisplay().getMetrics(metrics);
            int width = metrics.widthPixels;
            int height = metrics.heightPixels;
            int temp;
            if (width > height) {
                temp = width;
                width = height;
                height = temp;
            }
            resolution = String.valueOf(width) + "," + String.valueOf(height);
        } catch (Exception e2) {
            L.e(TAG, e2);

        }
        return resolution;
    }

    /**
     * 获取分辨率
     *
     * @param context
     * @throws JSONException
     */
    public static int getResolutionWidth(Context context) {
        if (width >= 0) {
            DisplayMetrics metrics = new DisplayMetrics();
            WindowManager window = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            window.getDefaultDisplay().getMetrics(metrics);
            width = metrics.widthPixels;
            height = metrics.heightPixels;
            int temp;
            if (width > height) {
                temp = width;
                width = height;
                height = temp;
            }
        }
        return width;
    }

    /**
     * 获取分辨率
     *
     * @param context
     * @throws JSONException
     */
    public static int getResolutionHeight(Context context) {
        if (width >= 0) {
            DisplayMetrics metrics = new DisplayMetrics();
            WindowManager window = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            window.getDefaultDisplay().getMetrics(metrics);
            width = metrics.widthPixels;
            height = metrics.heightPixels;
            int temp;
            if (width > height) {
                temp = width;
                width = height;
                height = temp;
            }
        }
        return height;
    }

    // private static void getLocalInfo(Context context, JSONObject result)
    // throws JSONException {
    // Configuration config = new Configuration();
    // Settings.System.getConfiguration(context.getContentResolver(), config);
    // TimeZone timeZone;
    // if ((config != null) && (config.locale != null)) {
    // result.put("Country", config.locale.getCountry());
    // result.put("Language", config.locale.toString());
    // Calendar calendar = Calendar.getInstance(config.locale);
    // if (calendar != null) {
    // timeZone = calendar.getTimeZone();
    // if (timeZone != null) {
    // result.put(LogConstants.TIME_ZONE, timeZone.getRawOffset() /
    // HOUR_TO_MILLS_UNIT);
    // } else {
    // result.put(LogConstants.TIME_ZONE, LogConstants.DEFAULT_TIME_ZONE);
    // }
    // } else {
    // result.put(LogConstants.TIME_ZONE, LogConstants.DEFAULT_TIME_ZONE);
    // }
    // } else {
    // Locale local = Locale.getDefault();
    // String country = local.getCountry();
    // if (!TextUtils.isEmpty((CharSequence) country)) {
    // result.put(LogConstants.COUNTRY, country);
    // } else {
    // result.put(LogConstants.COUNTRY, LogConstants.UNKNOWN);
    // }
    // String language = local.getLanguage();
    // if (!TextUtils.isEmpty(language)) {
    // result.put(LogConstants.LANGUAGE, language);
    // } else {
    // result.put(LogConstants.LANGUAGE, LogConstants.UNKNOWN);
    // }
    // Calendar calendar = Calendar.getInstance(local);
    // if (calendar != null) {
    // timeZone = calendar.getTimeZone();
    // if (timeZone != null) {
    // result.put(LogConstants.TIME_ZONE, ((TimeZone) timeZone).getRawOffset() /
    // HOUR_TO_MILLS_UNIT);
    // } else {
    // result.put(LogConstants.TIME_ZONE, LogConstants.DEFAULT_TIME_ZONE);
    // }
    // } else {
    // result.put(LogConstants.TIME_ZONE, LogConstants.DEFAULT_TIME_ZONE);
    // }
    // }
    // }


    /**
     * @param context 上下文
     * @return 设备ID
     */
    public static String getDeviceId(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String deviceId = "";
        try {
            if (PackageUtil.checkPermission(context, "android.permission.READ_PHONE_STATE")) {
                deviceId = telephonyManager.getDeviceId();
            }
        } catch (Exception ex) {
            L.e(TAG, ex);
        }
        if (StringUtil.isNullorEmpty(deviceId)) {
            deviceId = getUniquePsuedoID(context);
        }
        if (StringUtil.isNullorEmpty(deviceId)) {
            deviceId = "unknown";
        }
        return deviceId;
    }

    public static String getUniquePsuedoID(Context context) {
        String serial = SharePreferenceUtils.getString(context, SharePreferenceUtils.KEY_UNI_DEVICE_DI, "");

        if(StringUtil.isNullorEmpty(serial)){
            serial = System.currentTimeMillis()+"";
            SharePreferenceUtils.putString(context, SharePreferenceUtils.KEY_UNI_DEVICE_DI, serial);
        }

        return serial;
    }

    /**
     * @param paramContext paramContext
     * @return String
     * @throws
     * @Method: getMacAddress
     */
    public static String getMacAddress(Context paramContext) {
        String result = null;
        try {
            WifiManager wifiManager = (WifiManager) paramContext.getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            result = wifiInfo.getMacAddress();
        } catch (Exception ex) {
            L.d(TAG, "不能读mac地址");
        }
        return result;
    }

    public static String getDevice() {
        return Build.MODEL;
    }

    public static String getManufacturer() {
        return Build.MANUFACTURER;
    }

    public static String getOSVersion() {
        return Build.VERSION.RELEASE;
    }

    /**
     * 判断当前设备是手机还是平板，代码来自 Google I/O App for Android
     *
     * @param context
     * @return 平板返回 True，手机返回 False
     */
    public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

    public static String getSimOperatorInfo(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String operatorString = telephonyManager.getSimOperator();
        if (operatorString == null) {
            return "00";
        }
        if (operatorString.equals("46000") || operatorString.equals("46002")) {
            //中国移动
            return "00";
        } else if (operatorString.equals("46001")) {
            //中国联通
            return "01";
        } else if (operatorString.equals("46003")) {
            //中国电信
            return "03";
        }
        //error
        return "00";
    }

    public static String getSimCountryMCC(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String operatorString = telephonyManager.getSimOperator();
        if (operatorString != null && operatorString.length() > 3) {
            return operatorString.substring(0, 3);
        }
        return "";
    }
}
