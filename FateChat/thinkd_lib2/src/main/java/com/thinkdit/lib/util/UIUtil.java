package com.thinkdit.lib.util;

import android.content.Context;

/**
 * Created by admin on 2016/1/20.
 */
public class UIUtil {
    /**
     * dip转换为px
     *
     * @param context  上下文对象
     * @param dipValue dip值
     * @return px值
     */
    public static float dip2px(Context context, float dipValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return dipValue * scale + 0.5f;
    }

    /**
     * px转换为dip
     *
     * @param context 上下文对象
     * @param pxValue px值
     * @return dip值
     */
    public static float px2dip(Context context, float pxValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return pxValue / scale + 0.5f;
    }


    /**
     * 获取状态栏的高度
     *
     * @param context c
     * @return
     */
    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }
}
