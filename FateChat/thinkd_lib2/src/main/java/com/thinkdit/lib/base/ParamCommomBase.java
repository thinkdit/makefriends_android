package com.thinkdit.lib.base;


import com.thinkdit.lib.util.DeviceInfoUtil;
import com.thinkdit.lib.util.PackageUtil;
import com.thinkdit.lib.util.StringUtil;

import java.text.SimpleDateFormat;


public class ParamCommomBase extends RequestParamBuilder {

    public ParamCommomBase() {
        super();
        String osVersion = DeviceInfoUtil.getOSVersion();
        if (!StringUtil.isNullorEmpty(osVersion)) {
            super.add("osVersion", osVersion);
        }
        String device = DeviceInfoUtil.getDevice();
        if (!StringUtil.isNullorEmpty(device)) {
            super.add("deviceModel", device);
        }
//        String version = PackageUtil.getVersionName(BaseApplication.getInstance());
//        if (!StringUtil.isNullorEmpty(version)) {
//            super.add("version", version);
//        }
        String appVersion = PackageUtil.getVersionName(BaseApplication.getInstance());
        if (!StringUtil.isNullorEmpty(appVersion)) {
            super.add("appVersion", appVersion);
        }
        String deviceId = DeviceInfoUtil.getDeviceId(BaseApplication.getInstance());
        if (!StringUtil.isNullorEmpty(deviceId)) {
            super.add("deviceId", deviceId);
        }
    }

    private String getSeqId() {
        int value = (int) ((Math.random() * 9 + 1) * 100000);
        return new SimpleDateFormat("yyyyMMddHHmmss").format(new java.util.Date()) + value;
    }


}