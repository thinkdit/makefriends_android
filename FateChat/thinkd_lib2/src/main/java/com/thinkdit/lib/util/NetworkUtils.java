package com.thinkdit.lib.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;

import java.lang.ref.WeakReference;
import java.util.LinkedList;
import java.util.List;

public class NetworkUtils {

    private static final String TAG = "NetworkUtils";

    public static enum NetType {
        None,
        Wifi,
        G2,
        G3,
        G4,
        Cable,
    }

    /**
     * 检查当前网络是否可用
     *
     * @param activity
     * @return
     */

    public static boolean isNetworkAvailable(Context activity) {
        Context context = activity.getApplicationContext();
        // 获取手机所有连接管理对象（包括对wi-fi,net等连接的管理）
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivityManager == null) {
            return false;
        } else {
            // 获取NetworkInfo对象
            NetworkInfo[] networkInfo = connectivityManager.getAllNetworkInfo();

            if (networkInfo != null && networkInfo.length > 0) {
                for (int i = 0; i < networkInfo.length; i++) {
                    // 判断当前网络状态是否为连接状态
                    if (networkInfo[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static boolean isWifi(Context context) {
        return isWifi(getNetworkType(context));
    }

    public static boolean isWifi(NetType netType) {
        return netType == NetType.Wifi;
    }

    public static boolean isMobile(Context context) {
        return isMobile(getNetworkType(context));
    }

    public static boolean isMobile(NetType netType) {
        return netType == NetType.G2 || netType == NetType.G3 || netType == NetType.G4;
    }

    public static NetType getNetworkType(Context context) {
        ConnectivityManager mgr = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = mgr.getActiveNetworkInfo();

        if (networkInfo == null || !networkInfo.isConnected()) {
            return NetType.None;
        }

        switch (networkInfo.getType()) {
            case ConnectivityManager.TYPE_WIFI:
                return NetType.Wifi;

            case ConnectivityManager.TYPE_ETHERNET:
                return NetType.Cable;

            case ConnectivityManager.TYPE_MOBILE:
                switch (networkInfo.getSubtype()) {
                    case TelephonyManager.NETWORK_TYPE_GPRS:
                    case TelephonyManager.NETWORK_TYPE_EDGE:
                    case TelephonyManager.NETWORK_TYPE_CDMA:
                    case TelephonyManager.NETWORK_TYPE_IDEN:
                    case TelephonyManager.NETWORK_TYPE_1xRTT:
                        return NetType.G2;

                    case TelephonyManager.NETWORK_TYPE_UMTS:
                    case TelephonyManager.NETWORK_TYPE_EVDO_0:
                    case TelephonyManager.NETWORK_TYPE_EVDO_A:
                    case TelephonyManager.NETWORK_TYPE_EVDO_B:
                    case TelephonyManager.NETWORK_TYPE_HSDPA:
                    case TelephonyManager.NETWORK_TYPE_HSUPA:
                    case TelephonyManager.NETWORK_TYPE_HSPA:
                    case TelephonyManager.NETWORK_TYPE_HSPAP:
                    case TelephonyManager.NETWORK_TYPE_EHRPD:
                        return NetType.G3;

                    case TelephonyManager.NETWORK_TYPE_LTE:
                        return NetType.G4;

                    default:
                        break;
                }
                break;

            default:
                break;
        }

        return NetType.None;
    }
}
