package com.xindian.fatechat.manager;

import android.content.Context;

import com.hyphenate.easeui.domain.LocalSession;
import com.hyphenate.easeui.utils.DBHelper;
import com.xindian.fatechat.domain.LocalSessionDao;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by zjzhu on 28/6/16.
 * 会话管理类
 */
public class ISessionManager {
    private static ISessionManager sUserInfoManager;
    private Context mContext;
    private List<LocalSession> mlist;

    public static ISessionManager getManager(Context context) {
        if (sUserInfoManager == null) {
            sUserInfoManager = new ISessionManager(context.getApplicationContext());
        }
        return sUserInfoManager;
    }

    private ISessionManager(Context context) {
        mContext = context;
    }

    public void init() {
        mlist = DBHelper.intance().getDaoSession().getLocalSessionDao().loadAll();
    }

    public void insertOrReplace(LocalSession session) {
        DBHelper.intance().getDaoSession().getLocalSessionDao().insertOrReplace(session);
    }

    public LocalSession getSessionByID(int userId) {
        return DBHelper.intance().getDaoSession().getLocalSessionDao().queryBuilder().where
                (LocalSessionDao.Properties.UserId.eq(userId)).unique();
    }

    public void cleanSession(int userId) {
        LocalSession session = getSessionByID(userId);
        if (session != null) {
            session.setUnReadCount(0);
            DBHelper.intance().getDaoSession().getLocalSessionDao().update(session);
        }
    }

    public List<LocalSession> getSessionList() {
        if (DBHelper.intance().isInit() && DBHelper.intance().getDaoSession().getLocalSessionDao
                () != null) {
            List<LocalSession> list = DBHelper.intance().getDaoSession().getLocalSessionDao()
                    .loadAll();
            sort(list);
            return list;
        }
        return null;

    }

    private static void sort(List<LocalSession> data) {
        Collections.sort(data, new Comparator<LocalSession>() {
            public int compare(LocalSession o1, LocalSession o2) {
                long a = o1.getLasttime();
                long b = o2.getLasttime();
                if (a > b) {
                    return -1;
                } else {
                    return 1;
                }

            }
        });
    }
}
