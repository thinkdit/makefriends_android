/**
 * Copyright (C) 2016 Hyphenate Inc. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.xindian.fatechat;

import android.app.Activity;
import android.content.Context;

import com.hyphenate.easeui.domain.ChatUserEntity;

import com.hyphenate.easeui.ui.EaseChatFragment;

import com.hyphenate.easeui.utils.DBHelper;
import com.thinkdit.lib.base.BaseApplication;

public abstract class IMApplication extends BaseApplication {

    public static Context applicationContext;
    private static IMApplication instance;
    // login user name
    public final String PREF_USERNAME = "username";

    /**
     * nickname for current user, the nickname instead of ID be shown when user receive
     * notification from APNs
     */
    public static String currentUserNick = "";

    @Override
    public void onCreate() {
        super.onCreate();
        DemoHelper.getInstance().init(this);
        applicationContext = this;
        instance = this;
    }


    public static IMApplication getInstance() {
        return instance;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    public abstract void showReportView(Activity activity, ChatUserEntity chatUserEntity);

    public abstract void showGiftView(Activity activity, EaseChatFragment easeChatFragment, ChatUserEntity chatUserEntity);

}
