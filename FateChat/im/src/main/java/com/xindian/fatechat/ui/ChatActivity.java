package com.xindian.fatechat.ui;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.hyphenate.easeui.domain.ChatUserEntity;
import com.hyphenate.easeui.ui.EaseChatFragment;
import com.hyphenate.util.EasyUtils;
import com.readystatesoftware.systembartint.SystemBarTintManager;
import com.xindian.fatechat.IMApplication;
import com.xindian.fatechat.im.R;
import com.xindian.fatechat.runtimepermissions.PermissionsManager;

/**
 *
 */
public class ChatActivity extends BaseActivity {
    public static ChatActivity activityInstance;
    private EaseChatFragment chatFragment;

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.em_activity_chat);

        activityInstance = this;
        //get user id or group id
        //use EaseChatFratFragment
        chatFragment = new ChatFragment();
        chatFragment.setmOnReportClick(new EaseChatFragment.OnReportClick() {
            @Override
            public void onReportClick(Activity activity, ChatUserEntity chatUserEntity) {
                IMApplication.getInstance().showReportView(activity,chatUserEntity);
            }


            @Override
            public void onGiftClick(Activity activity, EaseChatFragment easeChatFragment,ChatUserEntity chatUserEntity) {
                IMApplication.getInstance().showGiftView(activity,easeChatFragment,chatUserEntity);
            }

        });
        //pass parameters to chat fragment
        chatFragment.setArguments(getIntent().getExtras());
        getSupportFragmentManager().beginTransaction().add(R.id.container, chatFragment).commit();
        chatFragment.setRoot((ViewGroup) findViewById(R.id.container));
        NotificationManager notificationManager = (NotificationManager) this.getSystemService
                (NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            // 创建状态栏的管理实例
            SystemBarTintManager tintManager = new SystemBarTintManager(this);
            // 激活状态栏设置
            tintManager.setStatusBarTintEnabled(true);
            // 激活导航栏设置
            tintManager.setNavigationBarTintEnabled(true);
            // 设置一个颜色给系统栏
            tintManager.setTintColor(Color.parseColor("#50000000"));
            // 设置一个样式背景给导航栏
            tintManager.setNavigationBarTintColor(Color.parseColor("#50000000"));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        activityInstance = null;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        // make sure only one chat activity is opened
        String username = intent.getStringExtra("userId");
//        if (toChatUsername.equals(username)) super.onNewIntent(intent);
//        else {
//            finish();
//            startActivity(intent);
//        }

    }

    @Override
    public void onBackPressed() {
        chatFragment.onBackPressed();
        if (EasyUtils.isSingleActivity(this)) {
//            Intent intent = new Intent(this, MainActivity.class);
//            startActivity(intent);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        PermissionsManager.getInstance().notifyPermissionsChange(permissions, grantResults);
        chatFragment.onRequestPermissionsResult(requestCode,permissions,grantResults);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        chatFragment.onActivityResult(requestCode,resultCode,data);
        
    }
}
