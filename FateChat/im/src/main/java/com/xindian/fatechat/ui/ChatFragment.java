package com.xindian.fatechat.ui;

import android.app.Activity;
import android.content.ClipData;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Toast;

import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMCmdMessageBody;
import com.hyphenate.chat.EMImageMessageBody;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.chat.EMTextMessageBody;
import com.hyphenate.chat.EMVoiceMessageBody;
import com.hyphenate.easeui.EaseConstant;
import com.hyphenate.easeui.domain.LocalMessage;
import com.hyphenate.easeui.domain.LocalSession;
import com.hyphenate.easeui.manager.MessageLimitManager;
import com.hyphenate.easeui.model.EventBusModel;
import com.hyphenate.easeui.ui.EaseChatFragment;
import com.hyphenate.easeui.ui.EaseChatFragment.EaseChatFragmentHelper;
import com.hyphenate.easeui.utils.DBHelper;
import com.hyphenate.easeui.utils.MessageHelper;
import com.hyphenate.easeui.widget.chatrow.EaseChatRow;
import com.hyphenate.easeui.widget.chatrow.EaseCustomChatRowProvider;
import com.hyphenate.util.EasyUtils;
import com.hyphenate.util.PathUtil;
import com.xindian.fatechat.Constant;
import com.xindian.fatechat.im.R;
import com.xindian.fatechat.manager.ISessionManager;
import com.xindian.fatechat.widget.ChatRowVoiceCall;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import static com.hyphenate.easeui.utils.DBHelper.intance;

public class ChatFragment extends EaseChatFragment implements EaseChatFragmentHelper {
    public static final String TAG = "ChatFragment";
    // constant start from 11 to avoid conflict with constant in base class
    private static final int ITEM_VIDEO = 11;
    private static final int ITEM_FILE = 12;
    private static final int ITEM_VOICE_CALL = 13;
    private static final int ITEM_VIDEO_CALL = 14;

    private static final int REQUEST_CODE_SELECT_VIDEO = 11;
    private static final int REQUEST_CODE_SELECT_FILE = 12;
    private static final int REQUEST_CODE_GROUP_DETAIL = 13;
    private static final int REQUEST_CODE_CONTEXT_MENU = 14;
    private static final int REQUEST_CODE_SELECT_AT_USER = 15;


    private static final int MESSAGE_TYPE_SENT_VOICE_CALL = 1;
    private static final int MESSAGE_TYPE_RECV_VOICE_CALL = 2;
    private static final int MESSAGE_TYPE_SENT_VIDEO_CALL = 3;
    private static final int MESSAGE_TYPE_RECV_VIDEO_CALL = 4;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    protected void setUpView() {
        setChatFragmentListener(this);
        Log.e("setUpView", "setUpView");
        if (!intance().isInit()) {
            Log.e("setUpView", "sendBroadcast");
            Intent i = new Intent();
            i.setAction("InitDBHelper");
            getContext().sendBroadcast(i);
        }
        super.setUpView();
        // set click listener
        titleBar.setLeftLayoutClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (EasyUtils.isSingleActivity(getActivity())) {
//                    Intent intent = new Intent(getActivity(), MainActivity.class);
//                    startActivity(intent);
                }
                onBackPressed();
            }
        });
//        ((EaseEmojiconMenu) inputMenu.getEmojiconMenu()).addEmojiconGroup
//                (EmojiconExampleGroupData.getData());
    }

    @Override
    protected void registerExtendMenuItem() {
        //use the menu in base class
        super.registerExtendMenuItem();
        //extend menu items
//        inputMenu.registerExtendMenuItem(R.string.attach_video, R.drawable
//                .em_chat_video_selector, ITEM_VIDEO, extendMenuItemClickListener);
//        inputMenu.registerExtendMenuItem(R.string.attach_file, R.drawable.em_chat_file_selector,
//                ITEM_FILE, extendMenuItemClickListener);
//        if (chatType == Constant.CHATTYPE_SINGLE) {
//            inputMenu.registerExtendMenuItem(R.string.attach_voice_call, R.drawable
//                    .em_chat_voice_call_selector, ITEM_VOICE_CALL, extendMenuItemClickListener);
//            inputMenu.registerExtendMenuItem(R.string.attach_video_call, R.drawable
//                    .em_chat_video_call_selector, ITEM_VIDEO_CALL, extendMenuItemClickListener);
//        }
        //end of red packet code
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ISessionManager.getManager(this.getActivity()).cleanSession(chatUserEntity.getToUserID());
        EventBusModel model = new EventBusModel();
        model.setAction(TAG);
        EventBus.getDefault().post(model);
    }

    @Subscribe
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_CONTEXT_MENU) {
            switch (resultCode) {
                case ContextMenuActivity.RESULT_CODE_COPY: // copy
                    //暂时只应用于文本的复制
                    if (contextMenuMessage.getBody() instanceof EMTextMessageBody) {
                        clipboard.setPrimaryClip(ClipData.newPlainText(null, ((EMTextMessageBody)
                                contextMenuMessage.getBody()).getMessage()));
                    } else {
                        Toast.makeText(getContext(), "暂仅支持文本复制", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case ContextMenuActivity.RESULT_CODE_DELETE: // delete
                    removeChatMessage();
                    break;


                default:
                    break;
            }
        }
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CODE_SELECT_VIDEO: //send the video
                    if (data != null) {
                        int duration = data.getIntExtra("dur", 0);
                        String videoPath = data.getStringExtra("path");
                        File file = new File(PathUtil.getInstance().getImagePath(), "thvideo" +
                                System.currentTimeMillis());
                        try {
                            FileOutputStream fos = new FileOutputStream(file);
                            Bitmap ThumbBitmap = ThumbnailUtils.createVideoThumbnail(videoPath, 3);
                            ThumbBitmap.compress(CompressFormat.JPEG, 100, fos);
                            fos.close();
                            sendVideoMessage(videoPath, file.getAbsolutePath(), duration);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case REQUEST_CODE_SELECT_FILE: //send the file
                    if (data != null) {
                        Uri uri = data.getData();
                        if (uri != null) {
                            sendFileByUri(uri);
                        }
                    }
                    break;
                case REQUEST_CODE_SELECT_AT_USER:
                    if (data != null) {
                        String username = data.getStringExtra("username");
                        inputAtUsername(username, false);
                    }
                    break;
                //end of red packet code
                default:
                    break;
            }
        }   
    }

    @Override
    public void onSetMessageAttributes(EMMessage message) {
        if(chatUserEntity.isRealUser()){
            message.setAttribute(EaseConstant.MESSAGE_ATTRIBUTE_USERID,chatUserEntity.getMyUserID());
        }else{
            message.setAttribute(EaseConstant.MESSAGE_ATTRIBUTE_USERID,chatUserEntity.getToUserID());
        }

        message.setAttribute(EaseConstant.EXTRA_USER_AVATOR, chatUserEntity.getMyAvator());
        message.setAttribute(EaseConstant.EXTRA_USER_NICKNAME, chatUserEntity.getMyNickName());
        message.setAttribute(EaseConstant.EXTRA_USER_POS, MessageLimitManager.instance(this
                .getActivity()).getAddress());
    }

    @Override
    public EaseCustomChatRowProvider onSetCustomChatRowProvider() {
        return new CustomChatRowProvider();
    }


    @Override
    public void onEnterToChatDetails() {
    }

    @Override
    public void onAvatarClick(String username) {
        //handling when user click avatar
//        Intent intent = new Intent(getActivity(), UserProfileActivity.class);
//        intent.putExtra("username", username);
//        startActivity(intent);
    }

    @Override
    public void onAvatarLongClick(String username) {
        inputAtUsername(username);
    }


    @Override
    public boolean onMessageBubbleClick(EMMessage message) {
        //end of red packet code
        return false;
    }

    @Override
    public void onCmdMessageReceived(List<EMMessage> messages) {
        //red packet code : 处理红包回执透传消息
        for (EMMessage message : messages) {
            EMCmdMessageBody cmdMsgBody = (EMCmdMessageBody) message.getBody();
            String action = cmdMsgBody.action();//获取自定义action
        }
        //end of red packet code
        super.onCmdMessageReceived(messages);
    }

    @Override
    public void onMessageBubbleLongClick(EMMessage message) {
        // no message forward when in chat room
        startActivityForResult((new Intent(getActivity(), ContextMenuActivity.class)).putExtra
                ("message", message.getType()).putExtra("ischatroom", false), REQUEST_CODE_CONTEXT_MENU);
    }

    /***
     * 删除单条消息
     */
    public void removeChatMessage() {
        //首先移除ui显示消息
        messageList.removeMsg(contextMenuMessage);
        //遍历会话
        for (EMMessage ms : conversation.getAllMessages()) {
            //判断是否是相同消息
            if (contextMenuMessage.getType().equals(EMMessage.Type.TXT) &&
                    ms.getMsgTime() == contextMenuMessage.getMsgTime() &&
                    ((EMTextMessageBody) ms.getBody()).getMessage().equals(((EMTextMessageBody) contextMenuMessage.getBody()).getMessage())) {
    
            }else if(contextMenuMessage.getType().equals(EMMessage.Type.IMAGE) &&
                    ms.getMsgTime() == contextMenuMessage.getMsgTime() &&
                    (((EMImageMessageBody) ms.getBody()).getRemoteUrl().equals(((EMImageMessageBody) contextMenuMessage.getBody()).getRemoteUrl())
                    ||((EMImageMessageBody) ms.getBody()).getLocalUrl().equals(((EMImageMessageBody) contextMenuMessage.getBody()).getLocalUrl())))
            {
                
            }else if(contextMenuMessage.getType().equals(EMMessage.Type.VOICE) &&
                    ms.getMsgTime() == contextMenuMessage.getMsgTime() &&
                    (((EMVoiceMessageBody) ms.getBody()).getLocalUrl().equals(((EMVoiceMessageBody) contextMenuMessage.getBody()).getLocalUrl())
                    ||((EMVoiceMessageBody) ms.getBody()).getRemoteUrl().equals(((EMVoiceMessageBody) contextMenuMessage.getBody()).getRemoteUrl())))
            {

            }else 
            {
                continue;
            }
            //转成本地消息
            LocalMessage localMessage = MessageHelper.ServerToLocalMsg(ms);
            //删除本地消息
            DBHelper.intance().deleteLocalMessageDao(localMessage);
            Log.i("IM_DEBUG","-->TO-->"+localMessage.getUserId());

            //存在删除的是最后一个item的情况，需要更新消息列表组
            LocalSession localSession = DBHelper.intance().getSessionByUserId(chatUserEntity.getToUserID());
            int count = messageList.getListView().getAdapter().getCount();
            if (count == 0) {
                localSession.setLastMsgContent("暂无消息");
                localSession.setLasttime(System.currentTimeMillis());//更新至删除消息的时间
            } else {
                EMMessage item = messageList.getItem(count - 1);
                if (item.getType().equals(EMMessage.Type.TXT)) {
                    EMTextMessageBody TextMessageBody = (EMTextMessageBody) item.getBody();
                    localSession.setLastMsgContent(TextMessageBody.getMessage());
                    localSession.setLastMsgType(localMessage.LOCAL_MESSAGE_TXT);
                } else if (item.getType().equals(EMMessage.Type.IMAGE)) {
                    localSession.setLastMsgContent("图片消息");
                    localSession.setLastMsgType(localMessage.LOCAL_MESSAGE_IMAGE);
                }else if (item.getType().equals(EMMessage.Type.VOICE)) {
                    localSession.setLastMsgContent("语音消息");
                    localSession.setLastMsgType(localMessage.LOCAL_MESSAGE_VOICE);
                }
                localSession.setLasttime(item.getMsgTime());
            }
            DBHelper.intance().insertLocalSessionDao(localSession);
            conversation.removeMessage(ms.getMsgId());
            EventBusModel model = new EventBusModel();
            model.setAction(TAG);
            EventBus.getDefault().post(model);
            break;
    }
}

    @Override
    public boolean onExtendMenuItemClick(int itemId, View view) {
        switch (itemId) {
            case ITEM_VIDEO:
                Intent intent = new Intent(getActivity(), ImageGridActivity.class);
                startActivityForResult(intent, REQUEST_CODE_SELECT_VIDEO);
                break;
            case ITEM_FILE: //file
                selectFileFromLocal();
                break;
            case ITEM_VOICE_CALL:
                startVoiceCall();
                break;
            case ITEM_VIDEO_CALL:
                startVideoCall();
                break;
            //end of red packet code
            default:
                break;
        }
        //keep exist extend menu
        return false;
    }

    /**
     * select file
     */
    protected void selectFileFromLocal() {
        Intent intent = null;
        if (Build.VERSION.SDK_INT < 19) { //api 19 and later, we can't use this way, demo just
            // select from images
            intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("*/*");
            intent.addCategory(Intent.CATEGORY_OPENABLE);

        } else {
            intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media
                    .EXTERNAL_CONTENT_URI);
        }
        startActivityForResult(intent, REQUEST_CODE_SELECT_FILE);
    }

    /**
     * make a voice call
     */
    protected void startVoiceCall() {
        if (!EMClient.getInstance().isConnected()) {
            Toast.makeText(getActivity(), R.string.not_connect_to_server, Toast.LENGTH_SHORT)
                    .show();
        } else {
//            startActivity(new Intent(getActivity(), VoiceCallActivity.class).putExtra("username",
//                    toChatUsername).putExtra("isComingCall", false));
//            // voiceCallBtn.setEnabled(false);
//            inputMenu.hideExtendMenuContainer();
        }
    }

    /**
     * make a video call
     */
    protected void startVideoCall() {
        if (!EMClient.getInstance().isConnected())
            Toast.makeText(getActivity(), R.string.not_connect_to_server, Toast.LENGTH_SHORT)
                    .show();
        else {
//            startActivity(new Intent(getActivity(), VideoCallActivity.class).putExtra("username",
//                    toChatUsername).putExtra("isComingCall", false));
//            // videoCallBtn.setEnabled(false);
//            inputMenu.hideExtendMenuContainer();
        }
    }

/**
 * chat row provider
 */
private final class CustomChatRowProvider implements EaseCustomChatRowProvider {
    @Override
    public int getCustomChatRowTypeCount() {
        //here the number is the message type in EMMessage::Type
        //which is used to count the number of different chat row
        return 12;
    }

    @Override
    public int getCustomChatRowType(EMMessage message) {
        if (message.getType() == EMMessage.Type.TXT) {
            //voice call
            if (message.getBooleanAttribute(Constant.MESSAGE_ATTR_IS_VOICE_CALL, false)) {
                return message.direct() == EMMessage.Direct.RECEIVE ?
                        MESSAGE_TYPE_RECV_VOICE_CALL : MESSAGE_TYPE_SENT_VOICE_CALL;
            } else if (message.getBooleanAttribute(Constant.MESSAGE_ATTR_IS_VIDEO_CALL,
                    false)) {
                //video call
                return message.direct() == EMMessage.Direct.RECEIVE ?
                        MESSAGE_TYPE_RECV_VIDEO_CALL : MESSAGE_TYPE_SENT_VIDEO_CALL;
            }
        }
        return 0;
    }

    @Override
    public EaseChatRow getCustomChatRow(EMMessage message, int position, BaseAdapter adapter) {
        if (message.getType() == EMMessage.Type.TXT) {
            // voice call or video call
            if (message.getBooleanAttribute(Constant.MESSAGE_ATTR_IS_VOICE_CALL, false) ||
                    message.getBooleanAttribute(Constant.MESSAGE_ATTR_IS_VIDEO_CALL, false)) {
                return new ChatRowVoiceCall(getActivity(), message, position, adapter);
            }
        }
        return null;
    }

}

}
