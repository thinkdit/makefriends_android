package com.jyd.paydemo;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.jyd.pay.PayActivity;
import com.jyd.pay.PayUtil;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by hx_Alex on 2017/6/17.
 */
public class XianlaiWxH5Pay implements PayUtil.OnPayClickListener{
    public static final String BASE_URL = "http://api.5taogame.com";
    public static final String PAY_URL = BASE_URL + "/pay/AppPayMoney";
    public static final String  PRODUCTCODE="xianlaijiaoyou";  
    private Context mContext;
    private float PayMoney;
    private int userId;
    private int pid;
    private Bean mBean;
    private PayUtil payUtil;
    private OnWxH5PayCallBack onWxH5PayCallBack;
    public XianlaiWxH5Pay(Context mContext,OnWxH5PayCallBack onWxH5PayCallBack) {
        this.mContext = mContext;
        this.onWxH5PayCallBack=onWxH5PayCallBack;
        if(mContext!=null) {
            if(payUtil==null) {
                payUtil = new PayUtil(mContext);
                payUtil.setOnPayClickListener(this);
            }
        }
    }
    public PayUtil getPayUtil() {
        return payUtil;
    }


    public void setData( float payMoney, int userId,int pid)
    {
        this.PayMoney = payMoney;
        this.userId = userId;
        this.pid=pid;
    }
    
    
    public void startPayWxByXianLai()
    {
        payMoney();
    }

    @Override
    public void onCheckPaySuccess() {
        if(pid!=0 && mBean!=null) {
            onWxH5PayCallBack.onWXH5PayReturnApp(pid, "用户已离开微信支付界面", mBean.orderid);
        }else 
        {
            onWxH5PayCallBack.onWXH5PayReturnApp(pid,"本次支付数据为空", mBean.orderid);
        }
    }

    @Override
    public void onError(String s) {
        onWxH5PayCallBack.onWXH5PayReturnApp(pid, s, mBean.out_trade_no);
    }

    private void payMoney() {
        
        OkGo.get(PAY_URL)//请求方式和url
                .tag(this)//标记网络请求  用于取消对应请求
                .cacheKey("cacheKey")// 设置当前请求的缓存key,建议每个不同功能的请求设置一个
                .params("user_id", userId)
                .params("product_code", PRODUCTCODE)
                .params("qudao_code", PRODUCTCODE)
                .params("imei", GetPhoneInfoUtils.getPhoneImei(mContext))
                .params("phone_shop", GetPhoneInfoUtils.getPhoneBrand())
                .params("pay_money", PayMoney)
                .params("pay_flag_code", "9")
                .params("pay_type_code", "4")//支付类型编码， 0，1，2，3， 4  次数 天 月 年
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String s, okhttp3.Call call, okhttp3.Response response) {
                        //解密S里面的字符串
                        try {
                            String encodeJson = EncodeJson.EncodeJson(s);
                            JSONObject jsonObj = new JSONObject(encodeJson);
                            Log.i("信息菜单：", jsonObj + "");
                            String code = jsonObj.get("code").toString();
                            if (code.equals("0")) {
                                Gson gson = new Gson();
                                mBean = gson.fromJson(encodeJson, Bean.class);
                                //上报所需信息至广播，由广播处理发送事件
                                Intent i=new Intent("WxPrePayInfoReport");
                                i.putExtra("tradeNo",mBean.orderid);
                                i.putExtra("bean",mBean);
                                mContext.sendBroadcast(i);
                                Log.i("bean信息：", mBean + "");;
//                                pay();
                            } else {

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    /**
     * 支付
     *
     */
    public void pay(Bean rBean) {
        if(mBean==null){
            if(rBean==null) {
                Toast.makeText(mContext, "预支付订单未获取成功,请重新尝试！", Toast.LENGTH_SHORT).show();
            }else 
            {
                mBean=rBean;
            }
        }
//        RequestBean bean = new RequestBean();
//        bean.service = mBean.service;//支付宝支付
//        bean.mch_id = mBean.mch_id;//商户号
//        bean.out_trade_no = mBean.out_trade_no;//订单号
//        bean.body = mBean.body;//商品描述
//        bean.total_fee = mBean.total_fee;//金额 单位：分
//        bean.mch_create_ip = mBean.mch_create_ip;//打印订单的机器ip
//        bean.notify_url = mBean.notify_url;//回调，此链接地址由使用方服务端提供，用来接收支付成功的推送
//        bean.nonce_str = mBean.nonce_str;//随机字符串
//        bean.sign = mBean.sign;//秘钥
//        bean.attach = mBean.version;//附加信息
//        payUtil.pay("http://qzf.qiaozhifu.com/web/gateway", bean);//开始支付
        Log.e("闲来微信支付url",mBean.returnUrl);
        if(mBean.returnUrl!=null && mContext!=null)
        {
            Intent i=new Intent(mContext, PayActivity.class);
            i.putExtra("url",mBean.returnUrl);
            mContext.startActivity(i);
        }
    }
    
    public interface  OnWxH5PayCallBack
    {
        void onWXH5PayReturnApp(int pid,String msg,String tradeNo);
    }

}
