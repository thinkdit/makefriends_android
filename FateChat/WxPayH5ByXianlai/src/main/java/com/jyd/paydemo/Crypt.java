package com.jyd.paydemo;

import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by Administrator on 2017/3/13.
 */

public class Crypt {
    private static final Crypt a = new Crypt();
    private static Key b;

    public static Crypt setkey(String key) {
        try {
            b = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return a;
    }

    public static String encrypt(String str) {
        byte[] encrypt = null;
        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            SecretKeySpec key = new SecretKeySpec(b.getEncoded(), "AES");
            cipher.init(Cipher.ENCRYPT_MODE, key);
            encrypt = cipher.doFinal(str.getBytes("UTF-8"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new String(Base64.encode(encrypt));
    }

    public static String decrypt(String str) {
        byte[] decrypt = null;
        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            SecretKeySpec key = new SecretKeySpec(b.getEncoded(), "AES");
            cipher.init(Cipher.DECRYPT_MODE, key);
            decrypt = cipher.doFinal(Base64.decode(str));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new String(decrypt).trim();
    }

    public static void main(String args[]){
//        Crypt.setkey("asc_efghiklmnius2018");
        Crypt.setkey("asc_efghiklm2018");
        String content="CP878|PU3004|SM001|4872034|V1";

        String result=Crypt.encrypt(content);
        System.out.println(result);
        result=Crypt.decrypt(result);
        System.out.println(result);

    }
}






