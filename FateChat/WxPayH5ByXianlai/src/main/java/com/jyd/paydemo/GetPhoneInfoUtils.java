package com.jyd.paydemo;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.telephony.TelephonyManager;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;

import static android.content.Context.TELEPHONY_SERVICE;

/**
 * Created by Administrator on 2017/3/16.
 */

public class GetPhoneInfoUtils {

    //获取手机的imei串
    public static String getPhoneImei(Context context) {
        String imei = ((TelephonyManager) context.getSystemService(TELEPHONY_SERVICE))
                .getDeviceId();
        return imei;
    }

    //得到app的版本信息
    public static String getAppVersionName(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            String localversion = packageInfo.versionName;
            return localversion;
        } catch (PackageManager.NameNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "";
        }
    }

    //如果没有传IMSI则获得android——id
    public static String getPhoneImsi(Context context) {
        TelephonyManager tm= (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        String number = tm.getLine1Number();

//        String mAndroid_id = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
//        if (mAndroid_id == null) {
//            return " ";
//        } else {
//            return mAndroid_id;
//        }
        return number;
    }

    public static String getClientId(Context context) {
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String deviceId = tm.getDeviceId();
        if (deviceId.equals("") || deviceId == null) {
            deviceId = getSerialNum();
        }
        return deviceId;
    }

    private static String getSerialNum() {
        try {
            Class<?> classZ = Class.forName("android.os.SystemProperties");
            Method get = classZ.getMethod("get", String.class);
            String serialNum = (String) get.invoke(classZ, "ro.serialno");
            return serialNum;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //得到手机的品牌
    public static String getPhoneBrand() {
        return Build.BRAND;
    }

    //得到手机型号
    public static String getPhoneType() {
        return Build.MODEL;
    }

    /*
   *
   * 得到手机内核
   * */
    public static String getKernelVersion() {
        String kernelVersion = "";
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream("/proc/version");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return kernelVersion;
        }
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream), 8 * 1024);
        String info = "";
        String line = "";
        try {
            while ((line = bufferedReader.readLine()) != null) {
                info += line;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                bufferedReader.close();
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            if (info != "") {
                final String keyword = "version ";
                int index = info.indexOf(keyword);
                line = info.substring(index + keyword.length());
                index = line.indexOf(" ");
                kernelVersion = line.substring(0, index);
            }
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }

        return kernelVersion;
    }

    /**
     * 获取版本号
     * @return 当前应用的版本号
     */
    public String getVersion(Context context) {
        try {
            PackageManager manager = context.getPackageManager();
            PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);
            String version = info.versionName;
            return version;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    /*
    * 获得APP首次安装的时间
    *
    * */

    public static long getAppFirstInstallTime(Context context) {
        PackageManager packageManager=context.getPackageManager();
        try {
            PackageInfo packageInfo=packageManager.getPackageInfo(context.getPackageName(), 0);
            long firstInstallTime=packageInfo.firstInstallTime;//应用第一次安装的时间

            //如下可获得更多信息
            ApplicationInfo applicationInfo=packageInfo.applicationInfo;
            String name=applicationInfo.name;
            String packageName=applicationInfo.packageName;
            String permission=applicationInfo.permission;//得到应用权限
            return firstInstallTime;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * 获取渠道名
     * @param ctx 此处习惯性的设置为activity，实际上context就可以
     * @return 如果没有获取成功，那么返回值为空
     */
    public static String getChannelName(Context ctx) {
        ApplicationInfo appInfo = null;
        String msg;
        try {
            appInfo = ctx.getPackageManager().getApplicationInfo(ctx.getPackageName(), PackageManager.GET_META_DATA);
            msg=appInfo.metaData.getString("UMENG_CHANNEL");
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return "";
        }
        return msg;
    }

    /**
     * @ param pkgname 手机应用的包名
     * @ return 存在则返回为空
     */
    public static boolean isPkgInstalled(String pkgName, Context ctx) {
        PackageInfo packageInfo = null;
        try {
            packageInfo = ctx.getPackageManager().getPackageInfo(pkgName, 0);
        } catch (PackageManager.NameNotFoundException e) {
            packageInfo = null;
            e.printStackTrace();
        }
        if (packageInfo == null) {
            return false;
        } else {
            return true;
        }
    }

}
