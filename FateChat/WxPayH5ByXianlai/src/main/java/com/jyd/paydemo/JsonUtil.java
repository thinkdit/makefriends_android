package com.jyd.paydemo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


public class JsonUtil {
    public static <T> T json2Bean(String jsonStr, Class<T> objClass) {
        Gson gson = new GsonBuilder().create();

        return gson.fromJson(jsonStr, objClass);
    }

}
