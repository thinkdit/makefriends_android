package com.jyd.paydemo;

import java.io.Serializable;

/**
 * author Administrator
 * date 2017/6/15.
 * version $version$
 */

public class Bean implements Serializable {

    /**
     * pay_flag_code : 6
     * order_encode_string : {"body":"加密助手","charset":"UTF-8","mch_create_ip":"119.39.19.10","mch_id":"25100009573121","nonce_str":"QZ442534","notify_url":"http://api.5taogame.com/callback/QiaoPayResultNoticefy","out_trade_no":"1706152156564963340100161","service":"qzf.android_sdk.wxpay","sign":"d7e0ed2169a7ef6ceefe337ce2b4c7a3","sign_type":"MD5","terminal_type":"Android","total_fee":100,"version":"2.0"}
     * code : 0
     * orderid : 1706152156564963340100161
     */

    public String pay_flag_code;
    public String order_encode_string;
    public String code;
    public String orderid;
    /**
     * body : 加密助手
     * charset : UTF-8
     * mch_create_ip : 175.13.249.86
     * mch_id : 25100009573121
     * nonce_str : QZ384702
     * notify_url : http://api.5taogame.com/callback/QiaoPayResultNoticefy
     * out_trade_no : 1706152316165211325100001
     * service : qzf.android_sdk.wxpay
     * sign : d7e0ed2169a7ef6ceefe337ce2b4c7a3
     * sign_type : MD5
     * terminal_type : Android
     * total_fee : 2
     * version : 2.0
     */

    public String body;
    public String charset;
    public String mch_create_ip;
    public String mch_id;
    public String nonce_str;
    public String notify_url;
    public String out_trade_no;
    public String service;
    public String sign;
    public String sign_type;
    public String terminal_type;
    public String total_fee;
    public String version;
    public String returnUrl;
}
