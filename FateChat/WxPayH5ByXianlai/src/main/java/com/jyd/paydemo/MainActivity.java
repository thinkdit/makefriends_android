package com.jyd.paydemo;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.jyd.pay.PayUtil;
import com.jyd.pay.RequestBean;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends Activity {
    PayUtil payUtil;
    public static final String BASE_URL = "http://api.5taogame.com";
    public static final String PAY_URL = BASE_URL + "/pay/AppPayMoney";
    private Bean mBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        payUtil = new PayUtil(this);
        payUtil.setOnPayClickListener(new PayUtil.OnPayClickListener() {
            @Override
            /**
             * 检查是否支付成功，由你们后端提供一个检查是否支付成功的接口
             * 由此方法进行请求接口，
             */
            public void onCheckPaySuccess() {

            }

            @Override
            /**
             * 支付异常。调起支付失败会调用此方法，
             */
            public void onError(String err) {

            }


        });
        findViewById(R.id.weixin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                payMoney();
            }
        });


    }

    private void payMoney() {
        OkGo.get(PAY_URL)//请求方式和url
                .tag(this)//标记网络请求  用于取消对应请求
                .cacheKey("cacheKey")// 设置当前请求的缓存key,建议每个不同功能的请求设置一个
                .params("user_id", "10001")
                .params("product_code", "xianlaijiaoyou")
                .params("qudao_code", "qianzhi")
                .params("imei", GetPhoneInfoUtils.getPhoneImei(this))
                .params("phone_shop", GetPhoneInfoUtils.getPhoneBrand())
                .params("pay_money", "0.01")
                .params("pay_flag_code", "6")
                .params("pay_type_code", "4")//支付类型编码， 0，1，2，3， 4  次数 天 月 年
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String s, okhttp3.Call call, okhttp3.Response response) {
                        //解密S里面的字符串

                        try {
                            String encodeJson = EncodeJson.EncodeJson(s);
                            JSONObject jsonObj = new JSONObject(encodeJson);
                            Log.i("信息菜单：", jsonObj + "");
                            String code = jsonObj.get("code").toString();
                            if (code.equals("0")) {
                                Gson gson = new Gson();
                                mBean = gson.fromJson(encodeJson, Bean.class);
                                Log.i("bean信息：", mBean + "");
                                pay();
                            } else {

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    /**
     * 支付
     *
     */
    public void pay() {
        RequestBean bean = new RequestBean();
        bean.service = mBean.service;//支付宝支付
        bean.mch_id = mBean.mch_id;//商户号
        bean.out_trade_no = mBean.out_trade_no;//订单号
        bean.body = mBean.body;//商品描述
        bean.total_fee = mBean.total_fee;//金额 单位：分
        bean.mch_create_ip = mBean.mch_create_ip;//打印订单的机器ip
        bean.notify_url = mBean.notify_url;//回调，此链接地址由使用方服务端提供，用来接收支付成功的推送
        bean.nonce_str = mBean.nonce_str;//随机字符串
        bean.sign = mBean.sign;//秘钥
        bean.attach = mBean.version;//附加信息
        payUtil.pay("http://qzf.qiaozhifu.com/web/gateway", bean);//开始支付
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        payUtil.destory();//此方法用于性能优化，请加上
    }
}
